
#include <assert.h>
#include <stdio.h>
#include <unistd.h>
#include <wchar.h>

#include <debug.h>

#include <array/struct.h>

#include <value/number/struct.h>
#include <value/struct.h>

#include "process_arg.h"
#include "process_flags.h"
#include "usage.h"
#include "write-to-stdout.h"

#include "scope/init.h"
#include "scope/push.h"
#include "scope/uninit.h"

#include "value/delete.h"
#include "value/print.h"

int depth;

int main(int n, const char **args) {
	int ret = 0, error = 0;
	struct value *value;
	struct scope *scope;
	int i = 0;
	wchar_t arg_varname[20];
	ENTER;

	if (n > 1) {
		n--, args++;

		bool return_last_value = false;
		error = process_flags(&args, &return_last_value);

		if (!error) {
			error = scope_init(&scope);

			for (const char **arg = args; !error && *arg; arg++) {
				error = process_arg(&arg, scope, &value);

				verpv(error);
				
				if (!error) {
					// Is this not the last argument?
					if (arg[1]) {
						// save this value:
						error = 0
						?: swprintf(arg_varname, 20, L"$%i", i++) < 1
						?: scope_push(scope, arg_varname, value,
							false);
					} else if (return_last_value) {
						if(value->kind == vk_number)
						{
							if(((struct number_value*) value)->kind
								== nvk_integer)
							{
								ret = ((struct number_value*) value)->integer;
								error = error ?: !(0 <= ret && ret <= 255);
							}
							error = error ?: delete_value(value);
						}
						else
						{
							TODO; // print error message, error = 1
						}
					} else {
						// print out last value:
						if (value->kind != vk_null) {
							error = 0
								?: value_print(value, write_to_stdout)
								?: write(1, "\n", 1) < 1;
						}

						error = error ?: delete_value(value);
					}
				}
			}

			error = error ?: scope_uninit(&scope);
		}
	} else {
		usage(args[0]);
	}
	EXIT;
	return error ?: ret;
}






