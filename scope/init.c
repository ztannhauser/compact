
#include <assert.h>
#include <stdbool.h>
#include <stdio.h>

#include <debug.h>

#include <avl/init_tree.h>
#include <avl/struct.h>
#include <linkedlist/new.h>
#include <linkedlist/struct.h>

#include "struct.h"
#include "open.h"
#include "push.h"

#include "variable/compare.h"
#include "variable/free.h"

#include "builtins/push_builtins.h"

#include "init.h"

int scope_init(struct scope **this) {
	int error = 0;
	ENTER;

	*this = malloc(sizeof(struct scope));

	avl_init_tree(&((*this)->variables),
				  (int (*)(const void *, const void *))compare_variable,
				  (void (*)(void *))free_variable);

	(*this)->stack = new_linkedlist();

	(*this)->null_value = NULL;
	(*this)->true_value = NULL;
	(*this)->false_value = NULL;
	(*this)->compare_value = NULL;

	error = 0 ?: scope_open(*this) ?: push_builtins(*this);
	;

	EXIT;
	return error;
}
