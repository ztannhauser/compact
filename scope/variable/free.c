
#include <assert.h>
#include <stdio.h>

#include <debug.h>

#include <linkedlist/delete.h>
#include <linkedlist/foreach.h>

#include "value/delete.h"

#include "struct.h"

#include "free.h"

int free_variable(struct variable *this) {
	int error;
	// ENTER;
	
	error = linkedlist_foreach(&(this->values),
		(int (*)(void *))delete_value);
	
	if(!error)
	{
		delete_linkedlist(&(this->values));
		free(this);
	}
	// EXIT;
	return error;
}
