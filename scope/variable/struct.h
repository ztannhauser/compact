
#ifndef VARIABLE_STRUCT_H
#define VARIABLE_STRUCT_H

#include <wchar.h>

#include <linkedlist/struct.h>

struct variable
{
	wchar_t name[256];
	struct linkedlist values; // elements are 'struct value*'
};

#endif
