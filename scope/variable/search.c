
#include <string.h>

#include <debug.h>

#include "search.h"

int search_variable(const wchar_t *a, struct variable *b) {
	return wcscmp(a, b->name);
}
