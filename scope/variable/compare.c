
#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <wchar.h>

#include <debug.h>

#include "struct.h"

#include "compare.h"

int compare_variable(const struct variable *a, const struct variable *b) {
	int ret;
	ret = wcscmp(a->name, b->name);
	return ret;
}
