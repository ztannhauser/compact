
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include <debug.h>

#include <avl/free_nodes.h>
#include <avl/struct.h>

#include <linkedlist/struct.h>

#include <value/boolean/delete.h>
#include <value/function/delete.h>
#include <value/null/delete.h>

#include "close.h"
#include "struct.h"

#include "uninit.h"

int scope_uninit(struct scope **scope) {
	int error;
	ENTER;

	error = 0 ?: scope_close(*scope) ?: (*scope)->stack.n > 0;

	avl_free_nodes(&((*scope)->variables));

	error = error ?: delete_null_value((*scope)->null_value);
	error = error ?: delete_boolean_value((*scope)->true_value);
	error = error ?: delete_boolean_value((*scope)->false_value);
	error = error ?: delete_function_value((*scope)->compare_value);

	free(*scope), *scope = NULL;

	EXIT;
	return error;
}
