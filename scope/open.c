#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include <debug.h>

#include <linkedlist/new.h>
#include <linkedlist/push_n.h>

#include "struct.h"
#include "open.h"

int scope_open(struct scope *scope)
{
	int error = 0;
	ENTER;

	struct frame *f = malloc(sizeof(struct frame));

	f->variables = new_linkedlist();

	linkedlist_push_n(&(scope->stack), f);

	EXIT;
	return error;
}
