
#include <assert.h>
#include <stdio.h>

#include <debug.h>

#include <linkedlist/delete.h>
#include <linkedlist/foreach.h>
#include <linkedlist/pop_n.h>
#include <linkedlist/struct.h>

#include "value/delete.h"

#include "variable/struct.h"

#include "struct.h"

#include "close.h"

static int callback(struct variable * var)
{
	int error;
	ENTER;
	struct value *val = var->values.last->data;
	error = delete_value(val);
	linkedlist_pop_n(&(var->values));
	EXIT;
	return error;
}

int scope_close(struct scope *scope) {
	int error;
	ENTER;
	
	struct frame *current = scope->stack.last->data;

	error = current == NULL;

	verpv(current);

	error = error ?: linkedlist_foreach(&(current->variables),
		(int (*)(void *))callback);
	
	delete_linkedlist(&(current->variables));

	free(current);

	linkedlist_pop_n(&(scope->stack));

	EXIT;
	return error;
}






