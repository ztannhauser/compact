
#include <assert.h>
#include <stdio.h>

#include <debug.h>

#include "value/function/new.h"

#include "push.h"

#include "push_builtin_func.h"

int scope_push_builtin_func(
	struct scope *scope,
	const wchar_t *name,
	void *ptr)
{
	int error = 0;
	struct function_value *ret;
	ENTER;

	verpv(scope);
	verpv(name);
	
	assert(scope && name);
	
	ret = new_function_value_as_builtin(name, ptr);
	
	error = scope_push(scope, name, (struct value *)ret, false);

	EXIT;
	return error;
}
