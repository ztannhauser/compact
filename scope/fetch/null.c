
#include <assert.h>
#include <stdbool.h>
#include <stdio.h>

#include <debug.h>

#include <value/null/struct.h>
#include <value/struct.h>

#include "null.h"

struct null_value *scope_fetch_null(struct scope* scope)
{
	struct null_value *ret;
	ENTER;
	ret = scope->null_value;
	ret->super.refcount++;
	EXIT;
	return ret;
}
