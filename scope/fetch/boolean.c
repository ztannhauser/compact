
#include <assert.h>
#include <stdbool.h>
#include <stdio.h>

#include <debug.h>

#include <value/boolean/struct.h>
#include <value/struct.h>

#include "boolean.h"

struct boolean_value *scope_fetch_boolean(struct scope* scope, bool val)
{
	struct boolean_value *ret;
	ENTER;
	
	ret = (val ? scope->true_value : scope->false_value);
	ret->super.refcount++;
	
	EXIT;
	return ret;
}
