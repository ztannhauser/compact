
#include <assert.h>
#include <stdbool.h>
#include <stdio.h>

#include <debug.h>

#include <value/function/struct.h>
#include <value/struct.h>

#include "../struct.h"

#include "compare.h"

struct function_value *scope_fetch_compare(struct scope* scope)
{
	struct function_value *ret;
	ENTER;
	ret = scope->compare_value;
	ret->super.refcount++;
	EXIT;
	return ret;
}
