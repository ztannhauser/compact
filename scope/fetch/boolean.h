
#include <stdbool.h>

#include "../struct.h"

struct boolean_value *scope_fetch_boolean(struct scope* scope, bool val)
	__attribute__ ((warn_unused_result));
