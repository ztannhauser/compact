
#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>

#include <debug.h>

#include <linkedlist/new.h>
#include <linkedlist/push_n.h>
#include <linkedlist/struct.h>

#include <avl/insert.h>
#include <avl/search.h>
#include <avl/struct.h>

#include <value/struct.h>

#include "struct.h"

#include "variable/compare.h"

#include "push.h"

int scope_push(
	struct scope *scope,
	const wchar_t *name,
	struct value *val,
	bool inc_refcount)
{
	int error = 0;
	struct avl_node *node;
	ENTER;

	verpv(scope);
	verpv(name);
	verpv(val);
	
	assert(scope && name && val);
	
	node = avl_tree_search(
		&(scope->variables),
		(int (*)(const void *, const void *)) compare_variable,
		(void *) name);

	verpv(node);

	struct variable *var;
	if (node) {
		var = node->item;
	} else {
		// create new node
		var = malloc(sizeof(*var));
		wcscpy(var->name, name);
		var->values = new_linkedlist();
		avl_insert(&(scope->variables), var);
	}

	// append new value to end
	{
		if (inc_refcount) {
			val->refcount++;
		}
		linkedlist_push_n(&(var->values), val);
	}

	// append node to current stack frame
	{
		struct frame *current = scope->stack.last->data;
		linkedlist_push_n(&(current->variables), var);
	}

	EXIT;
	return error;
}
