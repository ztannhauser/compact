
#ifndef SCOPE_STRUCT_H
#define SCOPE_STRUCT_H

#include <wchar.h>

#include <avl/struct.h>
#include <linkedlist/struct.h>

struct frame
{
	struct linkedlist variables; // elements are 'struct variable*'
};

struct scope
{
	struct avl_tree variables; // elements are 'struct variable*'
	struct linkedlist stack; // elements are 'struct frame*'
	
	// eternal values:
	struct null_value* null_value;
	struct boolean_value* true_value;
	struct boolean_value* false_value;
	struct function_value* compare_value;
	
};

#endif
