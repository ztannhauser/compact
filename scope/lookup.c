
#include <assert.h>
#include <stdio.h>

#include <debug.h>

#include <array/struct.h>

#include <avl/search.h>
#include <avl/struct.h>

#include <array/delete.h>
#include <array/struct.h>

#include <value/string/construct.h>
#include <value/string/to_utf8_encoded.h>

#include "value/struct.h"

#include "struct.h"

#include "variable/search.h"

#include "lookup.h"

static void undeclared_variable(const wchar_t *name) {
	struct string_value *sv;
	struct array converted;
	ENTER;

	sv = construct_string_value(name, wcslen(name));
	converted = string_value_to_utf8_encoded(sv, true);

	fprintf(stderr, "Undeclared variable: \"%s\"\n", converted.data);

	delete_array(&converted);

	EXIT;
}

int scope_lookup(struct scope *scope, const wchar_t *name,
				 struct value **result) {
	int error = 0;
	ENTER;

	struct avl_node *node =
		avl_tree_search(&(scope->variables),
						(int (*)(const void *, const void *))search_variable,
						(const void *)name);

	verpv(node);

	if (!node)
		undeclared_variable(name),
		error = 1;

	if (!error) {
		struct variable *var = node->data;
		
		if(var->values.n)
			*result = var->values.last->data,
			(*result)->refcount++;
		else
			undeclared_variable(name),
			error = 1;
	}

	EXIT;
	return error;
}












