
#include <stdlib.h>

#include <value/struct.h>

#include "struct.h"

int scope_lookup(
	struct scope* scope,
	const wchar_t* name,
	struct value** value)
	__attribute__ ((warn_unused_result));
