
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include <debug.h>

#include <array/new.h>
#include <array/push_n.h>
#include <array/struct.h>

#include <value/list/new.h>
#include <value/list/struct.h>
#include <value/number/struct.h>
#include <value/struct.h>

#include <scope/builtins/callback_header.h>

#include "sublist.h"

DECLARE_BUILTIN_FUNC(builtin_list_sublist)
{
	int error = 0;
	struct value* generic_list;
	struct value* generic_index;
	struct value* generic_length;
	struct list_value* list;
	struct number_value* index_number;
	struct number_value* length_number;
	size_t start_index;
	size_t end_index;
	struct array values;
	struct value* ele;
	
	ENTER;
	
	if(2 <= n && n <= 3)
	{
		generic_list = args[0];
		generic_index = args[1];
		
		error = 0
			|| generic_list->kind != vk_list
			|| generic_index->kind != vk_number
			;
		
		if(!error)
		{
			list = (struct list_value*) generic_list;
			index_number = (struct number_value*) generic_index;
			
			error = index_number->kind != nvk_integer;
		}
		
		if(!error)
		{
			start_index = index_number->integer;
			verpv(start_index);
			
			if(n == 3)
			{
				generic_length = args[2];
				error = generic_index->kind != vk_number;
				length_number = (struct number_value*) generic_index;
				error = length_number->kind != nvk_integer;
				if(!error)
				{
					end_index = start_index + length_number->integer;
					if(end_index <= list->values.n)
					{
						fprintf(stderr, "sublist(): requested index & length "
							"values refer outside of bounds of given list!\n");
						error = 1;
					}
				}
			}
			else
			{
				end_index = list->values.n;
			}
		}
		
		if(!error)
		{
			verpv(start_index);
			verpv(end_index);
			
			values = new_array(struct value*);
			
			for(size_t i = start_index;!error && i < end_index;i++)
			{
				ele = array_index(list->values, i, struct value*);
				ele->refcount++;
				array_push_n(&values, &ele);
			}
		}

		if(!error)
		{
			*result = (struct value*) new_list_value(values, NULL);
		}
	}
	else
	{
		fprintf(stderr, "sublist(): requires one list and a start "
			"index, a third parameter (length) is optional.\n");
		error = 1;
	}
	EXIT;
	return error;
}











