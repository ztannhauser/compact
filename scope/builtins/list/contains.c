
#include <assert.h>
#include <stdio.h>

#include <debug.h>

#include <array/bsearch.h>
#include <array/struct.h>

#include <scope/builtins/call_comparator.h>
#include <scope/fetch/boolean.h>

#include <value/list/struct.h>
#include <value/number/new.h>
#include <value/struct.h>

#include <scope/builtins/callback_header.h>

#include "../compare.h"

#include "contains.h"

DECLARE_BUILTIN_FUNC(builtin_list_contains) {
#if 0
	struct value* ret;
	ENTER;
	assert(n == 2);
	struct value* generic_list = params[0];
	assert(generic_list->kind == vk_list);
	struct list_value* list = (struct list_value*) generic_list;
	struct value* looking_for = params[1];
	bool contains;
	if(list->comparator)
	{
		int contains_callback(struct value* looking_for, struct value** element)
		{
			int ret;
			ENTER;
			ret = call_comparator(list->comparator, looking_for, *element);
			verpv(ret);
			EXIT;
			return ret;
		}
		contains = !!array_bsearch(&(list->values), looking_for,
			(int (*)(const void *, const void *)) contains_callback);
	}
	else
	{
		contains = false;
		for(uintmax_t i = 0, n = list->values.n;!contains && i < n;i++)
		{
			struct value* ele = array_index(list->values, i, struct value*);
			if(!default_compare(ele, looking_for))
			{
				contains = true;
			}
		}
	}
	ret = (struct value*) scope_fetch_boolean(contains);
	EXIT;
	return ret;
#endif
	TODO;
}
