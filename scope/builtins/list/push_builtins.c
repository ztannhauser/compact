

#include <assert.h>
#include <stdio.h>

#include <debug.h>

#include <scope/push_builtin_func.h>

#include "iteration/push_builtins.h"
#include "setmath/push_builtins.h"

#include "contains.h"
#include "count.h"
#include "filter.h"
#include "insert.h"
#include "pair.h"
#include "perm.h"
#include "range.h"
#include "sort.h"
#include "sublist.h"
#include "transpose.h"

#include "push_builtins.h"

int push_list_builtins(struct scope *scope) {
	int error = 0;
	ENTER;

	error = 0
		?: push_list_iteration_builtins(scope)
		?: push_list_setmath_builtins(scope)
		
		?: scope_push_builtin_func(scope, L"filter", 
			builtin_list_filter)
		?: scope_push_builtin_func(scope, L"perm", 
			builtin_list_perm)
		?: scope_push_builtin_func(scope, L"pair", 
			builtin_list_pair)
		?: scope_push_builtin_func(scope, L"sublist", 
			builtin_list_sublist)
		?: scope_push_builtin_func(scope, L"range", 
			builtin_list_range)
		?: scope_push_builtin_func(scope, L"contains", 
			builtin_list_contains)
		?: scope_push_builtin_func(scope, L"count", 
			builtin_list_count)
		?: scope_push_builtin_func(scope, L"sort", 
			builtin_list_sort)
		?: scope_push_builtin_func(scope, L"insert", 
			builtin_list_insert)
		?: scope_push_builtin_func(scope, L"transpose", 
			builtin_list_transpose)
		;

	EXIT;
	return error;
}







