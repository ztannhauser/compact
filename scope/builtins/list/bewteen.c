

// return a list of elements from the given list that are 'bewteen' the given
// maxmimum value and minimum value
// you can set inclusvity, somehow...

// if sorted:
// binary search
// otherwise:
// linear search on default comparator, unless one is provided

#include <assert.h>
#include <stdlib.h>

#include <debug.h>

#include <value/function/struct.h>
#include <value/list/new.h>
#include <value/list/struct.h>

#include <scope/builtins/call_comparator.h>
#include <scope/builtins/callback_header.h>

#include "bewteen.h"

#if 0
static void binary_search(
	struct array* values,
	struct list_value* list,
	struct function_value* compare,
	struct value* min,
	struct value* max)
{
	ENTER;

#if 0
	size_t n;
	void* head = array_bsearch_range(
		&(list->values),
		({
			int compar(const struct value** ele)
			{
				int ret;
				ENTER;
				
				if(call_comparator(compare, *ele, max) > 0) ret = 1;
				else if(call_comparator(compare, *ele, min) < 0) ret = -1;
				else ret = 0;
				verpv(ret);
				
				EXIT;
				return ret;
			}
			compar;
		}), &n);
		
	verpv(head), verpv(n);
#endif
	
	TODO;
	EXIT;
}

static void linear_search(
	struct array* values,
	struct list_value* list,
	struct function_value* compare,
	struct value* min,
	struct value* max)
{
	ENTER;
	TODO;
	EXIT;
}

#endif

DECLARE_BUILTIN_FUNC(builtin_sorted_list_bewteen) {
	assert(3 <= n && n <= 4);
#if 0
	struct value* generic_list = params[0];
	assert(generic_list->kind == vk_list);
	struct list_value* list = generic_list;
	struct value* min = params[1];
	struct value* max = params[2];
	struct value* generic_compare = (n == 4) ? params[3] : NULL;
	assert(!generic_compare || generic_compare->kind == vk_function);
	struct function_value* given_param_compare = generic_compare;
	struct function_value* given_list_compare = list->comparator;
	
	struct array values = new_array(struct value*);
	if(given_param_compare)
	{
		if(given_list_compare)
		{
			if(given_param_compare == given_list_compare)
			{
				// binary search using either
				TODO;
			}
			else
			{
				// linear search using param comparator
				TODO;
			}
		}
		else
		{
			// linear search using param comparator
			TODO;
		}
	}
	else
	{
		if(given_list_compare)
		{
			// binary search using list's comparator
			binary_search(&values, list, given_list_compare, min, max);
		}
		else
		{
			// linear search using default compare
			TODO;
		}
	}

	ret = new_list_value(
		values,
		given_list_compare ?
			given_list_compare->super.refcount++, given_list_compare :
			NULL
		);
#endif
	TODO;
}
