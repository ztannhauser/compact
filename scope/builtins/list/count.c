
#include <assert.h>
#include <stdio.h>

#include <debug.h>

#include <value/list/struct.h>
#include <value/number/new.h>
#include <value/struct.h>

#include "../compare.h"

#include <scope/builtins/callback_header.h>

#include "count.h"

DECLARE_BUILTIN_FUNC(builtin_list_count) {
#if 0
	struct value* ret;
	ENTER;
	assert(n == 2);
	struct value* generic_list = params[0];
	assert(generic_list->kind == vk_list);
	struct list_value* list = (struct list_value*) generic_list;
	struct value* element = params[1];
	uintmax_t count = 0;
	for(uintmax_t i = 0, n = list->values.n;i < n;i++)
	{
		struct value* ele = array_index(list->values, i, struct value*);
		if(!default_compare(ele, element))
		{
			count++;
		}
	}
	ret = (struct value*) new_number_value_as_integer(count);
	EXIT;
	return ret;
#endif
	TODO;
}
