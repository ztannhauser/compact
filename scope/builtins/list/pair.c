
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include <debug.h>

#include <array/foreach.h>
#include <array/mass_push_n.h>
#include <array/new.h>
#include <array/push_n.h>
#include <array/struct.h>

#include <call.h>

#include <scope/builtins/callback_header.h>

#include <value/inc.h>
#include <value/list/new.h>
#include <value/list/struct.h>
#include <value/struct.h>

#include "pair.h"

DECLARE_BUILTIN_FUNC(builtin_list_pair) {
#if 0
	struct value* ret;
	assert(n >= 1);
	for(size_t i = 0;i < n;i++)
		assert(params[i]->kind == vk_list);
	struct array values = new_array(struct value*);
	struct list_value** argument_lists = (struct list_value**) params;
	struct value* args[n];
	void f(size_t i)
	{
		ENTER;
		verpv(i);
		if(i < n)
		{
			struct list_value* l = argument_lists[i];
			for(size_t j = 0, m = l->values.n;j < m;j++)
			{
				args[i] = array_index(l->values, j, struct value*);
				f(i + 1);
			}
		}
		else
		{
			// call function using args
			struct array subvalues = new_array(struct value*);
			array_mass_push_n(&subvalues, args, n);
			arrayp_foreach(&subvalues, (void (*)(void*)) value_inc);
			struct value* result =
				(struct value*) new_list_value(subvalues, NULL);
			array_push_n(&values, &result);
		}
		EXIT;
	}
	f(0);
	
	ret = (struct value*) new_list_value(values, NULL);
	return ret;
#endif
	TODO;
}
