
#include <assert.h>

#include <debug.h>

#include <value/list/new.h>
#include <value/list/struct.h>
#include <value/number/new.h>
#include <value/struct.h>

#include <scope/builtins/call.h>
#include <scope/builtins/call_comparator.h>
#include <scope/builtins/callback_header.h>

#include "unique.h"

DECLARE_BUILTIN_FUNC(builtin_sorted_list_unique) {
	assert(n == 1);
#if 0
	struct value* generic_list = params[0];
	assert(generic_list->kind == vk_list);
	struct list_value* list = generic_list;
	struct value* comp = list->comparator;
	struct array values = new_array(struct value*);
	if(list->values.n > 1)
	{
		if(list->comparator)
		{
			struct value* last = NULL;
			for(uintmax_t i = 0, n = list->values.n;i < n;i++)
			{
				struct value* ele = array_index(list->values, i, struct value*);
				if(!last || call_comparator(comp, last, ele))
				{
					ele->refcount++;
					array_push_n(&values, &ele);
					last = ele;
				}
			}
		}
		else
		{
#if 0
			uintmax_t count = 0;
			for(uintmax_t i = 0, n = list->values.n;i < n;i++)
			{
				struct value* ele = array_index(list->values, i, struct value*);
				if(!default_compare(ele, element))
				{
					count++;
				}
			}
			ret = new_number_value_as_integer(count);
#endif
			TODO;
		}
	}
	else if(list->values.n == 1)
	{
		struct value* one_and_only =
			array_index(list->values, 0, struct value*);
		one_and_only->refcount++;
		array_push_n(&values, &one_and_only);
	}
	ret = new_list_value(values, comp ? comp->refcount++, comp : NULL);
#endif
	TODO;
}
