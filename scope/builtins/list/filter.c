
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include <debug.h>

#include <array/new.h>
#include <array/push_n.h>
#include <array/struct.h>

#include <call.h>
#include <value/delete.h>
#include <value/function/struct.h>
#include <value/list/new.h>
#include <value/list/struct.h>
#include <value/struct.h>

#include <scope/builtins/cast/bool.h>
#include <scope/builtins/callback_header.h>

#include "filter.h"

// calls callback function on each element of the given list, returns a list
// of the elements that the callback function returned 'true' for.

DECLARE_BUILTIN_FUNC(builtin_list_filter)
{
	int error = 0;
	struct value* arg0, *arg1;
	struct list_value* list;
	struct function_value* func;
	struct array values;
	struct value* ele;
	struct value* params[1];
	struct value* callback_ret;
	ENTER;
	
	if(n == 2)
	{
		arg0 = args[0], arg1 = args[1];
		if(arg0->kind == vk_list && arg1->kind == vk_function)
		{
			list = (struct list_value*) arg0;
			func = (struct function_value*) arg1;
			values = new_array(struct value*);
			for(size_t i = 0, n = list->values.n;
				!error && i < n;i++)
			{
				verpv(i);
				ele = array_index(list->values, i, struct value*);
				params[0] = ele;
				error = call(func, 1, params, scope, &callback_ret);
				if(!error)
				{
					// call boolean cast on filter_ret
					// if(bool == true):
					if(as_bool(callback_ret))
					{
						// increment refcount
						ele->refcount++;
						// add to values array,
						array_push_n(&values, &ele);
					}
					
					error = delete_value(callback_ret);
				}
			}
			if(list->comparator) list->comparator->super.refcount++;
			*result = (struct value*) new_list_value(values, list->comparator);
		}
		else
		{
			fprintf(stderr, "filter(): requires *function* and *list*\n");
			error = 1;
		}
	}
	else
	{
		fprintf(stderr, "filter(): requires function and list\n");
		error = 1;
	}
	EXIT;
	return error;
}





