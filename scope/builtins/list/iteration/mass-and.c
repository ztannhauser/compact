
#include <assert.h>
#include <stdbool.h>
#include <stdio.h>

#include <debug.h>

#include <array/struct.h>
#include <array/new.h>

#include <linkedlist/struct.h>

#include <call.h>

#include <value/delete.h>
#include <value/list/new.h>
#include <value/list/struct.h>
#include <value/struct.h>

#include <expression/evaluate.h>

#include <scope/builtins/callback_header.h>

#include <scope/builtins/cast/bool.h>
#include <scope/fetch/boolean.h>

#include "mass-and.h"

DECLARE_BUILTIN_FUNC(builtin_list_iteration_mass_and)
{
	int error = 0;
	struct value* generic_func;
	struct value* return_value;
	struct function_value* func;
	struct list_value** params;
	struct value** param_values;
	bool b = true;
	ENTER;
	if(n >= 2)
	{
		generic_func = (args[0]);
		if(generic_func->kind == vk_function)
		{
			func = (struct function_value*) generic_func;
		}
		else
		{
			TODO; // print error message
			error = 1;
		}

		if(!error)
		{
			args++, n--;
			for(size_t i = 0;!error && i < n;i++)
			{
				error = args[i]->kind != vk_list;
			}
		}

		if(!error)
		{
			param_values = alloca(sizeof(struct value*) * n);
		}
		
		if(!error)
		{
			params = (struct list_value**) args;
			
			for(size_t i = 0, m = params[0]->values.n;
				!error && i < m;i++)
			{
				for(size_t j = 0;!error && j < n;j++)
				{
					param_values[j] =
						array_index(params[j]->values, i, void*);
				}
				
				error = call(func, n, param_values, scope, &return_value);
				
				if(!error)
				{
					b = as_bool(return_value);
					error = delete_value(return_value);
				}
			}
		}
		
		if(!error)
		{
			*result = (struct value*) scope_fetch_boolean(scope, b);
		}
	}
	else
	{
		fprintf(stderr, "mass-and(): expected function and list!\n");
		error = 1;
	}
	EXIT;
	return error;
}









