
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include <debug.h>

#include <array/new.h>
#include <array/push_n.h>
#include <array/struct.h>

#include <scope/builtins/callback_header.h>

#include <call.h>
#include <value/list/new.h>
#include <value/list/struct.h>
#include <value/struct.h>

#include "pmap.h"

DECLARE_BUILTIN_FUNC(builtin_list_iteration_pmap) {
	ENTER;
#if 0
	assert(n >= 1);
	struct value* generic_callback = params[0];
	assert(generic_callback->kind == vk_function);
	struct function_value* callback = (struct function_value*) generic_callback;
	verpv(callback);
	params++, n--;
	for(size_t i = 0;i < n;i++)
		assert(params[i]->kind == vk_list);
	struct array values = new_array(struct value*);
	struct list_value** argument_lists = (struct list_value**) params;
	struct value* args[n];
	void f(size_t i)
	{
		ENTER;
		verpv(i);
		if(i < n)
		{
			struct list_value* l = argument_lists[i];
			for(size_t j = 0, m = l->values.n;j < m;j++)
			{
				args[i] = array_index(l->values, j, struct value*);
				f(i + 1);
			}
		}
		else
		{
			// call function using args
			struct value* result = call(callback, n, args);
			array_push_n(&values, &result);
		}
		EXIT;
	}
	f(0);
	
	ret = (struct value*) new_list_value(values, NULL);
#endif
	TODO;

	EXIT;
}
