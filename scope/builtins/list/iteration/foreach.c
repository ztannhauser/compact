
#include <assert.h>
#include <stdio.h>

#include <array/struct.h>
#include <debug.h>
#include <linkedlist/struct.h>

#include <call.h>

#include <value/delete.h>
#include <value/list/new.h>
#include <value/list/struct.h>
#include <value/struct.h>

#include <scope/builtins/callback_header.h>

#include <expression/evaluate.h>

#include <scope/fetch/null.h>

#include "foreach.h"

DECLARE_BUILTIN_FUNC(builtin_list_iteration_foreach)
{
	int error = 0;
	struct value* generic_func;
	struct function_value* func;
	struct list_value** params;
	struct value* return_value;
	struct value** param_values;
	ENTER;
	
	if(n >= 1)
	{
		generic_func = args[0];
		error = generic_func->kind != vk_function;
		
		if(!error)
		{
			func = (struct function_value*) generic_func;
			
			args++, n--;
			for(size_t i = 0;!error && i < n;i++)
			{
				error = args[i]->kind != vk_list;
			}
			
			if(!error)
			{
				param_values = alloca(sizeof(struct value*) * n);
				
				params = (struct list_value**) args;
				
				for(size_t i = 0, m = params[0]->values.n;
					!error && i < m;i++)
				{
					for(size_t j = 0;!error && j < n;j++)
					{
						param_values[j] =
							array_index(params[j]->values, i, void*);
					}
					
					error = call(func, n, param_values, scope, &return_value);
					
					if(!error)
					{
						error = delete_value(return_value);
					}
				}
			}
		}
		
		if(!error)
		{
			*result = (struct value*) scope_fetch_null(scope);
		}
	}
	else
	{
		fprintf(stderr, "foreach(): function pointer required!\n");
	}
	EXIT;
	return error;
}






