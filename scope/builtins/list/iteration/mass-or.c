
#include <assert.h>
#include <stdbool.h>
#include <stdio.h>

#include <array/struct.h>
#include <debug.h>
#include <linkedlist/struct.h>

#include <call.h>

#include <value/delete.h>
#include <value/list/new.h>
#include <value/list/struct.h>
#include <value/struct.h>

#include <expression/evaluate.h>

#include <scope/builtins/callback_header.h>

#include <scope/builtins/cast/bool.h>
#include <scope/fetch/boolean.h>

#include "mass-or.h"

DECLARE_BUILTIN_FUNC(builtin_list_iteration_mass_or) {
	ENTER;
#if 0
	assert(n >= 2);
	struct value* func = (generic_params[0]);
	assert(func->kind == vk_function);
	generic_params++, n--;
	
	// check that all other arguments are lists:
	for(size_t i = 0;i < n;i++)
	{
		assert(generic_params[i]->kind == vk_list);
	}
	
	struct list_value** params = (struct list_value**) generic_params;
	size_t length = params[0]->values.n;
	
	// check that they are all the same length:
	{
		for(size_t i = 1;i < n;i++)
		{
			assert(params[i]->values.n >= length);
		}
	}
	
	bool b = false;
	for(size_t i = 0;!b && i < length;i++)
	{
		struct value* param_values[n];
		for(size_t j = 0;j < n;j++)
		{
			param_values[j] = array_index(params[j]->values, i, void*);
		}
		struct value* ret = call(
			(struct function_value*) func, n, param_values);
		verpv(ret);
		b = as_bool(ret);
		delete_value(ret);
	}
	ret = (struct value*) scope_fetch_boolean(b);
#endif
	TODO;
	EXIT;
}
