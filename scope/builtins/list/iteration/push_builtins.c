
#include <assert.h>
#include <stdio.h>

#include <debug.h>

#include <scope/push_builtin_func.h>

#include "foreach.h"
#include "map.h"
#include "mass-and.h"
#include "mass-or.h"
#include "max.h"
#include "min.h"
#include "pmap.h"
#include "reduce.h"

#include "push_builtins.h"

int push_list_iteration_builtins(struct scope *scope)
{
	int error;
	ENTER;

	error = 0
		?: scope_push_builtin_func(scope, L"pmap", 
			builtin_list_iteration_pmap)
		?: scope_push_builtin_func(scope, L"map", 
			builtin_list_iteration_map)
		?: scope_push_builtin_func(scope, L"max", 
			builtin_list_iteration_max)
		?: scope_push_builtin_func(scope, L"min", 
			builtin_list_iteration_min)
		?: scope_push_builtin_func(scope, L"foreach", 
			builtin_list_iteration_foreach)
		?: scope_push_builtin_func(scope, L"mass-and", 
			builtin_list_iteration_mass_and)
		?: scope_push_builtin_func(scope, L"all", 
			builtin_list_iteration_mass_and)
		?: scope_push_builtin_func(scope, L"reduce", 
			builtin_list_iteration_reduce)
		?: scope_push_builtin_func(scope, L"mass-or", 
			builtin_list_iteration_mass_or)
		?: scope_push_builtin_func(scope, L"any", 
			builtin_list_iteration_mass_or)
		;

	EXIT;
	return error;
}





