
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include <debug.h>

#include <call.h>

#include <scope/builtins/callback_header.h>

#include <value/delete.h>
#include <value/list/struct.h>
#include <value/struct.h>

#include <scope/fetch/null.h>

#include "reduce.h"

DECLARE_BUILTIN_FUNC(builtin_list_iteration_reduce) {
	ENTER;
#if 0
	assert(3 == n);
	struct value
		*generic_function = params[0],
		*generic_list = params[1],
		*generic_inital_value = params[2];
	
	assert(generic_function->kind == vk_function);
	assert(generic_list->kind == vk_list);
	
	struct function_value* function = (struct function_value*) generic_function;
	struct list_value* list = (struct list_value*) generic_list;
	
	ret = generic_inital_value;
	ret->refcount++;
	
	for(size_t i = 0, n = list->values.n;i < n;i++)
	{
		struct value* element = array_index(list->values, i, struct value*);
		
		struct value* args[] = {ret, element};
		
		struct value* new_ret = call(function, 2, args);
		
		delete_value(ret);
		
		ret = new_ret;
	}
#endif
	TODO;
	EXIT;
}
