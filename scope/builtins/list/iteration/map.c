
#include <assert.h>
#include <stdio.h>

#include <debug.h>

#include <array/new.h>
#include <array/push_n.h>
#include <array/struct.h>

#include <linkedlist/struct.h>

#include <call.h>

#include <value/list/new.h>
#include <value/list/struct.h>
#include <value/struct.h>

#include <scope/builtins/callback_header.h>

#include <expression/evaluate.h>

#include "map.h"

DECLARE_BUILTIN_FUNC(builtin_list_iteration_map)
{
	int error = 0;
	struct value* generic_func;
	struct function_value* func;
	struct list_value** params;
	struct array return_values;
	struct value* return_value;
	struct value** param_values;
	ENTER;
	
	if(n >= 1)
	{
		generic_func = args[0];
		error = generic_func->kind != vk_function &&
			fprintf(stderr, "map: first parameter must be a function!\n");
		
		if(!error)
		{
			func = (struct function_value*) generic_func;
			
			args++, n--;
			for(size_t i = 0;!error && i < n;i++)
			{
				error = args[i]->kind != vk_list;
			}
		}
		
		if(!error)
		{
			param_values = alloca(sizeof(struct value*) * n);
		}
		
		if(!error)
		{
			params = (struct list_value**) args;
			return_values = new_array(struct value*);
			
			for(size_t i = 0, m = params[0]->values.n;
				!error && i < m;i++)
			{
				for(size_t j = 0;!error && j < n;j++)
				{
					param_values[j] =
						array_index(params[j]->values, i, void*);
				}
				
				error = call(func, n, param_values, scope, &return_value);
				
				if(!error)
				{
					array_push_n(&return_values, &return_value);
				}
			}
		}
		
		if(!error)
		{
			*result = (struct value*) new_list_value(return_values, NULL);
		}
	}
	else
	{
		fprintf(stderr, "map(): function pointer required!\n");
	}
	EXIT;
	return error;
}





