
#include <assert.h>
#include <stdio.h>

#include <debug.h>

#include <array/new.h>
#include <array/push_n.h>
#include <array/struct.h>

#include <linkedlist/struct.h>

#include <call.h>

#include <scope/builtins/compare.h>
#include <scope/fetch/null.h>
#include <scope/builtins/callback_header.h>

#include <value/delete.h>
#include <value/list/new.h>
#include <value/list/struct.h>
#include <value/struct.h>

#include <expression/evaluate.h>

#include <scope/builtins/call_comparator.h>

#include "min.h"

DECLARE_BUILTIN_FUNC(builtin_list_iteration_min)
{
	int error = 0;
	struct value* generic_func;
	struct value* generic_comparator;
	struct function_value* func;
	struct function_value* comparator;
	struct list_value** casted_params;
	struct value** param_values;
	struct value* func_ret;
	int comparator_ret;
	ENTER;
	if(2 <= n && n <= 3)
	{
		generic_func = args[0];
		if(generic_func->kind == vk_function)
		{
			args++, n--;
			
			func = (struct function_value*) generic_func;
			
			{
				generic_comparator = args[n - 1];
				if(generic_comparator->kind == vk_function)
				{
					comparator = (struct function_value*) generic_comparator;
					n--;
				}
				else
				{
					comparator = NULL;
				}
			}
			
			for(size_t i = 0;!error && i < n;i++)
			{
				error = (args[i]->kind != vk_list);
			}
			
			if(!error)
			{
				param_values = alloca(sizeof(struct value*) * n);
				casted_params = (struct list_value**) args;
				*result = (struct value*) scope_fetch_null(scope);
				
				for(size_t i = 0, m = casted_params[0]->values.n;
					!error && i < m;i++)
				{
					for(size_t j = 0;j < n;j++)
					{
						param_values[j] =
							array_index(casted_params[j]->values, i, void*);
					}
					
					error = call(func, n, param_values, scope, &func_ret);
					
					if(!error)
					{
						if(i == 0 || (
							comparator ? (
								error = call_comparator(
									comparator,
									func_ret,
									*result,
									scope,
									&comparator_ret),
								!error && comparator_ret < 0
							) : (
								default_compare(func_ret, *result) < 0
							)
						))
						{
							error = error ?: delete_value(*result);
							*result = func_ret;
						}
						else
						{
							error = error ?: delete_value(func_ret);
						}
					}
				}
			}
		}
		else
		{
			// print warning, error = 1
			TODO;
		}
	}
	else
	{
		// print warning, error = 1
		TODO;
	}
	EXIT;
	return error;
}














