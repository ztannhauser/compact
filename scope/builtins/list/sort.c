
#include <assert.h>
#include <gmp.h>
#include <stdio.h>
#include <stdlib.h>

#include <debug.h>

#include "array/clone.h"
#include "array/foreach.h"

#include <misc/msort.h>

#include <call.h>
#include <value/delete.h>
#include <value/function/struct.h>
#include <value/inc.h>
#include <value/list/new.h>
#include <value/list/struct.h>
#include <value/number/struct.h>
#include <value/struct.h>

#include <expression/evaluate.h>

#include <scope/builtins/call_comparator.h>
#include <scope/fetch/compare.h>
#include <scope/builtins/callback_header.h>

#include "../compare.h"
#include "../math/sign.h"

#include "sort.h"

DECLARE_BUILTIN_FUNC(builtin_list_sort)
{
	int error = 0;
	struct value* generic_list = NULL;
	struct value* generic_comparator = NULL;
	ENTER;
	
	if(1 <= n && n <= 2)
	{
		struct array values;
		struct list_value* list = NULL;
		struct function_value* comparator;
		
		generic_list = args[0], comparator = NULL;
		error = generic_list->kind != vk_list;
		if(!error)
		{
			list = (struct list_value*) generic_list;;
			if(n == 2)
			{
				generic_comparator = args[1];
				error = generic_comparator->kind != vk_function;
				comparator = (struct function_value*) generic_comparator;
			}
		}
		
		if(!error)
		{
			values = array_clone(&(list->values));
			error = arrayp_foreach(&values, (int (*) (void*)) value_inc);
		}
		
		int compare(void* void_a, void* void_b, int* result)
		{
			int error;
			struct value** a, **b;
			ENTER;
			a = void_a, b = void_b;
			verpv(a); verpv(b);
			if(comparator)
			{
				error = call_comparator(
					comparator,
					*a,
					*b,
					scope,
					result);
			}
			else
			{
				*result = default_compare(*a, *b);
				error = 0;
			}
			EXIT;
			return error;
		}
		
		error = msort(values.data, values.n, values.elesize, compare);
		
		*result = (struct value*) new_list_value
		(
			values, 
			comparator ? (comparator->super.refcount++, comparator):
				scope_fetch_compare(scope)
		);
	}
	else
	{
		fprintf(stderr, "sort function takes one list to be sorted and "
			"an optional comparator!\n");
		error = 1;
	}
	EXIT;
	return error;
}





