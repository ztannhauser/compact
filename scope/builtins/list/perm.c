
#include <assert.h>
#include <stdbool.h>
#include <stdio.h>

#include <debug.h>

#include <array/delete.h>
#include <array/foreach.h>
#include <array/mass_push_n.h>
#include <array/new.h>
#include <array/pop_n.h>
#include <array/push_n.h>
#include <array/struct.h>

#include <scope/builtins/callback_header.h>

#include <value/inc.h>
#include <value/list/new.h>
#include <value/list/struct.h>
#include <value/struct.h>

#include "perm.h"

DECLARE_BUILTIN_FUNC(builtin_list_perm) {
#if 0
	struct value* ret;
	ENTER;
	assert(params_n == 1);
	struct value* generic_list = params[0];
	assert(generic_list->kind == vk_list);
	struct list_value* list = (struct list_value*) generic_list;
	struct array values = new_array(struct list_value*);
	struct array building = new_array(struct value*);
	bool* b = calloc(list->values.n, sizeof(bool));
	size_t n = list->values.n;
	void f(size_t x)
	{
		ENTER;
		verpv(x);
		if(x < n)
		{
			for(size_t i = 0, j = x;j < n && i < n;i++)
			{
				if(!b[i])
				{
					struct value* ele = array_index(list->values, i, void*);
					array_push_n(&building, &ele);
					b[i] = true;
					f(x + 1);
					b[i] = false;
					array_pop_n(&building);
					j++;
				}
			}
		}
		else
		{
			struct array saved = new_array(struct value*);
			array_mass_push_n(&saved, building.data, building.n);
			arrayp_foreach(&saved, (void (*)(void *)) value_inc);
			struct list_value* lv = new_list_value(saved, NULL);
			verpv(lv);
			array_push_n(&values, &lv);
			verpv(lv->super.refcount);
		}
		EXIT;
	}
	f(0);
	verpv(values.n);
	free(b);
	delete_array(&building);
	ret = (struct value*) new_list_value(values, NULL);
	verpv(ret);
	EXIT;
	return ret;
#endif
	TODO;
}
