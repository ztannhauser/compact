
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include <debug.h>

#include <array/foreach.h>
#include <array/mass_push_n.h>
#include <array/new.h>
#include <array/pop.h>

#include <value/inc.h>
#include <value/list/new.h>
#include <value/list/struct.h>
#include <value/struct.h>

#include <scope/builtins/callback_header.h>

#include <scope/builtins/compare.h>

#include "difference.h"

DECLARE_BUILTIN_FUNC(builtin_list_setmath_difference) {
#if 0
	struct value* ret;
	ENTER;
	assert(n >= 1);
	
	struct value* generic_original = params[0];
	assert(generic_original->kind == vk_list);
	struct list_value* original = (struct list_value*) generic_original;
	
	struct array values = new_array(struct value*);
	arrayp_foreach(&(original->values), (void (*)(void *)) value_inc);
	array_mass_push_n(&values, original->values.data, original->values.n);
	
	for(size_t i = 1;i < n;i++)
	{
		struct value* generic_subtract_me = params[i];
		assert(generic_subtract_me->kind == vk_list);
		struct list_value* subtract_me =
			(struct list_value*) generic_subtract_me;
		
		for(size_t j = 0, m = subtract_me->values.n;j < m;j++)
		{
			struct value* ele =
				array_index(subtract_me->values, j, struct value*);
			for(size_t k = 0, o = values.n;k < o;k++)
			{
				struct value* ele_ele = array_index(values, k, struct value*);
				if(default_compare(ele_ele, ele) == 0)
				{
					array_pop(&values, k);
					break;
				}
			}
			
		}
	}
	
	ret = (struct value*) new_list_value(values, NULL);
	EXIT;
	return ret;
#endif
	TODO;
}
