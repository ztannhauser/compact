
#include <assert.h>
#include <stdio.h>

#include <debug.h>

#include <scope/push_builtin_func.h>

#include "difference.h"
#include "intersection.h"
#include "union.h"

#include "push_builtins.h"

int push_list_setmath_builtins(struct scope *scope) {
	int error = 0;
	ENTER;

#if 0
	error = 0
		?: scope_push_builtin_func(scope, L"difference", NULL, 
			builtin_list_setmath_difference)
		?: scope_push_builtin_func(scope, L"intersection", NULL, 
			builtin_list_setmath_intersection)
		?: scope_push_builtin_func(scope, L"union", NULL, 
			builtin_list_setmath_union)
		;
#endif

	EXIT;
	return error;
}
