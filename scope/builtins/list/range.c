
#include <assert.h>
#include <stdio.h>

#include <debug.h>
#include <defines.h>

#include <array/new.h>
#include <array/push_n.h>
#include <array/struct.h>

#include <linkedlist/struct.h>

#include <value/list/new.h>
#include <value/number/new.h>
#include <value/number/struct.h>
#include <value/struct.h>

#include <expression/evaluate.h>

#include <scope/fetch/compare.h>

#include <scope/builtins/callback_header.h>

#include "range.h"

static int range_on_ints(
	signed long start, 
	signed long end,
	signed long inc,
	struct array* values)
{
	int error = 0;
	struct number_value* num;
	ENTER;
	
	verpv(start);
	verpv(end);
	verpv(inc);
	
	if(inc > 0 ? (end > start) : (start > end))
	{
		for(signed long i = start;i < end;i += inc)
		{
			num = new_number_value_as_integer(i);
			array_push_n(values, &num);
		}
	}
	
	EXIT;
	return error;
}

#if 0
static struct list_value* range_on_longs(
	signed long start, 
	signed long end,
	signed long inc)
{
	struct list_value* ret;
	ENTER;
	
	verpv(start);
	verpv(end);
	verpv(inc);
	
	struct array values = new_array(struct value*);
	if(inc > 0 ? (end > start) : (start > end))
	{
		for(signed long i = start;i < end;i += inc)
		{
			struct number_value* ele = new_number_value_as_integer(i);
			array_push_n(&values, &ele);
		}
	}
	
	ret = new_list_value(values, NULL);
	verpv(ret);
	
	EXIT;
	return ret;
}

static struct list_value* range_on_quads(
	signed long start, 
	signed long end,
	signed long inc)
{
	struct list_value* ret;
	ENTER;
	
	verpv(start);
	verpv(end);
	verpv(inc);
	
	struct array values = new_array(struct value*);
	if(inc > 0 ? (end > start) : (start > end))
	{
		for(signed long i = start;i < end;i += inc)
		{
			struct number_value* ele = new_number_value_as_integer(i);
			array_push_n(&values, &ele);
		}
	}
	
	ret = new_list_value(values, NULL);
	verpv(ret);
	
	EXIT;
	return ret;
}
#endif

DECLARE_BUILTIN_FUNC(builtin_list_range)
{
	int error = 0;
	struct value* generic_start, *generic_end;
	struct number_value* number_start, *number_end;
	signed long start_long, end_long;
	struct array values;
	ENTER;
	
	if(2 <= n && n <= 3)
	{
		generic_start = args[0], generic_end = args[1];
		if(generic_start->kind == vk_number && generic_end->kind == vk_number)
		{
			
			number_start = (struct number_value*) generic_start;
			number_end = (struct number_value*) generic_end;
			
		}
		else
		{
			// print error message,
			TODO;
			error = 1;
		}
		
		if(!error)
		{
			values = new_array(struct value*);
			
			switch(number_start->kind)
			{
				case nvk_integer:
				{
					start_long = number_start->integer;
					switch(number_end->kind)
					{
						case nvk_integer:
						{
							end_long = number_end->integer;
							if(n == 3)
							{
								TODO;
							}
							else
							{
								error = range_on_ints(
									start_long,
									end_long,
									end_long > start_long ? 1 : -1,
									&values);
							}
							break;
						}
						
						case nvk_long:
						{
							TODO;
							break;
						}
						
						case nvk_decimal:
						{
							TODO;
							break;
						}
						
						default: TODO;
					}
					break;
				}
				
				case nvk_long:
				{
					switch(number_end->kind)
					{
						case nvk_integer:
						{
							TODO;
							break;
						}
						
						case nvk_long:
						{
							TODO;
							break;
						}
						
						case nvk_decimal:
						{
							TODO;
							break;
						}
						
						default: TODO;
					}
					break;
				}
				
				case nvk_decimal:
				{
					switch(number_end->kind)
					{
						case nvk_integer:
						{
							TODO;
							break;
						}
						
						case nvk_long:
						{
							TODO;
							break;
						}
						
						case nvk_decimal:
						{
							TODO;
							break;
						}
						
						default: TODO;
					}
					break;
				}
				
				default: TODO;
			}
		}
		
		if(!error)
		{
			*result = (struct value*) new_list_value(values, NULL);
		}
		
	}
	else
	{
		fprintf(stderr, "range: requires start and end, and optionally "
			"an increment number\n");
		error = 1;
	}
	EXIT;
	return error;
}






























