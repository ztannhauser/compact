
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include <debug.h>

#include <array/new.h>
#include <array/push_n.h>
#include <array/struct.h>

#include <value/list/new.h>
#include <value/list/struct.h>
#include <value/struct.h>

#include <scope/builtins/callback_header.h>

#include "transpose.h"

DECLARE_BUILTIN_FUNC(builtin_list_transpose) {
#if 0
	struct value* ret;
	ENTER;
	assert(n == 1);
	struct value* generic_big_list = params[0];
	assert(generic_big_list->kind == vk_list);
	struct list_value* big_list = (struct list_value*) generic_big_list;
	size_t major_n = big_list->values.n;
	
	if(major_n)
	{
		struct value* first = array_index(big_list->values, 0, struct value*);
		assert(first->kind == vk_list);
		size_t minor_n = ((struct list_value*) first)->values.n;
		verpv(minor_n);
		
		for(size_t i = 1;i < major_n;i++)
		{
			struct value* ele = array_index(big_list->values, i, struct value*);
			assert(ele->kind == vk_list);
			struct list_value* spef = (struct list_value*) ele;
			verpv(spef->values.n);
			assert(spef->values.n == minor_n);
		}
		
		struct array values = new_array(struct list_value*);
		for(size_t i = 0;i < minor_n;i++)
		{
			struct array major_values = new_array(struct value*);
			for(size_t j = 0;j < major_n;j++)
			{
				struct list_value* a =
					array_index(big_list->values, j, struct list_value*);
				struct value* b = array_index(a->values, i, struct value*);
				b->refcount++;
				array_push_n(&major_values, &b);
			}
			struct value* c = (struct value*) new_list_value(major_values, NULL);
			array_push_n(&values, &c);
		}
		
		ret = (struct value*) new_list_value(values, NULL);
	}
	else
	{
		ret = generic_big_list;
		ret->refcount++;
	}
	
	EXIT;
	return ret;
#endif
	TODO;
}
