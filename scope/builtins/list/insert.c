
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include <debug.h>

#include <array/clone.h>
#include <array/foreach.h>
#include <array/mass_push_n.h>
#include <array/new.h>
#include <array/push.h>
#include <array/push_n.h>
#include <array/struct.h>

#include <call.h>
#include <value/delete.h>
#include <value/inc.h>
#include <value/list/new.h>
#include <value/list/struct.h>
#include <value/struct.h>

#include <scope/builtins/call_comparator.h>
#include <scope/builtins/cast/bool.h>
#include <scope/fetch/null.h>
#include <scope/builtins/callback_header.h>

#include "insert.h"

// looking for index 4.5
// 0 1 2 3 4 5 6 7 8 9 A B C D E F
// |             |               |
// |     |     |
//         | | |
//         |

// looking for index 5.5
// 0 1 2 3 4 5 6 7 8 9 A B C D E F
// |             |               |
// |     |     |
//         | | |
//             |

#if 0
static size_t find_insertion_index(struct array *values, struct value *insertme,
								   struct function_value *comparator) {
	size_t ret;
	ENTER;

	verpv(values->n);

	if (values->n > 0) {
		bool found = false;

		ssize_t mid, start = 0, end = values->n - 1;
		verpv(start);
		verpv(end);

		while (!found && start < end) {
			verpv(start);
			verpv(end);
			mid = (start + end) / 2;
			verpv(mid);

			int c = call_comparator(comparator, insertme,
									array_index(*values, mid, struct value *));
			verpv(c);

			if (c > 0) {
				start = mid + 1;
			} else if (c < 0) {
				end = mid - 1;
			} else {
				HERE;
				ret = mid, found = true;
			}
		}

		if (!found) {
			// 'start' and 'end' have stepped on each other,
			int c =
				call_comparator(comparator, insertme,
								array_index(*values, start, struct value *));
			verpv(c);
			if (c > 0)
				ret = start + 1;
			else
				ret = start;
		}
	} else {
		ret = 0;
	}
	verpv(ret);
	EXIT;
	return ret;
}
#endif

DECLARE_BUILTIN_FUNC(builtin_list_insert) {
#if 0
	struct value* ret;
	ENTER;
	assert(1 <= n);
	
	struct value* generic_list = params[0];
	assert(generic_list->kind == vk_list);
	struct list_value* list = (struct list_value*) generic_list;
	n--, params++;
	
	struct array values = array_clone(&(list->values));
	arrayp_foreach(&values, (void (*)(void *)) value_inc);
	
	if(list->comparator)
	{
		for(size_t i = 0;i < n;i++)
		{
			struct value* param = params[i];
			
			size_t where = find_insertion_index(&values,
				param, list->comparator);
				
			verpv(where);
			
			param->refcount++;
			array_push(&values, where, &param);
		}
		
		list->comparator->super.refcount++;
	}
	else
	{
		// append each value to end of 'values' array
		for(size_t i = 0;i < n;i++)
		{
			struct value* param = params[i];
			verpv(param);
			param->refcount++;
			array_push_n(&values, &param);
		}
	}
	

	ret = (struct value*) new_list_value(values, list->comparator);
	EXIT;
	return ret;
#endif
	TODO;
}
