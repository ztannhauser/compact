

#include <value/struct.h>

#include <scope/struct.h>

#define DECLARE_BUILTIN_FUNC(name) \
	int name(\
		size_t n, struct value** args,\
		struct scope* scope,\
		struct value** result)
