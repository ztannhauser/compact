
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include <debug.h>

#include <value/delete.h>
#include <value/struct.h>

#include <scope/builtins/math/sign.h>

#include <call.h>

#include "call_comparator.h"

int call_comparator(
	struct function_value *comparator,
	struct value *a,
	struct value *b,
	struct scope* scope,
	int *result)
{
	int error = 0;
	struct value* args[2];
	struct value* comparator_ret_generic;
	struct number_value* comparator_ret_number;
	ENTER;

	args[0] = a, args[1] = b;
	error = call(comparator, 2, args, scope, &comparator_ret_generic);
	if(!error)
	{
		if(comparator_ret_generic->kind == vk_number)
		{
			comparator_ret_number = (struct number_value*) comparator_ret_generic;
			*result = cast_to_sign(comparator_ret_number);
			error = delete_value(comparator_ret_generic);
		}
		else
		{
			TODO; // print error message!
			error = 1;
		}
	}
	EXIT;
	return error;
}














