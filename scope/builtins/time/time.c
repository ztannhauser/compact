
#include <assert.h>
#include <stdlib.h>
#include <time.h>

#include <debug.h>

#include <value/number/new.h>

#include <scope/fetch/null.h>

#include <scope/builtins/callback_header.h>

#include "time.h"

DECLARE_BUILTIN_FUNC(builtin_time_time) {
#if 0
	time_t t = time(NULL);
	verpv(t);
	ret = new_number_value_as_integer(t);
#endif
	TODO;
}
