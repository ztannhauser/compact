
#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#include <debug.h>

#include "../../push_builtin_func.h"

#include "time.h"

#include "push_builtins.h"

int push_time_builtins(struct scope *scope) {
	int error;
	ENTER;

	error = 0
		?: scope_push_builtin_func(scope, L"time",
			builtin_time_time)
		;

	EXIT;
	return error;
}
