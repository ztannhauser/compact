

#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <debug.h>

#include <scope/fetch/null.h>

#include <call.h>

#include <value/delete.h>
#include <value/print.h>
#include <value/struct.h>

#include <expression/evaluate.h>

#include <scope/builtins/cast/bool.h>
#include <scope/builtins/callback_header.h>

#include "while1.h"

DECLARE_BUILTIN_FUNC(builtin_loops_while1) {
	int error = 0;
	struct value* arg;
	struct value* return_value;
	struct function_value* func;
	ENTER;
	if(n == 1)
	{
		arg = args[0];
		if(arg->kind == vk_function)
		{
			func = (struct function_value*) arg;
			
			while(!error)
			{
				error = 0
					?: call(func, 0, NULL, scope, &return_value)
					?: delete_value(return_value)
					;
			}
			
		}
		else
		{
			fprintf(stderr, "while1: requires one *function* to call!\n");
			error = 1;
		}
	}
	else
	{
		fprintf(stderr, "while1: takes one function to indefiniately call!\n");
		error = 1;
	}
	EXIT;
	return error;
}

























