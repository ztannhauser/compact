

#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <debug.h>

#include <scope/fetch/null.h>

#include <call.h>

#include <value/delete.h>
#include <value/print.h>
#include <value/struct.h>

#include <expression/evaluate.h>

#include <scope/builtins/cast/bool.h>
#include <scope/builtins/callback_header.h>

#include "while.h"

DECLARE_BUILTIN_FUNC(builtin_loops_while)
{
	int error = 0;
	struct value* arg;
	struct value* conditional;
	struct function_value* func;
	ENTER;
	if(n == 1)
	{
		arg = args[0];
		if(arg->kind == vk_function)
		{
			func = (struct function_value*) arg;
			for(bool cont = true;!error && cont;)
			{
				error = call(func, 0, NULL, scope, &conditional);
				if(!error)
				{
					verpv(conditional);
					cont = as_bool(conditional);
					verpvb(cont);
					error = delete_value(conditional);
				}
			}
			*result = (struct value*) scope_fetch_null(scope);
		}
		else
		{
			fprintf(stderr, "while: requires one *function*!\n");
			error = 1;
		}
	}
	else
	{
		fprintf(stderr, "while: requires one function!\n");
		error = 1;
	}
	EXIT;
	return error;
}







