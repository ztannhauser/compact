
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include <debug.h>

#include <scope/push_builtin_func.h>

#include "while.h"
#include "while1.h"

#include "push_builtins.h"

int push_loops_builtins(struct scope *scope) {
	int error = 0;
	ENTER;

	error = 0
		?: scope_push_builtin_func(scope, L"while",
			builtin_loops_while)
		?: scope_push_builtin_func(scope, L"while1",
			builtin_loops_while1)
		;

	EXIT;
	return error;
}
