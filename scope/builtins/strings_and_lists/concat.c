
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include <debug.h>

#include <array/mass_push_n.h>
#include <array/new.h>
#include <array/struct.h>

#include <value/inc.h>
#include <value/list/new.h>
#include <value/list/struct.h>
#include <value/new.h>
#include <value/string/new.h>
#include <value/string/struct.h>
#include <value/struct.h>

#include <scope/builtins/callback_header.h>

#include "concat.h"

DECLARE_BUILTIN_FUNC(builtin_string_and_lists_concat) {
#if 0
	struct value* ret;
	verpv(n_params);
	assert(n_params == 1);
	struct value* generic_list = params[0];
	assert(generic_list->kind == vk_list);
	struct list_value* list = (struct list_value*) generic_list;
	size_t n = list->values.n;
	assert(n);
	struct value* first = array_index(list->values, 0, struct value*);
	switch(first->kind)
	{
		case vk_string:
		{
#if 0
			struct array chars = new_array(char);
			for(size_t i = 0;i < n;i++)
			{
				struct value* ele = array_index(list->values, i, struct value*);
				assert(ele->kind == vk_string);
				struct string_value* spef_ele = (struct string_value*) ele;
				verpvsn(spef_ele->chars.data, spef_ele->chars.n);
				array_mass_push_n(&chars,
					spef_ele->chars.data,
					spef_ele->chars.n - spef_ele->is_null_terminated);
			}
			ret = (struct value*) new_string_value(chars, false);
#endif
			TODO;
			break;
		}
		case vk_list:
		{
#if 0
			struct array values = new_array(struct value*);
			for(size_t i = 0;i < n;i++)
			{
				struct value* ele = array_index(list->values, i, struct value*);
				assert(ele->kind == vk_list);
				struct list_value* spef_ele = ele;
				array_mass_push_n(&values, spef_ele->values.data, spef_ele->values.n);
			}
			arrayp_foreach(&values, value_inc);
			ret = new_list_value(values, NULL);
#endif
			TODO;
			break;
		}
		default: TODO;
	}
	return ret;
#endif
	TODO;
}
