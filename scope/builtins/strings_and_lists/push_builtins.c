
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include <debug.h>

#include <scope/push_builtin_func.h>

#include "concat.h"

#include "push_builtins.h"

int push_string_and_list_builtins(struct scope *scope) {
	int error = 0;
	ENTER;

	error = 0
		?: scope_push_builtin_func(scope, L"concat",
			builtin_string_and_lists_concat)
		;

	EXIT;
	return error;
}
