
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include <debug.h>

#include <scope/push_builtin_func.h>

#include "endswith.h"
#include "index.h"
#include "join.h"
#include "joinln.h"
#include "replace.h"
#include "rindex.h"
#include "split-ws.h"
#include "split.h"
#include "sprintf.h"
#include "sscanf.h"
#include "startswith.h"
#include "strcasecmp.h"
#include "strcmp.h"
#include "strlen.h"
#include "strverscasecmp.h"
#include "strverscmp.h"
#include "substr.h"
#include "substr0.h"
#include "trim.h"

#include "push_builtins.h"

int push_string_builtins(struct scope *scope) {
	int error = 0;
	ENTER;

	error = 0
		?: scope_push_builtin_func(scope, L"endswith", 
			builtin_string_endswith)
		?: scope_push_builtin_func(scope, L"index", 
			builtin_string_index)
		?: scope_push_builtin_func(scope, L"join", 
			builtin_string_join)
		?: scope_push_builtin_func(scope, L"joinln", 
			builtin_string_joinln)
		?: scope_push_builtin_func(scope, L"replace", 
			builtin_string_replace)
		?: scope_push_builtin_func(scope, L"rindex", 
			builtin_string_rindex)
		?: scope_push_builtin_func(scope, L"split", 
			builtin_string_split)
		?: scope_push_builtin_func(scope, L"split-ws", 
			builtin_string_split_ws)
		?: scope_push_builtin_func(scope, L"sprintf", 
			builtin_string_sprintf)
		?: scope_push_builtin_func(scope, L"sscanf", 
			builtin_string_sscanf)
		?: scope_push_builtin_func(scope, L"startswith", 
			builtin_string_startswith)
		?: scope_push_builtin_func(scope, L"strcasecmp", 
			builtin_string_strcasecmp)
		?: scope_push_builtin_func(scope, L"strcmp", 
			builtin_string_strcmp)
		?: scope_push_builtin_func(scope, L"strlen", 
			builtin_string_strlen)
		?: scope_push_builtin_func(scope, L"strverscasecmp",
			builtin_string_strverscasecmp)
		?: scope_push_builtin_func(scope, L"strverscmp", 
			builtin_string_strverscmp)
		?: scope_push_builtin_func(scope, L"substr", 
			builtin_string_substr)
		?: scope_push_builtin_func(scope, L"substr0", 
			builtin_string_substr0)
		?: scope_push_builtin_func(scope, L"trim", 
			builtin_string_trim)
		;

	EXIT;
	return error;
}







