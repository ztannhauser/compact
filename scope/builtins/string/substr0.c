
#include <assert.h>
#include <stdbool.h>
#include <stdio.h>

#include <debug.h>

#include <array/struct.h>
#include <array/new.h>

#include <value/number/struct.h>
#include <value/string/construct.h>
#include <value/string/struct.h>
#include <value/struct.h>

#include <scope/builtins/callback_header.h>

#include "substr0.h"

DECLARE_BUILTIN_FUNC(builtin_string_substr0)
{
	int error = 0;
	ENTER;
	if(n == 2)
	{
		struct value* generic_0, *generic_1;
		
		generic_0 = args[0], generic_1 = args[1];
		if(generic_0->kind == vk_string && generic_1->kind == vk_number)
		{
			size_t length;
			struct string_value* string;
			struct number_value* number;
			
			string = (struct string_value*) generic_0;
			number = (struct number_value*) generic_1;
			if(number->kind == nvk_integer)
			{
				length = number->integer;
				if(length <= string->wchars.n)
				{
					*result = (struct value*) 
						construct_string_value(string->wchars.datawc, length);
				}
				else
				{
					TODO;
					error = 1;
				}
			}
			else
			{
				TODO;
				error = 1;
			}
		}
		else
		{
			TODO;
			error = 1;
		}
	}
	else
	{
		TODO;
		error = 1;
	}
	EXIT;
	return error;
}
