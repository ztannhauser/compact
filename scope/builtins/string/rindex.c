
#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#include <debug.h>

#include <value/number/new.h>
#include <value/string/struct.h>
#include <value/struct.h>

#include <scope/builtins/callback_header.h>

#include "rindex.h"

DECLARE_BUILTIN_FUNC(builtin_string_rindex)
{
	int error = 0;
	ENTER;
	if(n == 2)
	{
		struct value* generic_s, *generic_c;
		
		generic_s = args[0], generic_c = args[1];
		if(generic_s->kind == vk_string && generic_c->kind == vk_string)
		{
			struct string_value* s, *c;
			
			s = (struct string_value*) generic_s;
			c = (struct string_value*) generic_c;
			
			if(c->wchars.n == 1)
			{
				wchar_t cc;
				bool found;
				size_t index;
				
				cc = array_index(c->wchars, 0, wchar_t);
				found = false;
				for(
					const wchar_t
						*b = s->wchars.datawc,
						*e = b + s->wchars.n,
						*i = e - 1;
					i >= b; i--)
				{
					if(cc == *i)
					{
						found = true;
						index = (i - b);
						break;
					}
				}
				
				if(found)
				{
					*result = (struct value*) new_number_value_as_integer(index);
				}
				else
				{
					TODO; // print error message
					error = 1;
				}
			}
			else
			{
				TODO; // print error message
				error = 1;
			}
		}
		else
		{
			TODO; // print error message
			error = 1;
		}
	}
	else
	{
		TODO; // print error message
		error = 1;
	}
	EXIT;
	return error;
}
