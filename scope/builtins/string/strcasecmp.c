
#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <array/struct.h>
#include <debug.h>

#include <value/number/new.h>
#include <value/string/struct.h>
#include <value/struct.h>

#include <scope/builtins/callback_header.h>

#include "strcasecmp.h"

int process_strcasecmp(struct string_value *a, struct string_value *b) {
	int ret;
	ENTER;
#if 0
	ret = 0;
	
	if(a->is_null_terminated) string_value_remove_null(a);
	if(b->is_null_terminated) string_value_remove_null(b);
	
	size_t min_n = a->chars.n > b->chars.n ? a->chars.n : b->chars.n;
	
	verpv(min_n);
	
	const char* a_ptr = a->chars.datac;
	const char *b_ptr = b->chars.datac;
	for(size_t i = 0;!ret && i < min_n;i++, a_ptr++, b_ptr++)
	{
		ret = to_lowercase(*a_ptr) - to_lowercase(*b_ptr);
	}
	
	if(!ret)
	{
		ret = a->chars.n - b->chars.n;
	}
	verpv(ret);
#endif
	TODO;
	EXIT;
	return ret;
}

DECLARE_BUILTIN_FUNC(builtin_string_strcasecmp) {
#if 0
	struct value* ret;
	ENTER;
	assert(n == 2);
	struct value* ga = params[0];
	struct value* gb = params[1];
	assert(ga->kind == vk_string);
	assert(gb->kind == vk_string);
	struct string_value* a = ga, *b = gb;
	ret = new_number_value_as_integer(process_strcasecmp(a, b));
	EXIT;
	return ret;
#endif
	TODO;
}
