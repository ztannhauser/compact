
#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#include <debug.h>

#include <value/number/struct.h>
#include <value/string/construct.h>
#include <value/string/struct.h>
#include <value/struct.h>

#include <scope/builtins/callback_header.h>

#include "substr.h"

DECLARE_BUILTIN_FUNC(builtin_string_substr) {
#if 0
	struct value* ret;
	ENTER;
	assert(2 <= params_n && params_n <= 3);
	struct value* generic_string = params[0];
	assert(generic_string->kind == vk_string);
	struct string_value* string = (struct string_value*) generic_string;
	size_t n = string->wchars.n;
	
	size_t index;
	{
		struct value* generic_index = params[1];
		assert(generic_index->kind == vk_number);
		struct number_value* number_index = (struct number_value*) generic_index;
		assert(number_index->kind == nvk_integer);
		index = number_index->integer;
	}
	assert(0 <= index && index <= n);
	
	size_t length;
	if(params_n == 3)
	{
		struct value* generic_index = params[2];
		assert(generic_index->kind == vk_number);
		struct number_value* number_index = (struct number_value*) generic_index;
		assert(number_index->kind == nvk_integer);
		length = number_index->integer;
		assert(index + length <= n);
	}
	else
	{
		length = n - index;
	}
	
	ret = (struct value*)
		construct_string_value(string->wchars.datawc + index, length);
	
	EXIT;
	return ret;
#endif
	TODO;
}
