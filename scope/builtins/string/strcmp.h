
#include "value/string/struct.h"

int process_strcmp(struct string_value* a, struct string_value* b);

#include <scope/builtins/callback_header.h>

DECLARE_BUILTIN_FUNC(builtin_string_strcmp);

