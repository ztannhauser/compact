
#include <assert.h>
#include <stdio.h>
#include <string.h>

#include <debug.h>

#include <char/is_digit.h>

#include <array/struct.h>

#include <value/number/new.h>
#include <value/string/struct.h>
#include <value/struct.h>

#include <scope/builtins/callback_header.h>

#include "strverscasecmp.h"

#if 0
static int process_strverscmp(struct array *a_wchars, struct array *b_wchars) {
	int ret = 0;
	ENTER;
	wchar_t *a_ptr = a_wchars->datawc, *b_ptr = b_wchars->datawc;
	wchar_t *a_end = a_ptr + a_wchars->n, *b_end = b_ptr + b_wchars->n;
	for (; !ret && a_ptr < a_end && b_ptr < b_end;) {
		verpvc(*a_ptr);
		verpvc(*b_ptr);
		if (is_digit(*a_ptr) && is_digit(*b_ptr)) {
			wchar_t *a_before_leading = a_ptr, *b_before_leading = b_ptr;
			while (a_ptr < a_end && (*a_ptr) == '0')
				a_ptr++;
			while (b_ptr < b_end && (*b_ptr) == '0')
				b_ptr++;
			wchar_t *a_number_end = a_ptr, *b_number_end = b_ptr;
			while (a_number_end < a_end && is_digit(*a_number_end))
				a_number_end++;
			while (b_number_end < b_end && is_digit(*b_number_end))
				b_number_end++;
			ret = (a_number_end - a_ptr) - (b_number_end - b_ptr);
			for (; !ret && a_ptr <= a_number_end && b_ptr <= b_number_end;
				 a_ptr++, b_ptr++) {
				ret = (*a_ptr) - (*b_ptr);
			}
			if (!ret) {
				ret = (a_number_end - a_before_leading) -
					  (b_number_end - b_before_leading);
			}
			a_ptr = a_number_end, b_ptr = b_number_end;
		} else {
			ret = (*a_ptr++) - (*b_ptr++);
		}
	}
	if (!ret) {
		ret = a_wchars->n - b_wchars->n;
	}
	verpv(ret);
	EXIT;
	return ret;
}
#endif

DECLARE_BUILTIN_FUNC(builtin_string_strverscmp) {
#if 0
	struct value* ret;
	ENTER;
	assert(n == 2);
	struct value* generic_a = params[0];
	struct value* generic_b = params[1];
	assert(generic_a->kind == vk_string);
	assert(generic_b->kind == vk_string);
	struct string_value
		*a = (struct string_value*) generic_a,
		*b = (struct string_value*) generic_b;
	
	ret = (struct value*) new_number_value_as_integer(
		process_strverscmp(&(a->wchars), &(b->wchars)));

	EXIT;
	return ret;
#endif
	TODO;
}
