
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include <array/mass_push_n.h>
#include <array/new.h>
#include <array/struct.h>
#include <debug.h>

#include <value/string/new.h>
#include <value/struct.h>

#include "../io/printf.h"

#include <scope/builtins/callback_header.h>

#include "sprintf.h"

DECLARE_BUILTIN_FUNC(builtin_string_sprintf) {
	ENTER;
#if 0
	assert(n >= 1);
	struct value* first = params[0];
	assert(first->kind == vk_string);
	struct array chars = new_array(char);
	void my_push(const char* c, size_t len)
	{
		array_mass_push_n(&chars, c, len);
	}
	n--, params++;
	process_format_string(my_push, (struct string_value*) first, n, params);
	ret = (struct value*) new_string_value(chars, false);
#endif
	TODO;
	EXIT;
}
