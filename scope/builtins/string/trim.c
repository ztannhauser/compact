
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <debug.h>

#include <scope/fetch/boolean.h>
#include <value/string/new.h>
#include <value/string/struct.h>
#include <value/struct.h>

#include <scope/builtins/callback_header.h>

#include "trim.h"

DECLARE_BUILTIN_FUNC(builtin_string_trim) {
	ENTER;
#if 0
	assert(n == 1);
	struct value* generic_string = params[0];
	assert(generic_string->kind == vk_string);
	struct string_value* string = generic_string;
	if(string->is_null_terminated) string_value_remove_null(string);
	struct array chars;
	char* b = string->chars.data;
	char* e = b + string->chars.n;
	verpvsn(b, e - b);
	char* b2; for(b2 = b;b2 < e && index(" \n\t\r", *b2);b2++);
	if(b2 < e)
	{
		verpvsn(b2, e - b2);
		char* e2; for(e2 = e - 1;b2 < e2 && index(" \n\t\r", *e2);e2--);
		verpvsn(b2, e2 - b2 + 1);
		chars = new_array_from_data(b2, e2 - b2 + 1, char);
	}
	else
	{
		chars = new_array(char);
	}
	ret = new_string_value(chars, false);
#endif
	TODO;
	EXIT;
}
