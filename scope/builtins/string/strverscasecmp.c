
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <debug.h>

#include <array/struct.h>

#include <char/is_digit.h>
#include <char/to_lowercase.h>

#include <value/number/new.h>
#include <value/string/struct.h>
#include <value/struct.h>

#include <scope/builtins/callback_header.h>

#include "strverscasecmp.h"

int process_strverscasecmp(struct string_value *left,
						   struct string_value *right) {
	int ret;
	ENTER;
	ret = 0;
	wchar_t *a_ptr = left->wchars.datawc, *b_ptr = right->wchars.datawc;
	wchar_t *a_end = a_ptr + left->wchars.n, *b_end = b_ptr + right->wchars.n;

	verpv(a_ptr);
	verpv(b_ptr);
	verpv(a_end);
	verpv(b_end);

	for (; !ret && a_ptr < a_end && b_ptr < b_end;) {
		// verpvc(*a_ptr);
		// verpvc(*b_ptr);
		if (is_digit(*a_ptr) && is_digit(*b_ptr)) {
			wchar_t *a_before_leading = a_ptr, *b_before_leading = b_ptr;
			while (a_ptr < a_end && (*a_ptr) == '0')
				a_ptr++;
			while (b_ptr < b_end && (*b_ptr) == '0')
				b_ptr++;
			wchar_t *a_number_end = a_ptr, *b_number_end = b_ptr;
			while (a_number_end < a_end && is_digit(*a_number_end))
				a_number_end++;
			while (b_number_end < b_end && is_digit(*b_number_end))
				b_number_end++;
			ret = (a_number_end - a_ptr) - (b_number_end - b_ptr);
			for (; !ret && a_ptr <= a_number_end && b_ptr <= b_number_end;
				 a_ptr++, b_ptr++) {
				ret = (*a_ptr) - (*b_ptr);
			}
			if (!ret) {
				ret = (a_number_end - a_before_leading) -
					  (b_number_end - b_before_leading);
			}
			a_ptr = a_number_end, b_ptr = b_number_end;
		} else {
			ret = to_lowercase(*a_ptr++) - to_lowercase(*b_ptr++);
		}
	}

	if (!ret) {
		ret = left->wchars.n - right->wchars.n;
	}

	verpv(ret);
	EXIT;
	return ret;
}

DECLARE_BUILTIN_FUNC(builtin_string_strverscasecmp)
{
	int error = 0;
	struct value* generic_a;
	struct value* generic_b;
	struct string_value* a;
	struct string_value* b;
	ENTER;
	
	if(n == 2)
	{
		generic_a = args[0], generic_b = args[1];
		
		if(generic_a->kind == vk_string && generic_b->kind == vk_string)
		{
			
			a = (struct string_value*) generic_a;
			b = (struct string_value*) generic_b;
			
			*result = (struct value*)
				new_number_value_as_integer(process_strverscasecmp(a, b));
			
		}
		else
		{
			fprintf(stderr, "strverscasecmp: requires 2 *strings* as "
				"arguments!\n");
			error = 1;
		}
	}
	else
	{
		fprintf(stderr, "strverscasecmp: requires 2 strings as arguments!\n");
		error = 1;
	}
	EXIT;
	return error;
}


















