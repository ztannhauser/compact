
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include <array/struct.h>
#include <debug.h>

#include <value/list/struct.h>
#include <value/string/new.h>
#include <value/string/struct.h>
#include <value/struct.h>

#include <scope/builtins/callback_header.h>

#include "joinln.h"

DECLARE_BUILTIN_FUNC(builtin_string_joinln) {
	ENTER;
#if 0
	assert(n == 1);
	struct value* generic_list = params[0];
	assert(generic_list->kind == vk_list);
	struct list_value* list = generic_list;
	struct array chars = new_array(char);
	for(size_t i = 0, n = list->values.n;i < n;i++)
	{
		struct value* ele = array_index(list->values, i, struct value*);
		assert(ele->kind == vk_string);
		struct string_value* spef_ele = ele;
		if(spef_ele->is_null_terminated)
		{
			string_value_remove_null(spef_ele);
		}
		array_mass_push_n(&chars, spef_ele->chars.data, spef_ele->chars.n);
		array_mass_push_n(&chars, "\n", 1);
	}
	ret = new_string_value(chars, false);
#endif
	TODO;
	EXIT;
}
