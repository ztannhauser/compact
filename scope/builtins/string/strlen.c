
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <debug.h>

#include <value/number/new.h>

#include <value/string/new.h>
#include <value/string/struct.h>
#include <value/struct.h>

#include <scope/builtins/callback_header.h>

#include "strlen.h"

DECLARE_BUILTIN_FUNC(builtin_string_strlen) {
	ENTER;
#if 0
	assert(n == 1);
	struct value* generic_string = params[0];
	assert(generic_string->kind == vk_string);
	struct string_value* string = (struct string_value*) generic_string;
	if(string->is_null_terminated) string_value_remove_null(string);
	verpv(string->chars.n);
	ret = (struct value*) new_number_value_as_integer(string->chars.n);
	verpv(ret);
#endif
	TODO;
	EXIT;
}
