
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <debug.h>

#include <array/struct.h>
#include <array/clone.h>
#include <array/push_n.h>
#include <array/delete.h>
#include <array/clear.h>
#include <array/new.h>

#include <value/list/new.h>
#include <value/list/struct.h>
#include <value/string/new.h>
#include <value/string/struct.h>
#include <value/struct.h>

#include <scope/builtins/callback_header.h>

#include "split.h"

DECLARE_BUILTIN_FUNC(builtin_string_split)
{
	int error = 0;
	ENTER;
	if(1 <= n && n <= 2)
	{
		struct value* generic_0, *generic_1;
		
		generic_0 = args[0], generic_1 = args[1];
		if(generic_0->kind == vk_string && generic_1->kind == vk_string)
		{
			struct array values;
			struct string_value* string, *delimiter;
			
			string = (struct string_value*) generic_0;
			delimiter = (struct string_value*) generic_1;
			
			values = new_array(struct string_value*);
			
			// move through string's characters, looking for delimiter:
			{
				struct array wchars;
				wchar_t* delimit_str;
				size_t delimit_n;
				struct string_value* v;
				const wchar_t *begin, *i, *end;
				
				wchars = new_array(wchar_t);
				delimit_n = delimiter->wchars.n;
				delimit_str = delimiter->wchars.datawc;
				for(begin = string->wchars.datawc,
					i = begin,
					end = begin + string->wchars.n;
					!error && i < end;)
				{
					verpv(i);
					if((i + delimit_n < end) &&
						(!memcmp(i, delimit_str, delimit_n)))
					{
						// found delimiter
						v = new_string_value(array_clone(&wchars));
						
						array_push_n(&values, &v);
						array_clear(&wchars);
						
						i += delimit_n;
					}
					else
					{
						// push char
						array_push_n(&wchars, i++);
					}
				}
				
				// push final 'chars' value
				if(!error)
				{
					struct string_value* v;
					
					v = new_string_value(array_clone(&wchars));
					
					array_push_n(&values, &v);
				}
				
				delete_array(&wchars);
			}
			
			if(!error)
			{
				*result = (struct value*) new_list_value(values, NULL);
			}
		}
		else
		{
			TODO; // print error message
			error = 1;
		}
	}
	else
	{
		TODO; // print error message
		error = 1;
	}
	EXIT;
	return error;
}

































