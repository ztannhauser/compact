
#include <value/string/struct.h>

int process_strverscasecmp(
	struct string_value* left,
	struct string_value* right);

#include <scope/builtins/callback_header.h>

DECLARE_BUILTIN_FUNC(builtin_string_strverscasecmp);
