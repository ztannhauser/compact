
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <array/struct.h>
#include <debug.h>

#include <value/number/new.h>
#include <value/string/struct.h>
#include <value/struct.h>

#include <scope/builtins/callback_header.h>

#include "strcmp.h"

int process_strcmp(struct string_value *a, struct string_value *b) {
	int ret;
	ENTER;
	size_t min_n = a->wchars.n < b->wchars.n ? a->wchars.n : b->wchars.n;
	verpv(min_n);
	ret = memcmp(a->wchars.data, b->wchars.data, sizeof(wchar_t) * min_n)
			  ?: a->wchars.n - b->wchars.n;
	verpv(ret);
	EXIT;
	return ret;
}

DECLARE_BUILTIN_FUNC(builtin_string_strcmp) {
	ENTER;
#if 0
	assert(n == 2);
	struct value* a = params[0];
	struct value* b = params[1];
	assert(a->kind == vk_string);
	assert(b->kind == vk_string);
	ret = new_number_value_as_integer(process_strcmp(a, b));
#endif
	TODO;
	EXIT;
}
