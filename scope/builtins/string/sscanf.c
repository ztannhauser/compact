
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include <array/struct.h>
#include <debug.h>

#include <value/list/new.h>
#include <value/number/new.h>
#include <value/string/new.h>
#include <value/string/struct.h>
#include <value/struct.h>

#include <scope/builtins/callback_header.h>

#include "sscanf.h"

DECLARE_BUILTIN_FUNC(builtin_string_sscanf) {
	ENTER;
#if 0
	assert(n == 2);
	struct value* gen_input = params[0], *gen_format_string = params[1];
	assert(gen_input->kind == vk_string);
	assert(gen_format_string->kind == vk_string);
	struct string_value* input = gen_input;
	struct string_value* format_string = gen_format_string;
	struct array chars = new_array(char);
	struct array values = new_array(struct value*);
	for(char
		*in_i = input->chars.data,
		*in_e = in_i + input->chars.n,
		*fs_i = format_string->chars.data,
		*fs_e = fs_i + format_string->chars.n;
		fs_i < fs_e;)
	{
		char inc = *in_i, fsc = *fs_i;
		verpvc(inc), verpvc(fsc);
		if(inc == fsc)
		{
			in_i++, fs_i++;
		}
		else
		{
			assert(fsc == '%'), fs_i++;
			switch(*fs_i++)
			{
				case 's':
				{
					char termin = *fs_i++;
					verpvc(termin);
					assert(termin != '%');
					for(;*in_i != termin;in_i++)
					{
						verpvc(*in_i);
						assert(in_i < in_e);
						array_push_n(&chars, in_i);
					}
					in_i++;
					verpvsn(chars.data, chars.n);
					struct string_value* sv =
						new_string_value(array_clone(&chars), false);
					array_push_n(&values, &sv);
					array_clear(&chars);
					break;
				}
				case 'i':
				{
					for(;is_num(*in_i);in_i++)
					{
						verpvc(*in_i);
						assert(in_i < in_e);
						array_push_n(&chars, in_i);
					}
					fs_i++, in_i++;
					verpvsn(chars.data, chars.n);
					array_push_n(&(chars), &nullchar);
					char* m;
					signed long val = strtol(chars.data, &m, 0);
					assert(!*m);
					verpv(val);
					struct number_value* nv =
						new_number_value_as_integer(val);
					array_push_n(&values, &nv);
					array_clear(&chars);
					break;
				}
				case 'l':
				{
					for(;is_num(*in_i);in_i++)
					{
						verpvc(*in_i);
						assert(in_i < in_e);
						array_push_n(&chars, in_i);
					}
					fs_i++, in_i++;
					verpvsn(chars.data, chars.n);
#if 0
					array_push_n(&(spef->chars), &nullchar);
					verpvs(spef->chars.data);
					mp_ptr mp = malloc(sizeof(__mpz_struct));
					assert(!mpz_init_set_str(mp, spef->chars.data, 0));
					array_pop_n(&(spef->chars));
					ret = new_number_value_as_long(mp);
			
					array_push_n(&values,
						new_string_value(array_clone(&chars)));
#endif
					TODO;
					array_clear(&chars);
					break;
				}
				default: TODO;
			}
		}
	}
	delete_array(&chars);
	ret = new_list_value(values, NULL);
#endif
	TODO;
	EXIT;
}
