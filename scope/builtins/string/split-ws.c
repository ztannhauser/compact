
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <array/struct.h>
#include <debug.h>

#include <value/list/new.h>
#include <value/list/struct.h>
#include <value/string/new.h>
#include <value/string/struct.h>
#include <value/struct.h>

#include <scope/builtins/callback_header.h>

#include "split-ws.h"

DECLARE_BUILTIN_FUNC(builtin_string_split_ws) {
#if 0
	struct value* ret;
	ENTER;
	assert(n == 1);
	struct value* generic_input = params[0];
	assert(generic_input->kind == vk_string);
	struct string_value* input = generic_input;
	if(input->is_null_terminated) string_value_remove_null(input);
	
	struct array values = new_array(struct string_value*);
	{
		struct array chars = new_array(char);
		for(
			const char
				*begin = input->chars.data,
				*i = begin,
				*end = begin + input->chars.n;
			i < end;i++)
		{
			if(is_wspace(*i))
			{
				// found delimiter
				verpvsn(chars.data, chars.n);
				if(chars.n)
				{
					struct string_value* v = new_string_value(
						array_clone(&chars), false);
					array_push_n(&values, &v);
					array_clear(&chars);
				}
			}
			else
			{
				// push char
				array_push_n(&chars, i);
			}
		}
		// push final 'chars' value
		if(chars.n)
		{
			struct string_value* v = new_string_value(array_clone(&chars), false);
			array_push_n(&values, &v);
		}
		delete_array(&chars);
	}

	ret = new_list_value(values, NULL);
	EXIT;
	return ret;
#endif
	TODO;
}
