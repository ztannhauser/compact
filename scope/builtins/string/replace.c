
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <debug.h>

#include <array/mass_push_n.h>
#include <array/new.h>
#include <array/struct.h>

#include <value/string/new.h>
#include <value/string/struct.h>
#include <value/struct.h>

#include <scope/builtins/callback_header.h>

#include "replace.h"

DECLARE_BUILTIN_FUNC(builtin_string_replace)
{
	int error = 0;
	ENTER;
	if(n == 3)
	{
		struct value* generic_source, *generic_find, *generic_replace;
		
		generic_source = args[0];
		generic_find = args[1];
		generic_replace = args[2];

		if(true
			&& generic_source->kind == vk_string
			&& generic_find->kind == vk_string
			&& generic_replace->kind == vk_string)
		{
			struct string_value* source, *find, *replace;
			struct array chars;
			void* here, *there, *end, *find_chars, *replace_chars;
			size_t find_n, replace_n;
			
			source = (struct string_value*) generic_source;
			find = (struct string_value*) generic_find;
			replace = (struct string_value*) generic_replace;

			chars = new_array(wchar_t);
			
			HERE;
			verpv(source->wchars.n);
			HERE;
			
			for(
				find_chars = find->wchars.datawc,
				find_n = sizeof(wchar_t) * find->wchars.n,
				replace_chars = replace->wchars.datawc,
				replace_n = replace->wchars.n,
				there = source->wchars.datawc,
				here = there,
				end = here + sizeof(wchar_t) * source->wchars.n;
				here = memmem(here, end - here, find_chars, find_n);
				here += find_n, there = here)
			{
				verpv(here); verpv(there); verpv(end);
				verpv(here - there);
				verpv((here - there) / sizeof(wchar_t));
				array_mass_push_n(&chars, there, (here - there) / sizeof(wchar_t));
				verpv(replace_n);
				array_mass_push_n(&chars, replace_chars, replace_n);
			}
			
			verpv(here); verpv(there);
			
			array_mass_push_n(&chars, there, (end - there) / sizeof(wchar_t));
			
			*result = (struct value*) new_string_value(chars);
		}
		else
		{
			error = 1;
			TODO;
		}
	}
	else
	{
		TODO;
		error = 1;
	}
	EXIT;
	return error;
}
















