
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include <debug.h>

#include <scope/builtins/callback_header.h>

#include "startswith.h"

DECLARE_BUILTIN_FUNC(builtin_string_startswith) {
#if 0
	struct value* ret;
	ENTER;
	assert(n == 2);
	assert(0);
	EXIT;
	return ret;
#endif
	TODO;
}
