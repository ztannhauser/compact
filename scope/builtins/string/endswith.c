
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <debug.h>

#include <scope/fetch/boolean.h>
#include <value/string/struct.h>
#include <value/struct.h>

#include <scope/builtins/callback_header.h>

#include "endswith.h"

DECLARE_BUILTIN_FUNC(builtin_string_endswith) {
#if 0
	struct value* ret;
	ENTER;
	assert(n == 2);
	struct value
		*gen_string_value = params[0],
		*gen_substring_value = params[1];
	assert(gen_string_value->kind == vk_string);
	assert(gen_substring_value->kind == vk_string);
	struct string_value* string_value =
		(struct string_value*) gen_string_value;
	struct string_value* substring_value =
		(struct string_value*) gen_substring_value;
	struct array* string = &(string_value->wchars);
	struct array* substring = &(substring_value->wchars);
	ret = (struct value*) scope_fetch_boolean(
		(string->n >= substring->n) &&
		!memcmp(
			string->datawc + string->n - substring->n,
			substring->data,
			substring->n * sizeof(wchar_t)));
	verpv(ret);
	EXIT;
	return ret;
#endif
	TODO;
}
