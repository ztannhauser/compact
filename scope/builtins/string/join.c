
#include <assert.h>
#include <stdio.h>

#include <debug.h>

#include <array/mass_push_n.h>
#include <array/new.h>
#include <array/struct.h>

#include <value/list/struct.h>
#include <value/new.h>
#include <value/string/new.h>
#include <value/string/struct.h>
#include <value/struct.h>

#include <scope/builtins/callback_header.h>

#include "join.h"

DECLARE_BUILTIN_FUNC(builtin_string_join) {
	ENTER;
#if 0
	assert(n == 1 || n == 2);
	struct value* generic_list = params[0];
	assert(generic_list->kind == vk_list);
	struct list_value* list = (struct list_value*) generic_list;
	const char* delimiter = "\n";
	size_t strlen_delimiter = 1;
	if(n == 2)
	{
		struct value* generic_delimiter_value = params[1];
		assert(generic_delimiter_value->kind == vk_string);
		struct string_value* delimiter_value =
			(struct string_value*) generic_delimiter_value;
		delimiter = delimiter_value->chars.datac;
		strlen_delimiter =
			delimiter_value->chars.n - delimiter_value->is_null_terminated;
	}
	struct array chars = new_array(char);
	for(size_t i = 0, n = list->values.n;i < n;i++)
	{
		struct value* ele = array_index(list->values, i, struct value*);
		assert(ele->kind == vk_string);
		struct string_value* spef_ele = (struct string_value*) ele;
		array_mass_push_n(&chars,
			spef_ele->chars.data,
			spef_ele->chars.n - spef_ele->is_null_terminated);
		if(i + 1 < n)
		{
			array_mass_push_n(&chars, delimiter, strlen_delimiter);
		}
	}
	// verpvsn(chars.data, chars.n);
	ret = (struct value*) new_string_value(chars, false);
#endif
	TODO;
	EXIT;
}
