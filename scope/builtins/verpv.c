
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <debug.h>

#include <write-to-stdout.h>

#include <scope/fetch/null.h>

#include <value/print.h>
#include <value/struct.h>

#include <scope/builtins/callback_header.h>

#include "verpv.h"

DECLARE_BUILTIN_FUNC(builtin_verpv) {
#if 0
	struct value* ret;
	ENTER;
	assert(n == 1);
	
	struct value* param = params[0];
	param->refcount++;
	
	value_print(param, write_to_stdout);
	assert(write(1, &newline, 1) == 1);
	
	ret = param;
	EXIT;
	return ret;
#endif
	TODO;
}
