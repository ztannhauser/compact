
#include <assert.h>
#include <stdio.h>
#include <unistd.h>

#include <array/struct.h>
#include <debug.h>

#include <scope/fetch/null.h>
#include <value/fd/struct.h>
#include <value/list/struct.h>
#include <value/pipe/struct.h>
#include <value/string/struct.h>
#include <value/struct.h>

#include <scope/builtins/callback_header.h>

#include "writelines.h"

DECLARE_BUILTIN_FUNC(builtin_io_writelines) {
	ENTER;
#if 0
	assert(n == 2);
	int fd;
	struct value* generic = params[0];
	switch(generic->kind)
	{
		case vk_fd:
		{
			struct fd_value* spef = generic;
			fd = spef->fd;
			break;
		}
		case vk_pipe:
		{
			struct pipe_value* spef = generic;
			fd = spef->fds[1];
			break;
		}
		default: TODO;
	}
	struct value* generic_list = params[1];
	assert(generic_list->kind == vk_list);
	struct list_value* list = generic_list;
	verpv(list->values.n);
	verpv(fd);
	for(size_t i = 0, n = list->values.n;i < n;i++)
	{
		struct value* generic_ele =
			array_index(list->values, i, struct value*);
		assert(generic_ele->kind == vk_string);
		struct string_value* ele = generic_ele;
		if(ele->is_null_terminated)
			string_value_remove_null(ele);
		if(write(fd, ele->chars.data, ele->chars.n) < ele->chars.n)
			perror("write"), abort();
		if(write(fd, &newline, 1) < 1)
			perror("write"), abort();
	}
	ret = scope_fetch_null();
#endif
	TODO;
	EXIT;
}
