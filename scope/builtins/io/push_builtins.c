
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include <debug.h>

#include <scope/push_builtin_func.h>

#include "file_exts/push_builtins.h"
#include "path/push_builtins.h"
#include "stat/push_builtins.h"

#include "close.h"
#include "foreachline.h"
#include "open.h"
#include "pread.h"
#include "printf.h"
#include "puts.h"
#include "pwrite.h"
#include "read-all.h"
#include "readlines.h"
#include "readln.h"
#include "write.h"
#include "writelines.h"
#include "writeln.h"

#include "push_builtins.h"

int push_io_builtins(struct scope *scope) {
	int error = 0;
	ENTER;

	error = 0
		?: push_io_file_exts_builtins(scope)
		?: push_io_path_builtins(scope)
		?: push_io_stat_builtins(scope)
		?: push_io_open_accessories(scope)
		
		?: scope_push_builtin_func(scope, L"open", 
			builtin_io_open)
		?: scope_push_builtin_func(scope, L"close", 
			builtin_io_close)
		?: scope_push_builtin_func(scope, L"foreachline", 
			builtin_io_foreachline)
		?: scope_push_builtin_func(scope, L"pread", 
			builtin_io_pread)
		?: scope_push_builtin_func(scope, L"printf", 
			builtin_io_printf)
		?: scope_push_builtin_func(scope, L"puts", 
			builtin_io_puts)
		?: scope_push_builtin_func(scope, L"pwrite", 
			builtin_io_pwrite)
		?: scope_push_builtin_func(scope, L"read-all", 
			builtin_io_read_all)
		?: scope_push_builtin_func(scope, L"readlines", 
			builtin_io_readlines)
		?: scope_push_builtin_func(scope, L"readln", 
			builtin_io_readln)
		?: scope_push_builtin_func(scope, L"write", 
			builtin_io_write)
		?: scope_push_builtin_func(scope, L"writelines", 
			builtin_io_writelines)
		?: scope_push_builtin_func(scope, L"writeln", 
			builtin_io_writeln)
		;

	EXIT;
	return error;
}





