
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include <debug.h>

#include <scope/builtins/callback_header.h>

#include <scope/push_builtin_func.h>

#include "fromhtml.h"

#include "push_builtins.h"

int push_io_file_exts_html_builtins(struct scope *scope)
{
	int error;
	ENTER;

	verpv(scope);
	
	error = 0
		?: scope_push_builtin_func(scope, L"fromhtml",
			builtin_file_exts_html_fromhtml)
		;
	
	EXIT;
	return error;
}
