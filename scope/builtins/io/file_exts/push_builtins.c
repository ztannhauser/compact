
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include <debug.h>

#include "csv/push_builtins.h"
#include "html/push_builtins.h"
#include "json/push_builtins.h"

#include "push_builtins.h"

int push_io_file_exts_builtins(struct scope *scope) {
	int error;
	ENTER;

	error = 0
		?: push_io_file_exts_csv_builtins(scope)
		?: push_io_file_exts_html_builtins(scope)
		?: push_io_file_exts_json_builtins(scope)
		;

	EXIT;
	return error;
}
