
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include <debug.h>

#include <scope/push_builtin_func.h>

#include "fromjson.h"
#include "tojson.h"

#include "push_builtins.h"

int push_io_file_exts_json_builtins(struct scope *scope) {
	int error;
	ENTER;

	error = 0
		?: scope_push_builtin_func(scope, L"fromjson",
			builtin_io_json_fromjson)
		?: scope_push_builtin_func(scope, L"tojson",
			builtin_io_json_tojson)
		;

	EXIT;
	return error;
}
