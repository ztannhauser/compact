
#include <assert.h>
#include <stdbool.h>
#include <gmp.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <quadmath.h>

#include <debug.h>

#include <utf-8/decode.h>
#include <utf-8/how_many.h>

#include <misc/read_wchar.h>

#include <char/is_wspace.h>
#include <char/from_hex.h>
#include <char/is_num.h>

#include <array/new.h>
#include <array/push_n.h>
#include <array/mass_push_n.h>
#include <array/delete.h>

#include <avl/init_tree.h>
#include <avl/insert.h>

#include <scope/builtins/callback_header.h>

#include <value/struct.h>
#include <value/fd/struct.h>
#include <value/number/new.h>
#include <value/string/struct.h>
#include <value/string/new.h>
#include <value/string/construct.h>
#include <value/list/new.h>
#include <value/object/struct.h>
#include <value/object/new.h>
#include <value/object/element_compare.h>
#include <value/object/element_free.h>

#include <scope/fetch/null.h>
#include <scope/fetch/boolean.h>

#include "fromjson.h"

enum token_id
{
	
	// characters
	t_open_square_char,
	t_close_square_char,
	t_open_curly_char,
	t_close_curly_char,
	t_comma_char,
	t_colon_char,
	
	// tokens
	t_string,
	t_decimal,
	t_long,
	t_true,
	t_false,
	t_null,
	t_ident,
	t_eof,
	
	t_n,
	
};

union token_data
{
	
	mpz_ptr mpz_long;
	__float128 decimal;
	struct array string; // elements of type 'wchar_t'
	wchar_t ident[256];
	
};

struct read_char_for_string_params
{
	struct string_value* string;
	size_t progress;
};

static int read_char_for_string(void* ptr, wchar_t* current_char)
{
	int error = 0;
	struct read_char_for_string_params* params;
	ENTER;
	
	params = (struct read_char_for_string_params*) ptr;
	
	verpv(params->progress);
	
	if(params->string->wchars.n == params->progress)
	{
		// end of string
		*current_char = '\0';
	}
	else
	{
		// read another character
		*current_char = array_index(
			params->string->wchars,
			params->progress++,
			wchar_t);
	}
	
	EXIT;
	return error;
}

static int read_char_for_fd(void* ptr, wchar_t* current_char)
{
	int error = 0;
	int fd;
	ENTER;
	
	fd  = *((int*) ptr);
	verpv(fd);
	
	error = read_wchar(fd, current_char);
	
	EXIT;
	return error;
}

static int read_token(
	int (*read_char)(void* ptr, wchar_t* current_char),
	void* ptr,
	wchar_t* current_char,
	enum token_id* current_token,
	union token_data* current_token_data)
{
	int error = 0;
	ENTER;
	while(!error && is_wspace(*current_char))
	{
		error = read_char(ptr, current_char);
	}
	verpv(*current_char);
	
	if(*current_char == '\0')
	{
		*current_token = t_eof;
	}
	else
	{
		switch(*current_char)
		{
			case ',':
				*current_token = t_comma_char;
				error = read_char(ptr, current_char);
				break;
			
			case ':':
				*current_token = t_colon_char;
				error = read_char(ptr, current_char);
				break;
			
			case '{':
				*current_token = t_open_curly_char;
				error = read_char(ptr, current_char);
				break;
			
			case '}':
				*current_token = t_close_curly_char;
				error = read_char(ptr, current_char);
				break;
			
			case '[':
				*current_token = t_open_square_char;
				error = read_char(ptr, current_char);
				break;
			
			case ']':
				*current_token = t_close_square_char;
				error = read_char(ptr, current_char);
				break;
			
			case '\"':
			{
				current_token_data->string = new_array(wchar_t);
				
				error = read_char(ptr, current_char);
				
				while(!error && *current_char != '\"')
				{
					verpvc(*current_char);
					
					if(*current_char == '\\')
					{
						#if 0
						read_char();
						wchar_t pushme;
						switch(*current_char)
						{
							case '\"': pushme = '\"'; break;
							case '\'': pushme = '\''; break;
							case '\\': pushme = '\\'; break;
							case 'r': pushme = '\r'; break;
							case 't': pushme = '\t'; break;
							case 'n': pushme = '\n'; break;
							case '0': pushme = '\0'; break;
							case 'x':
							{
								read_char(); char h = from_hex(*current_char);
								read_char(); char l = from_hex(*current_char);
								pushme = h << 4 | l;
								break;
							}
							case 'u':
							{
								// read XXXX
								read_char(); char hh = from_hex(*current_char);
								read_char(); char hl = from_hex(*current_char);
								read_char(); char lh = from_hex(*current_char);
								read_char(); char ll = from_hex(*current_char);
								pushme = hh;
								pushme = pushme << 4 | hl;
								pushme = pushme << 4 | lh;
								pushme = pushme << 4 | ll;
								break;
							}
							#if 0
							case 'U':
							{
								// read XXXXXXXX
								break;
							}
							#endif
							default: TODO;
						}
						array_push_n(&(token_data.string), &pushme);
						#endif
						TODO;
					}
					else
					{
						array_push_n(&(current_token_data->string), current_char);
					}
					
					error = read_char(ptr, current_char);
					
				}
				
				if(!error)
				{
					error = read_char(ptr, current_char);
				}
				
				if(!error)
				{
					*current_token = t_string;
				}
				
				break;
			}
			
			case '-': case '.':
			case '0' ... '9':
			{
				#if 0
				verpv(*current_char);
				bool number_contains_decmial_point = false;
				struct array number_buffer = new_array(char);
				// read into buffer
				{
					for(;is_num(*current_char);read_char())
					{
						if(*current_char == '.')
						{
							assert(!number_contains_decmial_point);
							number_contains_decmial_point = true;
						}
						array_push_n(&(number_buffer), current_char);
					}
					array_push_n(&(number_buffer), &nullchar);
					verpvs(number_buffer.datac);
				}
				if(number_contains_decmial_point)
				{
					char* m;
					token_data.decimal =
						strtoflt128(number_buffer.datac, &m);
					assert(!*m);
					token = t_decimal;
				}
				else
				{
					token_data.mpz_long = malloc(sizeof(MP_INT));
					int mpz_set_str_ret = 
						mpz_init_set_str(token_data.mpz_long,
							number_buffer.datac, 0);
					assert(!mpz_set_str_ret);
					token = t_long;
				}
				delete_array(&(number_buffer));
				#endif
				TODO;
				break;
			}
			
			// only accepts 'true', 'false', or 'null'
			case 'a' ... 'z':
			{
				int indent_strlen = 0;
				while('a' <= *current_char && *current_char <= 'z')
				{
					current_token_data->ident[indent_strlen++] = *current_char;
					error = read_char(ptr, current_char);
				}
				current_token_data->ident[indent_strlen] = '\0';
				if(!wcscmp(current_token_data->ident, L"true"))
				{
					*current_token = t_true;
				}
				else if(!wcscmp(current_token_data->ident, L"false"))
				{
					*current_token = t_false;
				}
				else if(!wcscmp(current_token_data->ident, L"null"))
				{
					*current_token = t_null;
				}
				else
				{
					TODO;
				}
				break;
			}
			
			default:
			{
				verpv(*current_char);
				TODO;
			}
		}
	}
	EXIT;
	return error;
}


static int read_json(
	int (*read_char)(void* ptr, wchar_t* current_char),
	void* ptr,
	wchar_t* current_char,
	enum token_id* current_token,
	union token_data* current_token_data,
	struct scope* scope,
	struct value** result)
{
	int error = 0;
	ENTER;
	
	switch(*current_token)
	{
		case t_null:
		{
			*result = (struct value*) scope_fetch_null(scope);
			
			error = read_token(
				read_char, ptr,
				current_char,
				current_token,
				current_token_data);
			
			break;
		}
		
		case t_true:
		{
			*result = (struct value*)
				scope_fetch_boolean(scope, true);
			
			error = read_token(
				read_char, ptr,
				current_char,
				current_token,
				current_token_data);
			
			break;
		}
		
		case t_false:
		{
			*result = (struct value*)
				scope_fetch_boolean(scope, false);
			
			error = read_token(
				read_char, ptr,
				current_char,
				current_token,
				current_token_data);
			
			break;
		}
		
		case t_long:
		{
			#if 0
			ret = (struct value*)
				new_number_value_as_long(token_data.mpz_long);
			read_token();
			#endif
			TODO;
			break;
		}
		
		case t_decimal:
		{
			#if 0
			ret = (struct value*)
				new_number_value_as_decimal(token_data.decimal);
			read_token();
			#endif
			TODO;
			break;
		}
		
		case t_string:
		{
			*result = (struct value*)
				new_string_value(current_token_data->string);
			
			error = read_token(
				read_char, ptr,
				current_char,
				current_token,
				current_token_data);
			
			break;
		}
		
		case t_open_curly_char:
		{
			struct avl_tree values;
			struct object_value_element* bun;
			
			avl_init_tree(&values,
				(int (*)(const void*,const void*)) element_compare,
				(void (*)(void*)) element_free);
			
			error = read_token(
				read_char, ptr,
				current_char,
				current_token,
				current_token_data);
			
			while(*current_token != t_close_curly_char)
			{
				bun = malloc(sizeof(struct object_value_element));
				
				// read string
				if(*current_token == t_string)
				{
					bun->label = new_string_value(current_token_data->string);
					
					error = read_token(
						read_char, ptr,
						current_char,
						current_token,
						current_token_data);
				}
				else
				{
					TODO; // print error message
					error = 1;
				}
				
				// assert colon:
				if(!error)
				{
					if(*current_token == t_colon_char)
					{
						error = read_token(
							read_char, ptr,
							current_char,
							current_token,
							current_token_data);
					}
					else
					{
						TODO; // print error message
						error = 1;
					}
				}
				
				// parse subexpression:
				if(!error)
				{
					error = read_json(
						read_char, ptr,
						current_char,
						current_token,
						current_token_data,
						scope,
						&(bun->value));
				}
				
				// append to subexpressions:
				if(!error)
				{
					
					avl_insert(&(values), bun);
					
					if(*current_token == t_comma_char)
					{
						error = read_token(
							read_char, ptr,
							current_char,
							current_token,
							current_token_data);
					}
				}
			}
			
			if(!error)
			{
				error = read_token(
					read_char, ptr,
					current_char,
					current_token,
					current_token_data);
			}
			
			if(!error)
			{
				*result = (struct value*)
					new_object_value(ok_generic, values);
			}
			
			break;
		}
		
		case t_open_square_char:
		{
			struct value* subexpr;
			struct array values = new_array(struct value*);
			
			error = read_token(
				read_char, ptr,
				current_char,
				current_token,
				current_token_data);
			
			while(!error && *current_token != t_close_square_char)
			{
				
				error = read_json(
					read_char, ptr,
					current_char,
					current_token,
					current_token_data,
					scope,
					&(subexpr));
				
				if(!error)
				{
					array_push_n(&values, &subexpr);
					
					if(*current_token == t_comma_char)
					{
						error = read_token(
							read_char, ptr,
							current_char,
							current_token,
							current_token_data);
					}
				}
			}
			
			if(!error)
			{
				error = read_token(
					read_char, ptr,
					current_char,
					current_token,
					current_token_data);
			}
			
			if(!error)
			{
				*result = (struct value*) new_list_value(values, NULL);
			}
			
			break;
		}
		
		default:
		{
			verpv(*current_token);
			TODO;
			break;
		}
	}
	EXIT;
	return error;
}


DECLARE_BUILTIN_FUNC(builtin_io_json_fromjson)
{
	int error = 0;
	wchar_t current_char;
	struct value* generic;
	struct fd_value* fd;
	struct string_value* string;
	struct read_char_for_string_params string_progress;
	enum token_id current_token;
	union token_data current_token_data;
	
	ENTER;
	
	if(n == 1)
	{
		generic = args[0];
		switch(generic->kind)
		{
			case vk_string:
			{
				string = (struct string_value*) generic;
				
				string_progress.string = string;
				string_progress.progress = 0;
				
				error = 0
					?: read_char_for_string(
						&(string_progress),
						&current_char)
					?: read_token(
						read_char_for_string,
						&(string_progress),
						&current_char,
						&current_token,
						&current_token_data)
					?: read_json(
						read_char_for_string,
						&(string_progress),
						&current_char,
						&current_token,
						&current_token_data,
						scope,
						result)
					;
				
				break;
			}
			
			case vk_fd:
			{
				fd = (struct fd_value*) generic;
				
				error = 0
					?: read_char_for_fd(
						&(fd->fd),
						&current_char)
					?: read_token(
						read_char_for_fd,
						&(fd->fd),
						&current_char,
						&current_token,
						&current_token_data)
					?: read_json(
						read_char_for_fd,
						&(fd->fd),
						&current_char,
						&current_token,
						&current_token_data,
						scope,
						result)
					;
				
				break;
			}
			
			case vk_pipe:
			{
				TODO;
				break;
			}
			default: TODO;
		}
	}
	else
	{
		TODO; // print error message
		error = 1;
	}
	
	EXIT;
	
	return error;
}





























