

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include <debug.h>

#include <scope/push_builtin_func.h>

#include "fromcsv.h"
#include "tocsv.h"

#include "push_builtins.h"

int push_io_file_exts_csv_builtins(struct scope *scope) {
	int error;
	ENTER;

	error = 0
		?: scope_push_builtin_func(scope, L"fromcsv",builtin_io_csv_fromcsv)
		?: scope_push_builtin_func(scope, L"tocsv", builtin_io_csv_tocsv)
		;

	EXIT;
	return error;
}
