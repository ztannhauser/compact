
#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <unistd.h>

#include <debug.h>

#include <misc/read_wchar.h>

#include <scope/builtins/callback_header.h>

#include <array/clear.h>
#include <array/clone.h>
#include <array/delete.h>
#include <array/new.h>
#include <array/push_n.h>
#include <array/struct.h>

#include <value/fd/struct.h>
#include <value/list/new.h>
#include <value/string/new.h>
#include <value/struct.h>

#include "fromcsv.h"

#if 0
static int read_word(
	int (*read_char)(void* source, wchar_t* current_char),
	void* source,
	wchar_t* current_char,
	struct array* line)
{
	int error = 0;
	ENTER;

	verpv(*current_char);

	#if 0
		struct array current_word = new_array(wchar_t);
		if(csv_current_char == '\"')
		{
			assert(read_char());
			for(bool cont = true;cont;)
			{
				verpvc(csv_current_char);
				if(csv_current_char == '\"')
				{
					assert(read_char());
					if(csv_current_char == '\"')
					{
						array_push_n(&current_word, &csv_current_char);
						assert(read_char());
					}
					else
					{
						cont = false;
					}
				}
				else
				{
					array_push_n(&current_word, &csv_current_char);
					assert(read_char());
				}
			}
		}
		else
		{
			while(
				csv_current_char != ',' &&
				csv_current_char != '\n')
			{
				verpvc(csv_current_char);
				array_push_n(&current_word, &csv_current_char);
				assert(read_char());
			}
		}
		struct string_value* str = new_string_value(current_word);
		array_push_n(&current_line, &str);
		
	#endif
	TODO;
	EXIT;
	return error;
}
#endif

#if 0
for(bool cont = true;cont;)
{
	verpvc(csv_current_char);
	switch(csv_current_char)
	{
		case '\r': assert(read_char());
		case '\n':
		{
			struct list_value* l =
				new_list_value(array_clone(&current_line), NULL);
			array_push_n(&lines, &l);
			array_clear(&current_line);
			if((cont = read_char()))
			{
				read_word();
			}
			break;
		}
		case ',':
		{
			assert(read_char());
			read_word();
			break;
		}
		default: TODO;
	}
}
delete_array(&current_line);
#endif

static int read_csv(
	int (*read_char)(void* source, wchar_t* current_char),
	void* source,
	wchar_t* current_char,
	struct value** result)
{
	int error = 0;
	bool inside_quotes;
	struct array line;
	struct array lines;
	struct array field_chars;
	struct string_value* field_string;
	struct list_value* field_list;
	ENTER;
	
	
	if(*current_char)
	{
		field_chars = new_array(wchar_t);
		line = new_array(struct string_value*);
		lines = new_array(struct list_value*);
		
		inside_quotes = false;
		
		while(!error && *current_char)
		{
			if(inside_quotes)
			{
				if(*current_char == '\"')
				{
					// read next character:
					error = read_char(source, current_char);
					if(!error)
					{
						if(*current_char == '\"')
						{
							HERE;
							array_push_n(&field_chars, L"\"");
						}
						else
						{
							HERE;
							inside_quotes = false;
						}
					}
				}
				else
				{
					array_push_n(&field_chars, current_char);
					error = read_char(source, current_char);
				}
			}
			else
			{
				switch(*current_char)
				{
					case '\r':
					{
						// I guess we're reading a windows file,
						// read next letter, and move on:
						error = read_char(source, current_char);
						break;
					}
					case '\n':
					{
						field_list = new_list_value(
							array_clone(&line), NULL);
						array_push_n(&lines, &field_list);
						array_clear(&line);
						error = read_char(source, current_char);
						break;
					}
					case ',':
					{
						field_string = new_string_value(
							array_clone(&field_chars));
						array_push_n(&line, &field_string);
						array_clear(&field_chars);
						error = read_char(source, current_char);
						break;
					}
					case '"':
					{
						inside_quotes = true;
						error = read_char(source, current_char);
						break;
					}
					default:
					{
						// normal character: push to line:
						array_push_n(&field_chars, current_char);
						error = read_char(source, current_char);
						break;
					}
				}
			}
		}
		
		if(!error)
		{
			// if nonempty, push last line.
			if(line.n)
			{
				TODO;
			}
		}
		if(!error)
		{
			delete_array(&field_chars);
			delete_array(&line);
		}
	}
	else
	{
		fprintf(stderr, "tocsv(): empty file!\n");
		error = 1;
	}
	
	if(error)
	{
		// free what we can
		TODO;
	}
	else
	{
		*result = (struct value*) new_list_value(lines, NULL);
	}
	
	EXIT;
	return error;
}

#if 0
static int read_string_char(void* source, wchar_t* current_char)
{
	ENTER;
	// return (csv_current_char = read_wchar(fd));
	TODO;
	EXIT;
}
#endif

static int read_fd_char(void* source, wchar_t* current_char)
{
	int error;
	struct fd_value* fd = source;
	ENTER;
	
	verpv(fd->fd);
	error = read_wchar(fd->fd, current_char);
	
	EXIT;
	return error;
}

DECLARE_BUILTIN_FUNC(builtin_io_csv_fromcsv)
{
	int error = 0;
	struct value* input;
	wchar_t current_char;
	ENTER;
	if(n == 1)
	{
		input = args[0];
		switch(input->kind)
		{
			case vk_string: TODO; break;
			case vk_fd:
			{
				
				error = 0
					?: read_fd_char(input, &current_char)
					?: read_csv(read_fd_char, input, &current_char, result);
				
				break;
			}
			case vk_pipe: TODO; break;
			default: TODO;
		}
	}
	else
	{
		fprintf(stderr, "fromcsv(): requires a path\n");
		error = 1;
	}
	EXIT;
	return error;
}





