
#include "value/string/struct.h"

void process_format_string(
	void (*push)(const char* c, size_t len),
	struct string_value* format_string,
	size_t n, struct value** params);

#include <scope/builtins/callback_header.h>

DECLARE_BUILTIN_FUNC(builtin_io_printf);
