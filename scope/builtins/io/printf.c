
#include <assert.h>
#include <ctype.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include <array/struct.h>
#include <char/is_digit.h>
#include <debug.h>

#include <value/fd/struct.h>
#include <value/number/struct.h>
#include <value/print.h>
#include <value/string/new.h>
#include <value/string/struct.h>
#include <value/struct.h>

#include <scope/builtins/callback_header.h>
#include <scope/builtins/cast/bin.h>
#include <scope/builtins/cast/hex.h>
#include <scope/fetch/null.h>

#include "printf.h"

void process_format_string(void (*push)(const char *c, size_t len),
						   struct string_value *format_string, size_t n,
						   struct value **params) {
	ENTER;
#if 0
	if(format_string->is_null_terminated)
		string_value_remove_null(format_string);
	
	for(char
		*current = format_string->chars.datac,
		*end = current + format_string->chars.n;
		current < end;)
	{
		if(*current == '%')
		{
			current++;
			
			enum {left, center, right} field_alignment = right; // (-)
			
			bool
				show_alternate_form = false, // (#)
				pad_with_zeros = false, // (0)
				put_blank_for_positive_number = false, // ( )
				always_show_sign_flag = false, // (+)
				thousands_comma_delimit = false; // (')
			
			int field_width; bool is_field_width_set = false;
			int digits_of_precision; bool is_digits_of_precision_set = false;
			
			HERE;
			
			for(bool cont = true;cont;)
			{
				switch(*current)
				{
					case '#': current++, show_alternate_form = true; break;
					case '0': current++, pad_with_zeros = true; break;
					case 'c': current++, field_alignment = center; break;
					case '-': current++, field_alignment = left; break;
					case ' ': current++, put_blank_for_positive_number = true; break;
					case '+': current++, always_show_sign_flag = true; break;
					case '\'': current++, thousands_comma_delimit = true; break;
					default: cont = false; break;
				}
			}
			
			if(show_alternate_form)
			{
				HERE;
				printbool(show_alternate_form);
				printbool(pad_with_zeros);
				printbool(put_blank_for_positive_number);
				printbool(always_show_sign_flag);
				printbool(thousands_comma_delimit);
				printbool(is_field_width_set);
				printbool(is_digits_of_precision_set);
				HERE;
			}
			
			if(is_digit(*current))
			{
				field_width = 0, is_field_width_set = true;
				do
				{
					field_width = field_width * 10 + (*current - '0');
					current++;
				}	
				while(is_digit(*current));
			}
			
			if(*current == '.')
			{
				current++;
				digits_of_precision = 0, is_digits_of_precision_set = true;
				while(is_digit(*current))
				{
					digits_of_precision =
						digits_of_precision * 10 + (*current++ - '0');
				}	
				verpv(digits_of_precision);
			}
			
			HERE;
			
			switch(*current++)
			{
				case 'i': // integer
				{
					TODO;
					break;
				}
				case 'f': // float
				{
					TODO;
					break;
				}
				case 'b': // binary
				{
					TODO;
					break;
				}
				case 'o': // octal
				{
					TODO;
					break;
				}
				case 'x':
				case 'X': // hexadecimal
				{
					TODO;
					break;
				}
				case 's': // string
				{
					assert(1 <= n--);
					struct value* param = *params++;
					assert(param->kind == vk_string);
					struct string_value* spef = (struct string_value*) param;
					if(spef->is_null_terminated)
						string_value_remove_null(spef);
					
					if(is_field_width_set)
					{
						verpv(field_width);
						if(field_width > spef->chars.n)
						{
							size_t d = field_width - spef->chars.n;
							verpv(d);
							verpv(field_alignment);
							// left-aligned, right-aligned?
							switch(field_alignment)
							{
								case left:
								{
									while(d--) push(&space, 1);
									push(spef->chars.datac, spef->chars.n);
									break;
								}
								case center:
								{
									size_t dl = d / 2, dr = d - dl;
									while(dl--) push(&space, 1);
									push(spef->chars.datac, spef->chars.n);
									while(dr--) push(&space, 1);
									break;
								}
								case right:
								{
									push(spef->chars.datac, spef->chars.n);
									while(d--) push(&space, 1);
									break;
								}
								default: TODO;
							}
						}
						else
						{
							HERE;
							// truncate string
							push(spef->chars.datac, field_width);
						}
					}
					else
					{
						// push whole string
						push(spef->chars.datac, spef->chars.n);
					}
					
					break;
				}
				default: assert(!"Unknown format string");
			}
		}
		else
		{
			push(current++, 1);
		}
	}
#endif
	TODO;
	EXIT;
}

DECLARE_BUILTIN_FUNC(builtin_io_printf) {
#if 0
	struct value* ret;
	ENTER;
	assert(n >= 1);
	struct value* first = params[0];
	int fd;
	void my_push(const char* c, size_t len)
	{
		assert(write(fd, c, len) == len);
	}
	switch(first->kind)
	{
		case vk_fd:
		{
			fd = ((struct fd_value*) first)->fd;
			n--, params++;
			first = params[0];
		}
		case vk_string:
		{
			// output fd wasn't given,
			// first param was format string
			fd = 1;
			break;
		}
		default: TODO;
	}
	assert(first->kind == vk_string);
	n--, params++, process_format_string(
		my_push, (struct string_value*) first, n, params);
	ret = (struct value*) scope_fetch_null();
	EXIT;
	return ret;
#endif
	TODO;
}
