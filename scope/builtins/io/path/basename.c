
#include <assert.h>
#include <string.h>
#include <stdio.h>
#include <strings.h>

#include <debug.h>

#include <array/struct.h>
#include <array/mass_push_n.h>
#include <array/new.h>

#include <scope/builtins/callback_header.h>

#include <value/string/struct.h>
#include <value/string/new.h>

#include "basename.h"

DECLARE_BUILTIN_FUNC(builtin_io_path_basename)
{
	int error = 0;
	if(n == 1)
	{
		struct value* generic;
		
		generic = args[0];
		
		if(generic->kind == vk_string)
		{
			struct string_value* path;
			
			path = (struct string_value*) generic;
			if(path->wchars.n)
			{
				wchar_t* last_slash_pos, *start, *end;
				
				for(
					last_slash_pos = NULL,
					start = path->wchars.datawc,
					end = start + path->wchars.n - 1;
					!last_slash_pos && start < end;end--)
					if(*end == L'/')
						last_slash_pos = end;
				
				if(last_slash_pos++)
				{
					struct array wchars;
					
					wchars = new_array(wchar_t);
					
					array_mass_push_n(&wchars, last_slash_pos,
						+ 0
						+ path->wchars.datawc
						+ path->wchars.n
						- last_slash_pos);
					*result = (struct value*) new_string_value(wchars);
				}
				else
					generic->refcount++,
					*result = generic;
			}
			else
				fprintf(stderr, "basename: requires *nonempty string* path"),
				error = 1;
		}
		else
			fprintf(stderr, "basename: requires *string* path"),
			error = 1;
	}
	else
		fprintf(stderr, "basename: requires path"),
		error = 1;
	return error;
}

















