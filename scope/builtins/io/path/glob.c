
#include <stdbool.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdio.h>
#include <glob.h>
#include <string.h>
#include <assert.h>

#include <debug.h>

#include <array/struct.h>
#include <array/new.h>
#include <array/push_n.h>
#include <array/delete.h>

#include <scope/builtins/callback_header.h>

#include <value/struct.h>
#include <value/string/struct.h>
#include <value/string/to_utf8_encoded.h>
#include <value/string/from_utf8_encoded.h>
#include <value/list/struct.h>
#include <value/list/new.h>

#include <scope/fetch/compare.h>

#include "glob.h"

DECLARE_BUILTIN_FUNC(builtin_io_path_glob)
{
	int error = 0;
	struct value* generic_pattern;
	struct string_value* pattern;
	struct string_value* string_path;
	struct array converted_pattern;
	struct array values;
	glob_t pglob;
	int glob_ret;
	char* path;
	ENTER;
	if(n == 1)
	{
		generic_pattern = args[0];
		if(generic_pattern->kind == vk_string)
		{
			pattern = (struct string_value*) generic_pattern;
			converted_pattern = string_value_to_utf8_encoded(pattern, true);
			values = new_array(struct value*);
			glob_ret = glob(converted_pattern.datac,
				GLOB_BRACE | GLOB_TILDE | GLOB_TILDE_CHECK, NULL, &pglob);
			
			switch(glob_ret)
			{
				case 0:
				{
					for(int i = 0, n = pglob.gl_pathc;!error && i < n;i++)
					{
						path = pglob.gl_pathv[i];
						verpvs(path);
						
						error = string_value_from_utf8_encoded(
							path, strlen(path), &string_path);
						
						if(!error)
						{
							array_push_n(&values, &string_path);
						}
					}
					break;
				}
				case GLOB_NOMATCH: break;
				case GLOB_NOSPACE:
				case GLOB_ABORTED:
				default:
				{
					TODO; /// print error message, error = 1;
				}
			}
			
			globfree(&pglob);
			delete_array(&converted_pattern);
			
			if(!error)
			{
				*result = (struct value*) new_list_value(values,
					scope_fetch_compare(scope));
			}
		}
		else
		{
			TODO;
		}
	}
	else
	{
		TODO;
	}
	EXIT;
	return error;
}



























