
#include <assert.h>
#include <string.h>

#include <debug.h>

#include <array/struct.h>

#include <scope/builtins/callback_header.h>

#include <value/string/struct.h>
#include <value/string/new.h>

#include "dirname.h"

DECLARE_BUILTIN_FUNC(builtin_io_path_dirname)
{
	#if 0
	struct value* gen_path = params[0];
	assert(gen_path->kind == vk_string);
	struct string_value* path = gen_path;
	assert(path->chars.n);
	if(!path->is_null_terminated)
		string_value_append_null(path);
	unsigned char* b = rindex(path->chars.data, '/');
	if(b)
	{
		#if 0
		b++;
		struct array chars = new_array(char);
		array_mass_push_n(&chars, b,
			+ 0
			+ path->chars.data
			+ path->chars.n
			- b
			- path->is_null_terminated);
		verpvsn(chars.data, chars.n);
		ret = new_string_value(chars, false);
		#endif
		TODO;
	}
	else
	{
		#if 0
		ret = path;
		ret->refcount++;
		#endif
		TODO;
	}
	#endif
	TODO;
}

















