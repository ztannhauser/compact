
#include <assert.h>
#include <dirent.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>

#include <debug.h>

#include <array/struct.h>

#include <scope/builtins/callback_header.h>

#include <value/list/new.h>
#include <value/list/struct.h>
#include <value/string/new.h>
#include <value/string/struct.h>
#include <value/struct.h>

#include "readdir.h"

DECLARE_BUILTIN_FUNC(builtin_io_path_readdir) {
	ENTER;
#if 0
	struct array filenames = new_array(struct value*);
	struct value* generic_path = params[0];
	assert(generic_path->kind == vk_string);
	struct string_value* path = generic_path;
	if(!path->is_null_terminated)
		string_value_append_null(path);
		
	DIR* dir = opendir(path->chars.data);
	assert(dir);
	
	for(struct dirent* dirent; dirent = readdir(dir);)
	{
		verpvs(dirent->d_name);
		if(strcmp(dirent->d_name, "."))
		if(strcmp(dirent->d_name, ".."))
		{
			struct array chars = new_array(char);
			array_mass_push_n(&chars,
				dirent->d_name,
				strlen(dirent->d_name) + 1);
			struct string_value* filename = new_string_value(chars, true);
			array_push_n(&filenames, &filename);
		}
	}
	closedir(dir);
	
	ret = new_list_value(filenames, NULL);
#endif
	TODO;
	EXIT;
}
