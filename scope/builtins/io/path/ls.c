
#include <assert.h>
#include <sys/types.h>
#include <dirent.h>
#include <string.h>
#include <stdio.h>

#include <debug.h>

#include <array/struct.h>
#include <array/new.h>
#include <array/push_n.h>
#include <array/mass_push_n.h>
#include <array/clear.h>
#include <array/delete.h>

#include <scope/builtins/callback_header.h>

#include <value/struct.h>
#include <value/string/to_utf8_encoded.h>
#include <value/string/from_utf8_encoded.h>
#include <value/string/struct.h>
#include <value/string/new.h>
#include <value/list/struct.h>
#include <value/list/new.h>

#include "ls.h"

// 'path' needs to be null-terminated

static int ls(
	struct array* filenames,
	const char* path,
	size_t strlen_path)
{
	int error = 0;
	DIR* dir;
	struct array chars;
	struct string_value* filename;
	ENTER;
	
	verpvs(path);
	dir = opendir(path);
	
	if(dir)
	{
		chars = new_array(char);
		for(struct dirent* dirent; !error && (dirent = readdir(dir));)
		{
			verpvs(dirent->d_name);
			if(dirent->d_name[0] != '.')
			{
				array_mass_push_n(&chars, path, strlen_path);
				array_mass_push_n(&chars,
					dirent->d_name, strlen(dirent->d_name) + 1);
				
				error = string_value_from_utf8_encoded(
					chars.datac, chars.n, &filename);
				
				if(!error)
				{
					array_push_n(filenames, &filename);
				}
				
				array_clear(&chars);
			}
		}
		
		delete_array(&chars);
		closedir(dir);
	}
	else
	{
		fprintf(stderr, "failed to open directory\n");
		error = 1;
	}
	EXIT;
	return error;
}

DECLARE_BUILTIN_FUNC(builtin_io_path_ls)
{
	int error = 0;
	struct array filenames;
	struct value* path_generic;
	struct string_value* path_string;
	struct array converted;
	ENTER;
	
	if(n <= 1)
	{
		filenames = new_array(struct value*);
		if(n == 1)
		{
			path_generic = args[0];
			if(path_generic->kind == vk_string)
			{
				path_string = (struct string_value*) path_generic;
				converted = string_value_to_utf8_encoded(path_string, false);
				array_push_n(&converted, "/");
				array_push_n(&converted, "\0");
				
				error = ls(&filenames, converted.datac, converted.n - 1);
				
				delete_array(&converted);
			}
			else
			{
				TODO; // error message, error = 1
			}
		}
		else
		{
			error = ls(&filenames, "./", 2);
		}
		
		if(!error)
		{
			*result = (struct value*) new_list_value(filenames, NULL);
		}
	}
	else
	{
		fprintf(stderr, "ls(): zero or one paths expected\n");
		error = 1;
	}
	EXIT;
	return error;
}






















