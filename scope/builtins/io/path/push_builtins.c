
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include <debug.h>

#include <scope/push_builtin_func.h>

#include "basename.h"
#include "dirname.h"
#include "glob.h"
#include "ls.h"
#include "readdir.h"

#include "push_builtins.h"

int push_io_path_builtins(struct scope *scope) {
	int error = 0;
	ENTER;

	error =
		0
		?: scope_push_builtin_func(scope, L"basename", 
			builtin_io_path_basename)
		?: scope_push_builtin_func(scope, L"dirname", 
			builtin_io_path_dirname)
		?: scope_push_builtin_func(scope, L"readdir", 
			builtin_io_path_readdir)
		?: scope_push_builtin_func(scope, L"glob", 
			builtin_io_path_glob)
		?: scope_push_builtin_func(scope, L"ls", 
			builtin_io_path_ls)
		;

	EXIT;
	return error;
}













