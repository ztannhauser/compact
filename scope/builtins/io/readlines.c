
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <debug.h>

#include <misc/read_wchar.h>

#include <utf-8/decode.h>
#include <utf-8/how_many.h>

#include <array/clear.h>
#include <array/clone.h>
#include <array/delete.h>
#include <array/new.h>
#include <array/push_n.h>
#include <array/struct.h>

#include <value/fd/struct.h>
#include <value/list/new.h>
#include <value/list/struct.h>
#include <value/pipe/struct.h>
#include <value/string/new.h>
#include <value/string/struct.h>
#include <value/struct.h>

#include "scope/builtins/callback_header.h"

#include "readlines.h"

DECLARE_BUILTIN_FUNC(builtin_io_readlines)
{
	int error = 0;
	int fd;
	struct value* arg;
	struct fd_value* fd_value;
	struct pipe_value* p_value;
	struct array line;
	struct array lines;
	wchar_t current_char;
	struct string_value* strval;
	ENTER;
	if(n == 1)
	{
		arg = args[0];
		switch(arg->kind)
		{
			case vk_fd:
			{
				fd_value = (struct fd_value*) arg;
				fd = fd_value->fd;
				break;
			}
			case vk_pipe:
			{
				p_value = (struct pipe_value*) arg;
				fd = p_value->fds[0];
				break;
			}
			default: TODO;
		}
		verpv(fd);
		line = new_array(wchar_t);
		lines = new_array(struct value*);
		for(;!(error = read_wchar(fd, &current_char)) && (current_char);)
		{
			if(current_char == '\n')
			{
				strval = new_string_value(array_clone(&line));
				array_push_n(&lines, &strval);
				array_clear(&line);
			}
			else
			{
				array_push_n(&line, &current_char);
			}
		}
		if(!error)
		{
			if(line.n)
			{
				strval = new_string_value(array_clone(&line));
				array_push_n(&lines, &strval);
				array_clear(&line);
			}
			delete_array(&line);
			*result = (struct value*) new_list_value(lines, NULL);
		}
	}
	else
	{
		fprintf(stderr, "readlines(): requires fd or pipe\n");
		error = 1;
	}
	EXIT;
	return error;
}





