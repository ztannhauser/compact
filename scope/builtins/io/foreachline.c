
#include <assert.h>
#include <unistd.h>
#include <stdio.h>

#include <debug.h>

#include <misc/read_wchar.h>

#include <array/struct.h>
#include <array/new.h>
#include <array/clone.h>
#include <array/push_n.h>
#include <array/clear.h>
#include <array/delete.h>

#include <value/struct.h>
#include <value/delete.h>
#include <value/fd/struct.h>
#include <value/pipe/struct.h>
#include <value/list/struct.h>
#include <value/list/new.h>
#include <value/string/struct.h>
#include <value/string/delete.h>
#include <value/string/new.h>

#include <scope/builtins/callback_header.h>

#include <scope/fetch/null.h>

#include <call.h>

#include "foreachline.h"

DECLARE_BUILTIN_FUNC(builtin_io_foreachline)
{
	int error = 0;
	int fd;
	struct array line;
	struct value* generic;
	struct value* ret_value;
	struct string_value* strval;
	struct fd_value* fd_value;
	struct pipe_value* pipe_value;
	struct function_value* func;
	struct value* function_args[1];
	wchar_t current_char;
	
	ENTER;
	
	if(2 == n)
	{
		generic = args[0];
		switch(generic->kind)
		{
			case vk_fd:
			{
				fd_value = (struct fd_value*) generic;
				fd = fd_value->fd;
				break;
			}
			case vk_pipe:
			{
				pipe_value = (struct pipe_value*) generic;
				fd = pipe_value->fds[0];
				break;
			}
			default:
			{
				fprintf(stderr, "foreachline(): first paramter *must* be "
					"a stream!\n");
				error = 1;
			};
		}
		
		if(!error)
		{
			verpv(fd);
			generic = args[1];
			if(generic->kind == vk_function)
			{
				func = (struct function_value*) generic;
			}
			else
			{
				fprintf(stderr, "foreachline: second parameter *must* be "
					"a function!\n");
				error = 1;
			}
		}
		
		if(!error)
		{
			verpv(func);
			
			line = new_array(wchar_t);
			for(;!error &&
				!(error = read_wchar(fd, &current_char)) &&
				(current_char);)
			{
				if(current_char == '\n')
				{
					strval = new_string_value(array_clone(&line));
					function_args[0] = (struct value*) strval;
					
					error = call(func, 1, function_args, scope, &ret_value);
					
					
					
					if(!error)
					{
						error = 0
							?: delete_string_value(strval)
							?: delete_value(ret_value);
					}
					
					if(!error)
					{
						array_clear(&line);
					}
				}
				else
				{
					array_push_n(&line, &current_char);
				}
			}
			
			if(!error && line.n)
			{
				TODO; // should be call function on remainder?
			}
			
			delete_array(&line);
			
			if(!error)
			{
				*result = (struct value*) scope_fetch_null(scope);
			}
		}
	}
	else
	{
		fprintf(stderr, "foreachline: requires a input stream, and a "
			"function pointer!\n");
		error = 1;
	}
	EXIT;
	return error;
};










