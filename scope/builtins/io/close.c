

#include <string.h>
#include <assert.h>
#include <unistd.h>
#include <fcntl.h>  
#include <unistd.h>
#include <stdbool.h>

#include <debug.h>

#include <array/struct.h>

#include <scope/builtins/callback_header.h>

#include <value/struct.h>
#include <value/pipe/struct.h>
#include <value/fd/struct.h>

#include <scope/fetch/null.h>

#include "close.h"

DECLARE_BUILTIN_FUNC(builtin_io_close)
{
	#if 0
	struct value* generic_fd = params[0];
	switch(generic_fd->kind)
	{
		case vk_fd:
		{
			struct fd_value* spef = generic_fd;
			close(spef->fd);
			spef->closed = true;
			break;
		}
		case vk_pipe:
		{
			struct pipe_value* spef = generic_fd;
			close(spef->fds[1]); // close write end of pipe
			break;
		}
		default: TODO;
	}
	ret = scope_fetch_null();
	#endif
	TODO;
}



























