
#include <assert.h>
#include <stdio.h>
#include <unistd.h>

#include <debug.h>

#include <array/delete.h>

#include <value/string/struct.h>
#include <value/string/to_utf8_encoded.h>
#include <value/struct.h>

#include <scope/fetch/null.h>
#include <scope/builtins/callback_header.h>

#include "puts.h"

DECLARE_BUILTIN_FUNC(builtin_io_puts)
{
	int error = 0;
	struct string_value* string;
	struct value* generic;
	struct array converted;
	ENTER;
	if(n == 1)
	{
		generic = args[0];
		if(generic->kind == vk_string)
		{
			string = (struct string_value*) generic;
			converted = string_value_to_utf8_encoded(string, false);
			
			error = 0
				?: write(1, converted.data, converted.n) < converted.n
				?: write(1, "\n", 1) < 1;
			
			if(!error)
			{
				delete_array(&converted);
				*result = (struct value*) scope_fetch_null(scope);
			}
		}
		else
		{
			fprintf(stderr, "puts: requires one *string* paramter\n");
			error = 1;
		}
	}
	else
	{
		fprintf(stderr, "puts: requires one string paramter\n");
		error = 1;
	}
	EXIT;
	return error;
}






