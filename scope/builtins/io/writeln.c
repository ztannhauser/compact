
#include <assert.h>
#include <stdlib.h>
#include <unistd.h>

#include <debug.h>

#include <value/fd/struct.h>
#include <value/pipe/struct.h>
#include <value/string/struct.h>
#include <value/struct.h>

#include <scope/fetch/null.h>
#include <scope/builtins/callback_header.h>

#include "writeln.h"

DECLARE_BUILTIN_FUNC(builtin_io_writeln) {
#if 0
	int fd;
	struct value* generic_fd = params[0];
	switch(generic_fd->kind)
	{
		case vk_fd:
		{
			struct fd_value* f = generic_fd;
			fd = f->fd;
			break;
		}
		case vk_pipe:
		{
			struct pipe_value* p = generic_fd;
			fd = p->fds[1];
			break;
		}
		default: TODO;
	}
	struct value* generic_string = params[1];
	assert(generic_string->kind == vk_string);
	struct string_value* str = generic_string;
	if(str->is_null_terminated)
		string_value_remove_null(str);
	if(
		write(fd, str->chars.data, str->chars.n) < str->chars.n ||
		write(fd, &newline, 1) < 1
	)
	{
		CHECK;
		perror("write"), abort();
	}
	ret = scope_fetch_null();
#endif
	TODO;
}
