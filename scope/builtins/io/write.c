
#include <assert.h>
#include <stdio.h>
#include <unistd.h>

#include <debug.h>

#include <array/delete.h>

#include <value/fd/struct.h>
#include <value/pipe/struct.h>
#include <value/string/struct.h>
#include <value/string/to_utf8_encoded.h>
#include <value/struct.h>

#include <scope/fetch/null.h>
#include <scope/builtins/callback_header.h>

#include "write.h"

DECLARE_BUILTIN_FUNC(builtin_io_write)
{
	int error = 0;
	ENTER;
	if(n == 2)
	{
		int fd;
		struct array escaped;
		struct value* generic;
		
		generic = args[0];
		switch(generic->kind)
		{
			case vk_fd:
			{
				struct fd_value* fv;
				
				fv = (struct fd_value*) generic;
				fd = fv->fd;
				break;
			}
			
			case vk_pipe:
			{
				struct pipe_value* pv;
				
				pv = (struct pipe_value*) generic;
				fd = pv->fds[1];
				break;
			}
			
			default:
			{
				TODO;
				error = 1;
				break;
			}
		}
		
		if(!error)
		{
			generic = args[1];
			if(generic->kind == vk_string)
			{
				struct string_value* sv;
				
				sv = (struct string_value*) generic;
				
				escaped = string_value_to_utf8_encoded(sv, false);
				
			}
			else
			{
				TODO;
				error = 1;
			}
		}
		
		if(!error)
		{
			if(write(fd, escaped.datac, escaped.n) < escaped.n)
			{
				perror("write");
				error = 1;
			}
		}
		
		if(!error)
		{
			*result = (struct value*) scope_fetch_null(scope);
		}
		
		delete_array(&escaped);
	}
	else
	{
		TODO;
		error = 1;
	}
	EXIT;
	return error;
}

















