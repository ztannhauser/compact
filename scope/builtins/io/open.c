#include <assert.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>

#include <debug.h>

#include <array/delete.h>
#include <array/struct.h>

#include <scope/push.h>
#include <scope/push_builtin_func.h>

#include <scope/builtins/callback_header.h>

#include <value/fd/new.h>
#include <value/number/new.h>
#include <value/number/struct.h>
#include <value/string/struct.h>
#include <value/string/to_utf8_encoded.h>
#include <value/struct.h>

#include "open.h"

DECLARE_BUILTIN_FUNC(builtin_io_creat)
{
	int error = 0;
	if(1 <= n && n <= 2)
	{
		struct value* generic;
		
		generic = args[0];
		if(generic->kind == vk_string)
		{
			struct string_value* path;
			mode_t mode;
			
			path = (struct string_value*) generic;
			if(n == 2)
			{
				generic = args[1];
				if(generic->kind == vk_number)
				{
					// assert integer type
					// set mode
					TODO;
				}
				else
				{
					error = 1;
					TODO;
				}
			}
			else
			{
				mode = 0666;
			}
			
			if(!error)
			{
				int creat_ret;
				struct array escaped;
				
				escaped = string_value_to_utf8_encoded(path, true);
				
				creat_ret = creat(escaped.datac, mode);
				
				verpv(creat_ret);
				
				if(creat_ret < 0)
				{
					perror("creat"), error = 1;
				}
				else
				{
					*result = (struct value*)
						new_fd_value(creat_ret, d_writeable, 0);
				}
				
				delete_array(&escaped);
			}
		}
		else
		{
			TODO;
			error = 1;
		}
	}
	else
	{
		TODO;
		error = 1;
	}
	return error;
}

int push_io_open_accessories(struct scope *scope) {
	int error;
	ENTER;

	error = 0
		?: scope_push(scope, L"O_APPEND", (struct value *)
			new_number_value_as_integer(O_APPEND), false)
		?: scope_push(scope, L"O_CLOEXEC", (struct value *)
			new_number_value_as_integer(O_CLOEXEC), false)
		?: scope_push(scope, L"O_CREAT", (struct value *)
			new_number_value_as_integer(O_CREAT), false)
		?: scope_push(scope, L"O_EXCL", (struct value *)
			new_number_value_as_integer(O_EXCL), false)
		?: scope_push(scope, L"O_NOATIME", (struct value *)
			new_number_value_as_integer(O_NOATIME), false)
		?: scope_push(scope, L"O_NOFOLLOW", (struct value *)
			new_number_value_as_integer(O_NOFOLLOW), false)
		?: scope_push(scope, L"O_PATH", (struct value *)
			new_number_value_as_integer(O_PATH), false)
		?: scope_push(scope, L"O_TRUNC", (struct value *)
			new_number_value_as_integer(O_TRUNC), false)
		?: scope_push_builtin_func(scope, L"creat",builtin_io_creat)
		;

	EXIT;
	return error;
}

DECLARE_BUILTIN_FUNC(builtin_io_open)
{
	int error = 0;
	struct value* generic_path;
	struct string_value* path;
	struct array converted;
	struct value* generic_flag;
	struct number_value* number_flag;
	struct value* generic_mode;
	int flags;
	mode_t mode;
	int open_ret;
	ENTER;
	
	if(1 <= n && n <= 3)
	{
		generic_path = args[0];
		error = (generic_path->kind != vk_string);
		if(!error)
		{
			path = (struct string_value*) generic_path;
			converted = string_value_to_utf8_encoded(path, true);
		}
		if(!error)
		{
			if(n == 2)
			{
				generic_flag = args[1];
				error = (generic_flag->kind != vk_number);
				if(!error)
				{
					number_flag = (struct number_value*) generic_flag;
					if(number_flag->kind == nvk_integer)
					{
						flags = number_flag->integer;
					}
					else
					{
						fprintf(stderr, "open(): 'flag' parameter "
							"must be an integer!\n");
						error = 1;
					}
				}
			}
			else
			{
				flags = O_RDONLY;
			}
		}
		if(!error)
		{
			if(n == 3)
			{
				generic_mode = args[2];
				error = !(generic_mode->kind == vk_number);
				TODO;
			}
			else
			{
				mode = 0666;
			}
		}
		if(!error)
		{
			open_ret = open(converted.datac, flags, mode);
			verpv(open_ret);
			if(open_ret < 0)
			{
				perror("open");
				error = 1;
			}
			else
			{
				*result = (struct value*) new_fd_value(open_ret, d_readable, 0);
			}
			delete_array(&converted);
		}
	}
	else
	{
		fprintf(stderr, "open() requires at least path, then optional "
			"flags, then optional 'mode'!\n");
		error = 1;
	}
	EXIT;
	return error;
}





