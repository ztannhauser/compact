
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <debug.h>

#include <misc/read_wchar.h>

#include <utf-8/decode.h>
#include <utf-8/how_many.h>

#include <array/new.h>
#include <array/push_n.h>
#include <array/struct.h>

#include <value/fd/struct.h>
#include <value/pipe/struct.h>
#include <value/string/new.h>
#include <value/string/struct.h>
#include <value/struct.h>

#include <scope/builtins/callback_header.h>

#include "read-all.h"

DECLARE_BUILTIN_FUNC(builtin_io_read_all)
{
	int error = 0;
	int fd;
	struct value* generic;
	struct fd_value* spef_fd;
	struct array all;
	wchar_t current_char;
	ENTER;
	
	if(n == 1)
	{
		generic = args[0];
		
		switch(generic->kind)
		{
			case vk_fd:
			{
				spef_fd = (struct fd_value*) generic;
				
				if(spef_fd->direction == d_readable)
				{
					fd = spef_fd->fd;
				}
				else
				{
					TODO; // "given fd is not readable"
					error = 1;
				}
				break;
			}
			
			case vk_pipe:
			{
				#if 0
				struct pipe_value* spef = (struct pipe_value*) generic_fd_value;
				fd = spef->fds[0];
				#endif
				TODO;
				break;
			}
			
			default: TODO;
		}
		
		verpv(fd);
		
		all = new_array(wchar_t);
		
		while(!(error = read_wchar(fd, &current_char)) && current_char)
		{
			array_push_n(&all, &current_char);
		}
		
		if(!error)
		{
			verpv(all.data);
			verpv(all.n);
			
			*result = (struct value*) new_string_value(all);
		}
	}
	else
	{
		TODO;
	}
	EXIT;
	return error;
}




































