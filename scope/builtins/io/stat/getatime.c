
#include <assert.h>
#include <stdio.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include <debug.h>

#include <value/number/new.h>
#include <value/object/construct.h>
#include <value/object/struct.h>
#include <value/string/struct.h>
#include <value/struct.h>

#include <scope/builtins/callback_header.h>

#include "getatime.h"

DECLARE_BUILTIN_FUNC(builtin_io_stat_getatime) {
	ENTER;
#if 0
	assert(n == 1);
	struct value* generic_path = params[0];
	assert(generic_path->kind == vk_string);
	struct string_value* path = generic_path;
	if(!path->is_null_terminated)
		string_value_append_null(path);
	struct stat statbuf;
	if(stat(path->chars.data, &statbuf) < 0) perror("stat"), abort();
	ret = new_number_value_as_integer(statbuf.st_atime);
	verpv(ret);
#endif
	TODO;
	EXIT;
}
