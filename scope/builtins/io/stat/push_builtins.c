
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include <debug.h>

#include <scope/push_builtin_func.h>

#include "getatime.h"
#include "getmtime.h"
#include "getsize.h"
#include "isdir.h"
#include "stat.h"
#include "touch.h"
#include "isemptydir.h"

#include "push_builtins.h"

int push_io_stat_builtins(struct scope *scope) {
	int error = 0;
	ENTER;

	error = 0
		?: scope_push_builtin_func(scope, L"getmtime", 
			builtin_io_stat_getmtime)
		?: scope_push_builtin_func(scope, L"setmtime", 
			builtin_io_stat_touch)
		?: scope_push_builtin_func(scope, L"getsize", 
			builtin_io_stat_getsize)
		?: scope_push_builtin_func(scope, L"getatime", 
			builtin_io_stat_getatime)
		?: scope_push_builtin_func(scope, L"stat", 
			builtin_io_stat_stat)
		?: scope_push_builtin_func(scope, L"isdir", 
			builtin_io_stat_isdir)
		?: scope_push_builtin_func(scope, L"isemptydir",
			builtin_io_stat_isemptydir)
		?: scope_push_builtin_func(scope, L"touch",
			builtin_io_stat_touch)
		;

	EXIT;
	return error;
}
