
#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include <debug.h>

#include <value/string/struct.h>
#include <value/struct.h>

#include <scope/fetch/boolean.h>
#include <scope/builtins/callback_header.h>

#include "isdir.h"

#if 0
static bool process_isdir(const char* path)
{
	bool ret;
	ENTER;
	struct stat statbuf;
	if(stat(path, &statbuf) < 0)
	{
		ret = false;
	}
	else
	{
		ret = S_ISDIR(statbuf.st_mode);
	}
	verpvb(ret);
	EXIT;
	return ret;
}
#endif

DECLARE_BUILTIN_FUNC(builtin_io_stat_isdir) {
	ENTER;
#if 0
	assert(n == 1);
	struct value* generic_path = params[0];
	assert(generic_path->kind == vk_string);
	struct string_value* path = generic_path;
	if(!path->is_null_terminated)
		string_value_append_null(path);
	ret = scope_fetch_boolean(process_isdir(path->chars.data));
#endif
	TODO;
	EXIT;
}
