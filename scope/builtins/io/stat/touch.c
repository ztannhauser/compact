
#include <assert.h>
#include <fcntl.h>
#include <stdio.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>

#include <debug.h>

#include <array/delete.h>

#include <value/fd/struct.h>
#include <value/number/struct.h>
#include <value/string/struct.h>
#include <value/string/to_utf8_encoded.h>
#include <value/struct.h>

#include <scope/fetch/null.h>

#include <scope/builtins/callback_header.h>

#include "touch.h"

DECLARE_BUILTIN_FUNC(builtin_io_stat_touch)
{
	int error = 0;
	int fd;
	struct value* generic;
	struct value* time_generic;
	struct fd_value* fd_spef;
	struct string_value* string_spef;
	struct number_value* time_number;
	struct array converted;
	struct timespec times[2];
	uintmax_t temp;
	ENTER;
	
	if(1 <= n && n <= 2)
	{
		generic = args[0];
		switch(generic->kind)
		{
			case vk_fd:
			{
				fd_spef = (struct fd_value*) generic;
				fd = fd_spef->fd;
				break;
			}
			case vk_string:
			{
				string_spef = (struct string_value*) generic;
				converted = string_value_to_utf8_encoded(string_spef, true);
				verpvs(converted.data);
				fd = open(converted.datac, O_NOFOLLOW);
				if(fd < 0)
				{
					perror("open");
					error = 1;
				}
				else
				{
					delete_array(&converted);
				}
				break;
			}
			default:
			{
				TODO; // print error message
				break;
			}
		}
		if(!error)
		{
			if(n == 2)
			{
				time_generic = args[1];
				switch(time_generic->kind)
				{
					case vk_number:
					{
						time_number = (struct number_value*) time_generic;
						switch(time_number->kind)
						{
							case nvk_integer:
							{
								times[0].tv_sec = times[1].tv_sec = time_number->integer;
								times[0].tv_nsec = times[1].tv_nsec = 0;
								break;
							}
							case nvk_long: 
							{
								temp = mpz_get_ui(time_number->mpz_long);
								times[0].tv_sec = times[1].tv_sec = temp;
								times[0].tv_nsec = times[1].tv_nsec = 0;
								break;
							}
							default: TODO;
						}
						break;
					}
					case vk_object:
					{
						TODO; // assert type == timespec
						break;
					}
					default:
					{
						TODO; // print error message
					}
				}
			}
			else
			{
				if(clock_gettime(CLOCK_REALTIME, &times[0]) < 0)
				{
					perror("clock_gettime");
					error = 1;
				}
				else
				{
					times[1] = times[0];
				}
			}
		}
		
		if(!error)
		{
			if(futimens(fd, times) < 0)
			{
				perror("futimens");
				error = 1;
			}
		}
		
		if(!error)
		{
			*result = (struct value*) scope_fetch_null(scope);
		}
	}
	else
	{
		fprintf(stderr, "touch: requires one path, time to set is optional!\n");
		error = 1;
	}
	EXIT;
	return error;
}






















