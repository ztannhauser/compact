
#include <assert.h>
#include <stdio.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include <debug.h>

#include <value/number/new.h>
#include <value/object/construct.h>
#include <value/object/struct.h>
#include <value/string/struct.h>
#include <value/struct.h>

#include <scope/builtins/callback_header.h>

#include "stat.h"

DECLARE_BUILTIN_FUNC(builtin_io_stat_stat) {
	ENTER;
#if 0
	assert(n == 1);
	struct value* generic_path = params[0];
	assert(generic_path->kind == vk_string);
	struct string_value* path = generic_path;
	if(!path->is_null_terminated)
		string_value_append_null(path);
	struct stat statbuf;
	if(stat(path->chars.data, &statbuf) < 0) perror("stat"), abort();
	ret = construct_object_value(
		ok_generic,
		"st_mode",  new_number_value_as_integer(statbuf.st_mode),
		"st_nlink", new_number_value_as_integer(statbuf.st_nlink),
		"st_size",  new_number_value_as_integer(statbuf.st_size),
		"st_atim", construct_object_value(
			ok_timespec,
			"tv_sec",  new_number_value_as_integer(statbuf.st_atim.tv_sec),
			"tv_nsec", new_number_value_as_integer(statbuf.st_atim.tv_nsec),
			NULL
		),
		"st_mtim", construct_object_value(
			ok_timespec,
			"tv_sec",  new_number_value_as_integer(statbuf.st_mtim.tv_sec),
			"tv_nsec", new_number_value_as_integer(statbuf.st_mtim.tv_nsec),
			NULL
		),
		"st_ctim", construct_object_value(
			ok_timespec,
			"tv_sec",  new_number_value_as_integer(statbuf.st_ctim.tv_sec),
			"tv_nsec", new_number_value_as_integer(statbuf.st_ctim.tv_nsec),
			NULL
		),
		"st_atime", new_number_value_as_integer(statbuf.st_atime),
		"st_mtime", new_number_value_as_integer(statbuf.st_mtime),
		"st_ctime", new_number_value_as_integer(statbuf.st_ctime),
		NULL
	);
	verpv(ret);
#endif
	TODO;
	EXIT;
}
