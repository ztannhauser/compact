
#include <stdbool.h>

bool process_isdir(const char* path);

#include <scope/builtins/callback_header.h>

DECLARE_BUILTIN_FUNC(builtin_io_stat_isdir);
