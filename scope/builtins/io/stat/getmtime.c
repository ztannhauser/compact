
#include <assert.h>
#include <stdio.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include <debug.h>

#include <array/delete.h>

#include <value/number/new.h>
#include <value/object/construct.h>
#include <value/object/struct.h>
#include <value/string/struct.h>
#include <value/string/to_utf8_encoded.h>
#include <value/struct.h>

#include <scope/builtins/callback_header.h>

#include "getmtime.h"

DECLARE_BUILTIN_FUNC(builtin_io_stat_getmtime) {
	int error = 0;
	struct value* generic_path;
	struct string_value* path;
	struct array converted;
	struct stat statbuf;
	ENTER;
	if(n == 1)
	{
		generic_path = args[0];
		if(generic_path->kind == vk_string)
		{
			path = (struct string_value*) generic_path;
			converted = string_value_to_utf8_encoded(path, true);
			if(stat(converted.datac, &statbuf) < 0)
			{
				perror("stat");
				error = 1;
			}
			else
			{
				*result = (struct value*)
					new_number_value_as_integer(statbuf.st_mtime);
			}
			delete_array(&converted);
		}
		else
		{
			TODO; // print message, error = 1
		}
	}
	else
	{
		TODO; // print message,
		error = 1;
	}
	EXIT;
	return error;
}







