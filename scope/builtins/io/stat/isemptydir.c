
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <dirent.h>
#include <string.h>
#include <assert.h>
#include <stdio.h>

#include <debug.h>

#include <array/struct.h>
#include <array/delete.h>

#include <value/string/to_utf8_encoded.h>

#include <scope/fetch/boolean.h>
#include <scope/builtins/callback_header.h>

#include "isemptydir.h"

DECLARE_BUILTIN_FUNC(builtin_io_stat_isemptydir) {
	int error = 0;
	ENTER;
	if(n == 1)
	{
		struct value* generic;
		
		generic = args[0];
		if(generic->kind == vk_string)
		{
			struct string_value* path;
			struct array escaped;
			struct stat statbuf;
			bool ret;
			
			path = (struct string_value*) generic;
			
			escaped = string_value_to_utf8_encoded(path, true);
			
			if(stat(escaped.datac, &statbuf) < 0)
			{
				ret = false;
			}
			else
			{
				DIR* dir;
				
				if(dir = opendir(escaped.datac))
				{
					struct dirent* ent;
					
					ret = true;
					while(ret && (ent = readdir(dir)))
					{
						if(true
						&& strcmp(ent->d_name, ".")
						&& strcmp(ent->d_name, ".."))
						{
							ret = false;
						}
					}
					
					closedir(dir);
				}
				else
				{
					ret = false;
				}
			}
			
			delete_array(&escaped);
			
			*result = (struct value*)
				scope_fetch_boolean(scope, ret);
		}
		else
		{
			TODO;
			error = 1;
		}
	}
	else
	{
		fprintf(stderr, "isemptydir: requires one path\n");
		error = 1;
	}
	EXIT;
	return error;
}

















