
#include <assert.h>
#include <stdio.h>
#include <sys/time.h>
#include <time.h>

#include <debug.h>

#include <scope/fetch/null.h>
#include <value/number/struct.h>
#include <value/struct.h>

#include <scope/builtins/callback_header.h>

#include "globals.h"

#include "srandom.h"

int set_random_seed()
{
	int error = 0;
	unsigned int seed;
	struct timeval tv;
	ENTER;
	error = (gettimeofday(&tv, NULL) < 0);
	seed = tv.tv_usec;
	srandom(seed), has_seed_been_set = true;
	EXIT;
	return error;
}

DECLARE_BUILTIN_FUNC(builtin_random_srandom) {
#if 0
	if(n == 1)
	{
		unsigned int seed;
		struct value* generic_seed_value = params[0];
		assert(generic_seed_value->kind == vk_number);
		struct number_value* seed_value = generic_seed_value;
		assert(seed_value->kind == nvk_integer);
		seed = seed_value->integer;
		srandom(seed), has_seed_been_set = true;
	}
	else
	{
		set_random_seed();
	}
	ret = scope_fetch_null();
#endif
	TODO;
}
