
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include <debug.h>

#include <scope/push_builtin_func.h>

#include "randint.h"
#include "random.h"
#include "srandom.h"

#include "push_builtins.h"

int push_random_builtins(struct scope *scope) {
	int error = 0;
	ENTER;

	error = 0
		?: scope_push_builtin_func(scope, L"randint",
			builtin_random_randint)
		?: scope_push_builtin_func(scope, L"random",
			builtin_random_random)
		?: scope_push_builtin_func(scope, L"srandom",
			builtin_random_srandom)
		;

	EXIT;
	return error;
}
