
#include <assert.h>
#include <stdio.h>

#include <debug.h>

#include <value/number/new.h>
#include <value/number/struct.h>
#include <value/struct.h>

#include <scope/builtins/callback_header.h>

#include "globals.h"
#include "srandom.h"

#include "randint.h"

DECLARE_BUILTIN_FUNC(builtin_random_randint)
{
	int error = 0;
	struct value* generic_base;
	struct number_value* number_base;
	struct value* generic_length;
	struct number_value* number_length;
	signed long base, length = 0;
	signed long val;
	ENTER;
	
	if(1 <= n && n <= 2)
	{
		if(n > 1)
		{
			generic_base = args[0];
			if(generic_base->kind == vk_number)
			{
				number_base = (struct number_value*) generic_base;
				if(number_base->kind == nvk_integer)
				{
					base = number_base->integer;
					n--, args++;
				}
				else
				{
					TODO; // print error message, error = 1;
				}
			}
			else
			{
				TODO; // print error message, error = 1
			}
		}
		else
		{
			base = 0;
		}
		
		if(!error)
		{
			generic_length = args[0];
			if(generic_length->kind == vk_number)
			{
				number_length = (struct number_value*) generic_length;
				if(number_length->kind == nvk_integer)
				{
					length = number_length->integer;
				}
				else
				{
					TODO; // print error message, error = 1;
				}
			}
		}
		
		if(!error && !has_seed_been_set)
		{
			error = set_random_seed();
		}
		
		if(!error)
		{
			val = base + random() % length;
			verpv(val);

			*result = (struct value*) new_number_value_as_integer(val);
		}
	}
	else
	{
		fprintf(stderr, "randint(): expected 1 or 2 "
			"integers (start and length)\n");
		error = 1;
	}
	EXIT;
	return error;
}














