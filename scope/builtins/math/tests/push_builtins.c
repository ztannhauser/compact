
#include <assert.h>
#include <stdio.h>

#include <debug.h>

#include <scope/push_builtin_func.h>

#include "isfin.h"
#include "isinf.h"
#include "isnan.h"
#include "isnonzero.h"
#include "iszero.h"

#include "push_builtins.h"

int push_math_tests_builtins(struct scope *scope) {
	int error = 0;
	ENTER;

	error = 0
		?: scope_push_builtin_func(scope, L"isfin", 
			builtin_math_tests_isfin)
		?: scope_push_builtin_func(scope, L"isinf", 
			builtin_math_tests_isinf)
		?: scope_push_builtin_func(scope, L"isnan", 
			builtin_math_tests_isnan)
		?: scope_push_builtin_func(scope, L"isnonzero", 
			builtin_math_tests_isnonzero)
		?: scope_push_builtin_func(scope, L"iszero",
			builtin_math_tests_iszero)
		;

	EXIT;
	return error;
}
