
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include <debug.h>

#include "value/boolean/struct.h"
#include "value/number/new.h"
#include "value/number/struct.h"
#include "value/struct.h"

#include "scope/fetch/boolean.h"

#include <scope/builtins/callback_header.h>

#include "isinf.h"

#if 0
static struct boolean_value *handle_isinf(struct number_value *given) {
	struct boolean_value *ret;
	ENTER;
	switch (given->kind) {
	case nvk_integer:
	case nvk_long: {
		ret = scope_fetch_boolean(false);
		break;
	}
	case nvk_decimal: {
		ret = scope_fetch_boolean(isinfq(given->decimal));
		break;
	}
	default:
		TODO;
	}
	EXIT;
	return ret;
}
#endif

DECLARE_BUILTIN_FUNC(builtin_math_tests_isinf) {
#if 0
	struct value* ret;
	assert(n == 1);
	struct value* given = (struct value*) params[0];
	assert(given->kind == vk_number);
	ret = (struct value*) handle_isinf((struct number_value*) given);
	return ret;
#endif
	TODO;
}
