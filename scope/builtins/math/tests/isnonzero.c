
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include "debug.h"
#include "defines.h"

#include "value/boolean/struct.h"
#include "value/number/new.h"
#include "value/number/struct.h"
#include "value/struct.h"

#include "scope/fetch/boolean.h"
#include <scope/builtins/callback_header.h>

#include "isnonzero.h"

#if 0
static struct boolean_value *handle_isnonzero(struct number_value *given) {
	struct boolean_value *ret;
	ENTER;
	switch (given->kind) {
	case nvk_integer: {
		ret = scope_fetch_boolean(given->integer != 0);
		break;
	}
	case nvk_long: {
		ret = scope_fetch_boolean(mpz_sgn(given->mpz_long));
		break;
	}
	case nvk_decimal: {
		ret = scope_fetch_boolean(fabsq(given->decimal) >
								  QUAD_CLOSE_ENOUGH_TO_ZERO);
		break;
	}
	default:
		TODO;
	}
	EXIT;
	return ret;
}
#endif

DECLARE_BUILTIN_FUNC(builtin_math_tests_isnonzero) {
#if 0
	struct value* ret;
	assert(n == 1);
	struct value* given = (struct value*) params[0];
	assert(given->kind == vk_number);
	ret = (struct value*) handle_isnonzero((struct number_value*) given);
	return ret;
#endif
	TODO;
}
