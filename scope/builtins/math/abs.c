
#include <assert.h>

#include <debug.h>

#include <value/number/new.h>
#include <value/number/struct.h>
#include <value/struct.h>

#include <scope/builtins/callback_header.h>

#include "abs.h"

DECLARE_BUILTIN_FUNC(builtin_math_abs) {
#if 0
	struct value* ret;
	assert(n == 1);
	struct value* generic_param = params[0];
	assert(generic_param->kind == vk_number);
	struct number_value* param = (struct number_value*) generic_param;
	switch(param->kind)
	{
		case nvk_integer:
		{
			ret = (struct value*) new_number_value_as_integer(labs(param->integer));
			break;
		}
		case nvk_long:
		{
			mpz_ptr mp = malloc(sizeof(MP_INT));
			mpz_init(mp);
			mpz_abs(mp, param->mpz_long);
			ret = (struct value*) new_number_value_as_long(mp);
			break;
		}
		case nvk_decimal:
		{
			ret = (struct value*) new_number_value_as_decimal(fabsq(param->decimal));
			break;
		}
		default: TODO;
	}
	return ret;
#endif
	TODO;
}
