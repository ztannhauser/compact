
#include <assert.h>
#include <stdio.h>

#include <debug.h>

#include <value/number/new.h>
#include <value/number/struct.h>
#include <value/struct.h>

#include <scope/builtins/callback_header.h>

#include "sub.h"

struct number_value *handle_numeral_subtract(struct number_value *left,
											 struct number_value *right) {
	struct number_value *ret;
	ENTER;
	switch (left->kind) {
	case nvk_integer: {
		switch (right->kind) {
		case nvk_integer: {
			signed long a = left->integer;
			signed long b = right->integer;
			// detect overflow
			if ((a > 0 && b < 0) && (b < a - LONG_MAX)) {
				mpz_ptr mp = malloc(sizeof(MP_INT));
				mpz_init_set_si(mp, a);
				unsigned long bb = b;
				mpz_add_ui(mp, mp, -bb);
				ret = new_number_value_as_long(mp);
			}
			// detect underflow
			else if ((a < 0 && b > 0) && (b > LONG_MIN + a)) {
				mpz_ptr mp = malloc(sizeof(MP_INT));
				mpz_init_set_si(mp, a);
				mpz_sub_ui(mp, mp, b);
				ret = new_number_value_as_long(mp);
			}
			// normal math
			else {
				ret = new_number_value_as_integer(a - b);
			}
			break;
		}
		case nvk_long: {
			mpz_ptr mp = malloc(sizeof(MP_INT));
			mpz_init(mp);
			mpz_ui_sub(mp, left->integer, right->mpz_long);
			ret = new_number_value_as_long(mp);
			break;
		}
		case nvk_decimal: {
			ret = new_number_value_as_decimal(left->integer - right->decimal);
			break;
		}
		default:
			TODO;
		}
		break;
	}
	case nvk_long: {
		switch (right->kind) {
		case nvk_integer: {
			mpz_ptr mp = malloc(sizeof(MP_INT));
			mpz_init(mp);
			mpz_sub_ui(mp, left->mpz_long, right->integer);
			ret = new_number_value_as_long(mp);
			break;
		}
		case nvk_long: {
			mpz_ptr mp = malloc(sizeof(MP_INT));
			mpz_init(mp);
			mpz_sub(mp, left->mpz_long, right->mpz_long);
			ret = new_number_value_as_long(mp);
			break;
		}
		case nvk_decimal: {
			TODO;
			break;
		}
		default:
			TODO;
		}
		break;
	}
	case nvk_decimal: {
		switch (right->kind) {
		case nvk_integer: {
			ret = new_number_value_as_decimal(left->decimal - right->integer);
			break;
		}
		case nvk_long: {
			ret = new_number_value_as_decimal(left->decimal -
											  mpz_get_d(right->mpz_long));
			break;
		}
		case nvk_decimal: {
			ret = new_number_value_as_decimal(left->decimal - right->decimal);
			break;
		}
		default:
			TODO;
		}
		break;
	}
	default:
		TODO;
	}
	EXIT;
	return ret;
}

DECLARE_BUILTIN_FUNC(builtin_math_arith_sub) {
#if 0
	struct value* ret;
	ENTER;
	assert(n == 2);
	struct value* gleft = params[0], *gright = params[1];
	assert(gleft->kind == vk_number && gright->kind == vk_number);
	ret = (struct value*) handle_numeral_subtract(
		(struct number_value*) gleft,
		(struct number_value*) gright);
	EXIT;
	return ret;
#endif
	TODO;
}
