
#include <value/number/struct.h>

int handle_numeral_multiply(
	struct number_value* left,
	struct number_value* right,
	struct number_value** result)
		__attribute__ ((warn_unused_result));

#include <scope/builtins/callback_header.h>

DECLARE_BUILTIN_FUNC(builtin_math_arith_mult);
