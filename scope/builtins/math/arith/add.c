
#include <assert.h>
#include <stdio.h>

#include <debug.h>

#include <value/number/new.h>
#include <value/number/struct.h>
#include <value/struct.h>

#include <scope/builtins/callback_header.h>

#include "add.h"

int handle_numeral_add(struct number_value *left, struct number_value *right,
					   struct number_value **result) {
	int error = 0;
	ENTER;
	switch (left->kind) {
	case nvk_integer: {
		switch (right->kind) {
		case nvk_integer: {
			signed long a = left->integer;
			signed long b = right->integer;
			verpv(a);
			verpv(b);

			// detect overflow
			if ((a > 0) && (b > LONG_MAX - a)) {
#if 0
						mpz_ptr mp = malloc(sizeof(MP_INT));
						mpz_init(mp);
						mpz_set_si(mp, a);
						mpz_add_ui(mp, mp, b);
						ret = new_number_value_as_long(mp);
						verpv(ret);
#endif
				TODO;
			}
			// detect underflow
			else if ((a < 0) && (b < LONG_MIN - a)) {
#if 0
						mpz_ptr mp = malloc(sizeof(MP_INT));
						mpz_init(mp);
						mpz_set_si(mp, a);
						mpz_sub_ui(mp, mp, -b);
						ret = new_number_value_as_long(mp);
						verpv(ret);
#endif
				TODO;
			}
			// normal math
			else {
				*result = new_number_value_as_integer(a + b);
			}

			break;
		}
#if 0
				case nvk_long:
				{
					mpz_ptr mp = malloc(sizeof(MP_INT));
					mpz_init(mp);
					mpz_add_ui(mp, right->mpz_long, left->integer);
					ret = new_number_value_as_long(mp);
					verpv(ret);
					break;
				}
				case nvk_decimal:
				{
					ret = new_number_value_as_decimal(
						left->integer + right->decimal
					);
					break;
				}
#endif
		default:
			TODO;
		}
		break;
	}
#if 0
		case nvk_long:
		{
			switch(right->kind)
			{
				case nvk_integer:
				{
					mpz_ptr mp = malloc(sizeof(MP_INT));
					mpz_init(mp);
					mpz_add_ui(mp, left->mpz_long, right->integer);
					ret = new_number_value_as_long(mp);
					verpv(ret);
					break;
				}
				case nvk_long:
				{
					mpz_ptr mp = malloc(sizeof(MP_INT));
					mpz_init(mp);
					mpz_add(mp, right->mpz_long, left->mpz_long);
					ret = new_number_value_as_long(mp);
					verpv(ret);
					break;
				}
				case nvk_decimal:
				{
					ret = new_number_value_as_decimal(
						mpz_get_d(left->mpz_long) + right->decimal
					);
					break;
				}
				default: TODO;
			}
			break;
		}
		case nvk_decimal:
		{
			switch(right->kind)
			{
				case nvk_integer:
				{
					ret = new_number_value_as_decimal(
						left->decimal + right->integer
					);
					break;
				}
				case nvk_decimal:
				{
					ret = new_number_value_as_decimal(
						left->decimal + right->decimal
					);
					break;
				}
				case nvk_long:
				{
					ret = new_number_value_as_decimal(
						left->decimal + mpz_get_d(right->mpz_long)
					);
					break;
				}
				default: TODO;
			}
			break;
		}
#endif
	default:
		TODO;
	}

	EXIT;
	return error;
}

DECLARE_BUILTIN_FUNC(builtin_math_arith_add) {
#if 0
	struct value* ret;
	assert(n == 2);
	ENTER;
	struct value* left = params[0], *right = params[1];
	assert(left->kind == vk_number && right->kind == vk_number);
	ret = (struct value*) handle_numeral_add(
		(struct number_value*) left,
		(struct number_value*) right);
	verpv(ret);
	EXIT;
	return ret;
#endif
	TODO;
}
