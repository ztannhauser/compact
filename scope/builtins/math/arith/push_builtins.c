
#include <assert.h>
#include <stdio.h>

#include <debug.h>

#include <scope/push_builtin_func.h>

#include "add.h"
#include "div.h"
#include "mult.h"
#include "negate.h"
#include "remain.h"
#include "sub.h"

#include "push_builtins.h"

int push_math_arith_builtins(struct scope *scope) {
	int error = 0;
	ENTER;

	error = 0
		?: scope_push_builtin_func(scope, L"add", 
			builtin_math_arith_add)
		?: scope_push_builtin_func(scope, L"div", 
			builtin_math_arith_div)
		?: scope_push_builtin_func(scope, L"mult", 
			builtin_math_arith_mult)
		?: scope_push_builtin_func(scope, L"negate", 
			builtin_math_arith_negate)
		?: scope_push_builtin_func(scope, L"remain", 
			builtin_math_arith_remain)
		?: scope_push_builtin_func(scope, L"sub", 
			builtin_math_arith_sub)
		;

	EXIT;
	return error;
}
