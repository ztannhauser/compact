
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include <debug.h>

#include "value/number/new.h"
#include "value/number/struct.h"

#include <scope/builtins/callback_header.h>

#include "remain.h"

struct number_value *
handle_numeral_remainder_divide(struct number_value *left,
								struct number_value *right) {
	struct number_value *ret;
	ENTER;
	switch (left->kind) {
	case nvk_integer: {
		switch (right->kind) {
		case nvk_integer: {
			ret = new_number_value_as_integer(left->integer % right->integer);
			break;
		}
		case nvk_long: {
			mpz_ptr result = malloc(sizeof(MP_INT));
			mpz_init_set_si(result, left->integer);
			mpz_mod(result, result, right->mpz_long);
			ret = new_number_value_as_long(result);
			break;
		}
		case nvk_decimal: {
			if (left->integer) {
				ret = new_number_value_as_decimal(
					fmodq(left->integer, right->decimal));
			} else {
				ret = new_number_value_as_integer(0);
			}
			break;
		}
		default:
			TODO;
		}
		break;
	}
	case nvk_long: {
		switch (right->kind) {
		case nvk_integer: {
			mpz_ptr result = malloc(sizeof(MP_INT));
			mpz_init_set_si(result, right->integer);
			mpz_mod(result, left->mpz_long, result);
			ret = new_number_value_as_long(result);
			break;
		}
		case nvk_long: {
			mpz_ptr result = malloc(sizeof(MP_INT));
			mpz_init(result);
			mpz_mod(result, left->mpz_long, right->mpz_long);
			ret = new_number_value_as_long(result);
			break;
		}
		case nvk_decimal: {
			if (mpz_sgn(left->mpz_long)) {
				ret = new_number_value_as_decimal(
					fmodq(mpz_get_d(left->mpz_long), right->decimal));
			} else {
				ret = new_number_value_as_integer(0);
			}
			break;
		}
		default:
			TODO;
		}
		break;
	}
	case nvk_decimal: {
		switch (right->kind) {
		case nvk_integer: {
			ret = new_number_value_as_decimal(
				fmodq(left->decimal, right->integer));
			break;
		}
		case nvk_long: {
			ret = new_number_value_as_decimal(
				fmodq(left->decimal, mpz_get_d(right->mpz_long)));
			break;
		}
		case nvk_decimal: {
			ret = new_number_value_as_decimal(
				fmodq(left->decimal, right->decimal));
			break;
		}
		default:
			TODO;
		}
		break;
	}
	default:
		TODO;
	}
	EXIT;
	return ret;
}

DECLARE_BUILTIN_FUNC(builtin_math_arith_remain) {
#if 0
	struct value* ret;
	assert(n == 2);
	struct value* generic_left = params[0], *generic_right = params[1];
	assert(generic_left->kind == vk_number && generic_right->kind == vk_number);
	ret = (struct value*) handle_numeral_remainder_divide(
		(struct number_value*) generic_left,
		(struct number_value*) generic_right);
	return ret;
#endif
	TODO;
}
