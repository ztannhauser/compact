
#include <assert.h>
#include <stdio.h>

#include <debug.h>

#include <value/number/new.h>
#include <value/number/struct.h>
#include <value/struct.h>

#include <scope/builtins/callback_header.h>

#include "div.h"

struct number_value *handle_numeral_divide(struct number_value *left,
										   struct number_value *right) {
	struct number_value *ret;
	ENTER;
	switch (left->kind) {
	case nvk_integer: {
		switch (right->kind) {
		case nvk_integer: {
			ret = new_number_value_as_integer(left->integer / right->integer);
			break;
		}
		case nvk_decimal: {
			ret = new_number_value_as_decimal(left->integer / right->decimal);
			break;
		}
		default:
			TODO;
		}
		break;
	}
	case nvk_long: {
		switch (right->kind) {
		case nvk_long: {
			mpz_ptr result = malloc(sizeof(MP_INT));
			mpz_init(result);
			mpz_tdiv_q(result, left->mpz_long, right->mpz_long);
			ret = new_number_value_as_long(result);
			break;
		}
		default:
			TODO;
		}
		break;
	}
	case nvk_decimal: {
		switch (right->kind) {
		case nvk_integer: {
			ret = new_number_value_as_decimal(left->decimal / right->integer);
			break;
		}
		case nvk_decimal: {
			ret = new_number_value_as_decimal(left->decimal / right->decimal);
			break;
		}
		default:
			TODO;
		}
		break;
	}
	default:
		TODO;
	}
	EXIT;
	return ret;
}

DECLARE_BUILTIN_FUNC(builtin_math_arith_div) {
#if 0
	struct value* ret;
	ENTER;
	struct value* generic_left = params[0], *generic_right = params[1];
	assert(generic_left->kind == vk_number && generic_right->kind == vk_number);
	ret = (struct value*) handle_numeral_divide(
		(struct number_value*) generic_left,
		(struct number_value*) generic_right);
	EXIT;
	return ret;
#endif
	TODO;
}
