
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include <debug.h>

#include "value/number/new.h"
#include "value/number/struct.h"
#include "value/struct.h"

#include <scope/builtins/callback_header.h>

#include "negate.h"

struct number_value *handle_numeral_negate(struct number_value *num) {
	struct number_value *ret;
	ENTER;
	switch (num->kind) {
	case nvk_integer: {
		ret = new_number_value_as_integer(-num->integer);
		break;
	}
	case nvk_long: {
		mpz_ptr result = malloc(sizeof(__mpz_struct));
		mpz_init(result);
		mpz_neg(result, num->mpz_long);
		ret = new_number_value_as_long(result);
		break;
	}
	case nvk_decimal: {
		ret = new_number_value_as_decimal(-num->decimal);
		break;
	}
	default:
		TODO;
	}
	EXIT;
	return ret;
}

DECLARE_BUILTIN_FUNC(builtin_math_arith_negate) {
#if 0
	struct value* ret;
	ENTER;
	assert(n == 1);
	struct value* generic_param = params[0];
	assert(generic_param->kind == vk_number);
	ret = (struct value*) handle_numeral_negate(
		(struct number_value*) generic_param);
	EXIT;
	return ret;
#endif
	TODO;
}
