
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include <debug.h>

#include <value/number/new.h>
#include <value/number/struct.h>
#include <value/struct.h>

#include <scope/builtins/callback_header.h>

#include "mult.h"

int handle_numeral_multiply(struct number_value *left,
							struct number_value *right,
							struct number_value **result) {
	int error = 0;
	ENTER;
	switch (left->kind) {
	case nvk_integer: {
		if (left->integer) {
			switch (right->kind) {
			case nvk_integer: {
				signed long a = left->integer;
				signed long b = right->integer;

				verpv(a);
				verpv(b);

				// check for overflow and  check for underflow
				if ((b > 0 && a > LONG_MAX / b) ||
					(b < 0 && a < LONG_MIN / b)) {
#if 0
							mpz_ptr result = malloc(sizeof(MP_INT));
							mpz_init_set_si(result, a);
							mpz_mul_si(result, result, b);
							ret = new_number_value_as_long(result);
#endif
				}
				// normal math
				else {
					*result = new_number_value_as_integer(a * b);
				}
				break;
			}
#if 0
					case nvk_long:
					{
						mpz_ptr result = malloc(sizeof(MP_INT));
						mpz_init_set_si(result, left->integer);
						mpz_mul(result, result, right->mpz_long);
						ret = new_number_value_as_long(result);
						break;
					}
					case nvk_decimal:
					{
						ret = new_number_value_as_decimal(
							left->integer * right->decimal);
						break;
					}
#endif
			default:
				TODO;
			}
		} else {
			// ret = new_number_value_as_integer(0);
			TODO;
		}
		break;
	}
#if 0
		case nvk_long:
		{
			if(mpz_sgn(left->mpz_long))
			{
				switch(right->kind)
				{
					case nvk_integer:
					{
						if(right->integer)
						{
							mpz_ptr result = malloc(sizeof(MP_INT));
							mpz_init_set_si(result, right->integer);
							mpz_mul(result, left->mpz_long, result);
							ret = new_number_value_as_long(result);
						}
						else
						{
							ret = new_number_value_as_integer(0);
						}
						break;
					}
					case nvk_long:
					{
						if(mpz_sgn(right->mpz_long))
						{
							mpz_ptr result = malloc(sizeof(MP_INT));
							mpz_init(result);
							mpz_mul(result, left->mpz_long, right->mpz_long);
							ret = new_number_value_as_long(result);
						}
						else
						{
							ret = new_number_value_as_integer(0);
						}
						break;
					}
					case nvk_decimal:
					{
						ret = new_number_value_as_decimal(
							mpz_get_d(left->mpz_long) * right->decimal);
						break;
					}
					default: TODO;
				}
			}
			else
			{
				ret = new_number_value_as_integer(0);
			}
			break;
		}
		case nvk_decimal:
		{
			switch(right->kind)
			{
				case nvk_integer:
				{
					if(right->integer)
					{
						ret = new_number_value_as_decimal(
							left->decimal * right->integer);
					}
					else
					{
						ret = new_number_value_as_integer(0);
					}
					break;
				}
				case nvk_long:
				{
					if(mpz_sgn(right->mpz_long))
					{
						ret = new_number_value_as_decimal(
							left->decimal * mpz_get_d(right->mpz_long));
					}
					else
					{
						ret = new_number_value_as_integer(0);
					}
					break;
				}
				case nvk_decimal:
				{
					ret = new_number_value_as_decimal(
						left->decimal * right->decimal);
					break;
				}
				default: TODO;
			}
			break;
		}
#endif
	default:
		TODO;
	}

	EXIT;
	return error;
}

DECLARE_BUILTIN_FUNC(builtin_math_arith_mult) {
#if 0
	struct value* ret;
	ENTER;
	struct value* generic_left = params[0], *generic_right = params[1];
	assert(generic_left->kind == vk_number && generic_right->kind == vk_number);
	ret = (struct value*) handle_numeral_multiply(
		(struct number_value*) generic_left,
		(struct number_value*) generic_right);
	EXIT;
	return ret;
#endif
	TODO;
}
