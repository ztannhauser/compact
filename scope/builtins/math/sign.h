
#include "value/number/struct.h"

int cast_to_sign(struct number_value *param)
	__attribute__ ((warn_unused_result));

#include <scope/builtins/callback_header.h>

DECLARE_BUILTIN_FUNC(builtin_math_sign);
