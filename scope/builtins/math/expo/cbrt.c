
#include <assert.h>
#include <inttypes.h>
#include <quadmath.h>
#include <stdio.h>
#include <stdlib.h>

#include <debug.h>

#include "value/number/new.h"
#include "value/number/struct.h"
#include "value/struct.h"

#include <scope/builtins/callback_header.h>

#include "cbrt.h"

// found online: https://gist.github.com/anonymous/729557

#if 0
static uint32_t icbrt64(uint64_t x) {
	int s;
	uint32_t y;
	uint64_t b;

	y = 0;
	for (s = 63; s >= 0; s -= 3) {
		y += y;
		b = 3 * y * ((uint64_t)y + 1) + 1;
		if ((x >> s) >= b) {
			x -= b << s;
			y++;
		}
	}
	return y;
}
#endif

#if 0
static struct number_value *handle_cbrt(struct number_value *given) {
	struct number_value *ret;
	ENTER;
	switch (given->kind) {
	case nvk_integer: {
		if (given->integer > 0) {
			unsigned long ul = given->integer;
			ret = new_number_value_as_integer(icbrt64(ul));
		} else if (given->integer < 0) {
			unsigned long ul = -given->integer;
			signed long sl = icbrt64(ul);
			ret = new_number_value_as_integer(sl);
		} else //  if(given_number->integer == 0)
		{
			ret = new_number_value_as_integer(0);
		}
		break;
	}
	case nvk_long: {
		mpz_ptr mp = malloc(sizeof(MP_INT));
		mpz_init(mp);
		mpz_root(mp, given->mpz_long, 3);
		ret = new_number_value_as_long(mp);
		break;
	}
	case nvk_decimal: {
		ret = new_number_value_as_decimal(cbrtq(given->decimal));
		break;
	}
	default:
		TODO;
	}
	EXIT;
	return ret;
}

#endif

DECLARE_BUILTIN_FUNC(builtin_math_expo_cbrt) {
#if 0
	struct value* ret;
	ENTER;
	assert(n == 1);
	struct value* given = (struct value*) params[0];
	assert(given->kind == vk_number);
	ret = (struct value*) handle_cbrt((struct number_value*) given);
	EXIT;
	return ret;
#endif
	TODO;
}
