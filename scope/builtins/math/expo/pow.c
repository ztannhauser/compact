
#include <assert.h>
#include <stdio.h>

#include <debug.h>
#include <linkedlist/struct.h>

#include <expression/evaluate.h>
#include <value/delete.h>
#include <value/number/new.h>
#include <value/number/struct.h>
#include <value/struct.h>

#include <scope/builtins/callback_header.h>

#include "pow.h"

#if 0
struct number_value *handle_numeral_exponent(struct number_value *base,
											 struct number_value *exp) {
	struct number_value *ret;
	ENTER;
	switch (base->kind) {
	case nvk_integer: {
		switch (exp->kind) {
		case nvk_integer: {
			mpz_ptr result = malloc(sizeof(MP_INT));
			mpz_init_set_si(result, base->integer);
			if (exp->integer >= 0) {
				mpz_pow_ui(result, result, exp->integer);
			} else {
				TODO;
			}
			ret = new_number_value_as_long(result);
			break;
		}
		case nvk_decimal: {
			ret =
				new_number_value_as_decimal(powq(base->integer, exp->decimal));
			break;
		}
		case nvk_long: {
			assert(exp->integer > 0);
			mpz_ptr result = malloc(sizeof(__mpz_struct));
			mpz_init(result);
			assert(mpz_fits_ulong_p(exp->mpz_long));
			mpz_ui_pow_ui(result, labs(base->integer),
						  mpz_get_ui(exp->mpz_long));
			if (base->integer < 0) {
				mpz_neg(result, result);
			}
			ret = new_number_value_as_long(result);
			break;
		}
		default:
			TODO;
		}
		break;
	}
	case nvk_long: {
		switch (exp->kind) {
		case nvk_integer: {
			assert(exp->integer > 0);
			mpz_ptr result = malloc(sizeof(__mpz_struct));
			mpz_init(result);
			mpz_pow_ui(result, base->mpz_long, exp->integer);
			ret = new_number_value_as_long(result);
			break;
		}
		case nvk_long: {
			mpz_ptr result = malloc(sizeof(__mpz_struct));
			mpz_init(result);
			assert(mpz_fits_ulong_p(exp->mpz_long));
			mpz_pow_ui(result, base->mpz_long, mpz_get_ui(exp->mpz_long));
			ret = new_number_value_as_long(result);
			break;
		}
		default:
			TODO;
		}
		break;
	}
	case nvk_decimal: {
		switch (exp->kind) {
		case nvk_integer: {
			ret =
				new_number_value_as_decimal(powq(base->decimal, exp->integer));
			break;
		}
		case nvk_long: {
			ret = new_number_value_as_decimal(
				powq(base->decimal, mpz_get_d(exp->mpz_long)));
			break;
		}
		case nvk_decimal: {
			ret =
				new_number_value_as_decimal(powq(base->decimal, exp->decimal));
			break;
		}
		default:
			TODO;
		}
		break;
	}
	default:
		TODO;
	}
	EXIT;
	return ret;
}
#endif

DECLARE_BUILTIN_FUNC(builtin_math_expo_pow) {
#if 0
	struct value* ret;
	ENTER;
	assert(n == 2);
	struct value* given_base = params[0];
	struct value* given_exp = params[1];
	assert(given_base->kind == vk_number && given_exp->kind == vk_number);
	ret = (struct value*) handle_numeral_exponent(
		(struct number_value*) given_base,
		(struct number_value*) given_exp);
	EXIT;
	return ret;
#endif
	TODO;
}
