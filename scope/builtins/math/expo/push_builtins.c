
#include <assert.h>
#include <stdio.h>

#include <debug.h>

#include <scope/push_builtin_func.h>

#include "cbrt.h"
#include "exp.h"
#include "exp10.h"
#include "exp2.h"
#include "expm1.h"
#include "log.h"
#include "log10.h"
#include "log1p.h"
#include "log2.h"
#include "pow.h"
#include "sqrt.h"

#include "push_builtins.h"

int push_math_expo_builtins(struct scope *scope) {
	int error = 0;
	ENTER;
	
	error = 0
		?: scope_push_builtin_func(scope, L"cbrt", 
			builtin_math_expo_cbrt)
		?: scope_push_builtin_func(scope, L"exp", 
			builtin_math_expo_exp)
		?: scope_push_builtin_func(scope, L"exp2", 
			builtin_math_expo_exp2)
		?: scope_push_builtin_func(scope, L"exp10", 
			builtin_math_expo_exp10)
		?: scope_push_builtin_func(scope, L"expm1", 
			builtin_math_expo_expm1)
		?: scope_push_builtin_func(scope, L"log", 
			builtin_math_expo_log)
		?: scope_push_builtin_func(scope, L"log1p",
			builtin_math_expo_log1p)
		?: scope_push_builtin_func(scope, L"log2",
			builtin_math_expo_log2)
		?: scope_push_builtin_func(scope, L"log10",
			builtin_math_expo_log10)
		?: scope_push_builtin_func(scope, L"pow",
			builtin_math_expo_pow)
		?: scope_push_builtin_func(scope, L"sqrt",
			builtin_math_expo_sqrt)
		;

	EXIT;
	return error;
}





