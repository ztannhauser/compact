
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include <debug.h>

#include "value/number/new.h"
#include "value/number/struct.h"
#include "value/struct.h"

#include <scope/builtins/callback_header.h>

#include "exp.h"

#if 0
static struct number_value *handle_exp(struct number_value *given) {
	struct number_value *ret;
	ENTER;
	switch (given->kind) {
	case nvk_decimal: {
		ret = new_number_value_as_decimal(expq(given->decimal));
		break;
	}
	default:
		TODO;
	}
	EXIT;
	return ret;
}
#endif

DECLARE_BUILTIN_FUNC(builtin_math_expo_exp) {
#if 0
	struct value* ret;
	ENTER;
	assert(n == 1);
	struct value* given = (struct value*) params[0];
	assert(given->kind == vk_number);
	ret = (struct value*) handle_exp((struct number_value*) given);
	EXIT;
	return ret;
#endif
	TODO;
}
