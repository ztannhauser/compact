

struct number_value* handle_numeral_exponent(
	struct number_value* left,
	struct number_value* right)
	__attribute__ ((warn_unused_result));


#include <scope/builtins/callback_header.h>

DECLARE_BUILTIN_FUNC(builtin_math_expo_pow);
