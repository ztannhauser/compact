
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include <debug.h>

#include "value/number/new.h"
#include "value/number/struct.h"
#include "value/struct.h"

#include <scope/builtins/callback_header.h>

#include "exp10.h"

#if 0
static struct number_value *handle_exp10(struct number_value *given) {
	struct number_value *ret;
	ENTER;
	switch (given->kind) {
	case nvk_integer: {
		signed long y = given->integer;
		if (y >= 0) {
			signed long x = 1;
			while (y--)
				x *= 10;
			ret = new_number_value_as_integer(x);
		} else {
			ret = new_number_value_as_decimal(powq(10, y));
		}
		break;
	}
	case nvk_decimal: {
		ret = new_number_value_as_decimal(powq(10, given->decimal));
		break;
	}
	default:
		TODO;
	}
	EXIT;
	return ret;
}
#endif

DECLARE_BUILTIN_FUNC(builtin_math_expo_exp10) {
#if 0
	struct value* ret;
	ENTER;
	assert(n == 1);
	struct value* given = (struct value*) params[0];
	assert(given->kind == vk_number);
	ret = (struct value*) handle_exp10((struct number_value*) given);
	EXIT;
	return ret;
#endif
	TODO;
}
