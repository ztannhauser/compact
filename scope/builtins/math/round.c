
#include <assert.h>

#include <debug.h>

#include <value/number/new.h>
#include <value/number/struct.h>
#include <value/struct.h>

#include <scope/builtins/callback_header.h>

#include "round.h"

DECLARE_BUILTIN_FUNC(builtin_math_round) {
#if 0
	struct value* ret;
	assert(1 <= n && n <= 2);
	struct value* generic_param_1 = params[0];
	assert(generic_param_1->kind == vk_number);
	struct number_value* param_1 = (struct number_value*) generic_param_1;
	switch(n)
	{
		case 1:
		{
			switch(param_1->kind)
			{
				case nvk_integer:
				case nvk_long:
				{
					ret = generic_param_1;
					ret->refcount++;
					break;
				}
				case nvk_decimal:
				{
					ret = (struct value*)
						new_number_value_as_decimal(
							roundq(param_1->decimal));
					break;
				}
				default: TODO;
			}
			break;
		}
		case 2:
		{
			struct value* generic_param_2 = params[1];
			assert(generic_param_2->kind == vk_number);
			struct number_value* param_2 = (struct number_value*) generic_param_2;
			switch(param_1->kind)
			{
				case nvk_integer:
				{
					switch(param_2->kind)
					{
						case nvk_integer:
						{
							signed long
								n = param_1->integer,
								d = param_2->integer,
								f = n / d * d, c = f + d,
								a = n - ((n - f < c - n) ? f : c);
							
							ret = (struct value*)
								new_number_value_as_integer(a);
							break;
						}
						default: TODO;
					}
					break;
				}
				case nvk_decimal:
				{
					switch(param_2->kind)
					{
						case nvk_decimal:
						{
							ret = (struct value*)
								new_number_value_as_decimal(
									remainderq(
										param_1->decimal,
										param_2->decimal
									)
								);
							break;
						}
						default: TODO;
					}
					break;
				}
				default: TODO;
			}
			break;
		}
		default: TODO;
	}
	return ret;
#endif
	TODO;
}
