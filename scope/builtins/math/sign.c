#include <assert.h>
#include <stdio.h>

#include <debug.h>

#include <value/number/new.h>
#include <value/number/struct.h>
#include <value/struct.h>

#include <scope/builtins/callback_header.h>

#include "sign.h"

int cast_to_sign(struct number_value *param)
{
	int ret;
	switch (param->kind) {
		case nvk_integer: {
			if (param->integer > 0)
				ret = 1;
			else if (param->integer < 0)
				ret = -1;
			else
				ret = 0;
			break;
		}
		case nvk_long: {
			ret = mpz_sgn(param->mpz_long);
			break;
		}
		case nvk_decimal: {
			if (param->decimal > 0)
				ret = 1;
			else if (param->decimal < 0)
				ret = -1;
			else
				ret = 0;
			break;
		}
		default:
			TODO;
	}
	return ret;
}

DECLARE_BUILTIN_FUNC(builtin_math_sign) {
#if 0
	struct value* ret;
	assert(n == 1);
	struct value* param = params[0];
	assert(param->kind == vk_number);
	ret = (struct value*) new_number_value_as_integer(
		handle_sign(
			(struct number_value*) param
		)
	);
	EXIT;
	return ret;
#endif
	TODO;
}
