
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include <debug.h>

#include <scope/push_builtin_func.h>

#include "arith/push_builtins.h"
#include "expo/push_builtins.h"
#include "tests/push_builtins.h"
#include "trig/push_builtins.h"

#include "abs.h"
#include "ceil.h"
#include "floor.h"
#include "round.h"
#include "sign.h"

#include "push_builtins.h"

int push_math_builtins(struct scope *scope) {
	int error = 0;
	ENTER;

	error = 0
		?: push_math_arith_builtins(scope)
		?: push_math_expo_builtins(scope)
		?: push_math_tests_builtins(scope)
		?: push_math_trig_builtins(scope)

		?: scope_push_builtin_func(scope, L"abs", 
			builtin_math_abs)
		?: scope_push_builtin_func(scope, L"ceil", 
			builtin_math_ceil)
		?: scope_push_builtin_func(scope, L"floor",
			builtin_math_floor)
		?: scope_push_builtin_func(scope,L"round",
			builtin_math_round)
		?: scope_push_builtin_func(scope, L"sign",
			builtin_math_sign)
		;

	EXIT;
	return error;
}










