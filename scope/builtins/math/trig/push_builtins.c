
#include <assert.h>
#include <stdio.h>

#include <debug.h>

#include <scope/push_builtin_func.h>

#include "acos.h"
#include "asin.h"
#include "atan.h"
#include "atan2.h"
#include "cos.h"
#include "hypot.h"
#include "sin.h"
#include "tan.h"

#include "push_builtins.h"

int push_math_trig_builtins(struct scope *scope) {
	int error = 0;
	ENTER;

	error = 0
		?: scope_push_builtin_func(scope, L"acos", 
			builtin_math_trig_acos)
		?: scope_push_builtin_func(scope, L"asin", 
			builtin_math_trig_asin)
		?: scope_push_builtin_func(scope, L"atan", 
			builtin_math_trig_atan)
		?: scope_push_builtin_func(scope, L"atan2",
			builtin_math_trig_atan2)
		?: scope_push_builtin_func(scope, L"cos", 
			builtin_math_trig_cos)
		?: scope_push_builtin_func(scope, L"hypot",
			builtin_math_trig_hypot)
		?: scope_push_builtin_func(scope, L"sin",
			builtin_math_trig_sin)
		?: scope_push_builtin_func(scope, L"tan",
			builtin_math_trig_tan)
		;

	EXIT;
	return error;
}
