
#include <assert.h>
#include <math.h>

#include <debug.h>

#include <value/number/new.h>
#include <value/number/struct.h>
#include <value/struct.h>

#include <scope/builtins/callback_header.h>

#include "hypot.h"

#if 0
// found at: http://www.codecodex.com/wiki/Calculate_an_integer_square_root
static signed long isqrt(signed long num) {
	if (0 == num) {
		return 0;
	}					   // Avoid zero divide
	int n = (num / 2) + 1; // Initial estimate, never low
	int n1 = (n + (num / n)) / 2;
	while (n1 < n) {
		n = n1;
		n1 = (n + (num / n)) / 2;
	}
	return n;
}
#endif

DECLARE_BUILTIN_FUNC(builtin_math_trig_hypot) {
#if 0
	struct value* ret;
	assert(n == 2);
	struct value* given_X = params[0];
	struct value* given_Y = params[1];
	assert(given_X->kind == vk_number);
	assert(given_Y->kind == vk_number);
	struct number_value* X = (struct number_value*) given_X;
	struct number_value* Y = (struct number_value*) given_Y;
	switch(X->kind)
	{
		case nvk_integer:
		{
			switch(Y->kind)
			{
				case nvk_integer:
				{
					signed long x = X->integer, y = Y->integer;
					ret = (struct value*) new_number_value_as_integer(
						isqrt(x * x + y * y)
					);
					break;
				}
				case nvk_decimal:
				{
					ret = (struct value*) new_number_value_as_decimal(
						hypotq(X->integer, Y->decimal));
					break;
				}
				default: TODO;
			}
			break;
		}
		case nvk_decimal:
		{
			switch(Y->kind)
			{
				case nvk_integer:
				{
					ret = (struct value*) new_number_value_as_decimal(
						hypotq(X->decimal, Y->integer));
					break;
				}
				case nvk_decimal:
				{
					ret = (struct value*) new_number_value_as_decimal(
						hypotq(X->decimal, Y->decimal));
					break;
				}
				default: TODO;
			}
			break;
		}
		default: TODO;
	}
	return ret;
#endif
	TODO;
}
