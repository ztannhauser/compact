
#include <assert.h>
#include <math.h>

#include <debug.h>

#include <value/number/new.h>
#include <value/number/struct.h>
#include <value/struct.h>

#include <scope/builtins/callback_header.h>

#include "sin.h"

DECLARE_BUILTIN_FUNC(builtin_math_trig_sin) {
#if 0
	struct value* ret;
	assert(n == 1);
	struct value* param = params[0];
	assert(param->kind == vk_number);
	struct number_value* spef = (struct number_value*) param;
	switch(spef->kind)
	{
		case nvk_integer:
		{
			switch(spef->integer)
			{
				case 0:
					ret = (struct value*) new_number_value_as_integer(0);
					break;
				default:
					ret = (struct value*)
						new_number_value_as_decimal(sinq(spef->integer));
					break;
			}
			break;
		}
		case nvk_decimal:
		{
			ret = (struct value*)
				new_number_value_as_decimal(sinq(spef->decimal));
			break;
		}
		default: TODO;
	}
	return ret;
#endif
	TODO;
}
