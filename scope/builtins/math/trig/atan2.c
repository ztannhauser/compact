
#include <assert.h>
#include <math.h>

#include <debug.h>

#include <value/number/new.h>
#include <value/number/struct.h>
#include <value/struct.h>

#include <scope/builtins/callback_header.h>

#include "atan2.h"

DECLARE_BUILTIN_FUNC(builtin_math_trig_atan2) {
#if 0
	struct value* ret;
	assert(n == 2);
	struct value* given_Y = params[0];
	struct value* given_X = params[1];
	assert(given_Y->kind == vk_number);
	assert(given_X->kind == vk_number);
	struct number_value* Y = (struct number_value*) given_Y;
	struct number_value* X = (struct number_value*) given_X;
	switch(Y->kind)
	{
		case nvk_decimal:
		{
			switch(X->kind)
			{
				case nvk_decimal:
				{
					ret = (struct value*) new_number_value_as_decimal(
						atan2q(Y->decimal, X->decimal));
					break;
				}
				default: TODO;
			}
			break;
		}
		default: TODO;
	}
	return ret;
#endif
	TODO;
}
