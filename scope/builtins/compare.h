
#include "value/struct.h"
#include "value/function/struct.h"

int default_compare(struct value* a, struct value* b)
	__attribute__ ((warn_unused_result));

int builtin_compare(
	size_t n, struct value** args,
	struct scope* scope,
	struct value** result)
	__attribute__ ((warn_unused_result));

