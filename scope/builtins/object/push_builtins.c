
#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#include <debug.h>

#include <scope/push_builtin_func.h>

#include "new.h"

#include "push_builtins.h"

int push_object_builtins(struct scope *scope) {
	int error = 0;
	ENTER;

	error = 0
		?: scope_push_builtin_func(scope, L"new",
			builtin_object_new)
		;

	EXIT;
	return error;
}
