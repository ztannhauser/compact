
#include <assert.h>
#include <stdlib.h>

#include <array/struct.h>
#include <avl/struct.h>
#include <debug.h>
#include <linkedlist/struct.h>

#include <call.h>

#include <value/delete.h>
#include <value/list/new.h>
#include <value/list/struct.h>
#include <value/object/element_compare.h>
#include <value/object/element_free.h>
#include <value/object/new.h>
#include <value/object/struct.h>
#include <value/struct.h>

#include <scope/builtins/callback_header.h>

#include <expression/evaluate.h>

#include "new.h"

DECLARE_BUILTIN_FUNC(builtin_object_new) {
#if 0
	assert(n >= 2);
	struct value* func = (generic_params[0]);
	assert(func->kind == vk_function);
	generic_params++, n--;
	for(size_t i = 0;i < n;i++)
	{
		assert(generic_params[i]->kind == vk_list);
	}
	struct list_value** params = generic_params;
	struct avl_tree values;
	avl_init_tree(&values, element_compare, element_free);
	for(size_t i = 0, m = params[0]->values.n;i < m;i++)
	{
		struct value* param_values[n];
		for(size_t j = 0;j < n;j++)
		{
			param_values[j] = array_index(params[j]->values, i, void*);
		}
		struct value* ret = call(func, n, param_values);
		verpv(ret);
		assert(ret->kind == vk_list);
		struct list_value* spef_ret = ret;
		assert(spef_ret->values.n == 2);
		struct value* gen_name_value =
			array_index(spef_ret->values, 0, struct value*);
		struct value* gen_value_value =
			array_index(spef_ret->values, 1, struct value*);
		assert(gen_name_value->kind == vk_string);
		gen_name_value->refcount++, gen_value_value->refcount++;
		delete_value(ret);
		struct object_value_element* ele = malloc(sizeof(*ele));
		ele->label = gen_name_value, ele->value = gen_value_value;
		avl_insert(&(values), ele);
	}
	ret = new_object_value(ok_generic, values);
#endif
	TODO;
}
