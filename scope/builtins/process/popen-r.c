

#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h> 
#include <unistd.h>

#include "debug.h"

#include <array/delete.h>
#include <array/free_elements.h>
#include <array/new.h>
#include <array/struct.h>

#include "exec/async_wp.h"

#include <value/fd/new.h>
#include <value/fd/struct.h>
#include <value/list/struct.h>
#include <value/string/struct.h>
#include <value/struct.h>

#include <scope/fetch/null.h>
#include <scope/builtins/callback_header.h>

#include "exec.h"

#include "popen-r.h"

DECLARE_BUILTIN_FUNC(builtin_process_popen_r)
{
	int error = 0;
	int pfd[2];
	pid_t pid;
	struct array command_args;
	ENTER;
	
	if(n == 1)
	{
		command_args = new_array(char*);
		error = build_args_list(args[0], &command_args);
		if(!error)
		{
			if(pipe2(pfd, O_CLOEXEC) == 0)
			{
				pid = async_wp((char**) command_args.data, NULL, pfd);
				
				if(close(pfd[1]) == 0)
					*result = (struct value*) new_fd_value(pfd[0],
						d_readable, pid);
				else
					perror("close"), error = 1;
			}
			else
			{
				TODO; // pipe() faild, print error message
				error = 1;
			}
			array_free_elements(&command_args), delete_array(&command_args);
		}
	}
	else
	{
		TODO;
	}
	
	EXIT;
	return error;
}









































