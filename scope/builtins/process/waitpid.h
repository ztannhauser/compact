

int push_waitpid_accessories(struct scope* scope)
	__attribute__ ((warn_unused_result));

#include <scope/builtins/callback_header.h>

DECLARE_BUILTIN_FUNC(builtin_process_waitpid);
