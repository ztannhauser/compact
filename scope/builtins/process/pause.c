

#include <assert.h>
#include <string.h>

#include <array/struct.h>
#include <debug.h>

#include <value/list/struct.h>
#include <value/string/struct.h>
#include <value/struct.h>

#include <scope/builtins/callback_header.h>

#include "pause.h"

DECLARE_BUILTIN_FUNC(builtin_process_pause) {
	TODO;
}
