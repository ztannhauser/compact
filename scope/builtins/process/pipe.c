

#include <assert.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>

#include <array/struct.h>
#include <debug.h>

#include <value/list/struct.h>
#include <value/pipe/new.h>
#include <value/string/struct.h>
#include <value/struct.h>

#include <scope/builtins/callback_header.h>

#include "pipe.h"

DECLARE_BUILTIN_FUNC(builtin_process_pipe) {
#if 0
	int fds[2];
	int pipe_ret = pipe2(fds, O_CLOEXEC);
	if(pipe_ret < 0)
	{
		perror("pipe");
		abort();
	}
	ret = new_pipe_value(fds);
#endif
	TODO;
}
