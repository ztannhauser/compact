
#include <assert.h>

#include <array/struct.h>
#include <debug.h>

#include <value/number/new.h>

#include "exec.h"

#include <scope/builtins/callback_header.h>

#include "async-v.h"

DECLARE_BUILTIN_FUNC(builtin_process_async_v) {
#if 0
	assert(n == 1);
	struct array args = new_array(char*);
	build_args_list(params[0], &args);
	pid_t pid = async_v((char**) args.data);
	delete_array(&args);
	ret = (struct value*) new_number_value_as_integer(pid);
#endif
	TODO;
}
