
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <array/struct.h>
#include <debug.h>

#include <value/boolean/struct.h>
#include <value/number/struct.h>
#include <value/struct.h>

#include <scope/fetch/null.h>
#include <scope/builtins/callback_header.h>

#include "exit.h"

DECLARE_BUILTIN_FUNC(builtin_process_exit) {
#if 0
	struct value* ret;
	ENTER;
	assert(0 <= n && n <= 1);
	int code;
	switch(n)
	{
		case 0:
		{
			code = 0;
			break;
		}
		case 1:
		{
			struct value* generic_code_value = params[0];
			switch(generic_code_value->kind)
			{
				case vk_boolean:
				{
					struct boolean_value* code_value =
						(struct boolean_value*) generic_code_value;
					code = (code_value->value) ? 1 : 0;
					break;
				}
				case vk_number:
				{
					struct number_value* code_value =
						(struct number_value*) generic_code_value;
					assert(code_value->kind == nvk_integer);
					code = code_value->integer;
					break;
				}
				default: TODO;
			}
			break;
		}
		default: TODO;
	}
	exit(code);
	TODO;
	ret = (struct value*) scope_fetch_null();
	EXIT;
	return ret;
#endif
	TODO;
}
