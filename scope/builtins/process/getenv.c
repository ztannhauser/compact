
#include <assert.h>
#include <stdio.h>
#include <string.h>

#include <debug.h>

#include <value/string/construct.h>
#include <value/string/struct.h>
#include <value/struct.h>

#include <scope/fetch/null.h>
#include <scope/builtins/callback_header.h>

#include "getenv.h"

DECLARE_BUILTIN_FUNC(builtin_process_getenv) {
	ENTER;
#if 0
	assert(n == 1);
	struct value* generic_variable = params[0];
	assert(generic_variable->kind == vk_string);
	struct string_value* variable = (struct string_value*) generic_variable;
	if(!variable->is_null_terminated)
		string_value_append_null(variable);
	char* value = getenv(variable->chars.data);
	verpvs(value);
	if(value)
	{
		ret = construct_string_value(value, strlen(value) + 1, true);
	}
	else
	{
		ret = scope_fetch_null();
	}
#endif
	TODO;
	EXIT;
}
