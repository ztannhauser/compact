

#include <assert.h>
#include <stdio.h>
#include <string.h>

#include <debug.h>

#include <exec/exec.h>

#include <array/delete.h>
#include <array/free_elements.h>
#include <array/new.h>
#include <array/push_n.h>
#include <array/struct.h>

#include <value/list/struct.h>
#include <value/number/new.h>
#include <value/string/struct.h>
#include <value/string/to_utf8_encoded.h>
#include <value/struct.h>
#include <scope/builtins/callback_header.h>

#include <scope/fetch/null.h>

#include "exec.h"

static const char *null = NULL;

int build_args_list(struct value *generic_list, struct array *args)
{
	struct value *ele;
	struct list_value *args_list;
	struct string_value *spef_ele;
	struct array converted;
	int error = 0;
	ENTER;
	
	if(generic_list->kind == vk_list)
	{
		args_list = (struct list_value *)generic_list;
		for (size_t i = 0, n = args_list->values.n;
			!error && i < n; i++)
		{
			ele = array_index(args_list->values, i, struct value *);
			if(ele->kind == vk_string)
			{
				spef_ele = (struct string_value *) ele;
				converted = string_value_to_utf8_encoded(spef_ele, true);
				verpvs(converted.data);
				array_push_n(args, &(converted.data));
			}
			else
			{
				fprintf(stderr, "exec: all arguments must be strings!\n");
				error = 1;
			}
		}

		if(!error && args->n == 0)
		{
			fprintf(stderr, "exec: command cannot be empty!\n");
			error = 1;
		}
		
		if(!error)
		{
			array_push_n(args, &null);
		}
	}
	else
	{
		fprintf(stderr, "exec: list must be a parameter!\n");
		error = 1;
	}
	EXIT;
	return error;
}

DECLARE_BUILTIN_FUNC(builtin_process_exec)
{
	int error = 0;
	struct array command_line;
	int wstatus;
	ENTER;
	if(n == 1)
	{
		command_line = new_array(char*);
		error = build_args_list(args[0], &command_line);
		if(!error)
		{
			error = exec(command_line.datacp, &wstatus);
			array_free_elements(&command_line);
			delete_array(&command_line);
		}
		if(!error)
		{
			*result = (struct value*) new_number_value_as_integer(wstatus);
		}
	}
	else
	{
		TODO;
	}
	EXIT;
	return error;
}

















