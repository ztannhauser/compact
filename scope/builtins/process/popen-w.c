

#include <assert.h>
#include <string.h>
#include <unistd.h>

#include "array/struct.h"
#include "debug.h"
#include "exec/async_wp.h"

#include <value/fd/new.h>
#include <value/fd/struct.h>
#include <value/list/struct.h>
#include <value/string/struct.h>
#include <value/struct.h>

#include <scope/fetch/null.h>
#include <scope/builtins/callback_header.h>

#include "exec.h"

#include "popen-w.h"

DECLARE_BUILTIN_FUNC(builtin_process_popen_w) {
#if 0
	assert(n == 1);
	struct array args = new_array(char*);
	build_args_list(params[0], &args);
	int pfd[2];
	pipe(pfd);
	(void) exec_wp((char**) args.data, pfd, NULL);
	close(pfd[0]);
	ret = (struct value*) new_fd_value(pfd[1], d_writeable, pid);
	array_free_elements(&args), delete_array(&args);
#endif
	TODO;
}
