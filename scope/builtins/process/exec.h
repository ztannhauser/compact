
#include "array/struct.h"

#include "value/struct.h"

#include <scope/builtins/callback_header.h>

int build_args_list(struct value *generic_list, struct array *args)
	__attribute__ ((warn_unused_result));

DECLARE_BUILTIN_FUNC(builtin_process_exec);
