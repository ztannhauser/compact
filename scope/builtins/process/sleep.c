
#include <assert.h>
#include <unistd.h>

#include <debug.h>

#include <value/number/struct.h>
#include <value/struct.h>

#include <scope/fetch/null.h>
#include <scope/builtins/callback_header.h>

#include "sleep.h"

DECLARE_BUILTIN_FUNC(builtin_process_sleep) {
#if 0
	struct value* generic_time_value = params[0];
	assert(generic_time_value->kind == vk_number);
	struct number_value* time_value = generic_time_value;
	switch(time_value->kind)
	{
		case nvk_integer:
		{
			sleep(time_value->integer);
			break;
		}
		case nvk_decimal:
		{
			usleep(time_value->decimal * 1000000);
			break;
		}
		default: TODO;
	}
	ret = scope_fetch_null();
#endif
	TODO;
}
