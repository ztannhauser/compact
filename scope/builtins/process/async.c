
#include <assert.h>

#include "debug.h"

#include "array/struct.h"
#include "exec/async.h"

#include <value/number/new.h>

#include "exec.h"

#include <scope/builtins/callback_header.h>

#include "async.h"

DECLARE_BUILTIN_FUNC(builtin_process_async) {
#if 0
	struct array args = new_array(char*);
	build_args_list(params[0], &args);
	pid_t pid = async(args.data);
	delete_array(&args);
	ret = new_number_value_as_integer(pid);
#endif
	TODO;
}
