

#include <fcntl.h>
#include <unistd.h>

#include <assert.h>
#include <string.h>

#include <array/struct.h>
#include <debug.h>

#include <value/fd/struct.h>
#include <value/list/struct.h>
#include <value/number/new.h>
#include <value/pipe/struct.h>
#include <value/string/struct.h>
#include <value/struct.h>

#include "exec.h"

#include <scope/builtins/callback_header.h>

#include "redirect.h"

#if 0
static int process_redirect_get_input_fd(size_t* n, struct value*** params)
{
	int ret;
	ENTER;
#if 0
	assert(*n >= 1);
	struct value* first = (*params)[0];
	switch(first->kind)
	{
		case vk_fd:
		{
			struct fd_value* spef = first;
			assert(spef->direction == d_readable);
			ret = spef->fd;
			(*n)--, (*params)++;
			break;
		}
		case vk_pipe:
		{
			ret = ((struct pipe_value*) first)->fds[0];
			(*n)--, (*params)++;
			break;
		}
		default:
		{
			ret = 0;
			break;
		}
	}
#endif
	TODO;
	EXIT;
	return ret;
}

static int process_redirect_get_output_fd(size_t* n, struct value*** params)
{
	int ret;
	ENTER;
#if 0
	assert(*n >= 1);
	struct value* last = (*params)[*n - 1];
	switch(last->kind)
	{
		case vk_fd:
		{
			struct fd_value* spef = last;
			assert(spef->direction == d_writeable);
			ret = spef->fd;
			(*n)--;
			break;
		}
		case vk_pipe:
		{
			ret = ((struct pipe_value*) last)->fds[1];
			(*n)--;
			break;
		}
		default:
		{
			ret = 1;
			break;
		}
	}
#endif
	TODO;
	EXIT;
	return ret;
}
#endif

DECLARE_BUILTIN_FUNC(builtin_process_redirect) {
#if 0
	int error;
	int in_fd = process_redirect_get_input_fd(&n, &params);
	int out_fd = process_redirect_get_output_fd(&n, &params);
	verpv(in_fd), verpv(out_fd);
	assert(n >= 1);
	size_t i, nm1 = n - 1;
	struct {int fds[2]} pipes[nm1];
	for(i = 0;i < nm1;i++)
	{
		error = pipe2(&(pipes[i].fds), O_CLOEXEC);
		verpv(pipes[i].fds[0]), verpv(pipes[i].fds[1]);
		if(error < 0) perror("pipe"), abort();
	}
	struct array args = new_array(char*);
	for(i = 0;i < n;i++)
	{
		int in = (i == 0) ? in_fd : pipes[i-1].fds[0];
		int out = (i == nm1) ? out_fd : pipes[i].fds[1];
		verpv(in), verpv(out);
		build_args_list(params[i], &args);
		pid_t child = exec_wfd(args.data, in, out);
		verpv(child);
		if(i == nm1)
		{
			ret = new_number_value_as_integer(child);
		}
		array_free_elements(&args), array_clear(&args);
	}
	for(i = 0;i < nm1;i++)
	{
		close(pipes[i].fds[0]), close(pipes[i].fds[1]);
	}
	delete_array(&args);
#endif
	TODO;
}
