
#include <assert.h>
#include <linux/limits.h>
#include <string.h>
#include <unistd.h>

#include <debug.h>

#include <value/string/construct.h>

#include <scope/builtins/callback_header.h>

#include "pwd.h"

DECLARE_BUILTIN_FUNC(builtin_process_pwd) {
#if 0
	char pwd[PATH_MAX];
	getcwd(pwd, PATH_MAX);
	ret = construct_string_value(pwd, strlen(pwd) + 1, true);
#endif
	TODO;
}
