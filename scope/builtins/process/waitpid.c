

#include <assert.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#include <debug.h>

#include <array/struct.h>

#include <scope/push_builtin_func.h>
#include <scope/builtins/callback_header.h>

#include <value/list/struct.h>
#include <value/number/new.h>
#include <value/number/struct.h>
#include <value/pipe/new.h>
#include <value/string/struct.h>
#include <value/struct.h>

#include "waitpid.h"

struct value *
builtin_process_waitpid_macros_WEXITSTATUS(size_t n, struct value **params) {
	struct value *ret;
	assert(n == 1);
	struct value *generic_status = params[0];
	assert(generic_status->kind == vk_number);
	struct number_value *status = (struct number_value *)generic_status;
	assert(status->kind == nvk_integer);
	ret = (struct value *)new_number_value_as_integer(
		WEXITSTATUS(status->integer));
	return ret;
}

struct value *builtin_process_waitpid_macros_WIFEXITED(size_t n,
													   struct value **params) {
	struct value *ret;
	assert(n == 1);
	struct value *generic_status = params[0];
	assert(generic_status->kind == vk_number);
	struct number_value *status = (struct number_value *)generic_status;
	assert(status->kind == nvk_integer);
	ret =
		(struct value *)new_number_value_as_integer(WIFEXITED(status->integer));
	return ret;
}

int push_waitpid_accessories(struct scope *scope) {
	int error = 0;
	ENTER;

	error = 0
		?: scope_push_builtin_func(scope, L"WEXITSTATUS",
			builtin_process_waitpid_macros_WEXITSTATUS)
		?: scope_push_builtin_func(scope, L"WIFEXITED",
			builtin_process_waitpid_macros_WIFEXITED)
		;

	EXIT;
	return error;
}

DECLARE_BUILTIN_FUNC(builtin_process_waitpid) {
#if 0
	struct value* ret;

	assert(n == 1);
	struct value* generic_pid = params[0];
	assert(generic_pid->kind == vk_number);
	struct number_value* pid = (struct number_value*) generic_pid;
	assert(pid->kind == nvk_integer);
	verpv(pid->integer);
	int wstatus;
	pid_t waitpid_ret = waitpid(pid->integer, &wstatus, 0);
	verpv(waitpid_ret);
	if(waitpid_ret < 0) perror("waitpid"), abort();
	HERE;
	ret = (struct value*) new_number_value_as_integer(wstatus);
	return ret;
#endif
	TODO;
}
