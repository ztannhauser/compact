
#include <assert.h>
#include <stdio.h>
#include <string.h>

#include <deps/exec/exec_v.h>

#include <debug.h>

#include <array/delete.h>
#include <array/free_elements.h>
#include <array/new.h>
#include <array/struct.h>

#include <value/list/struct.h>
#include <value/number/new.h>
#include <value/string/struct.h>
#include <value/struct.h>

#include <scope/fetch/null.h>
#include <scope/builtins/callback_header.h>

#include "exec.h"

#include "exec-v.h"

DECLARE_BUILTIN_FUNC(builtin_process_exec_v)
{
	int error = 0;
	int exit_code;
	struct array command_args;
	ENTER;
	if(n == 1)
	{
		command_args = new_array(char*);
		error = 0
			?: build_args_list(args[0], &command_args)
			?: exec_v(command_args.datacp, &exit_code);
		
		if(!error)
		{
			*result = (struct value*) new_number_value_as_integer(exit_code);
		}
		array_free_elements(&command_args), delete_array(&command_args);
	}
	else
	{
		fprintf(stderr, "exec-v(): requires one list of strings (command)!\n");
		error = 1;
	}
	EXIT;
	return error;
}















