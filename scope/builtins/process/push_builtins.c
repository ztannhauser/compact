
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include "debug.h"

#include "scope/push_builtin_func.h"

#include "async-v.h"
#include "async.h"
#include "exec.h"
#include "exec-v.h"
#include "exit.h"
#include "find.h"
#include "find-exec.h"
#include "getenv.h"
#include "pause.h"
#include "pipe.h"
#include "popen-r.h"
#include "popen-w.h"
#include "pwd.h"
#include "redirect.h"
#include "sleep.h"
#include "waitpid.h"

#include "push_builtins.h"

int push_process_builtins(struct scope *scope) {
	int error = 0;
	ENTER;

	error = 0
		?: push_waitpid_accessories(scope)
		?: scope_push_builtin_func(scope, L"async", builtin_process_async)
		?: scope_push_builtin_func(scope, L"async-v", builtin_process_async_v)
		?: scope_push_builtin_func(scope, L"exec", builtin_process_exec)
		?: scope_push_builtin_func(scope, L"exec-v", builtin_process_exec_v)
		?: scope_push_builtin_func(scope, L"exit", builtin_process_exit)
		?: scope_push_builtin_func(scope, L"find", builtin_process_find)
		?: scope_push_builtin_func(scope, L"find-exec", builtin_process_find_exec)
		?: scope_push_builtin_func(scope, L"getenv", builtin_process_getenv)
		?: scope_push_builtin_func(scope, L"pause", builtin_process_pause)
		?: scope_push_builtin_func(scope, L"pipe", builtin_process_pipe)
		?: scope_push_builtin_func(scope, L"popen-r", builtin_process_popen_r)
		?: scope_push_builtin_func(scope, L"popen-w", builtin_process_popen_w)
		?: scope_push_builtin_func(scope, L"pwd", builtin_process_pwd)
		?: scope_push_builtin_func(scope, L"redirect", builtin_process_redirect)
		?: scope_push_builtin_func(scope, L"sleep", builtin_process_sleep)
		?: scope_push_builtin_func(scope, L"waitpid", builtin_process_waitpid)
		;

	EXIT;
	return error;
}


















