
int builtin_verpv(
	size_t n, struct value** args,
	struct scope* scope,
	struct value** result);
