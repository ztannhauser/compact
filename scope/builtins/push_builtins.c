
#include <assert.h>
#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#include <debug.h>

#include <value/boolean/new.h>
#include <value/fd/new.h>
#include <value/function/new.h>
#include <value/null/new.h>
#include <value/number/new.h>

#include <scope/push.h>
#include <scope/push_builtin_func.h>

#include "cast/push_builtins.h"
#include "io/push_builtins.h"
#include "list/push_builtins.h"
#include "loops/push_builtins.h"
#include "math/push_builtins.h"
#include "misc/push_builtins.h"
#include "object/push_builtins.h"
#include "process/push_builtins.h"
#include "random/push_builtins.h"
#include "string/push_builtins.h"
#include "strings_and_lists/push_builtins.h"
#include "time/push_builtins.h"

#include "call.h"
#include "compare.h"
#include "print.h"
#include "verpv.h"

#include "push_builtins.h"

int push_builtins(struct scope *scope) {
	int error = 0;
	ENTER;

	scope->null_value = new_null_value();
	scope->true_value = new_boolean_value(true);
	scope->false_value = new_boolean_value(false);
	scope->compare_value =
		new_function_value_as_builtin(L"compare", builtin_compare);

	error =
		0
		?: push_cast_builtins(scope)
		?: push_io_builtins(scope)
		?: push_list_builtins(scope)
		?: push_loops_builtins(scope)
		?: push_math_builtins(scope)
		?: push_misc_builtins(scope)
		?: push_object_builtins(scope)
		?: push_process_builtins(scope)
		?: push_random_builtins(scope)
		?: push_string_builtins(scope)
		?: push_string_and_list_builtins(scope)
		?: push_time_builtins(scope)
		
		
		?: scope_push_builtin_func(scope, L"call", 
			builtin_call)
		?: scope_push_builtin_func(scope, L"print", 
			builtin_print)
		?: scope_push_builtin_func(scope, L"verpv", 
			builtin_verpv)

		?: scope_push(scope, L"null",
			(struct value*) scope->null_value, true)
		?: scope_push(scope, L"true",
			(struct value*) scope->true_value, true)
		?: scope_push(scope, L"false",
			(struct value*) scope->false_value, true)
		?: scope_push(scope, L"compare",
			(struct value*) scope->compare_value, true)
		
		?: scope_push(scope, L"stdin", 
			(struct value*) new_fd_value(0, d_readable, 0), false)
		?: scope_push(scope, L"stdout", 
			(struct value*) new_fd_value(1, d_writeable, 0), false)
		?: scope_push(scope, L"stderr", 
			(struct value*) new_fd_value(2, d_writeable, 0), false)
		?: scope_push(scope, L"LONG_MAX",  
			(struct value*) new_number_value_as_integer(LONG_MAX), false)
		?: scope_push(scope, L"LONG_MIN",  
			(struct value*) new_number_value_as_integer(LONG_MIN), false)
		?: scope_push(scope, L"NaN",  
			(struct value*) new_number_value_as_decimal(NAN), false)
		?: scope_push(scope, L"Inf",  
			(struct value*) new_number_value_as_decimal(INFINITY), false)
		?: scope_push(scope, L"M_E",  
			(struct value*) new_number_value_as_decimal(M_Eq), false)
		?: scope_push(scope, L"M_PI",  
			(struct value*) new_number_value_as_decimal(M_PIq), false)
		?: scope_push(scope, L"M_SQRT2",  
			(struct value*) new_number_value_as_decimal(M_SQRT2q), false)
		;

	EXIT;
	return error;
}
