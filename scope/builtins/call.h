
int builtin_call(
	size_t n, struct value** args,
	struct scope* scope,
	struct value** result)
	__attribute__ ((warn_unused_result));
