
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include <debug.h>

#include <call.h>

#include <scope/builtins/callback_header.h>

#include <value/list/struct.h>
#include <value/struct.h>

#include "call.h"

DECLARE_BUILTIN_FUNC(builtin_call) {
	int error;
#if 0
	ENTER;
	assert(n == 2);
	
	struct value* generic_func = params[0];
	assert(generic_func->kind == vk_function);
	struct function_value* func = (struct function_value*) generic_func;
	
	struct value* generic_params = params[1];
	assert(generic_params->kind == vk_list);
	struct list_value* func_params = (struct list_value*) generic_params;
	
	ret = call(func, func_params->values.n,
		(struct value**) func_params->values.datavp);
	verpv(ret);
#endif
	TODO;
	EXIT;
	return error;
}
