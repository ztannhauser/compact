
#include <assert.h>
#include <stdlib.h>
#include <quadmath.h>
#include <stdio.h>

#include "debug.h"
#include "defines.h"

#include <scope/builtins/callback_header.h>

#include <array/struct.h>

#include <scope/fetch/boolean.h>

#include <value/struct.h>
#include <value/list/struct.h>
#include <value/boolean/struct.h>
#include <value/string/struct.h>
#include <value/number/struct.h>
#include <value/print.h>

#include "bool.h"

bool as_bool(struct value* param)
{
	bool ret;
	switch(param->kind)
	{
		case vk_null:
		{
			ret = false;
			break;
		};
		case vk_boolean:
		{
			ret = ((struct boolean_value*) param)->value;
			break;
		}
		case vk_number:
		{
			struct number_value* spef = (struct number_value*) param;
			switch(spef->kind)
			{
				case nvk_integer:
				{
					ret = (spef->integer != 0);
					break;
				}
				case nvk_long:
				{
					ret = mpz_sgn(spef->mpz_long) != 0;
					break;
				}
				case nvk_decimal:
				{
					ret = fabsq(spef->decimal) > QUAD_CLOSE_ENOUGH_TO_ZERO;
					break;
				}
				default: TODO;
			}
			break;
		}
		case vk_string:
		{
			struct string_value* spef = (struct string_value*) param;
			ret = !!spef->wchars.n;
			break;
		}
		case vk_list:
		{
			struct list_value* spef = (struct list_value*) param;
			ret = !!spef->values.n;
			break;
		}
		default: TODO;
	}
	return ret;
}

DECLARE_BUILTIN_FUNC(builtin_cast_bool)
{
	int error = 0;
	ENTER;
	if(n == 1)
	{
		*result = (struct value*) scope_fetch_boolean(scope, as_bool(args[0]));
	}
	else
	{
		fprintf(stderr, "bool(): requires just one argument\n");
		error = 0;
	}
	EXIT;
	return error;
}



















