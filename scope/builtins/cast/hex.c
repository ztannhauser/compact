
#include <assert.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include <debug.h>

#include <array/struct.h>
#include <array/new.h>
#include <array/mass_push_n.h>

#include <scope/builtins/callback_header.h>

#include <value/struct.h>
#include <value/boolean/struct.h>
#include <value/number/struct.h>
#include <value/string/new.h>
#include <value/print.h>

#include "hex.h"

static const char
	*hexdecimal_prefix = "0x",
	*hex_digits = "0123456789ABCDEF",
	 minus_char = '-';

void build_hex(
	struct value* param,
	bool should_append_prefix,
	void (*push)(const char*, size_t))
{
	ENTER;
	switch(param->kind)
	{
		case vk_number:
		{
			struct number_value* spef = (struct number_value*) param;
			switch(spef->kind)
			{
				case nvk_integer:
				{
					void func(signed long x)
					{
						if(x > 0xF) func(x >> 4);
						push(&(hex_digits[x & 0xF]), 1);
					}
					signed long x = spef->integer;
					if(x < 0)
					{
						x = -x;
						push(&minus_char, 1);
					}
					if(should_append_prefix) push(hexdecimal_prefix, 2);
					func(x);
					break;
				}
				case nvk_long:
				{
					if(mpz_sgn(spef->mpz_long) < 0)
					{
						push(&minus_char, 1);
						if(should_append_prefix) push(hexdecimal_prefix, 2);
						mpz_ptr mp = malloc(sizeof(MP_INT));
						mpz_init(mp);
						mpz_abs(mp, spef->mpz_long);
						char* str = mpz_get_str(NULL, 16, mp);
						push(str, strlen(str));
						free(str);
						mpz_clear(mp), free(mp);
					}
					else
					{
						if(should_append_prefix) push(hexdecimal_prefix, 2);
						char* str = mpz_get_str(NULL, 16, spef->mpz_long);
						push(str, strlen(str));
						free(str);
					}
					break;
				}
				default: TODO;
			}
			break;
		}
		default: TODO;
	}
	EXIT;
}

DECLARE_BUILTIN_FUNC(builtin_cast_hex)
{
	#if 0
	struct value* ret;
	ENTER;
	assert(1 <= n && n <= 2);
	bool should_append_prefix = true;
	struct value* param = params[0];
	if(n == 2)
	{
		struct value* generic_append_prefix_value = params[1];
		assert(generic_append_prefix_value->kind == vk_boolean);
		struct boolean_value* append_prefix_value =
			(struct boolean_value*) generic_append_prefix_value;
		should_append_prefix = append_prefix_value->value;
	}
	struct array chars = new_array(char);
	void callback(const char* ptr, size_t len)
	{
		array_mass_push_n(&chars, ptr, len);
	}
	build_hex(param, should_append_prefix, callback);
	#if 0
	ret = (struct value*) new_string_value(chars, false);
	#endif
	TODO;
	EXIT;
	return ret;
	#endif
	TODO;
}










