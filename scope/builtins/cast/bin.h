
#include "value/struct.h"

#include <scope/builtins/callback_header.h>

void build_bin(struct value* val, void (*push)(const char*, size_t));

DECLARE_BUILTIN_FUNC(builtin_cast_bin);
