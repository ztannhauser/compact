

void build_hex(
	struct value* param,
	bool should_append_prefix,
	void (*push)(const char*, size_t));

DECLARE_BUILTIN_FUNC(builtin_cast_hex);
