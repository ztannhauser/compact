
#include <assert.h>
#include <stdlib.h>
#include <stdio.h>

#include <debug.h>
#include <array/struct.h>

#include <scope/builtins/callback_header.h>

#include <value/struct.h>
#include <value/number/new.h>
#include <value/string/struct.h>
#include <value/list/new.h>

#include "ord.h"

DECLARE_BUILTIN_FUNC(builtin_cast_ord)
{
	#if 0
	struct value* ret;
	ENTER;
	assert(n == 1);
	struct value* param = params[0];
	switch(param->kind)
	{
		case vk_string:
		{
			struct string_value* string = (struct string_value*) param;
			assert(string->wchars.n == 1);
			ret = (struct value*) new_number_value_as_integer(
				array_index(string->wchars, 0, wchar_t)
			);
			break;
		}
		default: TODO;
	}
	EXIT;
	return ret;
	#endif
	TODO;
}








