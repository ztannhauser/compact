
#include <assert.h>
#include <stdlib.h>
#include <stdio.h>

#include <debug.h>
#include <array/struct.h>

#include <scope/fetch/boolean.h>

#include <scope/builtins/callback_header.h>

#include <value/struct.h>
#include <value/boolean/struct.h>
#include <value/string/struct.h>
#include <value/number/struct.h>
#include <value/number/new.h>
#include <value/print.h>

#include "long.h"

DECLARE_BUILTIN_FUNC(builtin_cast_long)
{
	#if 0
	struct value* ret;
	ENTER;
	assert(n == 1);
	struct value* param = params[0];
	switch(param->kind)
	{
		case vk_boolean:
		{
			struct boolean_value* spef = (struct boolean_value*) param;
			mpz_ptr mp = malloc(sizeof(MP_INT));
			mpz_init_set_si(mp, spef->value ? 1 : 0);
			ret = (struct value*) new_number_value_as_long(mp);
			break;
		}
		case vk_number:
		{
			struct number_value* spef = (struct number_value*) param;
			switch(spef->kind)
			{
				case nvk_integer:
				{
					mpz_ptr mp = malloc(sizeof(MP_INT));
					mpz_init_set_si(mp, spef->integer);
					ret = (struct value*) new_number_value_as_long(mp);
					break;
				}
				case nvk_decimal:
				{
					mpz_ptr mp = malloc(sizeof(MP_INT));
					mpz_init_set_d(mp, (double) spef->decimal);
					ret = (struct value*) new_number_value_as_long(mp);
					break;
				}
				case nvk_long:
				{
					ret = param;
					ret->refcount++;
					break;
				}
				default: TODO;
			}
			break;
		}
		case vk_string:
		{
			#if 0
			struct string_value* spef = (struct string_value*) param;
			if(!spef->is_null_terminated)
				string_value_append_null(spef);
			mpz_ptr mp = malloc(sizeof(MP_INT));
			assert(!mpz_init_set_str(mp, spef->chars.datac, 0));
			ret = (struct value*) new_number_value_as_long(mp);
			#endif
			TODO;
			break;
		}
		default: TODO;
	}
	EXIT;
	return ret;
	#endif
	TODO;
}









