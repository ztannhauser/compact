
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <debug.h>

#include <scope/builtins/callback_header.h>

#include <misc/push_string_as_wchar.h>

#include <array/mass_push_n.h>
#include <array/new.h>
#include <array/struct.h>

#include <value/number/struct.h>
#include <value/print.h>
#include <value/string/new.h>
#include <value/struct.h>

#include "bin.h"

#if 0
static const wchar_t
	*binary_prefix = L"0b",
	*bin_digits = L"01",
	 minus_char = '-';

static void build_bin(
	struct value* param,
	void (*push)(const wchar_t*, size_t))
{
	ENTER;
	switch(param->kind)
	{
		case vk_number:
		{
			struct number_value* spef = (struct number_value*) param;
			switch(spef->kind)
			{
				case nvk_integer:
				{
					void func(signed long x)
					{
						if(x > 0x1) func(x >> 1);
						push(&(bin_digits[x & 0x1]), 1);
					}
					signed long x = spef->integer;
					if(x < 0)
					{
						x = -x;
						push(&minus_char, 1);
					}
					push(binary_prefix, 2);
					func(x);
					break;
				}
				case nvk_long:
				{
					if(mpz_sgn(spef->mpz_long) < 0)
					{
						push(&minus_char, 1);
						push(binary_prefix, 2);
						mpz_ptr mp = malloc(sizeof(MP_INT));
						mpz_init(mp);
						mpz_abs(mp, spef->mpz_long);
						char* str = mpz_get_str(NULL, 2, mp);
						push_string_as_wchar(push, str);
						free(str);
						mpz_clear(mp), free(mp);
					}
					else
					{
						push(binary_prefix, 2);
						char* str = mpz_get_str(NULL, 2, spef->mpz_long);
						push_string_as_wchar(push, str);
						free(str);
					}
					break;
				}
				default: TODO;
			}
			break;
		}
		default: TODO;
	}
	EXIT;
}
#endif

DECLARE_BUILTIN_FUNC(builtin_cast_bin) {
#if 0
	struct value* ret;
	ENTER;
	assert(n == 1);
	struct value* param = params[0];
	struct array chars = new_array(wchar_t);
	void callback(const wchar_t* ptr, size_t len)
	{
		array_mass_push_n(&chars, ptr, len);
	}
	build_bin(param, callback);
	ret = (struct value*) new_string_value(chars);
	EXIT;
	return ret;
#endif
	TODO;
}
