
#include <assert.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#include <debug.h>

#include <array/struct.h>
#include <array/new.h>
#include <array/mass_push_n.h>

#include <scope/builtins/callback_header.h>

#include <value/struct.h>
#include <value/number/struct.h>
#include <value/string/new.h>
#include <value/print.h>

#include "oct.h"

static const char
	*octal_prefix = "0",
	 minus_char = '-',
	*oct_digits = "012345678";

void build_oct(struct value* param, void (*push)(const char*, size_t))
{
	ENTER;
	switch(param->kind)
	{
		case vk_boolean:
		{
			TODO;
			break;
		}
		case vk_number:
		{
			struct number_value* spef = (struct number_value*) param;
			switch(spef->kind)
			{
				case nvk_integer:
				{
					void func(signed long x)
					{
						if(x > 07) func(x >> 3);
						push(&(oct_digits[x & 0x7]), 1);
					}
					signed long x = spef->integer;
					if(x < 0)
					{
						x = -x;
						push(&minus_char, 1);
					}
					push(octal_prefix, 1);
					func(x);
					break;
				}
				case nvk_long:
				{
					if(mpz_sgn(spef->mpz_long) < 0)
					{
						push(&minus_char, 1);
						push(octal_prefix, 1);
						mpz_ptr mp = malloc(sizeof(MP_INT));
						mpz_init(mp);
						mpz_abs(mp, spef->mpz_long);
						char* str = mpz_get_str(NULL, 8, mp);
						push(str, strlen(str));
						free(str);
						mpz_clear(mp), free(mp);
					}
					else
					{
						push(octal_prefix, 1);
						char* str = mpz_get_str(NULL, 8, spef->mpz_long);
						push(str, strlen(str));
						free(str);
					}
					break;
				}
				default: TODO;
			}
			break;
		}
		default: TODO;
	}
	EXIT;
}

DECLARE_BUILTIN_FUNC(builtin_cast_oct)
{
	#if 0
	struct value* ret;
	ENTER;
	assert(n == 1);
	struct value* param = params[0];
	struct array chars = new_array(wchar_t);
	void callback(const char* ptr, size_t len)
	{
		array_mass_push_n(&chars, ptr, len);
	}
	build_oct(param, callback);
	#if 0
	ret = (struct value*) new_string_value(chars, false);
	#endif
	TODO;
	EXIT;
	return ret;
	#endif
	TODO;
}










