
#include <stdbool.h>

#include <value/struct.h>

#include <scope/builtins/callback_header.h>

bool as_bool(struct value* param)
	__attribute__ ((warn_unused_result));

DECLARE_BUILTIN_FUNC(builtin_cast_bool);
