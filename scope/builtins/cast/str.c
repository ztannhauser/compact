
#include <assert.h>
#include <stdlib.h>
#include <stdio.h>

#include <debug.h>

#include <scope/builtins/callback_header.h>

#include <array/struct.h>
#include <array/new.h>
#include <array/mass_push_n.h>

#include <value/struct.h>
#include <value/string/new.h>
#include <value/print.h>

#include "str.h"

DECLARE_BUILTIN_FUNC(builtin_cast_str)
{
	int error = 0;
	struct value* arg;
	struct array chars;
	ENTER;
	if(n == 1)
	{
		arg = args[0];
		chars = new_array(wchar_t);
		int callback(const wchar_t* ptr, size_t len)
		{
			array_mass_push_n(&chars, ptr, len);
			return 0;
		}
		error = value_print(arg, callback);
		if(!error)
		{
			*result = (struct value*) new_string_value(chars);
		}
	}
	else
	{
		fprintf(stderr, "str(): requires just one value!\n");
		error = 1;
	}
	EXIT;
	return error;
}

