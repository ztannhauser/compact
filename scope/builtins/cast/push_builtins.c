
#include <assert.h>
#include <stdio.h>

#include <debug.h>

#include <scope/push_builtin_func.h>

#include "units/push_builtins.h"

#include "bin.h"
#include "bool.h"
#include "char.h"
#include "float.h"
#include "hex.h"
#include "int.h"
#include "long.h"
#include "oct.h"
#include "ord.h"
#include "str.h"

#include "push_builtins.h"

int push_cast_builtins(struct scope *scope) {
	int error = 0;
	ENTER;

	error = 0
		?: push_cast_units_builtins(scope)
		?: scope_push_builtin_func(scope, L"bin", builtin_cast_bin)
		?: scope_push_builtin_func(scope, L"bool", builtin_cast_bool)
		?: scope_push_builtin_func(scope, L"char", builtin_cast_char)
		?: scope_push_builtin_func(scope, L"float", builtin_cast_float)
		?: scope_push_builtin_func(scope, L"hex", builtin_cast_hex)
		?: scope_push_builtin_func(scope, L"int", builtin_cast_int)
		?: scope_push_builtin_func(scope, L"long", builtin_cast_long)
		?: scope_push_builtin_func(scope, L"oct", builtin_cast_oct)
		?: scope_push_builtin_func(scope, L"ord", builtin_cast_ord)
		?: scope_push_builtin_func(scope, L"str", builtin_cast_str)
		;

	EXIT;
	return error;
}
