
#include <assert.h>
#include <stdio.h>

#include <debug.h>
#include <array/struct.h>

#include <scope/fetch/boolean.h>

#include <scope/builtins/callback_header.h>

#include <value/struct.h>
#include <value/boolean/struct.h>
#include <value/string/struct.h>
#include <value/number/struct.h>
#include <value/number/new.h>
#include <value/print.h>

#include "int.h"

DECLARE_BUILTIN_FUNC(builtin_cast_int)
{
	#if 0
	struct value* ret;
	ENTER;
	assert(n == 1);
	struct value* param = params[0];
	switch(param->kind)
	{
		case vk_boolean:
		{
			struct boolean_value* spef = (struct boolean_value*) param;
			ret = (struct value*)
				new_number_value_as_integer(spef->value ? 1 : 0);
			break;
		}
		case vk_number:
		{
			struct number_value* spef = (struct number_value*) param;
			switch(spef->kind)
			{
				case nvk_integer:
				{
					ret = param;
					ret->refcount++;
					break;
				}
				case nvk_decimal:
				{
					ret = (struct value*) new_number_value_as_integer(
						(signed long) spef->decimal);
					break;
				}
				case nvk_long:
				{
					assert(mpz_fits_slong_p(spef->mpz_long));
					ret = (struct value*) new_number_value_as_integer(
						mpz_get_si(spef->mpz_long));
					break;
				}
				default: TODO;
			}
			break;
		}
		case vk_string:
		{
			#if 0
			struct string_value* spef = (struct string_value*) param;
			if(!spef->is_null_terminated) string_value_append_null(spef);
			char* m;
			signed long val = strtol(spef->chars.datac, &m, 0);
			assert(!*m);
			verpv(val);
			ret = (struct value*) new_number_value_as_integer(val);
			#endif
			TODO;
			break;
		}
		default: TODO;
	}
	EXIT;
	return ret;
	#endif
	TODO;
}









