
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include <debug.h>

#include <scope/push_builtin_func.h>

#include "deg.h"
#include "rad.h"

#include "push_builtins.h"

int push_cast_units_builtins(struct scope *scope) {
	int error;
	ENTER;

	error = 0
		?: scope_push_builtin_func(scope, L"deg", builtin_cast_units_deg)
		?: scope_push_builtin_func(scope, L"rad", builtin_cast_units_rad)
		;
	EXIT;
	return error;
}
