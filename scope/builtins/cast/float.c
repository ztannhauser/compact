
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include <debug.h>

#include <array/pop_n.h>
#include <array/push_n.h>
#include <array/struct.h>

#include <scope/builtins/callback_header.h>

#include <scope/fetch/boolean.h>

#include <value/boolean/struct.h>
#include <value/number/new.h>
#include <value/number/struct.h>
#include <value/print.h>
#include <value/string/struct.h>
#include <value/struct.h>

#include "float.h"

DECLARE_BUILTIN_FUNC(builtin_cast_float) {
#if 0
	struct value* ret;
	ENTER;
	assert(n == 1);
	struct value* param = params[0];
	switch(param->kind)
	{
		case vk_boolean:
		{
			struct boolean_value* spef = (struct boolean_value*) param;
			ret = (struct value*)
				new_number_value_as_decimal(spef->value ? 1.0 : 0.0);
			break;
		}
		case vk_number:
		{
			struct number_value* spef = (struct number_value*) param;
			switch(spef->kind)
			{
				case nvk_integer:
				{
					ret = (struct value*) new_number_value_as_decimal(
						(__float128) spef->integer);
					break;
				}
				case nvk_long:
				{
					ret = (struct value*) new_number_value_as_decimal(
						mpz_get_d(spef->mpz_long));
					break;
				}
				case nvk_decimal:
				{
					ret = param;
					ret->refcount++;
					break;
				}
				default: TODO;
			}
			break;
		}
		case vk_string:
		{
#if 0
			struct string_value* spef = (struct string_value*) param;
			array_push_n(&(spef->chars), &nullchar);
			char* m;
			__float128 val = strtoflt128(spef->chars.datac, &m);
			assert(!*m);
			array_pop_n(&(spef->chars));
			verpv(val);
			ret = (struct value*) new_number_value_as_decimal(val);
#endif
			TODO;
			break;
		}
		default: TODO;
	}
	EXIT;
	return ret;
#endif
	TODO;
}
