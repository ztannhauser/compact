
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

#include <debug.h>

#include <scope/builtins/callback_header.h>

#include <array/struct.h>
#include <array/new.h>

#include <value/struct.h>
#include <value/number/struct.h>
#include <value/string/struct.h>
#include <value/string/new.h>

#include "char.h"

DECLARE_BUILTIN_FUNC(builtin_cast_char)
{
	#if 0
	struct value* ret;
	ENTER;
	assert(1 == n);
	struct value* param = params[0];
	switch(param->kind)
	{
		case vk_number:
		{
			struct number_value* spef = (struct number_value*) param;
			switch(spef->kind)
			{
				case nvk_integer:
				{
					wchar_t cp = spef->integer;
					struct array chars = new_array_from_data(&cp, 1, wchar_t);
					ret = (struct value*) new_string_value(chars);
					break;
				}
				default: TODO;
			}
			break;
		}
		default: TODO;
	}
	EXIT;
	return ret;
	#endif
	TODO;
}

