
#include <value/struct.h>
#include <value/function/struct.h>

int call_comparator(
	struct function_value *comparator,
	struct value *a,
	struct value *b,
	struct scope* scope,
	int *result)
	__attribute__ ((warn_unused_result));
