
#include <assert.h>
#include <err.h>
#include <fcntl.h>
#include <linux/memfd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/syscall.h>
#include <unistd.h>

#include <array/struct.h>
#include <debug.h>

#include <value/fd/new.h>
#include <value/string/struct.h>
#include <value/struct.h>

#include <scope/builtins/callback_header.h>

#include "stream.h"

DECLARE_BUILTIN_FUNC(builtin_misc_stream) {
	ENTER;
#if 0
	assert(n == 1);
	struct value* generic_string = params[0];
	assert(generic_string->kind == vk_string);
	struct string_value* string = generic_string;
	if(string->is_null_terminated)
		string_value_remove_null(string);
	
	int fd;
	if((fd = syscall(SYS_memfd_create, "stream", MFD_CLOEXEC)) < 0)
		err(1, "memfd");
	if(pwrite(fd, string->chars.data, string->chars.n, 0) < 0)
		err(1, "write");

	verpv(fd);
	
	ret = new_fd_value(fd, d_readable);
#endif
	TODO;
	EXIT;
}
