
#include <assert.h>
#include <readline/history.h>
#include <readline/readline.h>
#include <stdio.h>
#include <stdlib.h>

#include <array/struct.h>
#include <debug.h>

#include <scope/fetch/null.h>
#include <value/string/new.h>
#include <value/string/struct.h>
#include <value/struct.h>

#include <scope/builtins/callback_header.h>

#include "readline.h"

DECLARE_BUILTIN_FUNC(builtin_misc_readline) {
#if 0
	struct value* generic_promt = params[0];
	assert(generic_promt->kind == vk_string);
	struct string_value* promt = generic_promt;
#if 0
	array_push_n(&(promt->chars), &nullchar);
#endif
	TODO;
	char* line = readline(promt->chars.data);
	verpv(line);
	struct array chars;
	if(line)
	{
		chars = new_array_from_data(line, strlen(line) + 1, char);
		ret = new_string_value(chars, true);
	}
	else
	{
		ret = scope_fetch_null();
	}
#endif
	TODO;
}
