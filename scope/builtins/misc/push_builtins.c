
#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#include <debug.h>

#include <scope/push_builtin_func.h>

#include "inc.h"
#include "readline.h"
#include "stream.h"

#include "push_builtins.h"

int push_misc_builtins(struct scope *scope) {
	int error = 0;
	ENTER;

	error = 0
		?: scope_push_builtin_func(scope, L"inc",
			builtin_misc_inc)
		?: scope_push_builtin_func(scope, L"readline",
			builtin_misc_readline)
		?: scope_push_builtin_func(scope, L"stream",
			builtin_misc_stream)
		;

	EXIT;
	return error;
}
