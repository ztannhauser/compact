
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include <array/struct.h>
#include <debug.h>

#include <value/inc.h>
#include <value/new.h>
#include <value/number/new.h>
#include <value/number/struct.h>
#include <value/struct.h>

#include <scope/builtins/callback_header.h>

#include "inc.h"

static uintmax_t inc = 0;

DECLARE_BUILTIN_FUNC(builtin_misc_inc) {

	int error = 0;
	ENTER;
	
	if(n == 0)
	{
		*result = (struct value*) new_number_value_as_integer(inc++);
	}
	else
	{
		fprintf(stderr, "inc(): requires 0 arguments!\n");
		error = 1;
	}
	
	EXIT;
	return error;
}
