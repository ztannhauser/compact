
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include "debug.h"
#include "defines.h"

#include <array/struct.h>

#include <scope/builtins/callback_header.h>

#include <value/struct.h>
#include <value/list/struct.h>
#include <value/number/struct.h>
#include <value/number/new.h>

#include <scope/builtins/string/strcmp.h>

#include "compare.h"

static int default_number_compare(
	struct number_value* a,
	struct number_value* b)
{
	int ret;
	ENTER;
	switch(a->kind)
	{
		case nvk_integer:
		{
			switch(b->kind)
			{
				case nvk_integer:
				{
					if(a->integer > b->integer) ret = 1;
					else if(a->integer < b->integer) ret = -1;
					else ret = 0;
					break;
				}
				case nvk_long: TODO;
				case nvk_decimal:
				{
					if(fabsq(a->integer - b->decimal)
						< QUAD_CLOSE_ENOUGH_TO_ZERO)
						ret = 0;
					else if(a->integer > b->decimal) ret = 1;
					else if(a->integer < b->decimal) ret = -1;
					else ret = 0;
					break;
				}
				default: TODO;
			}
			break;
		}
		case nvk_long:
		{
			switch(b->kind)
			{
				case nvk_integer:
				{
					ret = mpz_cmp_si(a->mpz_long, b->integer);
					break;
				}
				case nvk_long:
				{
					verpv(a->mpz_long);
					verpv(b->mpz_long);
					ret = mpz_cmp(a->mpz_long, b->mpz_long);
					verpv(ret);
					break;
				}
				case nvk_decimal: TODO;
				default: TODO;
			}
			break;
		}
		case nvk_decimal:
		{
			switch(b->kind)
			{
				case nvk_integer:
				{
					TODO;
					break;
				}
				case nvk_decimal:
				{
					if(fabsq(a->decimal - b->decimal) < QUAD_CLOSE_ENOUGH_TO_ZERO)
						ret = 0;
					else if(a->decimal > b->decimal) ret = 1;
					else if(a->decimal < b->decimal) ret = -1;
					else ret = 0;
					break;
				}
				case nvk_long: TODO;
				default: TODO;
			}
			break;
		}
		default: TODO;
	}
	EXIT;
	return ret;
}

int default_compare(struct value* a, struct value* b)
{
	int ret;
	ENTER;
	verpv(a);
	verpv(b);
	ret = a->kind - b->kind;
	verpv(ret);
	if(!ret)
	{
		switch(a->kind)
		{
			case vk_number:
			{
				ret = default_number_compare(
					(struct number_value*) a,
					(struct number_value*) b);
				verpv(ret);
				break;
			}
			case vk_string:
			{
				ret = process_strcmp(
					(struct string_value*) a,
					(struct string_value*) b);
				break;
			}
			case vk_list:
			{
				struct list_value* spef_a = (struct list_value*) a;
				struct list_value* spef_b = (struct list_value*) b;
				for(
					size_t i = 0,
					a_n = spef_a->values.n, b_n = spef_b->values.n,
					n = a_n > b_n ? a_n : b_n;
					!ret && i < n;i++)
				{
					struct value* a_ele = array_index(spef_a->values, i, void*);
					struct value* b_ele = array_index(spef_b->values, i, void*);
					ret = default_compare(a_ele, b_ele);
				}
				break;
			}
			case vk_object:
			{
				#if 0
				switch( object type)
				{
					case timespec:
					{
						TODO;
						break;
					}
					case timeval:
					{
						call 'timercmp()'
						break;
					}
					default:
					{
						// look at python's object comparsion
						move through values, comparing
						break;
					}
				}
				#endif
				TODO;
			}
			default: TODO;
		}
	}
	EXIT;
	return ret;
}

DECLARE_BUILTIN_FUNC(builtin_compare)
{
	int error = 0;
	ENTER;
	
	error = 0
		?: n != 2
		;
	
	if(!error)
	{
		*result = (struct value*)
			new_number_value_as_integer(
				default_compare(args[0], args[1])
			);
	}
	
	EXIT;
	return error;
}




















