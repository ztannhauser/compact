
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <debug.h>

#include <write-to-stdout.h>

#include <scope/fetch/null.h>

#include <scope/builtins/callback_header.h>

#include <value/print.h>

#include "print.h"

DECLARE_BUILTIN_FUNC(builtin_print) {
	int error = 0;
	struct value* arg;
	ENTER;
	if(n == 1)
	{
		arg = args[0];
		
		error = 0
			?: value_print(arg, write_to_stdout)
			?: write(1, "\n", 1) < 1;
		
		if(!error)
		{
			*result = (struct value*) scope_fetch_null(scope);
		}
	}
	else
	{
		fprintf(stderr, "print: requires only one value\n");
		error = 1;
	}
	EXIT;
	return error;
}
