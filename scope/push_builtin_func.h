
#include <wchar.h>

#include <scope/struct.h>

#include <value/function/struct.h>

int scope_push_builtin_func(
	struct scope *scope,
	const wchar_t *name,
	void *ptr)
	__attribute__ ((warn_unused_result));
