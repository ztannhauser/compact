
#include <stdbool.h>

#include <value/struct.h>

#include <scope/struct.h>

int scope_push(
	struct scope *scope,
	const wchar_t *name,
	struct value *val,
	bool inc_refcount)
	__attribute__ ((warn_unused_result));
