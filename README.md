# compact

## TODO List:

### Short-Term:

 - [X] rename functions in parser from 'bison' to 'grammar'
 - [X] break binary_expression evaluator into subdirectory
 - [ ] add test cases and implementations for current builtin functions
 - [ ] create two seperate string datatypes: one for utf-8, another for bytes.
 - [ ] Implement function for 'ftw'/'nftw'
 - [ ] implement string functions:
   - [ ] **strlen(string)**
   - [ ] **startswith(string, prefix)**
   - [ ] **endswith(string, suffix)**
   - [ ] **contains(string, substr)**
 - [ ] implement object functions:
   - [ ] **name(func, list)**
   - [ ] **has(obj, name)**
   - [ ] **assign(dest-obj, src-objs ...)**
   - [ ] **keys(obj)**
   - [ ] **get(obj, name, default)**
 - [ ] implement list functions:
   - [ ] **contains(list, element)**
   - [ ] **count(list, element)**
   - [ ] **length(list)**
   - [ ] **reshape(list, lengths ...)**
 - [ ] implement setmath functions:
   - [ ] **union(lists ...)**
   - [ ] **intersection(lists ...)**
   - [ ] **difference(whole-list, lists-subtract ... )**
   - [ ] **cross-product(list)**

### Long-Term:

 - [X] remove external dependencies
 - [ ] put all variable declarations at the top of the function pointer
 - [ ] have proper error handling
 - [ ] remove all globals
