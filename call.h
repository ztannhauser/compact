
#include <scope/struct.h>

#include <value/struct.h>
#include <value/function/struct.h>

int call(
	struct function_value* func,
	size_t n,
	struct value** args,
	struct scope* scope,
	struct value** result)
	__attribute__ ((warn_unused_result));
