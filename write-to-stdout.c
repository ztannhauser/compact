
#include <assert.h>
#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <wchar.h>

#include <debug.h>

#include <utf-8/encode.h>

#include "write-to-stdout.h"

// encodes output before writing

int write_to_stdout(const wchar_t *w, size_t l) {
	int error = 0;
	ENTER;

	while (!error && l--) {
		char encoding[6];
		int strlen_encoding = utf8_encode(*w++, encoding);
		verpv(strlen_encoding);
		if (write(1, encoding, strlen_encoding) < strlen_encoding) {
			fprintf(stderr, "error on write: %s\n", strerror(errno));
			error = 1;
		}
	}

	EXIT;
	return error;
}
