

CC = gcc

CPPFLAGS += -I ./ -I ./deps
CPPFLAGS += -D_GNU_SOURCE

CFLAGS += -Wall
CFLAGS += -Werror
CFLAGS += -Wno-parentheses
CFLAGS += -Wno-unused-but-set-variable
CFLAGS += -O3

DFLAGS += -g
DFLAGS += -D DEBUGGING=1

LDLIBS += -lm
LDLIBS += -lgmp
LDLIBS += -lquadmath
LDLIBS += -lreadline
LDLIBS += -lgumbo

TARGET = compact

ARGS = '3 + 3'

# --gen-suppressions=yes

default: $(TARGET)

install: ~/bin/$(TARGET)

~/bin/$(TARGET): $(TARGET)
	cp $(TARGET) ~/bin/$(TARGET)

run: $(TARGET)
	./$(TARGET) $(ARGS)

valrun: $(TARGET)
	valgrind --gen-suppressions=yes ./$(TARGET) $(ARGS)

drun: $(TARGET).d
	./$(TARGET).d $(ARGS)

dvalrun: $(TARGET).d
	valgrind --gen-suppressions=yes ./$(TARGET).d $(ARGS)

test: $(TARGET)
	bash 'tests/run-tests.sh'

srclist.mk:
	find -type f -regex '.*\.c' | sort | sed 's/^/SRCS += /' > srclist.mk

include srclist.mk

OBJS = $(SRCS:.c=.o) 
DOBJS = $(SRCS:.c=.d.o) 
DEPENDS = $(SRCS:.c=.mk)

$(TARGET): $(OBJS) $(DEPS)
	$(CC) $(LDFLAGS) $(OBJS) $(LOADLIBES) $(LDLIBS) -o $@

$(TARGET).d: $(DOBJS) $(DEPS)
	$(CC) $(LDFLAGS) $(DOBJS) $(LOADLIBES) $(LDLIBS) -o $@

%.mk: %.c
	$(CPP) -MM -MT $@ $(CPPFLAGS) -MF $@ $< || (gedit $< && false)

%.o: %.c %.mk
	$(CC) -c $(CPPFLAGS) $(CFLAGS) $< -o $@ || (gedit $< && false)

%.d.o: %.c %.mk
	$(CC) -c $(DFLAGS) $(CPPFLAGS) $(CFLAGS) $< -o $@ || (gedit $< && false)

.PHONY: clean deep-clean

clean:
	rm $(OBJS) $(DEPENDS) srclist.mk $(TARGET)

deep-clean:
	find -type f -regex '.*\.mk' -delete
	find -type f -regex '.*\.o' -delete
	find -type f -executable -delete

include $(DEPENDS)






