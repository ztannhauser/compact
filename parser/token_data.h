
#ifndef TOKEN_DATA_H
#define TOKEN_DATA_H

#include <inttypes.h>
#include <gmp.h>

#include <array/struct.h>

union token_data
{
	intmax_t integer;
	mpz_ptr mpz_long;
	__float128 decimal;
	struct array string; // elements of type 'wchar_t'
	struct
	{
		wchar_t ident[256];
		int strlen_ident;
	};
};

#endif
