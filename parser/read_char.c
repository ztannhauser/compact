
#include <assert.h>
#include <stdbool.h>
#include <stdio.h>

#include <debug.h>

#include <utf-8/decode.h>
#include <utf-8/how_many.h>

#include "read_char.h"

int read_char(const char **str, wchar_t *current_char) {
	int error = 0;
	unsigned char c;
	ENTER;

	if ((c = **str)) {
		verprintf("not on the null terminator right now\n");
		verpv(c);
		(*str)++;
		if (c > 127) {
			TODO;
		} else {
			// it's ASCCI: easy!
			*current_char = c;
		}
	} else {
		verprintf("we've already hit the null terminator\n");
		*current_char = '\0';
	}

	verpv(*current_char);

	EXIT;
	return error;
}

#if 0
	unsigned char c = *line++;
	verpv(c);
	
	// is this char the start of a utf8 character?
	if(c > 127)
	{
		char utf8_character[6];
		int utf8_character_length = 0;
		
		// append the one character we have
		utf8_character[utf8_character_length++] = c;
		
		int how_many = utf8_how_many(c);
		
		// push all other chars for this escaped character
		for(i = 1;i < how_many;i++)
		{
			utf8_character[utf8_character_length++] = *line++;
		}
		
		// make conversion
		current_char = utf8_decode(utf8_character);
	}
	else
	{
	}
	verpv(current_char);
#endif
