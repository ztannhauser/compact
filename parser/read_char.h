
int read_char(
	const char** str,
	wchar_t* current_char)
		__attribute__ ((warn_unused_result))
		__attribute__((nonnull (1, 2)));
