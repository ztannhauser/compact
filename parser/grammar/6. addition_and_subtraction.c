
#include <assert.h>
#include <stdbool.h>
#include <stdio.h>

#include <debug.h>

#include <expression/binary/new.h>
#include <expression/binary/struct.h>

#include "../read_token.h"
#include "../token_id.h"

#include "5. multiply_divide_and_remainder.h"
#include "6. addition_and_subtraction.h"

static const enum binary_operation lookup[] = {
	[t_plus_char] = bo_addition,
	[t_minus_char] = bo_subtraction,
};

int grammar_addition_and_subtraction(const char **str, wchar_t *current_char,
									 enum token *current_token,
									 union token_data *current_token_data,
									 struct expression **result) {
	int error = 0;
	enum binary_operation bo;
	struct expression *next;
	ENTER;

	error = grammar_multiply_divide_and_remainder(
		str, current_char, current_token, current_token_data, result);

	verpv(error);

	while (!error && (*current_token == t_plus_char ||
					  *current_token == t_minus_char || false)) {
		bo = lookup[*current_token];

		// eat opeartor:
		error =
			read_token(str, current_char, current_token, current_token_data);

		if (!error) {
			error = grammar_multiply_divide_and_remainder(
				str, current_char, current_token, current_token_data, &next);

			if (!error) {
				*result = (struct expression *)new_binary_expression(
					bo, *result, next);
			}
		}
	}
	EXIT;
	return error;
}
