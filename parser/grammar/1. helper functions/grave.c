
#include <wchar.h>
#include <assert.h>
#include <stdio.h>
#include <stdbool.h>

#include <debug.h>

#include <linkedlist/struct.h>
#include <linkedlist/new.h>
#include <linkedlist/push_n.h>
#include <linkedlist/pop_n.h>

#include <expression/struct.h>
#include <expression/literal/new.h>

#include <value/function/new.h>

#include "../3. unary.h"
#include "../root.h"

#include "../../read_token.h"
#include "../../token_id.h"
#include "../../token_data.h"

#include "grave.h"

int helper_parse_grave(
	const char **str,
	wchar_t *current_char,
	enum token *current_token,
	union token_data *current_token_data,
	struct expression **result)
{
	int error = 0;
	struct linkedlist param_names;
	struct expression *inner;
	bool is_root_paren_expr;
	ENTER;
	
	// eat first '`' mark:
	{
		error = read_token(str, current_char,
			current_token, current_token_data);
	}

	if (!error)
	{
		param_names = new_linkedlist();
		if (*current_token == t_grave_accent_char)
		{
			error = read_token(str, current_char, current_token,
				current_token_data);
			if(!error)
			{
				linkedlist_push_n(&param_names, wcsdup(L"x"));
				if (*current_token == t_grave_accent_char)
				{
					error = read_token(str, current_char, current_token,
						current_token_data);
					if(!error)
					{
						linkedlist_push_n(&param_names, wcsdup(L"y"));
						if (*current_token == t_grave_accent_char)
						{
							error = read_token(str, current_char,
								current_token, current_token_data);
							if(!error)
							{
								linkedlist_push_n(&param_names, wcsdup(L"z"));
							}
						}
					}
				}
			}
		}
	}

	if(!error)
	{
		// catch paren expression
		if(*current_token == t_open_paren_char)
		{
			// eat open '(':
			{
				error = read_token(str, current_char, current_token,
					current_token_data);
			}
			
			is_root_paren_expr = true;
		}
		else
		{
			is_root_paren_expr = false;
		}
	}
		
	if(!error)
	{
		
		error = (is_root_paren_expr ? grammar_root : grammar_unary)
		(
			str,
			current_char,
			current_token,
			current_token_data,
			&inner
		);
		
	}
	
	if(!error && is_root_paren_expr)
	{
		// assert ')':
		{
			error = *current_token != t_close_paren_char;
		}
		
		// eat ')':
		if(!error)
		{
			error = read_token(str, current_char, current_token,
				current_token_data);
		}
	}
	
	if (!error)
	{
		*result = (struct expression *) new_literal_expression
		(
			(struct value *) new_function_value_as_userdef
			(
				param_names,
				inner
			)
		);
	}

	EXIT;
	return error;
}




















