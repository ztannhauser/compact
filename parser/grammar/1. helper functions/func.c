

#include <wchar.h>
#include <assert.h>
#include <stdio.h>

#include <debug.h>

#include <linkedlist/struct.h>
#include <linkedlist/new.h>
#include <linkedlist/push_n.h>

#include <expression/struct.h>
#include <expression/literal/new.h>
#include <value/function/new.h>

#include "../root.h"
#include "../../read_token.h"
#include "../../token_id.h"
#include "../../token_data.h"

#include "func.h"

int helper_parse_function(
	const char **str,
	wchar_t *current_char,
	enum token *current_token,
	union token_data *current_token_data,
	struct expression **result)
{
	int error = 0;
	wchar_t *param_name;
	struct expression *body;
	struct linkedlist param_names;
	ENTER;
	

	error = 0
		// read next token: (eat 'func' token)
		?: read_token(str, current_char, current_token, current_token_data)
		
		// error if the next token is not an open paren
		?: *current_token != t_open_paren_char &&
			fprintf(stderr, "There should be an '(' after 'func'!\n")
		
		// read next token: (eat open paren)
		?: read_token(str, current_char, current_token,
			current_token_data);

	if (!error)
	{
		param_names = new_linkedlist();

		while (!error && *current_token != t_close_paren_char)
		{
			error = (*current_token != t_ident);

			if (!error)
			{
				param_name = wcsdup(current_token_data->ident);

				linkedlist_push_n(&param_names, param_name);

				// eat identifer:
				{
					error = read_token(
						str, 
						current_char,
						current_token,
						current_token_data);
				}

				if (!error && *current_token == t_comma_char)
				{
					// eat comma:
					error = read_token(
						str, 
						current_char, 
						current_token,
						current_token_data);
				}

				if (!error && *current_token == t_eof)
				{
					fprintf(stderr, "Unexpected EOF while parsing "
									"parameter list for function!\n");
					error = 1;
				}
			}
		}
	}

	if(!error)
	{
		error = 0
			// Eat close paren:
			?: read_token(str, current_char, current_token,
				current_token_data)

			// next token better be an open curly:
			?: *current_token != t_open_curly_char &&
				fprintf(stderr,
					"There should be an '{' after function parameter list!\n")

			// Eat open curly:
			?: read_token(str, current_char,
				current_token,
				current_token_data)

			// parse body of function:
			?: grammar_root(str, current_char,
				current_token,
				current_token_data,
				&body)

			// After the body, there
			// better be a close curly:
			?: *current_token != t_close_curly_char &&
				fprintf(stderr, "There should be an '}' after function "
					"body! (provided: %i)\n", *current_token)

			// Eat close curly:
			?: read_token(
				str,
				current_char,
				current_token,
				current_token_data);

	}
	
	if (!error)
	{
		*result = (struct expression *) new_literal_expression(
			(struct value *) new_function_value_as_userdef(
				param_names,
				body));
	}

	EXIT;
	return error;
}

