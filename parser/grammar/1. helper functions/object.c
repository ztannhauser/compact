
#include <wchar.h>
#include <assert.h>
#include <stdio.h>

#include <debug.h>

#include <array/new.h>

#include <linkedlist/new.h>
#include <linkedlist/push_n.h>

#include <value/string/new.h>
#include <value/string/from_utf8_encoded.h>

#include <expression/struct.h>
#include <expression/object/new.h>
#include <expression/object/struct.h>

#include "../defines.h"
#include "../../token_names.h"
#include "../../read_token.h"
#include "../../token_id.h"
#include "../../token_data.h"

#include "object.h"

int helper_parse_object(
	const char **str,
	wchar_t *current_char,
	enum token *current_token,
	union token_data *current_token_data,
	struct expression **result)
{
	int error = 0;
	struct linkedlist subexpressions;
	struct object_expression_bundle* bun;
	ENTER;
	
	subexpressions = new_linkedlist();
	
	// eat '{':
	{
		error = read_token(str, current_char, current_token, current_token_data);
	}
	
	
	while(!error && *current_token != t_close_curly_char)
	{
		HERE;
		bun = malloc(sizeof(*bun));
		// read ident, or string
		// remember to write to strlen field in 'bun':
		{
			switch(*current_token)
			{
				case t_ident:
				{
					bun->label = new_string_value(
						new_array_from_data(
							current_token_data->ident,
							current_token_data->strlen_ident,
							wchar_t));
					break;
				}
				
				case t_string:
				{
					bun->label = new_string_value(current_token_data->string);
					break;
				}
				
				default:
				{
					fprintf(stderr,
						"copmact: expecting identifer or string before '%s'\n",
							token_names[*current_token]);
					TODO; // expecting ident or string, not <token name>
					error = 1;
				}
			}
		}
		
		if(!error)
		{
			error = 0
				// read next time (hopefully a colon)
				?: read_token(str, 
					current_char, current_token, current_token_data) &&
						fprintf(stderr, "Unmessaged error #1\n")
				// assert that it's a colon:
				?: *current_token != t_colon_char &&
						fprintf(stderr, "Unmessaged error #2\n")
				// eat colon:
				?: read_token(
					str, 
					current_char,
					current_token,
					current_token_data) &&
						fprintf(stderr, "Unmessaged error #3\n")
				// parse field value:
				?: grammar_above_comma_delimited(
					str,
					current_char,
					current_token,
					current_token_data,
					&(bun->subexpr));
		}
		
		if(!error)
		{
			// append to subexpressions
			linkedlist_push_n(&subexpressions, bun);
			if(*current_token == t_comma_char)
			{
				error = read_token(
					str, 
					current_char,
					current_token,
					current_token_data);
			}
		}
	}
	
	// eat '}':
	{
		error = read_token(str, current_char, current_token, current_token_data);
	}
	
	if(!error)
	{
		*result = (struct expression*) new_object_expression(subexpressions);
	}
	
	EXIT;
	return error;
}





















