
#include <wchar.h>
#include <assert.h>
#include <stdio.h>

#include <debug.h>

#include <expression/struct.h>
#include <expression/paren/new.h>

#include "../root.h"
#include "../../read_token.h"
#include "../../token_id.h"
#include "../../token_data.h"

#include "paren.h"

int helper_parse_paren(
	const char **str,
	wchar_t *current_char,
	enum token *current_token,
	union token_data *current_token_data,
	struct expression **result)
{
	int error = 0;
	struct expression* inner;
	ENTER;

	HERE;
	error = 0
		// beter have an '(':
		?: *current_token != t_open_paren_char
		
		// eat open paren:
		?: read_token(str, current_char, current_token,
			current_token_data) &&
				fprintf(stderr, "Error when reading after open paren!\n")

		// parse body of function:
		?: grammar_root(str, current_char, current_token,
			current_token_data, &inner);

	HERE;
	verpv(error);

	if (!error)
	{
		error = 0
			// we better be on a close paren:
			?: *current_token != t_close_paren_char &&
				fprintf(stderr, "Expected close paren\n")
			// prepare for next time:
			?: read_token(str, current_char, current_token,
				current_token_data);
		verpv(error);
	}

	if (!error)
	{
		*result = (struct expression *)
			new_paren_expression(inner);
	}
	
	EXIT;
	return error;
}






