
#include <wchar.h>
#include <assert.h>
#include <stdio.h>

#include <debug.h>

#include <linkedlist/new.h>
#include <linkedlist/push_n.h>

#include <expression/struct.h>
#include <expression/list/new.h>

#include "../defines.h"
#include "../../read_token.h"
#include "../../token_id.h"
#include "../../token_data.h"

#include "list.h"

int helper_parse_list(
	const char **str,
	wchar_t *current_char,
	enum token *current_token,
	union token_data *current_token_data,
	struct expression **result)
{
	int error = 0;
	struct linkedlist subexpressions;
	struct expression *subexpr;
	ENTER;
	
	
	subexpressions = new_linkedlist();
	
	// eat open square:
	{
		error = read_token(str, current_char, current_token, current_token_data);
	}

	// until error or close square:
	while (!error && *current_token != t_close_square_char)
	{
		error = grammar_above_comma_delimited(str,
			current_char, current_token,
			current_token_data, &subexpr);

		if(!error)
		{
			linkedlist_push_n(&subexpressions, subexpr);
		}

		if (!error && *current_token == t_comma_char)
		{
			error = read_token(
				str,
				current_char,
				current_token,
				current_token_data);
		}

		if (!error && *current_token == t_eof)
		{
			fprintf(stderr, "Unexpected EOF with parsing list!\n");
			error = 1;
		}
	}

	if (!error)
	{
		error = 0
			?: *current_token != t_close_square_char
			?: read_token(str, current_char, current_token, current_token_data);
	}
	
	if(!error)
	{
		*result = (struct expression *) new_list_expression(subexpressions);
	}


	EXIT;
	return error;
}

























