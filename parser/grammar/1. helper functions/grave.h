
int helper_parse_grave(
	const char **str,
	wchar_t *current_char,
	enum token *current_token,
	union token_data *current_token_data,
	struct expression **result);
