
#include <assert.h>
#include <stdio.h>

#include <debug.h>

#include <expression/binary/new.h>
#include <expression/binary/struct.h>

#include "../read_token.h"

#include "11. bitwise_xor.h"
#include "12. bitwise_or.h"

int grammar_bitwise_or(const char **str, wchar_t *current_char,
					   enum token *current_token,
					   union token_data *current_token_data,
					   struct expression **result) {
	int error = 0;
	ENTER;

	error = grammar_bitwise_xor(str, current_char, current_token,
								current_token_data, result);

	verpv(error);

	while (!error && *current_token == t_vertical_bar_char) {
#if 0
		read_token();
		struct expression* next = grammar_bitwise_xor();
		ret = (struct expression*)
			new_binary_expression(bo_bitwise_or, ret, next);
		verpv(ret);
		verpvc(current_token);
#endif
		TODO;
	}

	EXIT;
	return error;
}
