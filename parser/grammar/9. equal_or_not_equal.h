
#include <expression/struct.h>

#include "../token_id.h"
#include "../token_data.h"

int grammar_equal_or_not_equal(
	const char** str,
	wchar_t* current_char,
	enum token* current_token,
	union token_data* current_token_data,
	struct expression** result)
		__attribute__ ((warn_unused_result))
		__attribute__((nonnull (1, 2, 3, 4, 5)));
