
#include <assert.h>
#include <stdio.h>

#include <debug.h>

#include <expression/unary/new.h>

#include "../read_token.h"
#include "../token_id.h"

#include "2. function_call_or_index.h"
#include "3. unary.h"

static const enum unary_operation lookup[] = 
{
	[t_plus_char] = uo_numerical_plus,
	[t_tilda_char] = uo_bitwise_negate,
	[t_minus_char] = uo_numerical_negate,
	[t_exclamation_char] = uo_logical_negate,
};

int grammar_unary(
	const char **str,
	wchar_t *current_char,
	enum token *current_token,
	union token_data *current_token_data,
	struct expression **result)
{
	int error = 0;
	struct expression* inner;
	enum token operation;
	ENTER;

	switch (*current_token) {
		case t_plus_char:
		case t_minus_char:
		case t_exclamation_char:
		case t_tilda_char:
		{
			operation = *current_token;
			verpv(operation);
			error = read_token(str, current_char,
				current_token, current_token_data);
			if(!error)
			{
				error = grammar_unary(str, current_char, current_token,
					current_token_data, &inner);
			}
			if(!error)
			{
				*result = (struct expression*) new_unary_expression(lookup[operation], inner);
			}
			break;
		}
		default:
		{
			error = grammar_function_call(
				str, current_char, current_token,
				current_token_data, result);

			verpv(error);

			break;
		}
	}
	EXIT;
	return error;
}












