
#include <assert.h>
#include <stdbool.h>
#include <stdio.h>

#include <debug.h>

#include <expression/binary/new.h>
#include <expression/binary/struct.h>

#include "../read_token.h"

#include "3. unary.h"
#include "4. exponential.h"

int grammar_exponential(const char **str, wchar_t *current_char,
						enum token *current_token,
						union token_data *current_token_data,
						struct expression **result) {
	int error = 0;
	ENTER;

	error = grammar_unary(str, current_char, current_token, current_token_data,
						  result);

	verpv(error);

	while (!error && *current_token == t_double_asterick) {
#if 0
		read_token();
		struct expression* next = grammar_unary();
		ret = (struct expression*)
			new_binary_expression(bo_exponential, ret, next);
#endif
		TODO;
	}
	EXIT;
	return error;
}
