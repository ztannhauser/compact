
#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>

#include <debug.h>

#include <array/struct.h>

#include <linkedlist/new.h>
#include <linkedlist/push_n.h>
#include <linkedlist/struct.h>

#include <expression/literal/new.h>
#include <expression/variable/new.h>

#include <value/function/new.h>
#include <value/number/new.h>
#include <value/string/construct.h>
#include <value/string/new.h>

#include "../read_token.h"
#include "../token_names.h"

#include "1. helper functions/func.h"
#include "1. helper functions/grave.h"
#include "1. helper functions/list.h"
#include "1. helper functions/object.h"
#include "1. helper functions/paren.h"

#include "1. paren_idents_or_literals.h"

int grammar_paren_idents_or_idents(
	const char **str,
	wchar_t *current_char,
	enum token *current_token,
	union token_data *current_token_data,
	struct expression **result)
{
	int error;
	struct expression* base;
	ENTER;

	switch (*current_token)
	{
		case t_ident:
		{
			HERE;
			base = (struct expression *) new_variable_expression(
				(wchar_t *) &(current_token_data->ident));

			// prepare for next time:
			error = read_token(str, current_char, current_token,
							   current_token_data);
			HERE;
			break;
		}
		
		case t_integer:
		{
			HERE;
			base = (struct expression *) new_literal_expression(
				(struct value *) new_number_value_as_integer(
					current_token_data->integer));

			// prepare for next time:
			error = read_token(str, current_char, current_token,
							   current_token_data);
			break;
		}
		
		case t_mpz_long:
		{
			HERE;
			#if 0
			ret = (struct expression*) new_literal_expression(
				(struct value*) new_number_value_as_long(
					current_token_data.mpz_long
				)
			);
			verpv(ret);
			// prepare for next time:
			error = read_token(str, current_char,
				current_token, current_token_data);
			#endif
			TODO;
			break;
		}
		
		case t_decimal:
		{
			HERE;
			#if 0
			ret = (struct expression*) new_literal_expression(
			(struct value*) new_number_value_as_decimal(
				current_token_data.decimal
			)
			);
			verpv(ret);
			// prepare for next time:
			error = read_token(str, current_char,
			current_token, current_token_data);
			#endif
			TODO;
			break;
		}
		
		case t_string:
		{
			verpv(current_token_data->string.n);

			// create literal expression and the string it results in:
			{
				base = (struct expression *) new_literal_expression(
					(struct value *) new_string_value(
						current_token_data->string));
			}

			// prepare for next time:
			{
				error = read_token(
					str, 
					current_char, 
					current_token,
					current_token_data);
			}
			
			break;
		}
		
		case t_open_square_char:
		{
			
			error = helper_parse_list(
				str,
				current_char,
				current_token,
				current_token_data,
				&base);
			
			break;
		}
		
		case t_open_curly_char:
		{
			
			error = helper_parse_object(
				str,
				current_char,
				current_token,
				current_token_data,
				&base);
			
			break;
		}
		
		case t_func:
		{
			error = helper_parse_function(
				str,
				current_char,
				current_token,
				current_token_data,
				&base);
			
			break;
		}
		
		case t_grave_accent_char:
		{
			error = helper_parse_grave(
				str,
				current_char,
				current_token,
				current_token_data,
				&base);
			
			break;
		}
		
		case t_open_paren_char:
		{
			error = helper_parse_paren(
				str,
				current_char,
				current_token,
				current_token_data,
				&base);
			
			break;
		}
		
		default:
		{
			fprintf(stderr, "Unexpected token %s\n",
				token_names[*current_token]);
			error = 1;
			break;
		}
	}

	if(!error)
	{
		*result = base;
	}

	EXIT;
	return error;
}







