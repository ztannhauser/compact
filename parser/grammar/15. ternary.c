
#include <assert.h>
#include <stdio.h>

#include <debug.h>

#include <expression/ternary/new.h>

#include "../read_token.h"

#include "14. logical_or.h"
#include "15. ternary.h"

int grammar_ternary(
	const char **str,
	wchar_t *current_char,
	enum token *current_token,
	union token_data *current_token_data,
	struct expression **result)
{
	int error = 0;
	struct expression *true_case;
	struct expression *false_case;
	ENTER;

	error = grammar_logical_or(
		str, current_char, current_token,
		current_token_data, result);

	verpv(error);

	if (!error) {
		switch (*current_token) {
			case t_question_char: {
				// eat question mark:
				error = read_token(str, current_char, current_token,
								   current_token_data);

				if (!error) {
					error = 0
					// read 'true' case:
					?: grammar_logical_or(
						str, current_char, current_token,
						current_token_data, &true_case)

					// better be a colon afterwards:
					?: *current_token != t_colon_char

					// eat token:
					?: read_token(str, current_char,
						current_token,
						current_token_data)

					// read 'false' case:
					?: grammar_logical_or(
						str, current_char,
						current_token,
						current_token_data,
						&false_case);
				}

				if (!error) {
					*result = (struct expression *)new_ternary_expression(
						*result, true_case, false_case);
				}

				break;
			}
			case t_questioncolon: {
				// eat question mark:
				error = 0
					// eat question-colon:
					?: read_token(
						str, current_char, current_token,
						current_token_data)
					?: grammar_ternary(
						str, current_char, current_token,
						current_token_data, &false_case)
					;
				
				if(!error)
				{
					*result = (struct expression*)
						new_ternary_expression(*result, NULL, false_case);
				}
				break;
			}
			default: {
				// nothing to do!
				break;
			}
		}
	}
	EXIT;
	return error;
}
