
#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>

#include <array/struct.h>
#include <debug.h>
#include <linkedlist/new.h>
#include <linkedlist/push_n.h>
#include <linkedlist/struct.h>

#include <expression/delete.h>
#include <expression/call/new.h>
#include <expression/field/new.h>
#include <expression/index/new.h>

#include <value/string/construct.h>
#include <value/string/new.h>

#include "../read_token.h"

#include "1. paren_idents_or_literals.h"

#include "2. helper functions/call.h"
#include "2. helper functions/get_field.h"
#include "2. helper functions/indexing.h"

#include "2. function_call_or_index.h"

int grammar_function_call(
	const char **str,
	wchar_t *current_char,
	enum token *current_token,
	union token_data *current_token_data,
	struct expression **result)
{
	int error = 0;
	struct expression* working;
	ENTER;

	error = grammar_paren_idents_or_idents(
		str,
		current_char,
		current_token,
		current_token_data,
		&working);

	verpv(error);

	while (true
		&& !error
		&& (false
			|| *current_token == t_open_paren_char
			|| *current_token == t_open_square_char
			|| *current_token == t_period_char))
	{
		switch (*current_token)
		{
			case t_open_paren_char:
			{
				error = helper_parse_function_call(
					str, current_char, current_token,
					current_token_data, &working);
				
				break;
			}
			
			case t_open_square_char:
			{
				error = helper_parse_indexing(
					str, current_char, current_token,
					current_token_data, &working);
				
				break;
			}
			
			case t_period_char:
			{
				error = helper_parse_get_field(
					str, current_char, current_token,
					current_token_data, &working);
				
				break;
			}
			
			default: TODO;
			
		}
	}
	
	if(!error)
	{
		*result = working;
	}
	
	EXIT;
	return error;
}





