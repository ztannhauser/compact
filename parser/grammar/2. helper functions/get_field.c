

#include <wchar.h>
#include <assert.h>
#include <stdio.h>

#include <debug.h>

#include <array/new.h>

#include <value/string/new.h>

#include <expression/struct.h>
#include <expression/field/new.h>

#include "../../read_token.h"
#include "../../token_id.h"
#include "../../token_data.h"

#include "get_field.h"

int helper_parse_get_field(
	const char **str,
	wchar_t *current_char,
	enum token *current_token,
	union token_data *current_token_data,
	struct expression **result)
{
	int error = 0;
	ENTER;
	
	// eat dot:
	{
		error = read_token(str, current_char, current_token, current_token_data);
	}

	if (!error)
	{
		switch (*current_token)
		{
			case t_ident:
			{
				*result = (struct expression *) new_field_expression(
					*result,
					new_string_value(
						new_array_from_data(
							current_token_data->ident,
							current_token_data->strlen_ident,
							wchar_t)
						)
					);
				
				break;
			}
			
			case t_string:
			{
				*result = (struct expression *) new_field_expression(
					*result,
					new_string_value(current_token_data->string)
				);
				
				break;
			}
			
			default:
			{
				TODO;
			}
		}
	}

	if (!error) {
		// preare for next time:
		error = read_token(str, current_char, current_token,
			current_token_data);
	}

	EXIT;
	return error;
}
	


























