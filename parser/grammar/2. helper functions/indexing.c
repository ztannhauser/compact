

#include <wchar.h>
#include <assert.h>
#include <stdio.h>

#include <debug.h>

#include <expression/delete.h>
#include <expression/struct.h>
#include <expression/index/new.h>

#include "../root.h"
#include "../../read_token.h"
#include "../../token_id.h"
#include "../../token_data.h"

#include "indexing.h"

int helper_parse_indexing(
	const char **str,
	wchar_t *current_char,
	enum token *current_token,
	union token_data *current_token_data,
	struct expression **result)
{
	int error = 0;
	struct expression* index_expression = NULL;
	ENTER;
	
	error = 0
		// eat open square:
		?: read_token(str, current_char,
			current_token, current_token_data)
		// parse index expression:
		?: grammar_root(
			str, current_char, current_token, current_token_data,
			&index_expression)
		// after expression better be a close square:
		?: *current_token != t_close_square_char
		// eat close square:
		?: read_token(str, current_char,
			current_token, current_token_data)
		;
		
	if(error)
	{
		error = delete_expression(index_expression);
	}
	else
	{
		*result = (struct expression*)
			new_index_expression(*result, index_expression);
	}
	
	EXIT;
	return error;
}
	


























