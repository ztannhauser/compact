
#include <wchar.h>
#include <assert.h>
#include <stdbool.h>
#include <stdio.h>

#include <debug.h>

#include <linkedlist/struct.h>
#include <linkedlist/new.h>
#include <linkedlist/push_n.h>

#include <expression/struct.h>
#include <expression/call/new.h>
#include <expression/paren/new.h>

#include "../defines.h"
#include "../16. let_and_feedback.h"
#include "../../read_token.h"
#include "../../token_id.h"
#include "../../token_data.h"

#include "call.h"

int helper_parse_function_call(
	const char **str,
	wchar_t *current_char,
	enum token *current_token,
	union token_data *current_token_data,
	struct expression **result)
{
	int error = 0;
	struct linkedlist params;
	struct expression *param;
	ENTER;
	
	params = new_linkedlist();
	
	// eat '(':
	{
		error = read_token(str, current_char,
			current_token, current_token_data);
	}
	

	while(true
		&& !error
		&& *current_token != t_close_paren_char
		&& *current_token != t_eof)
	{
		
		error = grammar_above_comma_delimited(
			str,
			current_char,
			current_token,
			current_token_data,
			&param);
		
		if(!error)
		{
			linkedlist_push_n(&params, param);
		}
		
		if(!error && *current_token == t_comma_char)
		{
			error = read_token(
				str,
				current_char,
				current_token,
				current_token_data);
		}
		
		if(!error && *current_token == t_eof)
		{
			fprintf(stderr, "Unexpected EOF while reading "
				"function call parameters!\n");
			error = 1;
		}
	}

	if (!error)
	{
		error = 0
			// there better be a close paren:
			?: *current_token != t_close_paren_char &&
				fprintf(stderr, "There needs to be a ')' after function "
					"call!\n")
			// eat close paren:
			?: read_token(str, current_char, current_token,
				current_token_data);
	}
	
	if (!error)
	{
		*result = (struct expression *)
			new_call_expression(*result, params);
	}
	
	EXIT;
	return error;
}
	




















