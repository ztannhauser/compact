
#include <assert.h>
#include <stdio.h>
#include <string.h>

#include <debug.h>

#include <linkedlist/delete.h>
#include <linkedlist/new.h>
#include <linkedlist/push_n.h>
#include <linkedlist/struct.h>

#include <expression/delete.h>
#include <expression/feedback/new.h>
#include <expression/feedback/struct.h>
#include <expression/let/new.h>
#include <expression/let/struct.h>

#include "./defines.h"
#include "../read_token.h"

#include "15. ternary.h"
#include "16. let_and_feedback.h"

static int grammer_let(
	const char **str,
	wchar_t *current_char,
	enum token *current_token,
	union token_data *current_token_data,
	struct expression **result)
{
	int error = 0;
	struct expression *subexpr;
	struct let_expression_bundle *bun;
	ENTER;

	struct linkedlist bundles = new_linkedlist();

	// eat 'let' token:
	error = read_token(str, current_char, current_token,
					   current_token_data);

	if (!error && *current_token == t_eof) {
		fprintf(stderr,
				"Unexpected EOF when parsing 'let' expression\n");
		error = 1;
	}

	if (!error)
	{
		while (!error && *current_token != t_colon_char)
		{
			HERE;
			bun = malloc(sizeof(*bun));
			verpv(bun);

			verpv(error);
			
			verpv(*current_token);
			verpv(t_ident);
			
			// the next thing better be an ident:
			error = (*current_token != t_ident);

			HERE;
			verpv(error);
			
			if (!error)
			{
				wcscpy(bun->name, current_token_data->ident);

				error = 0
					// read next token: (eat ident)
					?: read_token(str, current_char, current_token,
						current_token_data) &&
							fprintf(stderr, "messageless error 1\n")

					// error if the next token isn't '=':
					?: *current_token != t_equals_char &&
						fprintf(stderr, "messageless error 2\n")

					// read next token: (eat '=')
					?: read_token(str, current_char,
						current_token,
						current_token_data) &&
							fprintf(stderr, "messageless error 3\n")

					// parse expression:
					?: grammar_above_comma_delimited(
						str, current_char,
						current_token,
						current_token_data,
						&(bun->subexpr));
			}

			HERE;
			verpv(error);
			
			verpv(*current_token);
			verpv(t_comma_char);
			
			if (!error)
			{
				HERE;

				// append to subexpressions
				linkedlist_push_n(&bundles, bun);
				
				HERE;
				// if there's a comma, eat it
				if (*current_token == t_comma_char)
				{
					HERE;
					error = read_token(str, current_char, current_token,
									   current_token_data);
				}
			}

			HERE;
			verpv(error);

			if (!error)
			{
				// if there's EOF: error
				if (*current_token == t_eof)
				{
					fprintf(stderr, "Unexpected EOF when parsing let "
									"expression!\n");
					error = 1;
				}
			}

			HERE;
			verpv(error);
		}

		// done reading variables '=' expression
		HERE;
		verpv(error);
	}

	HERE;
	verpv(error);

	if (!error) {
		error = 0
			// eat colon token:
			?: read_token(str, current_char, current_token,
			current_token_data)

			// read subexpression:
			?: grammar_above_comma_delimited(str, current_char, current_token,
			current_token_data, &subexpr);
		;
	}

	if (!error) {
		*result =
			(struct expression *)new_let_expression(bundles, subexpr);
	}

	EXIT;
	return error;
}

static int grammar_feedback(
	const char **str,
	wchar_t *current_char,
	enum token *current_token,
	union token_data *current_token_data,
	struct expression **result)
{
	int error = 0;
	struct expression* subexpr;
	struct expression* nth_expression;
	struct linkedlist names;
	struct linkedlist inital_expressions;
	wchar_t* varname;
	ENTER;
	names = new_linkedlist();
	inital_expressions = new_linkedlist();
	
	// eat 'static' token:
	error = read_token(str, current_char, current_token, current_token_data);
	
	// loop through inital expression:
	while(!error && *current_token != t_colon_char)
	{
		// read ident:
		{
			if(*current_token == t_ident)
			{
				varname = wcsdup(current_token_data->ident);
				linkedlist_push_n(&names, varname);
			}
			else
			{
				fprintf(stderr, "Error when parsing feedback expression: "
					"expected identifer, received %i\n", *current_token);
				error = 1;
			}
		}
		
		if(!error)
		{
			error = 0
				// eat ident token:
				?: read_token(str, current_char,
					current_token, current_token_data)
				// assert '=' token:
				?: (*current_token != t_equals_char)
				// eat '=' token:
				?: read_token(str, current_char,
					current_token, current_token_data)
				// parse subexpression:
				?: grammar_above_comma_delimited(str, current_char,
					current_token, current_token_data, &subexpr);
		}
		
		// append to subexpressions:
		if(!error)
		{
			linkedlist_push_n(&inital_expressions, subexpr);
		}
		
		// if there is a comma, eat it:
		if(!error && *current_token == t_comma_char)
		{
			error = read_token(str, current_char,
				current_token, current_token_data);
		}
	}
	
	// eat ':' token:
	if(!error)
	{
		error = 0
			?: read_token(str, current_char,
				current_token, current_token_data)
			?: grammar_above_comma_delimited(str, current_char, current_token,
				current_token_data, &nth_expression);
	}
	
	// if we made it this far, we create the expression:
	if(!error)
	{
		*result = (struct expression*) new_feedback_expression(
			names,
			inital_expressions,
			nth_expression);
	}
	
	EXIT;
	return error;
}

int grammar_let_and_feedback(
	const char **str,
	wchar_t *current_char,
	enum token *current_token,
	union token_data *current_token_data,
	struct expression **result)
{
	int error = 0;
	ENTER;
	switch(*current_token)
	{
		case t_let:
		{
			error = grammer_let(
				str,
				current_char,
				current_token,
				current_token_data,
				result);
			
			break;
		}

		case t_feedback:
		{
			error = grammar_feedback(
				str,
				current_char,
				current_token,
				current_token_data,
				result);
			
			break;
		}
		default:
		{
			error = grammar_ternary(
				str,
				current_char,
				current_token,
				current_token_data,
				result);
			
			break;
		}
	}

	EXIT;
	return error;
}
