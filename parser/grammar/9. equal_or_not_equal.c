#include <assert.h>
#include <stdio.h>

#include <debug.h>

#include <expression/binary/new.h>
#include <expression/binary/struct.h>

#include "../read_token.h"
#include "../token_id.h"

#include "8. comparison.h"
#include "9. equal_or_not_equal.h"

static const enum binary_operation lookup[] = {
	[t_equal_to] = bo_equal_to,
	[t_not_equal_to] = bo_not_equal_to,
};

int grammar_equal_or_not_equal(const char **str, wchar_t *current_char,
							   enum token *current_token,
							   union token_data *current_token_data,
							   struct expression **result) {
	int error = 0;
	enum binary_operation bo;
	struct expression *second;
	ENTER;

	error = grammar_comparison(str, current_char, current_token,
							   current_token_data, result);

	verpv(error);

	if (!error) {
		switch (*current_token) {
			case t_equal_to:
			case t_not_equal_to: {
				bo = lookup[*current_token];

				error = 0
							?: read_token(str, current_char, current_token,
										  current_token_data)
								   ?: grammar_comparison(
										  str, current_char, current_token,
										  current_token_data, &second);

				if (!error) {
					*result = (struct expression *)new_binary_expression(
						bo, *result, second);
				}

				break;
			}
			default: {
				// nothing to do
				break;
			}
		}
	}
	EXIT;
	return error;
}
