
#include <assert.h>
#include <debug.h>
#include <stdbool.h>
#include <stdio.h>

#include <expression/binary/new.h>
#include <expression/binary/struct.h>

#include "../read_token.h"
#include "../token_id.h"

#include "4. exponential.h"
#include "5. multiply_divide_and_remainder.h"

static const enum binary_operation lookup[] = {
	[t_asterisk_char] = bo_multiply,
	[t_slash_char] = bo_divide,
	[t_percent_char] = bo_remainder_divide,
};

int grammar_multiply_divide_and_remainder(const char **str,
										  wchar_t *current_char,
										  enum token *current_token,
										  union token_data *current_token_data,
										  struct expression **result) {
	int error = 0;
	struct expression *next;
	enum binary_operation bo;

	ENTER;

	error = grammar_exponential(str, current_char, current_token,
								current_token_data, result);

	verpv(error);

	while (!error && (*current_token == t_asterisk_char ||
					  *current_token == t_slash_char ||
					  *current_token == t_percent_char || false)) {
		bo = lookup[*current_token];
		verpv(bo);
		error =
			read_token(str, current_char, current_token, current_token_data);
		if (error) {
			// clean up
			TODO;
		} else {
			error = grammar_exponential(str, current_char, current_token,
										current_token_data, &next);
			if (error) {
				// clean up both
				TODO;
			} else {
				*result = (struct expression *)new_binary_expression(
					bo, *result, next);
			}
		}
	}
	EXIT;
	return error;
}
