#include <assert.h>
#include <debug.h>
#include <stdio.h>

#include <expression/binary/new.h>
#include <expression/binary/struct.h>

#include "../read_token.h"

#include "6. addition_and_subtraction.h"
#include "7. bitshift.h"

#if 0
static const enum binary_operation lookup[] = 
{
	[t_left_bitshift] = bo_leftshift,
	[t_right_bitshift] = bo_rightshift,
};
#endif

int grammar_bitshift(const char **str, wchar_t *current_char,
					 enum token *current_token,
					 union token_data *current_token_data,
					 struct expression **result) {
	int error = 0;
	ENTER;

	error = grammar_addition_and_subtraction(str, current_char, current_token,
											 current_token_data, result);

	verpv(error);

	if (!error) {
		switch (*current_token) {
#if 0
			case t_left_bitshift:
			case t_right_bitshift:
			{
				enum binary_operation bo = lookup[current_token];
				read_token();
				struct expression* second = grammar_addition_and_subtraction();
				ret = (struct expression*) new_binary_expression(
					bo, first, second);
				verpv(ret);
				break;
			}
#endif
			default: {
				// nothing to do,
				break;
			}
		}
	}
	EXIT;
	return error;
}
