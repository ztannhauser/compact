
#include <assert.h>
#include <stdio.h>

#include <debug.h>

#include <expression/comma/new.h>
#include <expression/comma/struct.h>

#include "../read_token.h"
#include "../token_data.h"
#include "../token_id.h"

#include "16. let_and_feedback.h"
#include "17. comma_delimited.h"

int grammar_comma_delimited(
	const char **str,
	wchar_t *current_char,
	enum token *current_token,
	union token_data *current_token_data,
	struct expression **result)
{
	int error = 0;
	struct expression *left, *right;
	ENTER;

	error = grammar_let_and_feedback(
		str,
		current_char,
		current_token,
		current_token_data,
		&left);

	while (!error && *current_token == t_comma_char)
	{
		// eat comma:
		error = 0
			?: read_token(str, current_char, current_token,
				current_token_data)
			?: grammar_comma_delimited(
				str, current_char, current_token,
				current_token_data, &right);
		
		if(!error)
		{
			left = (struct expression*) new_comma_expression(left, right);
		}
	}
	
	if(!error)
	{
		*result = left;
	}
	
	EXIT;
	return error;
}



















