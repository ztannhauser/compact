
#include <assert.h>
#include <stdio.h>

#include <debug.h>

#include <expression/binary/new.h>
#include <expression/binary/struct.h>

#include "../read_token.h"

#include "9. equal_or_not_equal.h"
#include "10. bitwise_and.h"

int grammar_bitwise_and(const char **str, wchar_t *current_char,
						enum token *current_token,
						union token_data *current_token_data,
						struct expression **result) {
	int error = 0;
	ENTER;

	error = grammar_equal_or_not_equal(str, current_char, current_token,
									   current_token_data, result);

	verpv(error);

	while (!error && *current_token == t_ampersand_char) {
#if 0
		read_token();
		struct expression* next = grammar_equal_or_not_equal();
		ret = (struct expression*)
			new_binary_expression(bo_bitwise_and, ret, next);
#endif
		TODO;
	}

	EXIT;
	return error;
}
