
#include <assert.h>
#include <stdio.h>

#include <debug.h>

#include <expression/binary/new.h>
#include <expression/binary/struct.h>

#include "../read_token.h"

#include "12. bitwise_or.h"
#include "13. logical_and.h"

int grammar_logical_and(const char **str, wchar_t *current_char,
						enum token *current_token,
						union token_data *current_token_data,
						struct expression **result) {
	int error = 0;
	struct expression* left, *right;
	ENTER;

	error = grammar_bitwise_or(str, current_char, current_token,
							   current_token_data, &left);

	verpv(error);

	while (!error && *current_token == t_logical_and)
	{
		error = 0
			?: read_token(str, current_char, current_token, current_token_data)
			?: grammar_bitwise_or(str, current_char, current_token, current_token_data, &right);

		if(!error)
		{
			left = (struct expression*)
				new_binary_expression(bo_logical_and, left, right);
		}
	}
	
	if(!error)
	{
		*result = left;
	}

	EXIT;
	return error;
}
