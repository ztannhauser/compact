
#include <assert.h>
#include <stdio.h>
#include <wchar.h>

#include <debug.h>

#include "../token_data.h"
#include "../token_id.h"

#include "17. comma_delimited.h"
#include "root.h"

int grammar_root(const char **str, wchar_t *current_char,
				 enum token *current_token,
				 union token_data *current_token_data,
				 struct expression **result) {
	int error = 0;
	ENTER;

	error = grammar_comma_delimited(
		str, current_char, current_token,
		current_token_data, result);

	verpv(error);
	
	EXIT;
	return error;
}
