
#include <assert.h>
#include <debug.h>
#include <stdio.h>

#include <expression/binary/new.h>
#include <expression/binary/struct.h>

#include "../read_token.h"
#include "../token_id.h"

#include "7. bitshift.h"
#include "8. comparison.h"

static const enum binary_operation lookup[] = 
{
	[t_less_than_char] = bo_less_than,
	[t_greater_than_char] = bo_greater_than,
	[t_less_than_equal_to] = bo_less_than_equal_to,
	[t_greater_than_equal_to] = bo_greater_than_equal_to,
};

int grammar_comparison(const char **str, wchar_t *current_char,
					   enum token *current_token,
					   union token_data *current_token_data,
					   struct expression **result) {
	int error = 0;
	enum binary_operation bo;
	struct expression* second;
	ENTER;

	error = grammar_bitshift(str, current_char, current_token,
							 current_token_data, result);

	verpv(error);

	if (!error) {
		switch (*current_token) {
			case t_less_than_char:
			case t_greater_than_char:
			case t_less_than_equal_to:
			case t_greater_than_equal_to:
			{
				bo = lookup[*current_token];
				error = read_token(str, current_char,
					current_token, current_token_data);
				if(!error)
				{
					error = grammar_bitshift(str, current_char,
						current_token, current_token_data, &second);
				}
				if(!error)
				{
					*result = (struct expression*) new_binary_expression(
						bo, *result, second);
				}
				break;
			}
			default: {
				// nothing to do
				break;
			}
		}
	}
	EXIT;
	return error;
}










