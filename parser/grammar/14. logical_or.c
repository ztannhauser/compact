
#include <assert.h>
#include <stdio.h>

#include <debug.h>

#include <expression/binary/new.h>
#include <expression/binary/struct.h>

#include "../read_token.h"

#include "13. logical_and.h"
#include "14. logical_or.h"

int grammar_logical_or(
	const char **str, 
	wchar_t *current_char,
	enum token *current_token,
	union token_data *current_token_data,
	struct expression **result)
{
	int error = 0;
	struct expression *base, *next;
	ENTER;

	error = grammar_logical_and(
		str,
		current_char, 
		current_token,
		current_token_data,
		&base);

	verpv(error);

	while (!error && *current_token == t_logical_or)
	{
		error = 0
			
			?: read_token(
				str,
				current_char,
				current_token,
				current_token_data)
			
			?: grammar_logical_and(
				str,
				current_char,
				current_token,
				current_token_data,
				&next);
		
		if(!error)
		{
			base = (struct expression*)
				new_binary_expression(bo_logical_or, base, next);
		}
	}

	if(!error)
	{
		*result = base;
	}
	
	EXIT;
	return error;
}






























