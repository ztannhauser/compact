
#ifndef TOKEN_ID_H
#define TOKEN_ID_H

enum token
{
	// characters
	t_comma_char,
	t_carrot_char,
	t_tilda_char,
	t_grave_accent_char,
	t_percent_char,
	t_plus_char,
	t_slash_char,
	t_minus_char,
	t_open_square_char,
	t_close_square_char,
	t_open_paren_char,
	t_close_paren_char,
	t_open_curly_char,
	t_close_curly_char,
	t_less_than_char,
	t_greater_than_char,
	t_asterisk_char,
	t_vertical_bar_char,
	t_equals_char,
	t_period_char,
	t_colon_char,
	t_ampersand_char,
	t_question_char,
	t_exclamation_char, // 24
	
	// tokens
	t_greater_than_equal_to,
	t_less_than_equal_to,
	t_double_asterick,
	t_right_bitshift,
	t_left_bitshift,
	t_questioncolon, // 30
	t_not_equal_to,
	t_logical_and,
	t_logical_or,
	t_mpz_long,
	t_feedback,
	t_equal_to,
	t_integer,
	t_decimal,
	t_string,
	t_ident, // 40
	t_func,
	t_let,
	t_eof,
	t_n
};

#endif

