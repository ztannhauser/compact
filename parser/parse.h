
int parse(
	const char* str,
	struct expression** ret)
		__attribute__ ((warn_unused_result))
		__attribute__((nonnull (1, 2)));
