
#include "token_id.h"

#include "token_names.h"

char* token_names[t_n] = 
{
	
	// characters:
	[t_comma_char] = ",",
	[t_carrot_char] = "^",
	[t_tilda_char] = "~",
	[t_grave_accent_char] = "`",
	[t_percent_char] = "%",
	[t_plus_char] = "+",
	[t_slash_char] = "/",
	[t_minus_char] = "-",
	[t_open_square_char] = "[",
	[t_close_square_char] = "]",
	[t_open_paren_char] = "(",
	[t_close_paren_char] = ")",
	[t_open_curly_char] = "{",
	[t_close_curly_char] = "}",
	[t_less_than_char] = "<",
	[t_greater_than_char] = ">",
	[t_asterisk_char] = "*",
	[t_vertical_bar_char] = "|",
	[t_equals_char] = "=",
	[t_period_char] = ".",
	[t_colon_char] = ":",
	[t_ampersand_char] = "&",
	[t_question_char] = "?",
	[t_exclamation_char] = "!",
	
	// other:
	[t_greater_than_equal_to] = ">=",
	[t_less_than_equal_to] = "<=",
	[t_double_asterick] = "**",
	[t_right_bitshift] = ">>",
	[t_left_bitshift] = "<<",
	[t_questioncolon] = "?:",
	[t_not_equal_to] = "!=",
	[t_logical_and] = "&&",
	[t_logical_or] = "||",
	[t_mpz_long] = "long",
	[t_feedback] = "feedback",
	[t_equal_to] = "==",
	[t_integer] = "integer",
	[t_decimal] = "decimal",
	[t_string] = "string",
	[t_ident] = "identifer",
	[t_func] = "function",
	[t_let] = "let",
	[t_eof] = "EOF",
};












































