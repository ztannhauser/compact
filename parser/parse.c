
#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <wchar.h>

#include <debug.h>

#include "grammar/root.h"

#include "./read_char.h"
#include "./read_token.h"
#include "./token_data.h"
#include "./token_id.h"

#include "parse.h"

int parse(const char *str, struct expression **result) {
	int error = 0;
	ENTER;

	wchar_t current_char;
	enum token current_token;
	union token_data current_token_data;

	error = 0
		?: error
		?: read_char(&str, &current_char)
		?: read_token(&str, &current_char, &current_token,
			&current_token_data)
		?: grammar_root(
			&str, &current_char, &current_token,
			&current_token_data, result);

	verpv(error);

	EXIT;
	return error;
}
