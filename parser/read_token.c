
#include <assert.h>
#include <errno.h>
#include <gmp.h>
#include <quadmath.h>
#include <stdio.h>
#include <string.h>

#include "debug.h"

#include "char/from_hex.h"
#include "char/is_wspace.h"

#include "array/delete.h"
#include "array/mass_push_n.h"
#include "array/new.h"
#include "array/push_n.h"
#include "array/struct.h"

#include "char/is_num.h"

#include "./read_char.h"
#include "./token_data.h"
#include "./token_id.h"

#include "read_token.h"

// static const char nullchar = '\0';

int read_number_token(const char **str, wchar_t *current_char,
					  enum token *current_token,
					  union token_data *current_token_data) {
	int error = 0;
	char current_char_as_char;
	bool number_contains_decmial_point = false;
	struct array number_buffer;
	char *strtol_remainder;

	ENTER;

	verpv(*current_char);
	number_buffer = new_array(char);
	// read into digits buffer (number_buffer)
	{
		for (; !error && is_num(*current_char);) {
			if (*current_char == '.') {
				if (number_contains_decmial_point) {
					fprintf(
						stderr,
						"There can be only one decimal point in a number!\n");
					error = 1;
				}
				number_contains_decmial_point = true;
			}

			if (!error)
				current_char_as_char = *current_char,
				array_push_n(&(number_buffer), &current_char_as_char);

			error = error ?: read_char(str, current_char);
		}
		if (!error) {
			array_push_n(&(number_buffer), ""); // if no error: append '\0'
		} else {
			delete_array(&(number_buffer)); // if error: free memory
		}
	}
	verpvs(number_buffer.data);
	if (!error) {
		bool enforced_long = false;
		if (*current_char == 'L') {
			enforced_long = true;
			error = read_char(str, current_char);
		}
		if (number_contains_decmial_point) {
#if 0
			// attempt to parse as quad
			char* m;
			current_token_data.decimal = strtoflt128(number_buffer.datac, &m);
			if(!*m)
			{
				current_token = t_decimal;
			}
			else
			{
				TODO;
			}
#endif
			TODO;
		} else {
			// unless overwritten by 'L',
			// attempt to read as integer
			// before mpz_long

			if (!enforced_long) {
				errno = 0;
				current_token_data->integer =
					strtol(number_buffer.datac, &strtol_remainder, 0);
				// look for error:
				switch (errno) {
					case 0:
						// everything went well,
						break;
					case ERANGE:
						enforced_long = true;
						break;
					default:
						perror("strtol"), error = 1;
						break;
				}
			}

			if (!error) {

				if (enforced_long) {
#if 0
					current_token_data.mpz_long =
						malloc(sizeof(__mpz_struct));
					mpz_init(current_token_data.mpz_long);
					int mpz_set_str_ret =
						mpz_set_str(
							current_token_data.mpz_long,
							number_buffer.datac, 0);
					if(mpz_set_str_ret == 0)
					{
						current_token = t_mpz_long;
					}
					else
					{
						mpz_clear(current_token_data.mpz_long);
						free(current_token_data.mpz_long);
						assert(!"Invaild integer!");
					}
#endif
					TODO;
				} else {
					verpv(current_token_data->integer);
					*current_token = t_integer;
				}
			}
		}
		delete_array(&(number_buffer));
	}
	EXIT;
	return error;
}

static int read_string_token(const char **str, wchar_t *current_char,
							 enum token *current_token,
							 union token_data *current_token_data) {
	int error = 0;
	wchar_t pushme;
	ENTER;

	*current_token = t_string;
	current_token_data->string = new_array(wchar_t);

	// eat open quote:
	error = read_char(str, current_char);

	if (!error) {
		// iterate until close quote:
		while (!error && *current_char != '\"') {
			verpv(*current_char);
			if (*current_char == '\\') {
				error = read_char(str, current_char);
				switch (*current_char) {
					case '\"': pushme = '\"'; break;
					case '\'': pushme = '\''; break;
					case '\\': pushme = '\\'; break;
					case 'r': pushme = '\r'; break;
					case 't': pushme = '\t'; break;
					case 'n': pushme = '\n'; break;
					case '0': pushme = '\0'; break;
#if 0
					case 'x': // \xXX
					{
						read_char(); char h = from_hex(current_char);
						read_char(); char l = from_hex(current_char);
						pushme = h << 4 | l;
						break;
					}
					case 'u': // \uXXXX
					{
						read_char(); char hh = from_hex(current_char);
						read_char(); char hl = from_hex(current_char);
						read_char(); char lh = from_hex(current_char);
						read_char(); char ll = from_hex(current_char);
						
						pushme = hh;
						pushme = pushme << 4 | hl;
						pushme = pushme << 4 | lh;
						pushme = pushme << 4 | ll;
						
						verpv(pushme);
						break;
					}
					case 'U': // \uXXXXXXXX
					{
						char encoding[6];
						int n = handle_utf8_escape(
							read_char,
							&current_char,
							encoding);
						array_mass_push_n(
							&(current_token_data.string), encoding, n);
						push_pushme = false;
						break;
					}
#endif
					default: {
						fprintf(stderr, "Unknown escape character!\n");
						error = 1;
					};
				}
				array_push_n(&(current_token_data->string), &pushme);
			} else {
				array_push_n(&(current_token_data->string), current_char);
			}

			error = error ?: read_char(str, current_char);
		}
	}

	if (!error) {
		// eat close quote:
		error = read_char(str, current_char);
	}
	EXIT;
	return error;
}

static int read_ident_token(const char **str, wchar_t *current_char,
							enum token *current_token,
							union token_data *current_token_data) {
	int error;
	int strlen;
	ENTER;

	error = 0, strlen = 0;

	while (!error && *current_char &&
		   index("ABCDEFGHIJKLMNOPQRSTUVWXYZ"
				 "abcdefghijklmnopqrstuvwxyz"
				 "1234658790"
				 "$_-",
				 *current_char)) {
		current_token_data->ident[strlen++] = *current_char;
		error = read_char(str, current_char);
	}

	current_token_data->ident[strlen] = '\0';
	current_token_data->strlen_ident = strlen;

	if (!wcscmp(L"func", current_token_data->ident)) {
		*current_token = t_func;
	} else if (!wcscmp(L"lambda", current_token_data->ident)) {
		*current_token = t_func;
	} else if (!wcscmp(L"function", current_token_data->ident)) {
		*current_token = t_func;
	} else if (!wcscmp(L"let", current_token_data->ident)) {
		*current_token = t_let;
	} else if (!wcscmp(L"feedback", current_token_data->ident)) {
		*current_token = t_feedback;
	} else if (!wcscmp(L"static", current_token_data->ident)) {
		*current_token = t_feedback;
	} else {
		*current_token = t_ident;
	}
	EXIT;
	return error;
}

int read_token(const char **str, wchar_t *current_char,
			   enum token *current_token,
			   union token_data *current_token_data) {
	int error = 0;
	ENTER;

	while (!error && is_wspace(*current_char)) {
		error = read_char(str, current_char);
	}

	if (!error) {
		if (*current_char == '\0') {
			*current_token = t_eof;
		} else {
#define NEXT_CHAR (error = read_char(str, current_char))
			switch (*current_char) {
				case ',':
					*current_token = t_comma_char, NEXT_CHAR;
					break;
				case '^':
					*current_token = t_carrot_char, NEXT_CHAR;
					break;
				case '~':
					*current_token = t_tilda_char, NEXT_CHAR;
					break;
				case '`':
					*current_token = t_grave_accent_char, NEXT_CHAR;
					break;
				case '.':
					*current_token = t_period_char, NEXT_CHAR;
					break;
				case ':':
					*current_token = t_colon_char, NEXT_CHAR;
					break;
				case '%':
					*current_token = t_percent_char, NEXT_CHAR;
					break;
				case '+':
					*current_token = t_plus_char, NEXT_CHAR;
					break;
				case '-':
					*current_token = t_minus_char, NEXT_CHAR;
					break;
				case '/':
					*current_token = t_slash_char, NEXT_CHAR;
					break;
				case '{':
					*current_token = t_open_curly_char, NEXT_CHAR;
					break;
				case '}':
					*current_token = t_close_curly_char, NEXT_CHAR;
					break;
				case '(':
					*current_token = t_open_paren_char, NEXT_CHAR;
					break;
				case ')':
					*current_token = t_close_paren_char, NEXT_CHAR;
					break;
				case '[':
					*current_token = t_open_square_char, NEXT_CHAR;
					break;
				case ']':
					*current_token = t_close_square_char, NEXT_CHAR;
					break;
				case '<':
				{
#if 0
					read_char();
					switch(current_char)
					{
						case '<':
							current_token = t_left_bitshift;
							read_char();
							break;
						case '=':
							current_token = t_less_than_equal_to;
							read_char();
							break;
						case '>':
							current_token = t_not_equal_to;
							read_char();
							break;
						default:
							current_token = t_less_than_char;
							break;
					}
#endif
TODO;
					break;
				}
				case '=': {
					error = read_char(str, current_char);
					if (!error) {
						switch (*current_char) {
							case '<':
								TODO;
								break;
							case '=':
								*current_token = t_equal_to;
								error = read_char(str, current_char);
								break;
							case '>':
								TODO;
								break;
							default:
								*current_token = t_equals_char;
								break;
						}
					}
					break;
				}
				case '>':
				{
					error = read_char(str, current_char);
					if(!error)
					{
						switch(*current_char)
						{
							case '<':
								*current_token = t_not_equal_to;
								error = read_char(str, current_char);
								break;
							case '=':
								*current_token = t_greater_than_equal_to;
								error = read_char(str, current_char);
								break;
							case '>':
								*current_token = t_right_bitshift;
								error = read_char(str, current_char);
								break;
							default:
								*current_token = t_greater_than_char;
								break;
						}
					}
					break;
				}
				case '*': {
					// check for repeat '*'
					error = read_char(str, current_char);
					if (!error) {
						if (*current_char == '*') {
							*current_token = t_double_asterick;
							error = read_char(str, current_char);
						} else {
							*current_token = t_asterisk_char;
						}
					}
					break;
				}
				case '|':
				{
					// check for repeat '|'
					error = read_char(str, current_char);
					if(!error)
					{
						if(*current_char == '|')
						{
							*current_token = t_logical_or;
							error = read_char(str, current_char);
						}
						else
						{
							*current_token = t_vertical_bar_char;
						}
					}
					break;
				}
				case '&':
				{
					// check for repeat '&'
					error = read_char(str, current_char);
					if(*current_char == '&')
					{
						*current_token = t_logical_and;
						error = read_char(str, current_char);
					}
					else
					{
						*current_token = t_ampersand_char;
					}
					break;
				}

				case '?': {
					error = read_char(str, current_char);
					if (!error) {
						if (*current_char == ':') {
							*current_token = t_questioncolon;
							error = read_char(str, current_char);
						} else {
							*current_token = t_question_char;
						}
					}
					break;
				}

				case '!':
				{
					error = read_char(str, current_char);
					if(*current_char == '=')
					{
						*current_token = t_not_equal_to;
						error = read_char(str, current_char);
					}
					else
					{
						*current_token = t_exclamation_char;
					}
					break;
				}

				case '0' ... '9': {
					error = read_number_token(str, current_char, current_token,
											  current_token_data);

					break;
				}

				case '\"': {
					error = read_string_token(str, current_char, current_token,
											  current_token_data);

					break;
				}

				case '$':
				case 'a' ... 'z':
				case 'A' ... 'Z': {
					error = read_ident_token(str, current_char, current_token,
											 current_token_data);

					break;
				}
				default: {
					fprintf(stderr, "Unrecongized token '%c' (ord: %i)\n",
							*current_char, *current_char);

					error = 1;
					break;
				}
			}
		}
	}
	EXIT;
	return error;
}
