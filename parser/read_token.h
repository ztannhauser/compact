
#include "./token_id.h"
#include "./token_data.h"

int read_token(
	const char** str,
	wchar_t* current_char,
	enum token* current_token,
	union token_data* current_token_data)
		__attribute__ ((warn_unused_result))
		__attribute__((nonnull (1, 2, 3, 4)));
