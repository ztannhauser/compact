#include <assert.h>
#include <stdio.h>
#include <wchar.h>

#include "debug.h"

#include <value/delete.h>
#include <value/function/struct.h>
#include <value/struct.h>

#include <scope/close.h>
#include <scope/open.h>
#include <scope/push.h>

#include <expression/evaluate.h>

#include "call.h"

// doesn't free values

int call(struct function_value *func, size_t n, struct value **args,
		 struct scope *scope, struct value **result) {
	int error = 0;
	size_t i = 0;
	ENTER;

	if (func->is_builtin) {
		error = (func->builtin)(n, args, scope, result);
	} else {
		error = scope_open(scope);

		// push each variable with corrasponding name:
		{
			error = !(n == func->param_names.n);

			for (struct llnode *current = func->param_names.first;
				 !error && current; current = current->next, i++) {
				error = scope_push(scope, current->data, args[i], true);
			}
		}

		error = 0
					?: expression_evaluate(func->body, scope, result)
						   ?: scope_close(scope);
	}

	EXIT;
	return error;
}
