
#ifndef OBJECT_VALUE_STRUCT_H
#define OBJECT_VALUE_STRUCT_H

#include "avl/struct.h"

#include "../struct.h"

struct object_value_element
{
	struct string_value* label;
	struct value* value;
};

enum object_kind
{
	ok_tm,
	ok_generic,
	ok_timeval,
	ok_timespec,
	ok_n,
};

struct object_value
{
	struct value super;
	enum object_kind kind;
	struct avl_tree values;
};

#endif
