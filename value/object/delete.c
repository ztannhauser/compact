
#include <assert.h>

#include "debug.h"

#include "avl/free_nodes.h"

#include "linkedlist/struct.h"

#include "../delete.h"

#include "struct.h"

#include "delete.h"

static int delete_object_value(struct object_value *this)
{
	int error = 0;
	ENTER;
	if (!--this->super.refcount)
	{
		avl_free_nodes(&(this->values));
		free(this);
	}
	EXIT;
	return error;
}

int callbackptr_delete_object_value(struct value *this)
{
	int error;
	ENTER;
	
	error = 0
		?: this->kind != vk_object
		?: delete_object_value((struct object_value *)this);
	
	EXIT;
	return error;
}
