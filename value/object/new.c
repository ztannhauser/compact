
#include <assert.h>
#include <stdio.h>

#include "debug.h"

#include "linkedlist/struct.h"

#include "../new.h"
#include "../struct.h"

#include "callbacks.h"
#include "struct.h"

#include "new.h"

struct object_value *new_object_value(enum object_kind kind,
									  struct avl_tree values) {
	struct object_value *this;
	this = (struct object_value *)new_value(
		vk_object, &(object_value_callbacks), sizeof(*this));
	verpv(this);
	this->values = values;
	return this;
}
