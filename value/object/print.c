
#include <assert.h>
#include <stdio.h>
#include <string.h>

#include "debug.h"

#include "../print.h"
#include "../string/print.h"

#include "struct.h"

#include "print.h"

int object_value_print(
	struct object_value* this,
	int (*push)(const wchar_t*, size_t))
{
	int error;
	struct avl_node* i;
	struct object_value_element* ele;
	ENTER;
	
	error = push(L"{", 1);
	for(i = this->values.leftmost;!error && i; i = i->next)
	{
		ele = i->data;
		
		error = 0
			?: string_value_print(ele->label, push)
			?: push(L": ", 2)
			?: value_print(ele->value, push);
		
		if(!error && i->next)
		{
			error = push(L", ", 2);
		}
	}
	
	if(!error)
	{
		error = push(L"}", 1);
	}
	
	EXIT;
	return error;
}


int callbackptr_object_value_print(
	struct value *this,
	int (*push)(const wchar_t *, size_t))
{
	int error;
	ENTER;
	
	error = 0
		?: this->kind != vk_object
		?: object_value_print((struct object_value*) this, push);

	EXIT;
	return error;
}














