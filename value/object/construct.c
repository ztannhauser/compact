

#include <assert.h>
#include <stdarg.h>
#include <stdbool.h>
#include <string.h>

#include "debug.h"

#include "value/string/construct.h"

#include "element_compare.h"
#include "element_free.h"
#include "new.h"
#include "struct.h"

#include "construct.h"

struct object_value *construct_object_value(enum object_kind kind, ...) {
	struct object_value *this;
#if 0
	ENTER;
	struct avl_tree values;
	avl_init_tree(&values,
		(int (*)(const void *, const void *)) element_compare,
		(void (*) (void*)) element_free);
	va_list ap;
	va_start(ap, kind);
	verpv(values.root);
	for(const char* label;(label = va_arg(ap, char*));)
	{
		struct object_value_element* ele = malloc(sizeof(*ele));
		verpvs(label);
		ele->label = construct_string_value(label, strlen(label) + 1, true);
		ele->value = va_arg(ap, void*);
		avl_insert(&(values), ele);
		verpv(values.root);
	}
	va_end(ap);
	this = new_object_value(kind, values);
	EXIT;
#endif
	TODO;
	return this;
}
