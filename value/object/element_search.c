
#include "debug.h"

#include "scope/builtins/string/strverscasecmp.h"

#include "struct.h"

#include "element_search.h"

int element_search(struct string_value *a, struct object_value_element *b) {
	int ret;
	ret = process_strverscasecmp(a, b->label);
	return ret;
}
