
#include "struct.h"

struct object_value* construct_object_value(
	enum object_kind kind, ...)
	__attribute__ ((warn_unused_result));
