
#include "../struct.h"

#include "delete.h"
#include "print.h"

#include "callbacks.h"

struct value_callbacks object_value_callbacks = {
	.print = callbackptr_object_value_print,
	.delete = callbackptr_delete_object_value,
};
