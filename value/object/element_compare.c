
#include "debug.h"

#include "scope/builtins/string/strverscasecmp.h"

#include "struct.h"

#include "element_compare.h"

int element_compare(const struct object_value_element *a,
					const struct object_value_element *b) {
	int ret;
	ret = process_strverscasecmp(a->label, b->label);
	return ret;
}
