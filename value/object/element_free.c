
#include <stdio.h>
#include <assert.h>

#include <debug.h>

#include <value/delete.h>
#include <value/string/delete.h>

#include "struct.h"

#include "element_free.h"

int element_free(struct object_value_element *a)
{
	int error;
	ENTER;
	
	error = 0
		?: delete_string_value(a->label)
		?: delete_value(a->value);
	
	if(!error)
	{
		free(a);
	}
	
	EXIT;
	return error;
}
