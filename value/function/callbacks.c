
#include "../struct.h"

#include "delete.h"
#include "print.h"

#include "callbacks.h"

struct value_callbacks function_value_callbacks = {
	.print = callbackptr_function_value_print,
	.delete = callbackptr_delete_function_value,
};
