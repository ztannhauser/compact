
#include "debug.h"
#include <assert.h>
#include <stdio.h>
#include <string.h>

#include "array/struct.h"
#include "linkedlist/struct.h"

#include "expression/print.h"

#include "../struct.h"

#include "struct.h"

#include "print.h"

static int function_value_print(struct function_value *this,
								int (*push)(const wchar_t *, size_t)) {
	int error;
	wchar_t *argname;
	ENTER;

	if (this->is_builtin) {
		error = push(this->builtin_name, wcslen(this->builtin_name));
	} else {
		error = 0 ?: push(L"func", 4) ?: push(L"(", 1);

		HERE;
		verpv(error);

		for (struct llnode *current = this->param_names.first;
			 !error && current; current = current->next) {
			argname = current->data;
			error = error ?: push(argname, wcslen(argname));
			if (!error && current->next) {
				error = error ?: push(L", ", 2);
			}
		}

		HERE;
		verpv(error);

		error = 0
					?: error
						   ?: push(L")", 1)
								  ?: push(L"{", 1)
										 ?: expression_print(this->body, push)
												?: push(L"}", 1);
		;
	}

	EXIT;
	return error;
}

int callbackptr_function_value_print(struct value *this,
									 int (*push)(const wchar_t *, size_t)) {
	int error = 0;
	ENTER;

	error =
		0
			?: this->kind != vk_function
				   ?: function_value_print((struct function_value *)this, push);

	D(push(L"\n", 1));
	EXIT;
	return error;
}
