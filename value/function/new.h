
#include "linkedlist/struct.h"

#include "value/struct.h"
#include "value/function/struct.h"

struct function_value* new_function_value_as_builtin(
	const wchar_t* builtin_name,
	int (*builtin)(
		size_t n, struct value** args,
		struct scope* scope,
		struct value** result
	) __attribute__ ((warn_unused_result)));


struct function_value* new_function_value_as_userdef(
	struct linkedlist param_names,
	struct expression* body)
	__attribute__ ((warn_unused_result));
