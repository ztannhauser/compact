
#ifndef FUNCTION_VALUE_H
#define FUNCTION_VALUE_H

#include <stdbool.h>

#include <linkedlist/struct.h>

#include "../struct.h"

struct scope;

struct function_value
{
	struct value super;
	bool is_builtin;
	union
	{
		struct
		{
			struct linkedlist param_names; // elements of type 'wchar_t*'
			struct expression* body;
		};
		struct
		{
			const wchar_t* builtin_name;
			int (*builtin)(
				size_t n, struct value** args,
				struct scope* scope,
				struct value** result
			) __attribute__ ((warn_unused_result));
		};
	};
};

#endif
