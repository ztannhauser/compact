#include "debug.h"
#include <assert.h>
#include <stdio.h>

#include "linkedlist/delete.h"
#include "linkedlist/foreach.h"
#include "linkedlist/free-elements.h"

#include <expression/delete.h>

#include "struct.h"

#include "delete.h"

int delete_function_value(struct function_value *this)
{
	int error = 0;
	ENTER;
	if (!--this->super.refcount)
	{
		if (!this->is_builtin)
		{
			linkedlist_free_elements(&(this->param_names));
			
			delete_linkedlist(&(this->param_names));
			
			error = delete_expression(this->body);
		}
		free(this);
	}
	EXIT;
	return error;
}

int callbackptr_delete_function_value(struct value *this)
{
	int error;
	ENTER;
	
	error = 0
		?: this->kind != vk_function
		?: delete_function_value((struct function_value *)this)
		;

	EXIT;
	return error;
}
