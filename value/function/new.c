#include <assert.h>
#include <stdio.h>

#include "debug.h"

#include "linkedlist/struct.h"

#include "../new.h"
#include "../struct.h"

#include "callbacks.h"
#include "struct.h"

#include "new.h"

struct function_value *new_function_value_as_builtin(
	const wchar_t *builtin_name,
	int (*builtin)(
		size_t n,
		struct value **args,
		struct scope *scope,
		struct value **result) __attribute__((warn_unused_result)
	)
)
{
	struct function_value *this;
	ENTER;
	this = (struct function_value*) new_value(
		vk_function, &(function_value_callbacks), sizeof(*this));
	this->is_builtin = true;
	this->builtin = builtin;
	this->builtin_name = builtin_name;
	EXIT;
	return this;
}

struct function_value * new_function_value_as_userdef(
	struct linkedlist param_names,
	struct expression *body
) {
	struct function_value *this;
	ENTER;
	this = (struct function_value *)new_value(
		vk_function, &(function_value_callbacks), sizeof(*this));
	this->is_builtin = false;
	this->param_names = param_names;
	this->body = body;
	EXIT;
	return this;
}







