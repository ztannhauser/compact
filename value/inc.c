#include <assert.h>
#include <stdio.h>

#include "debug.h"

#include "struct.h"

#include "inc.h"

int value_inc(struct value *this) {
	this->refcount++;
	return 0;
}
