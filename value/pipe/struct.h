
#ifndef PIPE_VALUE_STRUCT_H
#define PIPE_VALUE_STRUCT_H

#include "../struct.h"

struct pipe_value
{
	struct value super;
	int fds[2];
};

#endif
