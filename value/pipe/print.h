
#include "struct.h"

int callbackptr_pipe_value_print(
	struct value* this,
	int (*push)(const wchar_t*, size_t))
	__attribute__ ((warn_unused_result));
