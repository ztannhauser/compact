
#include <assert.h>
#include <stdio.h>
#include <string.h>

#include "debug.h"

#include "../new.h"
#include "../struct.h"

#include "callbacks.h"
#include "struct.h"

#include "new.h"

struct pipe_value *new_pipe_value(int *fds) {
	struct pipe_value *this = (struct pipe_value *)new_value(
		vk_pipe, &(pipe_value_callbacks), sizeof(struct pipe_value));
	verpv(this);
	memcpy(this->fds, fds, sizeof(int) * 2);
	return this;
}
