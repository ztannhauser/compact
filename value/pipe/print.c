
#include <assert.h>
#include <stdio.h>
#include <string.h>

#include "array/struct.h"
#include "debug.h"

#include "struct.h"

#include "print.h"

#if 0
static void pipe_value_print(
	struct pipe_value* this,
	void (*push)(const wchar_t*, size_t))
{
	ENTER;
#if 0
	char buffer[32];
	int len = snprintf(buffer, 32, "<%i,%i>", this->fds[0], this->fds[1]);
	push(buffer, len);
#endif
	TODO;
	EXIT;
}

#endif

int callbackptr_pipe_value_print(struct value *this,
								 int (*push)(const wchar_t *, size_t)) {
	int error = 0;
	ENTER;
#if 0
	assert(this->kind == vk_list);
	pipe_value_print(
		(struct pipe_value*) this,
		push
	);
#endif

	D(push(L"\n", 1));
	
	TODO;
	EXIT;
	return error;
}
