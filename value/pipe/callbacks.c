
#include "../struct.h"

#include "delete.h"
#include "print.h"

#include "callbacks.h"

struct value_callbacks pipe_value_callbacks = {
	.print = callbackptr_pipe_value_print,
	.delete = callbackptr_delete_pipe_value,
};
