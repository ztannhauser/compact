#include <assert.h>
#include <stdio.h>

#include "debug.h"

#include "struct.h"

#include "print.h"

int value_print(struct value *this, int (*push)(const wchar_t *, size_t)) {
	int error = 0;
	ENTER;
	error = error ?: (this->callbacks->print)(this, push);
	EXIT;
	return error;
}
