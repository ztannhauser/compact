#include <assert.h>
#include <stdio.h>

#include "debug.h"

#include "struct.h"

#include "delete.h"

int delete_value(struct value *this)
{
	int error = 0;
	ENTER;
	if(this)
	{
		error = (this->callbacks->delete)(this);
	}
	EXIT;
	return error;
}
