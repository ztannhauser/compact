
#ifndef STRING_VALUE_H
#define STRING_VALUE_H

#include <stdbool.h>

#include "array/struct.h"

#include "../struct.h"

#if 0
struct character
{
	bool is_unicode;
	union
	{
		uint8_t hex_code;
		wchar_t utf8_code;
	};
};
#endif

struct string_value
{
	struct value super;
	struct array wchars; // elements of type 'wchar_t'
};

#endif

