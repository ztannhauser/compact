
#include "delete.h"
#include "print.h"

#include "../struct.h"

#include "callbacks.h"

struct value_callbacks string_value_callbacks = {
	.print = callbackptr_string_value_print,
	.delete = callbackptr_delete_string_value,
};
