
#include "struct.h"

int string_value_from_utf8_encoded(
	const char *str,
	size_t strlen,
	struct string_value** result)
	__attribute__ ((warn_unused_result));
