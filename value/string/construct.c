
#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <sys/types.h>
#include <wchar.h>

#include "debug.h"

#include "array/new.h"
#include "array/struct.h"

#include "new.h"

#include "construct.h"

struct string_value *construct_string_value(const wchar_t *src, size_t len) {
	struct string_value *this;
	ENTER;
	struct array chars = new_array_from_data(src, len, wchar_t);
	this = new_string_value(chars);
	EXIT;
	return this;
}
