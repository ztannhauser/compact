
#include <assert.h>
#include <stdio.h>

#include <debug.h>

#include <utf-8/encode.h>

#include <array/mass_push_n.h>
#include <array/new.h>
#include <array/push_n.h>

#include "struct.h"

#include "to_utf8_encoded.h"

// should append null character to end of string

struct array string_value_to_utf8_encoded(struct string_value *this,
										  bool should_append_null) {
	struct array ret;
	ENTER;
	ret = new_array(char);

	for (wchar_t *start = this->wchars.datawc, *moving = start,
				 *end = start + this->wchars.n;
		 moving < end; moving++) {
		if (*moving > 127) {
			char utf8_buffer[6];
			int utf8_buffer_length = utf8_encode(*moving, utf8_buffer);
			array_mass_push_n(&ret, utf8_buffer, utf8_buffer_length);
		} else {
			char c = *moving;
			array_push_n(&ret, &c);
		}
	}

	if (should_append_null) {
		array_push_n(&ret, "");
	}

	EXIT;
	return ret;
}
