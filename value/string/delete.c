#include <assert.h>
#include <stdio.h>

#include "debug.h"

#include "array/delete.h"
#include "array/struct.h"

#include "struct.h"

#include "delete.h"

int delete_string_value(struct string_value *this)
{
	int error = 0;
	ENTER;
	if (!--this->super.refcount)
	{
		delete_array(&(this->wchars));
		free(this);
	}
	EXIT;
	return error;
}

int callbackptr_delete_string_value(struct value *this)
{
	int error;
	ENTER;
	
	error = 0
		?: this->kind != vk_string
		?: delete_string_value((struct string_value*) this)
		;
	
	EXIT;
	return error;
}
