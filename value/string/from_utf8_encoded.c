
#include <stdio.h>
#include <stdlib.h>

#include <debug.h>

#include <array/new.h>
#include <array/push_n.h>
#include <array/struct.h>

#include <utf-8/decode.h>
#include <utf-8/how_many.h>

#include "new.h"
#include "struct.h"

#include "from_utf8_encoded.h"

int string_value_from_utf8_encoded(
	const char *str,
	size_t strlen,
	struct string_value** result)
{
	int error = 0;
	int how_many;
	wchar_t wc;
	ENTER;
	struct array wchars = new_array(wchar_t);
	
	while(!error && *str)
	{
		error = 0
			?: utf8_how_many(*str, &how_many)
			?: utf8_decode(str, &wc);
		
		if(!error)
		{
			array_push_n(&wchars, &wc);
			str += how_many;
		}
	}
	
	if(!error)
	{
		*result = new_string_value(wchars);
	}
	EXIT;
	return error;
}
