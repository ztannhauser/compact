
#include <stdbool.h>
#include <wchar.h>

struct string_value* construct_string_value(const wchar_t* src, size_t len);
