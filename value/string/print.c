#include <assert.h>
#include <stdio.h>
#include <string.h>

#include <debug.h>

#include <utf-8/encode.h>

#include "struct.h"

#include "print.h"

static const wchar_t *quote = L"\"";

int string_value_print(struct string_value *this,
					   int (*push)(const wchar_t *, size_t)) {
	int error;
	wchar_t utf8_buffer[20];
	int utf8_buffer_length;
	ENTER;
	error = push(quote, 1);

	verpv(this->wchars.n);

	for (wchar_t *start = this->wchars.datawc, *moving = start,
				 *end = start + this->wchars.n;
		 !error && moving < end; moving++)
	{
		wchar_t m = *moving;
		switch (m)
		{
			case '\0': push(L"\\0", 2); break;
			case '\n': push(L"\\n", 2); break;
			case '\r': push(L"\\r", 2); break;
			case '\t': push(L"\\t", 2); break;
			case '\"': push(L"\\\"", 2); break;
			case '\'': push(L"\\\'", 2); break;
			case '\\': push(L"\\\\", 2); break;
			default:
			{
				if(index(" ", m) || index("abcdefghijklmnopqrstuvwxyz", m) ||
					 index("ABCDEFGHIJKLMNOPQRSTUVWXYZ", m) ||
					 index("`1234567890", m) || index("~!@#$%^&*()", m) ||
					 index("-=[]\\;',./", m) || index("_+{}|:\"<>?", m) ||
					 false)
				{
					// push char
					error = push(&m, 1);
				}
				else
				{
					// write \U code
					utf8_buffer_length =
						m > 0xFFFF ?
						swprintf(utf8_buffer, 20, L"\\U%08X", m):
						swprintf(utf8_buffer, 20, L"\\u%04X", m);
						error = push(utf8_buffer, utf8_buffer_length);
				}
			}
		}
	}

	error = error ?: push(quote, 1);

	EXIT;
	return error;
}

int callbackptr_string_value_print(struct value *this,
								   int (*push)(const wchar_t *, size_t)) {
	int error = 0;
	ENTER;

	error = 0
				?: this->kind != vk_string
					   ?: string_value_print((struct string_value *)this, push);

	D(push(L"\n", 1));

	EXIT;
	return error;
}
