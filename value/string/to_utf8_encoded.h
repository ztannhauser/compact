
#include <stdbool.h>

#include "struct.h"

struct array string_value_to_utf8_encoded(
	struct string_value* this,
	bool should_append_null);
