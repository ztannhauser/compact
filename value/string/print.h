
#include "value/string/struct.h"

int string_value_print(
	struct string_value* this,
	int (*push)(const wchar_t*, size_t))
	__attribute__ ((warn_unused_result));

int callbackptr_string_value_print(
	struct value* this,
	int (*push)(const wchar_t*, size_t))
	__attribute__ ((warn_unused_result));
