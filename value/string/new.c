
#include <assert.h>
#include <stdbool.h>
#include <stdio.h>

#include "debug.h"

#include "array/struct.h"

#include "callbacks.h"
#include "struct.h"

#include "../new.h"

#include "new.h"

struct string_value *new_string_value(struct array wchars) {
	ENTER;
	struct string_value *this = (struct string_value *)new_value(
		vk_string, &(string_value_callbacks), sizeof(*this));
	this->wchars = wchars;
	EXIT;
	return this;
}
