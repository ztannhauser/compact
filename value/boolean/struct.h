
#ifndef BOOLEAN_VALUE_STRUCT
#define BOOLEAN_VALUE_STRUCT

#include <stdbool.h>

#include "../struct.h"

struct boolean_value
{
	struct value super;
	bool value;
};

#endif
