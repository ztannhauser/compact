
#include <assert.h>
#include <stdbool.h>
#include <stdio.h>

#include "debug.h"

#include "../new.h"
#include "../struct.h"

#include "callbacks.h"
#include "struct.h"

#include "new.h"

struct boolean_value *new_boolean_value(bool v) {
	struct boolean_value *this = (struct boolean_value *)new_value(
		vk_boolean, &(boolean_value_callbacks), sizeof(struct boolean_value));
	verpv(this);
	this->value = v;
	return this;
}
