
#include <assert.h>
#include <stdio.h>
#include <string.h>

#include "debug.h"

#include "struct.h"

#include "print.h"

static int boolean_value_print(
	struct boolean_value* this,
	int (*push)(const wchar_t*,size_t))
{
	int error = 0;
	ENTER;
	
	error = this->value ? push(L"true", 4) : push(L"false", 5);

	EXIT;
	return error;
}


int callbackptr_boolean_value_print(
	struct value *this,
	int (*push)(const wchar_t *, size_t))\
{
	int error;
	ENTER;
	
	error = 0
		?: this->kind != vk_boolean
		?: boolean_value_print((struct boolean_value*) this, push);
	
	EXIT;
	return error;
}






