#include <assert.h>
#include <stdio.h>

#include "debug.h"

#include "struct.h"

#include "delete.h"

int delete_boolean_value(struct boolean_value *this) {
	int error = 0;
	ENTER;

	if (!--this->super.refcount) {
		free(this);
	}

	EXIT;
	return error;
}

int callbackptr_delete_boolean_value(struct value *this)
{
	int error;
	ENTER;
	
	error = 0
		?: this->kind != vk_boolean
		?: delete_boolean_value((struct boolean_value *)this);
	
	EXIT;
	return error;
}
