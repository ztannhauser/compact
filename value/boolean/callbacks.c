
#include "../struct.h"

#include "delete.h"
#include "print.h"

#include "callbacks.h"

struct value_callbacks boolean_value_callbacks = {
	.print = callbackptr_boolean_value_print,
	.delete = callbackptr_delete_boolean_value,
};
