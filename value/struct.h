
#ifndef VALUE_STRUCT_H
#define VALUE_STRUCT_H

#include <stdlib.h>

struct value;

struct value_callbacks
{
	int (*print)(
		struct value* this,
		int (*push)(const wchar_t*, size_t)
		__attribute__ ((warn_unused_result))
	) __attribute__ ((warn_unused_result));
	
	int (*delete)(
		struct value* this
	) __attribute__ ((warn_unused_result));
};

enum value_kind
{
	vk_null,
	vk_boolean,
	vk_fd,
	vk_pipe,
	vk_number,
	vk_string,
	vk_list,
	vk_object,
	vk_function,
	vk_n
};

struct value
{
	enum value_kind kind;
	struct value_callbacks* callbacks;
	int refcount;
};

#endif // VALUE_STRUCT_H
