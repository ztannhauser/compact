
#include "struct.h"

struct value* new_value(
	enum value_kind kind,
	struct value_callbacks* callbacks,
	int size);
