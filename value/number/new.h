
#include <gmp.h>
#include <quadmath.h>
#include <inttypes.h>

struct number_value* new_number_value_as_integer(intmax_t integer);

struct number_value* new_number_value_as_long(mpz_ptr mpz_long);

struct number_value* new_number_value_as_decimal(__float128 decimal);
