
#ifndef NUMBER_VALUE_H
#define NUMBER_VALUE_H

#include <stdbool.h>
#include <inttypes.h>
#include <gmp.h>

#include "../struct.h"

enum number_kind
{
	nvk_integer,
	nvk_long,
	nvk_decimal,
	nvk_n
};

struct number_value
{
	struct value super;
	enum number_kind kind;
	union
	{
		// nvk_integer:
		struct
		{
			intmax_t integer;
		};
		// nvk_long:
		struct
		{
			mpz_ptr mpz_long; // needs to be free'd
		};
		// nvk_decimal:
		struct
		{
			__float128 decimal;
		};
	};
};

#endif
