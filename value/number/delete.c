
#include <assert.h>
#include <unistd.h>
#include <stdio.h>

#include "debug.h"

#include "struct.h"

#include "delete.h"

static int delete_number_value(struct number_value *this) {
	int error = 0;
	ENTER;
	if (!--this->super.refcount) {
		switch (this->kind) {
			case nvk_integer:
				break;
			case nvk_long: {
				mpz_clear(this->mpz_long), free(this->mpz_long);
				break;
			}
			case nvk_decimal:
				break;
			default:
				TODO;
		}
		free(this);
	}
	EXIT;
	return error;
}

int callbackptr_delete_number_value(struct value *this)
{
	int error;
	ENTER;
	
	error = 0
		?: this->kind != vk_number
		?: delete_number_value((struct number_value *)this)
		;

	EXIT;
	return error;
}
