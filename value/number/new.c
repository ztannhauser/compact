
#include <stdio.h>

#include "../new.h"
#include "../struct.h"

#include "debug.h"

#include "callbacks.h"
#include "struct.h"

#include "new.h"

struct number_value *new_number_value_as_integer(intmax_t integer) {
	struct number_value *this;
	ENTER;
	this = (struct number_value *)new_value(
		vk_number, &(number_value_callbacks), sizeof(*this));
	verpv(this);
	this->kind = nvk_integer;
	this->integer = integer;
	verpv(this);
	verpv(integer);
	EXIT;
	return this;
}

struct number_value *new_number_value_as_long(mpz_ptr mpz_long) {
	struct number_value *this;
	ENTER;
	this = (struct number_value *)new_value(
		vk_number, &(number_value_callbacks), sizeof(*this));
	verpv(this);
	this->kind = nvk_long;
	this->mpz_long = mpz_long;
	EXIT;
	return this;
}

struct number_value *new_number_value_as_decimal(__float128 decimal) {
	struct number_value *this;
	ENTER;
	this = (struct number_value *)new_value(
		vk_number, &(number_value_callbacks), sizeof(*this));
	verpv(this);
	this->kind = nvk_decimal;
	this->decimal = decimal;
	EXIT;
	return this;
}
