
#include <stdlib.h>

#include "struct.h"

int callbackptr_number_value_print(
	struct value* this,
	int (*push)(const wchar_t*, size_t))
	__attribute__ ((warn_unused_result));
