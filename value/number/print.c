
#include <assert.h>
#include <quadmath.h>
#include <stdio.h>
#include <string.h>
#include <wchar.h>

#include "debug.h"

#include <misc/push_string_as_wchar.h>

#include "struct.h"

#include "print.h"

static int number_value_print(struct number_value *this,
							  int (*push)(const wchar_t *, size_t)) {
	int error = 0;
	ENTER;
	switch (this->kind) {
		case nvk_integer: {
			wchar_t buffer[32];
			int len = swprintf(buffer, 32, L"%" PRIiMAX, this->integer);
			error = error ?: len <= 0 ?: push(buffer, len);
			break;
		}
#if 0
		case nvk_long:
		{
			char* str = mpz_get_str(NULL, 10, this->mpz_long);
			push_string_as_wchar(push, str);
			push(L"L", 1);
			
			free(str);
			break;
		}
		case nvk_decimal:
		{
			if(isnanq(this->decimal))
			{
				push(L"NaN", 3);
			}
			else if(isinfq(this->decimal))
			{
				if(this->decimal < 0)
				{
					push(L"-", 1);
				}
				push(L"Inf", 3);
			}
			else
			{
				char buffer[256];
				int len = quadmath_snprintf(buffer, 256, "%.30Qf", this->decimal);
				assert(len > 0);
				while(buffer[len - 1] == '0' && buffer[len - 2] != '.') len--;
				buffer[len] = '\0';
				push_string_as_wchar(push, buffer);
			}
			break;
		}
#endif
		default:
			TODO;
	}

	EXIT;
	return error;
}

int callbackptr_number_value_print(struct value *this,
								   int (*push)(const wchar_t *, size_t)) {
	int error = 0;
	ENTER;
	error = 0
				?: error
					   ?: this->kind != vk_number
							  ?: number_value_print((struct number_value *)this,
													push);
	EXIT;
	return error;
}
