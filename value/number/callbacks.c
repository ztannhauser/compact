
#include "delete.h"
#include "print.h"

#include "../struct.h"

#include "callbacks.h"

struct value_callbacks number_value_callbacks = {
	.print = callbackptr_number_value_print,
	.delete = callbackptr_delete_number_value,
};
