
#include "../struct.h"

#include "delete.h"
#include "print.h"

#include "callbacks.h"

struct value_callbacks list_value_callbacks = {
	.print = callbackptr_list_value_print,
	.delete = callbackptr_delete_list_value,
};
