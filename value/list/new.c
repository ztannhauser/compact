
#include <assert.h>
#include <stdio.h>

#include "debug.h"
#include "linkedlist/struct.h"

#include "value/function/struct.h"

#include "../new.h"
#include "../struct.h"

#include "callbacks.h"
#include "struct.h"

#include "new.h"

struct list_value *new_list_value(struct array values,
								  struct function_value *comparator) {
	struct list_value *this;
	assert(!comparator || comparator->super.kind == vk_function);
	this = (struct list_value *)new_value(vk_list, &(list_value_callbacks),
										  sizeof(*this));
	verpv(this);
	this->values = values;
	this->comparator = comparator;
	return this;
}
