
#include "array/struct.h"

#include "value/function/struct.h"

struct list_value* new_list_value(
	struct array values,
	struct function_value* comparator);
