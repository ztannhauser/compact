
#include <assert.h>
#include <stdio.h>

#include "debug.h"

#include "array/delete.h"
#include "array/foreach.h"

#include "linkedlist/struct.h"

#include "../delete.h"
#include "../function/delete.h"

#include "struct.h"

#include "delete.h"

static int delete_list_value(struct list_value *this)
{
	int error = 0;
	ENTER;
	if (!--this->super.refcount) {
		error = arrayp_foreach(&(this->values), (int (*)(void *))delete_value);
		delete_array(&(this->values));
		if (this->comparator) {
			error = delete_function_value(this->comparator);
		}
		free(this);
	} else {
		verpv(this->super.refcount);
	}
	EXIT;
	return error;
}

int callbackptr_delete_list_value(struct value *this)
{
	int error;
	ENTER;
	
	error = 0
		?: this->kind != vk_list
		?: delete_list_value((struct list_value *)this)
		;

	EXIT;
	return error;
}










