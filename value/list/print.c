
#include <assert.h>
#include <stdio.h>

#include "debug.h"

#include "../print.h"

#include "struct.h"

#include "print.h"

static int list_value_print(
	struct list_value* this,
	int (*push)(const wchar_t*, size_t))
{
	int error = 0;
	ENTER;
	error = push(L"[", 1);
	for(int i = 0, n = this->values.n;!error && i < n;i++)
	{
		struct value* ele = array_index(this->values, i, struct value*);
		error = value_print(ele, push);
		if(!error && i + 1 < n)
		{
			error = push(L", ", 2);
		}
	}
	error = error ?: push(L"]", 1);
	EXIT;
	return error;
}

int callbackptr_list_value_print(
	struct value *this,
	int (*push)(const wchar_t *, size_t))
{
	int error;
	ENTER;
	
	error = 0
		?: this->kind != vk_list
		?: list_value_print((struct list_value*) this, push)
		;
	
	EXIT;
	return error;
}











