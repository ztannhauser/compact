#include <assert.h>
#include <stdio.h>

#include "debug.h"

#include "struct.h"

#include "new.h"

struct value *new_value(enum value_kind kind, struct value_callbacks *callbacks,
						int size) {
	struct value *this;
	ENTER;
	this = malloc(size);
	verpv(this);
	this->kind = kind;
	this->callbacks = callbacks;
	this->refcount = 1;
	EXIT;
	return this;
}
