
#include "debug.h"
#include <assert.h>
#include <stdio.h>

#include "../new.h"
#include "../struct.h"

#include "callbacks.h"
#include "struct.h"

#include "new.h"

struct null_value *new_null_value() {
	struct null_value *this = (struct null_value *)new_value(
		vk_null, &(null_value_callbacks), sizeof(struct null_value));
	verpv(this);
	return this;
}
