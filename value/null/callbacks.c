
#include "../struct.h"

#include "delete.h"
#include "print.h"

#include "callbacks.h"

struct value_callbacks null_value_callbacks = {
	.print = callbackptr_null_value_print,
	.delete = callbackptr_delete_null_value,
};
