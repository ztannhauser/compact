
#include <assert.h>
#include <stdio.h>

#include "debug.h"

#include "struct.h"

#include "delete.h"

int delete_null_value(struct null_value *this)
{
	int error = 0;
	ENTER;

	if (!--this->super.refcount) {
		free(this);
	}

	EXIT;
	return error;
}

int callbackptr_delete_null_value(struct value *this)
{
	int error;
	ENTER;
	
	error = 0
		?: this->kind != vk_null
		?: delete_null_value((struct null_value *)this)
		;

	EXIT;
	return error;
}
