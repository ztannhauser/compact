
#include <assert.h>
#include <stdio.h>

#include "debug.h"

#include "struct.h"

#include "print.h"

static int null_value_print(
	struct null_value* this,
	int (*push)(const wchar_t*, size_t))
{
	int error;
	ENTER;
	
	error = push(L"null", 4);
	
	EXIT;
	return error;
}

int callbackptr_null_value_print(struct value *this,
								 int (*push)(const wchar_t *, size_t)) {
	int error = 0;
	ENTER;
	
	error = 0
		?: this->kind != vk_null
		?: null_value_print((struct null_value*) this, push);
	
	EXIT;
	return error;
}
