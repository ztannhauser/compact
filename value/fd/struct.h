
#ifndef VALUE_FD_STRUCT_H
#define VALUE_FD_STRUCT_H

#include <stdbool.h>

#include "../struct.h"

enum fd_direction
{
	d_readable,
	d_writeable
};

struct fd_value
{
	struct value super;
	enum fd_direction direction;
	bool closed;
	int fd;
	pid_t pid;
};

#endif
