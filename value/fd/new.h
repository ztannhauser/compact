
#include "struct.h"

struct fd_value *new_fd_value(
	int fd,
	enum fd_direction direction,
	pid_t pid);
