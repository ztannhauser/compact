
#include <assert.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>

#include "debug.h"

#include "../struct.h"

#include "struct.h"

#include "delete.h"

static int delete_fd_value(struct fd_value *this)
{
	int error = 0;
	int wstatus;
	ENTER;
	
	if(!--this->super.refcount)
	{
		if(!this->closed D(&& this->fd != 1))
			if(close(this->fd) < 0)
				perror("close"), error = 1;
			else if(this->pid && waitpid(this->pid, &wstatus, 0) < 0)
				perror("waitpid"), error = 1;
		
		free(this);
	}
	
	EXIT;
	return error;
}

int callbackptr_delete_fd_value(struct value *this)
{
	int error;
	ENTER;
	
	error = 0
		?: this->kind != vk_fd
		?: delete_fd_value((struct fd_value*) this)
		;
	
	EXIT;
	return error;
}
