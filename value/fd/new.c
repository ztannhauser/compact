
#include <stdio.h>

#include "debug.h"

#include "../new.h"
#include "../struct.h"

#include "callbacks.h"
#include "struct.h"

#include "new.h"

struct fd_value *new_fd_value(
	int fd,
	enum fd_direction direction,
	pid_t pid)
{
	struct fd_value *this;
	ENTER;
	
	this = (struct fd_value *)new_value(
		vk_fd,
		&(fd_value_callbacks),
		sizeof(struct fd_value));
	
	verpv(this);
	this->fd = fd;
	this->pid = pid;
	this->closed = false;
	this->direction = direction;
	
	EXIT;
	return this;
}
