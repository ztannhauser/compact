
#include <assert.h>
#include <stdio.h>
#include <string.h>

#include "array/struct.h"
#include "debug.h"

#include "struct.h"

#include "print.h"

static int fd_value_print(
	struct fd_value* this,
	int (*push)(const wchar_t*, size_t))
{
	wchar_t buffer[32];
	int len, error = 0;
	ENTER;
	
	len = swprintf(buffer, 32, L"<%i>", this->fd);
	error = push(buffer, len);
	
	EXIT;
	return error;
}

int callbackptr_fd_value_print(struct value *this,
							   int (*push)(const wchar_t *, size_t)) {
	int error;
	ENTER;
	
	error = 0
		?: this->kind != vk_fd
		?: fd_value_print( (struct fd_value*) this, push);
	
	EXIT;
	return error;
}
