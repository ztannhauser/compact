
#include "../struct.h"

#include "delete.h"
#include "print.h"

#include "callbacks.h"

struct value_callbacks fd_value_callbacks = {
	.print = callbackptr_fd_value_print,
	.delete = callbackptr_delete_fd_value,
};
