
#include <stdbool.h>
#include <stdio.h>
#include <string.h>

#include <debug.h>

#include "process_flags.h"

int process_flags(const char ***args, bool *return_last_value)
{
	int error = 0;
	ENTER;
	for (bool cont = true; cont && **args;)
	{
		if (!strcmp(**args, "--lastval-exit-code"))
			(*args)++, *return_last_value = true;
		else
			cont = false;
	}
	EXIT;
	return error;
}
