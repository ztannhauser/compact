
#include <stdbool.h>

#include "is_digit.h"

bool is_digit(char c) {
	return ('0' <= c && c <= '9');
};
