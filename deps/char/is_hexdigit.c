#include <stdbool.h>

#include "is_digit.h"

#include "is_hexdigit.h"

bool is_hexdigit(char c) {
	return is_digit(c) || ('a' <= c && c <= 'f') || ('A' <= c && c <= 'F');
};
