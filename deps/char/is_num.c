#include <stdbool.h>

#include "is_hexdigit.h"

#include "is_num.h"

bool is_num(char c) {
	return (is_hexdigit(c)) || (c == '.') || (c == 'b') || (c == 'x') ||
		   (c == 'e');
};
