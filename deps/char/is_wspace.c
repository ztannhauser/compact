
#include <stdbool.h>

#include "is_wspace.h"

bool is_wspace(char c) {
	return (c == ' ') || (c == '\t') || (c == '\n');
}
