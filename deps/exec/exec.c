#include <assert.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#include "debug.h"

#include "exec.h"

int exec(char **args, int* wstatus)
{
	int error = 0;
	pid_t child;
	ENTER;
	
	child = fork();
	verpv(child);
	
	if(child < 0)
	{
		perror("fork");
		error = 1;
	}
	else
	{
		if (child) {
			if (waitpid(child, wstatus, 0) < 0) {
				perror("waitpid");
				error = 1;
			}
		} else {
			if (execvp(args[0], args) < 0) {
				perror("execvp");
				error = 1;
			}
		}
	}
	EXIT;
	return error;
}
