#include <assert.h>
#include <stdio.h>
#include <unistd.h>

#include "debug.h"

#include "async_wp.h"

pid_t async_wp(
	char **args,
	int stdin_pipe[2],
	int stdout_pipe[2])
{
	pid_t child;
	ENTER;
	if ((child = fork())) {
		return child;
	} else {
		if (stdin_pipe)
			dup2(stdin_pipe[0], 0), close(stdin_pipe[1]);
		if (stdout_pipe)
			dup2(stdout_pipe[1], 1), close(stdout_pipe[0]);
		if (execvp(args[0], args) == -1) {
			perror("execvp");
		}
	}
	EXIT;
	return child;
}
