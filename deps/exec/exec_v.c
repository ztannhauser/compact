#include <stdio.h>

#include "exec.h"

#include "exec_v.h"

int exec_v(char **args, int *wstatus)
{
	printf("$");
	for (char **m = args; *m; m++) {
		printf(" '%s'", *m);
	}
	printf("\n");
	return exec(args, wstatus);
}
