#include <stdio.h>
#include <stdlib.h>

#include "struct.h"

#include "push_n.h"

void linkedlist_push_n(struct linkedlist *this, void *ptr) {
	struct llnode *new = malloc(sizeof(struct llnode));
	new->prev = this->last;
	new->next = NULL;
	new->data = ptr;
	if (this->last) {
		this->last->next = new;
	}
	this->last = new;
	if (!this->first) {
		this->first = new;
	}
	this->n++;
}
