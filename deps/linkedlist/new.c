#include <stdlib.h>

#include "struct.h"

#include "new.h"

struct linkedlist new_linkedlist() {
	struct linkedlist ret;
	ret.n = 0;
	ret.first = NULL;
	ret.last = NULL;
	return ret;
}
