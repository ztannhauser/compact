#include <stdlib.h>

#include "struct.h"

#include "free-elements.h"

void linkedlist_free_elements(struct linkedlist *l)
{
	for(struct llnode *current = l->first; current; current = current->next)
	{
		free(current->data);
	}
}
