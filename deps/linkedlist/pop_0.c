#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include "struct.h"

#include "pop_0.h"

void linkedlist_pop_0(struct linkedlist *this) {
	struct llnode *oldfirst = this->first;
	if (oldfirst) {
		this->first = oldfirst->next;
		if (this->first) {
			this->first->prev = NULL;
		}
		if (this->last == oldfirst) {
			this->last = NULL;
		}
		free(oldfirst);
		this->n--;
	}
}
