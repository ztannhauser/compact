#include <stdlib.h>

#include "clear.h"
#include "struct.h"

#include "delete.h"

void delete_linkedlist(struct linkedlist *this) {
	linkedlist_clear(this);
}
