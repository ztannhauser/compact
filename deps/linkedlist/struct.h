
#ifndef LINKEDLIST_STRUCT_H
#define LINKEDLIST_STRUCT_H

struct llnode
{
	struct llnode* prev, *next;
	void* data;
};

struct linkedlist
{
	struct llnode* first, *last;
	unsigned long n;
};

#endif

