#include <stdlib.h>

#include "struct.h"

#include "clear.h"

void linkedlist_clear(struct linkedlist *this) {
	for (struct llnode *current = this->first,
					   *next = current ? current->next : NULL;
		 current; current = next, next = current ? current->next : NULL) {
		free(current);
	}
	this->n = 0;
	this->first = this->last = NULL;
}
