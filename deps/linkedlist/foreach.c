#include <stdlib.h>

#include "struct.h"

#include "foreach.h"

int linkedlist_foreach(struct linkedlist *l, int (*callback)(void *))
{
	int error = 0;
	for(struct llnode *current = l->first;
		!error && current; current = current->next)
	{
		error = callback(current->data);
	}
	return error;
}
