#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include "struct.h"

#include "pop_n.h"

void linkedlist_pop_n(struct linkedlist *this) {
	struct llnode *oldlast = this->last;
	if (oldlast) {
		this->last = oldlast->prev;
		if (this->last) {
			this->last->next = NULL;
		}
		if (this->first == oldlast) {
			this->first = NULL;
		}
		free(oldlast);
		this->n--;
	}
}
