
#include <stdlib.h>

#include "push_string_as_wchar.h"

void push_string_as_wchar(void (*push)(const wchar_t *, size_t), char *str) {
	for (wchar_t t; *str;)
		t = *str++, push(&t, 1);
}
