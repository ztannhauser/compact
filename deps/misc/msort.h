
int msort(
	void* data,
	size_t n,
	size_t size,
	int (*cmp)(void* a, void* b, int* result));
