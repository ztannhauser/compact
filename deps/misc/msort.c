
#include <stdbool.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <stdio.h>

#include <debug.h>

#include "msort.h"

static int msort_helper(
	void* data_a,
	void* data_b,
	bool* is_dest_in_a,
	size_t n,
	size_t size,
	int (*cmp)(void* a, void* b, int* result))
{
	int error = 0, c;
	bool is_left_child_dest_in_a, is_right_child_dest_in_a;
	size_t first_half_n, second_half_n;
	void* dest_ptr, *left_ptr, *right_ptr;
	void* left_end, *right_end;
	ENTER;
	
	if(n <= 1)
	{
		*is_dest_in_a = true;
	}
	else
	{
	
		first_half_n = n / 2, second_half_n = n - first_half_n;
		
		verpv(first_half_n);
		verpv(second_half_n);
		
		error = 0
			?: msort_helper(data_a, data_b,
				&is_left_child_dest_in_a, first_half_n, size, cmp)
			?: msort_helper(data_a + size * first_half_n,
				data_b + size * first_half_n,
				&is_right_child_dest_in_a, second_half_n, size, cmp);
		
		left_ptr = is_left_child_dest_in_a ? data_a : data_b;
		right_ptr = is_right_child_dest_in_a ? data_a : data_b;
		right_ptr += size * first_half_n;
		dest_ptr = is_left_child_dest_in_a ? data_b : data_a;
		
		verpv(left_ptr);
		verpv(right_ptr);
		verpv(dest_ptr);
		
		left_end = left_ptr + size * first_half_n;
		right_end = right_ptr + size * second_half_n;
			
		verpv(left_end);
		verpv(right_end);
		
		while(!error && left_ptr < left_end && right_ptr < right_end)
		{
			verpv(left_ptr);
			verpv(right_ptr);
			error = cmp(left_ptr, right_ptr, &c);
			if(!error)
			{
				if(c < 0)
				{
					memcpy(dest_ptr, left_ptr, size);
					dest_ptr += size, left_ptr += size;
				}
				else
				{
					memcpy(dest_ptr, right_ptr, size);
					dest_ptr += size, right_ptr += size;
				}
			}
		}
		
		verpv(left_ptr);
		verpv(right_ptr);
		
		for(;!error && left_ptr < left_end;left_ptr += size)
			memcpy(dest_ptr, left_ptr, size), dest_ptr += size;
			
		for(;!error && right_ptr < right_end;right_ptr += size)
			memcpy(dest_ptr, right_ptr, size), dest_ptr += size;
		
		verpv(left_ptr);
		verpv(right_ptr);
		
		*is_dest_in_a = !is_left_child_dest_in_a;
	}
	
	verpvb(*is_dest_in_a);
	
	EXIT;
	return error;
}

int msort(
	void* data,
	size_t n,
	size_t size,
	int (*cmp)(void* a, void* b, int* result))
{
	int error = 0;
	bool is_result_in_data;
	void* temp_space;
	ENTER;
	
	verpv(n);
	verpv(size);
	verpv(data);
	
	if(n > 1)
	{
		temp_space = malloc(n * size);

		verpv(temp_space);
		
		error = msort_helper(data, temp_space,
			&is_result_in_data, n, size, cmp);
		
		if(!is_result_in_data)
		{
			memcpy(data, temp_space, size * n);
		}
		
		free(temp_space);
	}
	
	EXIT;
	return error;
}

















