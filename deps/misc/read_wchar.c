
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <wchar.h>

#include <debug.h>

#include <utf-8/decode.h>
#include <utf-8/how_many.h>

#include "read_wchar.h"

int read_wchar(int fd, wchar_t* current_char)
{
	int error = 0;
	int r;
	unsigned char c;
	char utf8_character[6];
	int utf8_character_length;
	int how_many;
	ENTER;
	r = read(fd, &c, 1);
	verpv(r);
	if(r > 0)
	{
		if (c > 127)
		{
			utf8_character_length = 0;
			utf8_character[utf8_character_length++] = c;
			error = utf8_how_many(c, &how_many);
			for (int i = 1; !error && i < how_many; i++)
			{
				error = (read(fd, &c, 1) < 1);
				if(!error)
				{
					utf8_character[utf8_character_length++] = c;
				}
			}
			
			if(!error)
			{
				error = utf8_decode(utf8_character, current_char);
			}
			
		} else {
			*current_char = c;
		}
	} else if (r < 0)
		perror("read"), error = 1;
	else
		*current_char = '\0';
	verpv(*current_char);
	EXIT;
	return error;
}




















