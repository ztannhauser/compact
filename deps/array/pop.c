
#include <assert.h>
#include <string.h>

#include "struct.h"

#include "pop.h"

void array_pop(struct array *a, unsigned long index) {
	assert(a->n);
	assert(0 <= index && index <= a->n - 1);
	size_t mem_index = index * a->elesize;
	unsigned char *i_t = &(a->data[mem_index]);
	unsigned char *ip1_t = &(a->data[mem_index + a->elesize]);
	a->n--;
	memmove(i_t, ip1_t, a->n * a->elesize - mem_index);
}
