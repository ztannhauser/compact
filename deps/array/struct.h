
#ifndef ARRAY_STRUCT_H
#define ARRAY_STRUCT_H

#include <inttypes.h>
#include <wchar.h>

struct array
{
	union {
		unsigned char *data;
		signed char *datasc;
		char *datac; char **datacp;
		void *datav; void **datavp;
		wchar_t *datawc;
	};
	uintmax_t n;
	uintmax_t elesize;
	uintmax_t mem_length;
};

#define arrayptr_index(array, index)\
	array_index(array, index, void*)

#define array_index(array, index, datatype)\
	(((datatype *) ((array).data))[index])

#define arrayp_index(array, index)\
	((array).data + (index) * (array).elesize)

#endif
