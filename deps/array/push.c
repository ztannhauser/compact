
#include <assert.h>
#include <stdlib.h>
#include <string.h>

#include "allocate_for_more.h"
#include "struct.h"

#include "push.h"

void *array_push(struct array *this, unsigned long index, void *ele) {
	assert(0 <= index && index <= this->n);
	array_allocate_for_more(this, 1);
	void *dest = this->data + index * this->elesize;
	memmove(dest + this->elesize, dest, (this->n - index) * this->elesize);
	if (ele) {
		memcpy(dest, ele, this->elesize);
	}
	this->n++;
	return dest;
}
