
#include <string.h>

#include "allocate_for_more.h"
#include "struct.h"

#include "push_n.h"

void *array_push_n(struct array *this, const void *ele) {
	array_allocate_for_more(this, 1);
	void *dest = this->data + this->n * this->elesize;
	if (ele) {
		memcpy(dest, ele, this->elesize);
	}
	this->n++;
	return dest;
}
