#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "allocate_for_more.h"
#include "struct.h"

#include "mass_push_n.h"

void *array_mass_push_n(struct array *this, const void *ele, unsigned long n) {
	array_allocate_for_more(this, n);
	void *dest = this->data + this->n * this->elesize;
	if (ele) {
		memcpy(dest, ele, n * this->elesize);
	}
	this->n += n;
	return dest;
}
