#include <stdio.h>
#include <stdlib.h>

#include "./struct.h"

#include "init_node.h"

struct avl_node *avl_init_node(struct avl_node *node, void *item) {
	node->parent = NULL;
	node->next = node->prev = NULL;
	node->left = node->right = NULL;
	node->height = 0;
	node->item = item;
	return node;
}
