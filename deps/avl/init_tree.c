#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include "struct.h"

#include "init_tree.h"

struct avl_tree *avl_init_tree(struct avl_tree *this,
							   int (*compare)(const void *a, const void *b),
							   void (*free)(void *a)) {
	assert(this), assert(compare);
	this->root = NULL;
	this->leftmost = this->rightmost = NULL;
	this->n = 0;
	this->compare = compare;
	this->free = free;
	return this;
}
