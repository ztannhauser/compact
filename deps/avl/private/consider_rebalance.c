#include <stdio.h>
#include <stdlib.h>

#include "../struct.h"

#include "./rotate_left_node.h"
#include "./rotate_right_node.h"
#include "./update_height.h"

#include "consider_rebalance.h"

static int private_avl_get_node_balance(struct avl_node *node) {
	return (node->left ? node->left->height : 0) -
		   (node->right ? node->right->height : 0);
}

struct avl_node *private_avl_consider_rebalance(struct avl_node *node,
												struct avl_node *parent) {
	private_avl_update_height(node);
	int bal = private_avl_get_node_balance(node);
	if (bal > 1) {
		int c = private_avl_get_node_balance(node->left);
		if (c >= 0) {
			node = private_avl_rotate_right_node(node, parent);
		} else {
			node->left = private_avl_rotate_left_node(node->left, node);
			node = private_avl_rotate_right_node(node, parent);
		}
	} else if (bal < -1) {
		int c = private_avl_get_node_balance(node->right);
		if (c <= 0) {
			node = private_avl_rotate_left_node(node, parent);
		} else {
			node->right = private_avl_rotate_right_node(node->right, node);
			node = private_avl_rotate_left_node(node, parent);
		}
	}
	return node;
}
