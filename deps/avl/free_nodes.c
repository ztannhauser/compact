#include <stdio.h>
#include <stdlib.h>

#include "./struct.h"

#include "free_nodes.h"

void avl_free_nodes(struct avl_tree *this) {
	if (this->n) {
		for (struct avl_node *i = this->leftmost, *j = i->next; i;
			 i = j, j = i ? i->next : NULL) {
			(this->free)(i->data);
			free(i);
		}
		this->root = NULL;
	}
}
