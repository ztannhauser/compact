#include <stdio.h>
#include <stdlib.h>

#include "./init_node.h"
#include "./insert_node.h"
#include "./struct.h"

#include "insert.h"

struct avl_node *avl_insert(struct avl_tree *this, void *data) {
	struct avl_node *new = malloc(sizeof(struct avl_node));
	avl_init_node(new, data);
	avl_insert_node(this, new);
	return new;
}
