#!/bin/bash
set -ev

# ./compact --lastval-exit-code '
valgrind --gen-suppressions=yes ./compact.debug --lastval-exit-code '
	let call-test = function(test-case) {
		read-all(popen-r(["./compact"] + test-case.inputs))
	},
	process-test-case = function(test-case) {
		puts("Running test " + str(test-case) + ":");
		let actual-output = call-test(test-case): (
			puts("\tActual Output: " + str(actual-output));
			test-case.output == actual-output
		)
	},
	process-test-file = function(json-path) {
		puts("Running test file \"" + json-path + "\" ...");
		let json = fromjson(open(json-path)):
			mass-and(process-test-case, json)
	},
	json-paths = sort(
		filter(
			ls-r("tests"),
			``endswith(x, ".json")
		),
		strverscasecmp
	):
	mass-and(process-test-file, json-paths) ? 0 : 1
'

