
#include <stdlib.h>

#include "../struct.h"

#include "delete.h"
#include "evaluate.h"
#include "print.h"

#include "callbacks.h"

struct expression_callbacks let_expression_callbacks = {
	.evaluate = callbackptr_let_expression_evaluate,
	.print = callbackptr_let_expression_print,
	.delete = callbackptr_delete_let_expression,
};
