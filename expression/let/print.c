
#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <wchar.h>

#include <debug.h>

#include "../print.h"

#include "struct.h"

#include "print.h"

int let_expression_print(struct let_expression *this,
						 int (*push)(const wchar_t *, size_t)) {
	int error = 0;
	ENTER;

	error = push(L"let ", 4);

	HERE;
	verpv(error);

	for (struct llnode *current = this->bundles.first; !error && current;
		 current = current->next) {
		struct let_expression_bundle *ele = (current->data);

		HERE;
		verpv(error);

		error = 0
					?: push(ele->name, wcslen(ele->name))
						   ?: push(L" = ", 3)
								  ?: expression_print(ele->subexpr, push);

		HERE;
		verpv(error);

		if (!error && current->next) {
			error = push(L", ", 2);
		}
	}

	HERE;
	verpv(error);

	error = 0 ?: error ?: push(L": ", 2) ?: expression_print(this->inner, push);

	EXIT;
	return error;
}

int callbackptr_let_expression_print(struct expression *this,
									 int (*push)(const wchar_t *, size_t)) {
	int error;
	ENTER;

	error =
		0
			?: this->kind != ek_let
				   ?: let_expression_print((struct let_expression *)this, push);
	;

	EXIT;
	return error;
}
