
#ifndef LET_EXPRESSION_STRUCT_H
#define LET_EXPRESSION_STRUCT_H

#include <linkedlist/struct.h>

#include "../struct.h"

struct let_expression_bundle
{
	wchar_t name[256];
	struct expression* subexpr;
};

struct let_expression
{
	struct expression super;
	struct linkedlist bundles; // struct let_expression_bundle*
	struct expression* inner;
};

#endif
