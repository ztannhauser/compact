
#include <assert.h>
#include <stdio.h>

#include <debug.h>

#include <linkedlist/delete.h>
#include <linkedlist/foreach.h>
#include <linkedlist/struct.h>

#include <value/delete.h>

#include "../delete.h"

#include "struct.h"

#include "delete.h"

static int delete_bundle(struct let_expression_bundle *this) {
	int error;
	ENTER;
	verpv(this);
	error = delete_expression(this->subexpr);
	free(this);
	EXIT;
	return error;
}

static int delete_let_expression(struct let_expression *this) {
	int error;
	ENTER;
	
	error = 0
		?: linkedlist_foreach(&(this->bundles),
			(int (*)(void *)) delete_bundle)
		?: delete_expression(this->inner)
		;
	
	if(!error)
	{
		delete_linkedlist(&(this->bundles));
		free(this);
	}
	
	EXIT;
	return error;
}

int callbackptr_delete_let_expression(struct expression *this) {
	int error;
	ENTER;
	
	error = 0
		?: this->kind != ek_let
		?: delete_let_expression((struct let_expression *)this)
		;
	
	EXIT;
	return error;
}














