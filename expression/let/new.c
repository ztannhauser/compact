
#include <stdio.h>

#include <debug.h>
#include <linkedlist/struct.h>

#include "../new.h"
#include "../struct.h"

#include "callbacks.h"
#include "struct.h"

#include "new.h"

struct let_expression *new_let_expression(struct linkedlist bundles,
										  struct expression *inner) {
	struct let_expression *this = (struct let_expression *)new_expression(
		ek_let, &(let_expression_callbacks), sizeof(*this));
	verpv(this);
	this->bundles = bundles;
	this->inner = inner;
	return this;
}
