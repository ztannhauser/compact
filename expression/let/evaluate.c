
#include <assert.h>
#include <stdio.h>
#include <string.h>

#include <debug.h>

#include <linkedlist/foreach.h>
#include <linkedlist/struct.h>

#include <scope/close.h>
#include <scope/open.h>
#include <scope/push.h>

#include "../evaluate.h"

#include "struct.h"

#include "evaluate.h"

static int let_expression_evaluate(struct let_expression *this,
								   struct scope *scope, struct value **result) {
	int error = 0;
	struct value *subexpr_value;
	ENTER;

	error = scope_open(scope);

	for (struct llnode *current = this->bundles.first; !error && current;
		 current = current->next) {
		struct let_expression_bundle *ele = current->data;

		error =
			0
				?: expression_evaluate(ele->subexpr, scope, &subexpr_value)
					   ?: scope_push(scope, ele->name, subexpr_value, false);
	}

	error = 0
				?: error
					   ?: expression_evaluate(this->inner, scope, result)
							  ?: scope_close(scope);

	EXIT;
	return error;
}

int callbackptr_let_expression_evaluate(struct expression *this,
										struct scope *scope,
										struct value **result) {
	int error = 0;
	ENTER;

	error = 0
				?: this->kind != ek_let
					   ?: let_expression_evaluate((struct let_expression *)this,
												  scope, result);

	EXIT;
	return error;
}
