
#ifndef BINARY_EXPRESSION_H
#define BINARY_EXPRESSION_H

#include "../struct.h"

enum binary_operation
{
	bo_less_than,
	bo_less_than_equal_to,
	bo_greater_than,
	bo_greater_than_equal_to,
	bo_equal_to,
	bo_not_equal_to,
	
	bo_logical_or,
	bo_logical_and,
	
	bo_leftshift,
	bo_rightshift,
	bo_bitwise_and,
	bo_bitwise_or,
	bo_bitwise_xor,
	
	bo_addition,
	bo_subtraction,
	bo_multiply,
	bo_divide,
	bo_remainder_divide,
	bo_exponential,
	
	bo_n,
};

struct binary_expression
{
	struct expression super;
	enum binary_operation operation;
	struct expression* left;
	struct expression* right;
};

#endif

