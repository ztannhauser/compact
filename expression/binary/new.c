
#include <stdio.h>

#include <debug.h>

#include "../new.h"
#include "../struct.h"

#include "callbacks.h"
#include "struct.h"

#include "new.h"

struct binary_expression *new_binary_expression(char operation,
												struct expression *left,
												struct expression *right) {
	struct binary_expression *this = (struct binary_expression *)new_expression(
		ek_binary, &(binary_expression_callbacks), sizeof(*this));
	verpv(this);
	this->operation = operation;
	this->left = left;
	this->right = right;
	return this;
}
