
#include <assert.h>
#include <debug.h>
#include <stdio.h>

#include "../delete.h"

#include "struct.h"

#include "delete.h"

static int delete_binary_expression(struct binary_expression *this) {
	int error;
	ENTER;
	
	error = 0
		?: delete_expression(this->left)
		?: delete_expression(this->right)
		;
	
	if(!error)
	{
		free(this);
	}
	
	EXIT;
	return error;
}

int callbackptr_delete_binary_expression(struct expression *this) {
	int error;
	ENTER;
	
	error = 0
		?: this->kind != ek_binary
		?: delete_binary_expression((struct binary_expression *)this)
		;
	
	EXIT;
	return error;
}
