
#include <assert.h>
#include <stdio.h>

#include "debug.h"

#include "value/delete.h"
#include "value/struct.h"

#include "expression/evaluate.h"
#include "expression/struct.h"

#include "scope/builtins/math/arith/sub.h"

#include "subtraction.h"

int subtraction_operator(
	struct expression *eleft,
	struct expression *eright,
	struct scope *scope,
	struct value **result)
{
	int error = 0;
	struct value* left;
	struct value* right;
	ENTER;
	
	error = 0
		?: expression_evaluate(eleft, scope, &left)
		?: expression_evaluate(eright, scope, &right)
		?: left->kind != right->kind;
	
	if(!error)
	{
		switch(left->kind)
		{
			case vk_number:
			{
				*result = (struct value*) handle_numeral_subtract(
					(struct number_value*) left,
					(struct number_value*) right);
				break;
			}
			default: TODO;
		}
	}
	
	if(!error)
	{
		error = 0
			?: delete_value(left)
			?: delete_value(right);
	}
	EXIT;
	return error;
}



















