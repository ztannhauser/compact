
#include <assert.h>
#include <stdio.h>

#include "debug.h"

#include "value/delete.h"
#include "value/number/new.h"
#include "value/number/struct.h"
#include "value/struct.h"

#include "expression/evaluate.h"
#include "expression/struct.h"

#include "leftshift.h"

struct value *leftshift_operator(struct expression *eleft,
								 struct expression *eright) {
#if 0
	struct value* ret;
	ENTER;
	struct value* left = expression_evaluate(eleft);
	struct value* right = expression_evaluate(eright);
	assert(left->kind == right->kind);
	switch(left->kind)
	{
		case vk_number:
		{
			struct number_value* spef_left = (struct number_value*) left;
			switch(right->kind)
			{
				case vk_number:
				{
					struct number_value* spef_right = (struct number_value*) right;
					switch(spef_left->kind)
					{
						case nvk_integer:
						{
							switch(spef_right->kind)
							{
								case nvk_integer:
								{
									signed long l = spef_left->integer;
									signed long r = spef_right->integer;
									if(r > 0)
									{
										signed long x = l, rr = r;
										while(rr && x < LONG_MAX / 2)
											x <<= 1, rr--;
										if(rr != 0)
										{
											// roll over found, use mpz_longs
											// instead
											mpz_ptr result = malloc(sizeof(MP_INT));
											mpz_init_set_si(result, l);
											assert(r >= 0);
											mpz_mul_2exp(result, result, r);
											ret = (struct value*)
												new_number_value_as_long(result);
										}
										else
										{
											ret = (struct value*)
												new_number_value_as_integer(x);
										}
									}
									else if(r < 0)
									{
										ret = (struct value*)
											new_number_value_as_integer(l << r);
									}
									else // if(r == 0)
									{
										ret = left;
										ret->refcount++;
									}
									break;
								}
								default: TODO;
							}
							break;
						}
						case nvk_long:
						{
							switch(spef_right->kind)
							{
								case nvk_integer:
								{
									signed long r = spef_right->integer;
									verpv(r);
									mpz_ptr result = malloc(sizeof(MP_INT));
									mpz_init(result);
									assert(r >= 0);
									mpz_mul_2exp(result,
										spef_left->mpz_long, r);
									ret = (struct value*)
										new_number_value_as_long(result);
									break;
								}
								default: TODO;
							}
							break;
						}
						default: TODO;
					}
					break;
				}
				default: TODO;
			}
			break;
		}
		default: TODO;
	}
	delete_value(left), delete_value(right);
	EXIT;
	return ret;
#endif
	TODO;
}
