
#include <assert.h>
#include <stdio.h>

#include "debug.h"

#include "array/foreach.h"
#include "array/mass_push_n.h"
#include "array/new.h"
#include "array/struct.h"

#include "value/delete.h"
#include "value/inc.h"
#include "value/list/new.h"
#include "value/list/struct.h"
#include "value/string/new.h"
#include "value/string/struct.h"
#include "value/struct.h"

#include "expression/evaluate.h"
#include "expression/struct.h"

#include "scope/builtins/math/arith/add.h"

#include "addition.h"

static int string_concat(
	struct string_value* a,
	struct string_value* b,
	struct string_value** result)
{
	int error = 0;
	struct array chars;
	ENTER;
	
	chars = new_array(wchar_t);
	
	array_mass_push_n(&chars, a->wchars.data, a->wchars.n);
	array_mass_push_n(&chars, b->wchars.data, b->wchars.n);
	
	*result = new_string_value(chars);
	
	EXIT;
	return error;
}

static int list_concat(
	struct list_value* a,
	struct list_value* b,
	struct list_value** result)
{
	int error = 0;
	struct array values;
	ENTER;
	if(a->comparator &&
		(a->comparator == b->comparator))
	{
		TODO;
	}
	else
	{
		values = new_array(struct value*);
		
		error = 0
			?: arrayp_foreach(&a->values, (int (*)(void *)) value_inc)
			?: arrayp_foreach(&b->values, (int (*)(void *)) value_inc);
		
		array_mass_push_n(&values, a->values.data, a->values.n);
		array_mass_push_n(&values, b->values.data, b->values.n);
		*result = new_list_value(values, NULL);
	}
	
	EXIT;
	return error;
}


int addition_operator(
	struct expression *eleft,
	struct expression *eright,
	struct scope *scope,
	struct value **result)
{
	int error = 0;
	struct value *left, *right;
	ENTER;

	error = expression_evaluate(eleft, scope, &left);

	if (!error) {
		error = expression_evaluate(eright, scope, &right);
		if (!error) {
			if (left->kind != right->kind) {
				fprintf(stderr, "Addition operator must be given values"
								" of same type!");
				error = 1;
			} else {
				switch (left->kind)
				{
					case vk_number:
					{
						error = handle_numeral_add(
							(struct number_value *) left,
							(struct number_value *) right,
							(struct number_value **) result);
						
						break;
					}
					case vk_string:
					{
						
						error = string_concat(
							(struct string_value *) left,
							(struct string_value *) right,
							(struct string_value **) result);
						
						break;
					}
					
					case vk_list:
					{
						
						error = list_concat(
							(struct list_value *) left,
							(struct list_value *) right,
							(struct list_value **) result);
						
						break;
					}
					
					default:
					{
						TODO; // "Unsupported types for the '+' operator!
						error = 1;
					}
				}
			}

			error = error ?: delete_value(right);
		}

		error = error ?: delete_value(left);
	}

	EXIT;
	return error;
}
