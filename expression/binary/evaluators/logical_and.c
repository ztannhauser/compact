
#include <assert.h>
#include <stdbool.h>
#include <stdio.h>

#include "debug.h"

#include "value/delete.h"
#include "value/struct.h"

#include "expression/evaluate.h"

#include "scope/builtins/cast/bool.h"
#include "scope/fetch/boolean.h"

#include "logical_and.h"

int logical_and_operator(
	struct expression *eleft,
	struct expression *eright,
	struct scope *scope,
	struct value **result)
{
	int error = 0;
	bool bool_and = true;
	struct value* val = NULL;
	ENTER;
	
	// left:
	{
		if(!error)
		{
			error = expression_evaluate(eleft, scope, &val);
			if(!error)
			{
				bool_and = as_bool(val);
			}
		}
		
		if(val) error = delete_value(val), val = NULL;
	}
	
	// right:
	{
		if(!error && bool_and)
		{
			error = expression_evaluate(eright, scope, &val);
			if(!error)
			{
				bool_and = as_bool(val);
			}
		}
		
		if(val) error = delete_value(val), val = NULL;
	}
	
	if(!error)
	{
		verpvb(bool_and);
		*result = (struct value*) scope_fetch_boolean(scope, bool_and);
	}
	
	EXIT;
	return error;
}
















