
#include <assert.h>
#include <stdio.h>

#include "debug.h"

#include "value/delete.h"
#include "value/number/new.h"
#include "value/number/struct.h"
#include "value/struct.h"

#include "expression/evaluate.h"
#include "expression/struct.h"

#include "rightshift.h"

struct value *rightshift_operator(struct expression *eleft,
								  struct expression *eright) {
#if 0
	struct value* ret;
	ENTER;
	struct value* gleft = expression_evaluate(eleft);
	struct value* gright = expression_evaluate(eright);
	assert(gleft->kind == gright->kind);
	switch(gleft->kind)
	{
		case vk_number:
		{
			struct number_value* left = (struct number_value*) gleft;
			struct number_value* right = (struct number_value*) gright;
			verpv(left);
			verpv(right);
			switch(left->kind)
			{
				case nvk_integer:
				{
					switch(right->kind)
					{
						case nvk_integer:
						{
							ret = (struct value*) new_number_value_as_integer(
								left->integer >> right->integer);
							break;
						}
						default: TODO;
					}
					break;
				}
				case nvk_long:
				{
					switch(right->kind)
					{
						case nvk_integer:
						{
							signed long r = right->integer;
							mpz_ptr result = malloc(sizeof(MP_INT));
							mpz_init(result);
							assert(r >= 0);
							mpz_fdiv_q_2exp(result, left->mpz_long, r);
							ret = (struct value*) new_number_value_as_long(result);
							break;
						}
						default: TODO;
					}
					break;
				}
				default: TODO;
			}
			break;
		}
		default: TODO;
	}
	delete_value(gleft), delete_value(gright);
	EXIT;
	return ret;
#endif
	TODO;
}
