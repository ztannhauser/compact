
#include <assert.h>
#include <stdio.h>

#include "debug.h"

#include "value/delete.h"
#include "value/struct.h"

#include "expression/evaluate.h"
#include "expression/struct.h"

#include "scope/builtins/math/arith/mult.h"

#include "multiply.h"

int multiply_operator(struct expression *eleft, struct expression *eright,
					  struct scope *scope, struct value **result) {
	int error;
	struct value *left, *right;
	ENTER;

	error = expression_evaluate(eleft, scope, &left);

	if (!error) {
		error = expression_evaluate(eright, scope, &right);
		if (!error) {
			if (left->kind != right->kind) {
				fprintf(stderr, "Multiplication operator must be given values"
								" of same type!");
				error = 1;
			} else {
				switch (left->kind) {
					case vk_number: {
						error = handle_numeral_multiply(
							(struct number_value *)left,
							(struct number_value *)right,
							(struct number_value **)result);

						break;
					}
					default:
						TODO;
				}
			}

			error = error ?: delete_value(right);
		}

		error = error ?: delete_value(left);
	}

	EXIT;
	return error;
}
