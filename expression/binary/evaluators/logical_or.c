
#include <assert.h>
#include <stdbool.h>
#include <stdio.h>

#include "debug.h"

#include "value/delete.h"
#include "value/struct.h"

#include "expression/evaluate.h"

#include "scope/builtins/cast/bool.h"
#include "scope/fetch/boolean.h"

#include "logical_or.h"

int logical_or_operator(
	struct expression *eleft,
	struct expression *eright,
	struct scope* scope,
	struct value** result)
{
	int error = 0;
	struct value* value;
	bool val = false;
	ENTER;
	
	if(!error && !val)
	{
		error = expression_evaluate(eleft, scope, &value);
		
		if(!error)
		{
			val = as_bool(value);
		}
		
		if(!error)
		{
			error = delete_value(value);
		}
	}
	
	if(!error && !val)
	{
		error = expression_evaluate(eright, scope, &value);
		
		if(!error)
		{
			val = as_bool(value);
		}
		
		if(!error)
		{
			error = delete_value(value);
		}
	}
	
	if(!error)
	{
		*result = (struct value*) scope_fetch_boolean(scope, val);
	}
	
	EXIT;
	return error;
}

















