
#include <assert.h>
#include <stdio.h>

#include "debug.h"

#include "value/delete.h"
#include "value/struct.h"

#include "expression/evaluate.h"
#include "expression/struct.h"

#include "scope/builtins/math/arith/remain.h"

#include "remainder_divide.h"

struct value *remainder_divide_operator(struct expression *eleft,
										struct expression *eright) {
#if 0
	struct value* ret;
	ENTER;
	struct value* left = expression_evaluate(eleft);
	struct value* right = expression_evaluate(eright);
	assert(left->kind == right->kind);
	switch(left->kind)
	{
		case vk_number:
		{
			ret = (struct value*) handle_numeral_remainder_divide(
				(struct number_value*) left,
				(struct number_value*) right);
			break;
		}
		default: TODO;
	}
	delete_value(left), delete_value(right);
	EXIT;
	return ret;
#endif
	TODO;
}
