
#include <assert.h>
#include <gmp.h>
#include <stdio.h>

#include "debug.h"

#include "expression/evaluate.h"
#include "expression/struct.h"

#include "value/delete.h"
#include "value/number/new.h"
#include "value/number/struct.h"
#include "value/struct.h"

#include "bitwise_xor.h"

struct value *bitwise_xor_operator(struct expression *eleft,
								   struct expression *eright) {
#if 0
	struct value* ret;
	ENTER;
	struct value* gleft = expression_evaluate(eleft);
	struct value* gright = expression_evaluate(eright);
	assert(gleft->kind == vk_number && gright->kind == vk_number);
	struct number_value* left = (struct number_value*) gleft;
	struct number_value* right = (struct number_value*) gright;
	switch(left->kind)
	{
		case nvk_integer:
		{
			switch(right->kind)
			{
				case nvk_integer:
				{
					ret = (struct value*)new_number_value_as_integer(
						left->integer ^ right->integer);
					break;
				}
				default: TODO;
			}
			break;
		}
		case nvk_long:
		{
			switch(right->kind)
			{
				case nvk_long:
				{
					mpz_ptr result = malloc(sizeof(__mpz_struct));
					mpz_init(result);
					mpz_xor(result, left->mpz_long, right->mpz_long);
					ret = (struct value*) new_number_value_as_long(result);
					break;
				}
				default: TODO;
			}
			break;
		}
		default: TODO;
	}
	delete_value(gleft), delete_value(gright);
	EXIT;
	return ret;
#endif
	TODO;
}
