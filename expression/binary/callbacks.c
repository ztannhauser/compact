
#include <stdlib.h>

#include "../struct.h"

#include "delete.h"
#include "evaluate.h"
#include "print.h"

#include "callbacks.h"

struct expression_callbacks binary_expression_callbacks = {
	.evaluate = callbackptr_binary_expression_evaluate,
	.print = callbackptr_binary_expression_print,
	.delete = callbackptr_delete_binary_expression,
};
