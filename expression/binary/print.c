
#include <assert.h>
#include <stdio.h>
#include <string.h>

#include <array/struct.h>
#include <debug.h>

#include <expression/print.h>

#include "struct.h"

#include "print.h"

static const wchar_t *lookup[] = {
	[bo_less_than] = L" < ",		[bo_less_than_equal_to] = L" <= ",
	[bo_greater_than] = L" > ",		[bo_greater_than_equal_to] = L" >= ",
	[bo_equal_to] = L" == ",		[bo_not_equal_to] = L" != ",

	[bo_logical_or] = L" || ",		[bo_logical_and] = L" && ",

	[bo_leftshift] = L" << ",		[bo_rightshift] = L" >> ",
	[bo_bitwise_and] = L" & ",		[bo_bitwise_or] = L" | ",
	[bo_bitwise_xor] = L" ^ ",

	[bo_addition] = L" + ",			[bo_subtraction] = L" - ",
	[bo_multiply] = L" * ",			[bo_divide] = L" / ",
	[bo_remainder_divide] = L" % ", NULL,
};

static int binary_expression_print(struct binary_expression *this,
								   int (*push)(const wchar_t *, size_t)) {
	int error;
	ENTER;

	const wchar_t *op = lookup[this->operation];

	error = 0
				?: !op
					   ?: expression_print(this->left, push)
							  ?: push(op, wcslen(op))
									 ?: expression_print(this->right, push);

	EXIT;
	return error;
}

int callbackptr_binary_expression_print(struct expression *this,
										int (*push)(const wchar_t *, size_t)) {
	int error;
	ENTER;

	error = 0
				?: (this->kind != ek_binary)
					   ?: binary_expression_print(
							  (struct binary_expression *)this, push);

	EXIT;
	return error;
}
