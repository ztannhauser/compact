
#include <assert.h>
#include <stdbool.h>
#include <stdio.h>

#include <debug.h>

#include <array/struct.h>

#include <scope/builtins/cast/bool.h>
#include <scope/builtins/compare.h>
#include <scope/builtins/math/arith/add.h>
#include <scope/builtins/math/arith/mult.h>
#include <scope/builtins/math/arith/sub.h>
#include <scope/fetch/boolean.h>

#include <value/boolean/struct.h>
#include <value/delete.h>
#include <value/inc.h>
#include <value/list/new.h>
#include <value/list/struct.h>
#include <value/number/new.h>
#include <value/number/struct.h>
#include <value/string/new.h>
#include <value/string/struct.h>
#include <value/struct.h>

#include "evaluators/addition.h"
#include "evaluators/bitwise_and.h"
#include "evaluators/bitwise_or.h"
#include "evaluators/bitwise_xor.h"
#include "evaluators/divide.h"
#include "evaluators/exponential.h"
#include "evaluators/leftshift.h"
#include "evaluators/logical_and.h"
#include "evaluators/logical_or.h"
#include "evaluators/multiply.h"
#include "evaluators/remainder_divide.h"
#include "evaluators/rightshift.h"
#include "evaluators/subtraction.h"

#include "../evaluate.h"

#include "struct.h"

#include "evaluate.h"

static int value_compare(
	struct expression* eleft,
	struct expression* eright,
	struct scope* scope,
	int* result)
{
	int error;
	struct value* left;
	struct value* right;
	ENTER;
	
	error = 0
		?: expression_evaluate(eleft, scope, &left)
		?: expression_evaluate(eright, scope, &right);
	
	if(!error)
	{
		*result = default_compare(left, right);
	}
	
	error = 0
		?: error
		?: delete_value(left)
		?: delete_value(right);
	
	EXIT;
	return error;
}

static int binary_expression_evaluate(struct binary_expression *this,
									  struct scope *scope,
									  struct value **result) {
	int error;
	int value_compare_ret;
	ENTER;
	switch (this->operation) {
		case bo_addition:
			error = addition_operator(this->left, this->right, scope, result);
			break;
		case bo_bitwise_and:
/*			ret = bitwise_and_operator(left, right); break;*/
			TODO;
		case bo_bitwise_or:
/*			ret = bitwise_or_operator(left, right); break;*/
			TODO;
		case bo_bitwise_xor:
/*			ret = bitwise_xor_operator(left, right); break;*/
			TODO;
		case bo_divide:
/*			ret = divide_operator(left, right); break;*/
			TODO;
		case bo_exponential:
/*			ret = exponential_operator(left, right); break;*/
			TODO;
		case bo_leftshift:
/*			ret = leftshift_operator(left, right); break;*/
			TODO;
		case bo_logical_and:
			error = logical_and_operator(this->left, this->right, scope, result);
			break;
			TODO;
		case bo_logical_or:
			error = logical_or_operator(this->left, this->right, scope, result);
			break;
		case bo_multiply:
			error = multiply_operator(this->left, this->right, scope, result);
			break;
		case bo_remainder_divide:
/*			ret = remainder_divide_operator(left, right); break;*/
			TODO;
		case bo_rightshift:
/*			ret = rightshift_operator(left, right); break;*/
			TODO;
		case bo_subtraction:
			error = subtraction_operator(this->left, this->right, scope, result);
			break;

		case bo_greater_than:
		{
			error = value_compare(this->left, this->right,
				scope, &value_compare_ret);
			if(!error)
			{
				*result = (struct value*)
					scope_fetch_boolean(scope, value_compare_ret > 0);
			}
			break;
		}
		case bo_greater_than_equal_to:
		{
			#if 0
			ret = (struct value*)
				scope_fetch_boolean(value_compare(left, right) >= 0);
			#endif
			TODO;
			break;
		}
		case bo_less_than:
		{
			#if 0
			ret = (struct value*)
				scope_fetch_boolean(value_compare(left, right) < 0);
			#endif
			TODO;
			break;
		}
		case bo_less_than_equal_to:
		{
			#if 0
			ret = (struct value*)
				scope_fetch_boolean(value_compare(left, right) <= 0);
			#endif
			TODO;
			break;
		}
		case bo_equal_to:
		{
/*			ret = (struct value*)*/
/*				scope_fetch_boolean(value_compare(left, right) == 0);*/
			TODO;
			break;
		}
		case bo_not_equal_to:
		{
			#if 0
			ret = (struct value*)
				scope_fetch_boolean(value_compare(left, right) != 0);
			#endif
			TODO;
			break;
		}
		default: { TODO; }
	}
	EXIT;
	return error;
}

int callbackptr_binary_expression_evaluate(struct expression *this,
										   struct scope *scope,
										   struct value **result) {
	int error;
	ENTER;

	error = 0
				?: this->kind != ek_binary
					   ?: binary_expression_evaluate(
							  (struct binary_expression *)this, scope, result);

	EXIT;
	return error;
}
