
#include <expression/struct.h>

struct binary_expression* new_binary_expression(
	char operation,
	struct expression* left,
	struct expression* right);
