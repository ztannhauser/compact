
#include <assert.h>
#include <stdio.h>
#include <string.h>

#include "debug.h"

#include "avl/search.h"
#include "avl/struct.h"

#include "linkedlist/struct.h"

#include <call.h>

#include <value/delete.h>
#include <value/list/delete.h>
#include <value/list/struct.h>
#include <value/number/delete.h>
#include <value/number/new.h>
#include <value/number/struct.h>
#include <value/object/element_search.h>
#include <value/object/struct.h>
#include <value/string/struct.h>
#include <value/struct.h>

#include "../evaluate.h"
#include "../struct.h"

#include "struct.h"

#include "evaluate.h"

static int object_get_field(
	struct object_value* object,
	struct string_value* fieldname,
	struct value** result)
{
	int error = 0;
	struct avl_node* node;
	struct object_value_element* ele;
	ENTER;
	
	verpv(object);
	
	node = avl_tree_search(
		&(object->values),
		(int (*)(const void *, const void *)) element_search,
		fieldname);
		
	verpv(node);
	
	if(node)
	{
		ele = node->data;
		ele->value->refcount++;
		*result = ele->value;
	}
	else
	{
		fprintf(stderr, "compact: cannot get field from object!\n");
		error = 1;
	}
	
	EXIT;
	return error;
}

static int list_get_field(
	struct list_value* list,
	struct string_value* fieldname,
	struct value** result)
{
	// could be 'length' or 'n', 'first', or'last'
	int error = 0;
	ENTER;
	
	if(fieldname->wchars.n == 6 &&
		!wmemcmp(fieldname->wchars.datawc, L"length", 6))
	{
		*result = (struct value*) new_number_value_as_integer(list->values.n);
	}
	else if(fieldname->wchars.n == 1 &&
		!wmemcmp(fieldname->wchars.datawc, L"n", 1))
	{
		*result = (struct value*) new_number_value_as_integer(list->values.n);
	}
	else if(fieldname->wchars.n == 5 &&
		!wmemcmp(fieldname->wchars.datawc, L"first", 5))
	{
		// ret = (struct value*) new_number_value_as_integer(spef->wchars.n);
		TODO;
	}
	else if(fieldname->wchars.n == 4 &&
		!wmemcmp(fieldname->wchars.datawc, L"last", 4))
	{
		// ret = (struct value*) new_number_value_as_integer(spef->wchars.n);
		TODO;
	}
	else
	{
		TODO; // print error message, error = 1
	}
	
	EXIT;
	return error;
}

static int string_get_field(
	struct string_value* string,
	struct string_value* fieldname,
	struct value** result)
{
	int error = 0;
	ENTER;
	if(fieldname->wchars.n == 1 &&
		!wmemcmp(fieldname->wchars.datawc, L"n", 1))
	{
		*result = (struct value*) new_number_value_as_integer(string->wchars.n);
	}
	else
	{
		TODO; // print error message, error = 1;
	}
	EXIT;
	return error;
}


static int field_expression_evaluate(struct field_expression *this,
									 struct scope *scope,
									 struct value **result) {
	int error = 0;
	struct value *target;
	ENTER;

	error = expression_evaluate(this->object, scope, &target);

	if (!error)
	{
		switch (target->kind)
		{
			case vk_object:
			{
				error = object_get_field(
					(struct object_value*) target,
					this->fieldname,
					result);
				break;
			}
			case vk_list:
			{
				error = list_get_field(
					(struct list_value*) target,
					this->fieldname,
					result);
				break;
			}
			case vk_string:
			{
				error = string_get_field(
					(struct string_value*) target,
					this->fieldname,
					result);
				break;
			}
			default:
			{
				TODO; // print error message
				break;
			}
		}
	}

	error = error ?: delete_value(target);

	EXIT;
	return error;
}

int callbackptr_field_expression_evaluate(
	struct expression *this,
	struct scope *scope,
	struct value **result)
{
	int error = 0;
	ENTER;

	error = 0
		?: this->kind != ek_field
		?: field_expression_evaluate(
			(struct field_expression *)this, scope, result);

	EXIT;
	return error;
}







