
#include <assert.h>
#include <stdio.h>

#include <debug.h>

#include <linkedlist/struct.h>

#include <value/string/delete.h>

#include "../delete.h"

#include "struct.h"

#include "delete.h"

static int delete_field_expression(struct field_expression *this)
{
	int error;
	ENTER;
	
	error = 0
		?: delete_string_value(this->fieldname)
		?: delete_expression(this->object)
		;

	if(!error)
	{
		free(this);
	}
	
	EXIT;
	return error;
}

int callbackptr_delete_field_expression(struct expression *this)
{
	int error;
	ENTER;
	
	error = 0
		?: this->kind != ek_field
		?: delete_field_expression((struct field_expression *)this)
		;
	
	EXIT;
	return error;
}
