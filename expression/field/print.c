
#include <assert.h>
#include <stdio.h>
#include <string.h>

#include "debug.h"

#include "linkedlist/struct.h"

#include <value/string/struct.h>

#include "../print.h"

#include "struct.h"

#include "print.h"

static int field_expression_print(struct field_expression *this,
								  int (*push)(const wchar_t *, size_t)) {
	int error;
	ENTER;

	error = 0
				?: expression_print(this->object, push)
					   ?: push(L".", 1)
							  ?: push(this->fieldname->wchars.datawc,
									  this->fieldname->wchars.n);

	EXIT;
	return error;
}

int callbackptr_field_expression_print(struct expression *this,
									   int (*push)(const wchar_t *, size_t)) {
	int error;
	ENTER;

	error = 0
				?: this->kind != ek_field
					   ?: field_expression_print(
							  (struct field_expression *)this, push);

	EXIT;
	return error;
}
