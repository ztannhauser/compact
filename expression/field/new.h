
#include "value/string/struct.h"

#include "struct.h"

struct field_expression* new_field_expression(
	struct expression* object,
	struct string_value* fieldname);
