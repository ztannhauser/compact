
#include <stdio.h>
#include <string.h>

#include <debug.h>
#include <linkedlist/struct.h>

#include "../new.h"
#include "../struct.h"

#include "callbacks.h"
#include "struct.h"

#include "new.h"

struct field_expression *new_field_expression(struct expression *object,
											  struct string_value *fieldname) {
	struct field_expression *this = (struct field_expression *)new_expression(
		ek_field, &(field_expression_callbacks), sizeof(*this));
	verpv(this);
	this->object = object;
	this->fieldname = fieldname;
	return this;
}
