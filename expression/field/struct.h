
#ifndef FIELD_EXPRESSION_STRUCT_H
#define FIELD_EXPRESSION_STRUCT_H

#include "value/string/struct.h"

#include "../struct.h"

struct field_expression
{
	struct expression super;
	struct expression* object;
	struct string_value* fieldname;
};

#endif
