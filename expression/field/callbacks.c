
#include <stdlib.h>

#include "../struct.h"

#include "delete.h"
#include "evaluate.h"
#include "print.h"

#include "callbacks.h"

struct expression_callbacks field_expression_callbacks = {
	.evaluate = callbackptr_field_expression_evaluate,
	.print = callbackptr_field_expression_print,
	.delete = callbackptr_delete_field_expression,
};
