
#ifndef CALL_EXPRESSION_STRUCT_H
#define CALL_EXPRESSION_STRUCT_H

#include <linkedlist/struct.h>

#include "../struct.h"

struct call_expression
{
	struct expression super;
	struct expression* func;
	struct linkedlist params; 
};

#endif
