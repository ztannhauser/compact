
#include <assert.h>
#include <stdio.h>

#include <debug.h>
#include <linkedlist/struct.h>

#include "../print.h"

#include "struct.h"

#include "print.h"

static int call_expression_print(struct call_expression *this,
								 int (*push)(const wchar_t *, size_t)) {
	int error;
	ENTER;

	error = 0 ?: expression_print(this->func, push) ?: push(L"(", 1);

	for (struct llnode *current = this->params.first; !error && current;
		 current = current->next) {
		struct expression *ele = current->data;
		error = expression_print(ele, push);
		if (!error && current->next) {
			error = push(L", ", 2);
		}
	}

	error = error ?: push(L")", 1);

	EXIT;
	return error;
}

int callbackptr_call_expression_print(struct expression *this,
									  int (*push)(const wchar_t *, size_t)) {
	int error;
	ENTER;

	error = 0
				?: this->kind != ek_call
					   ?: call_expression_print((struct call_expression *)this,
												push);

	EXIT;
	return error;
}
