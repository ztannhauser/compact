
#include <assert.h>
#include <stdio.h>

#include <debug.h>
#include <linkedlist/delete.h>
#include <linkedlist/foreach.h>
#include <linkedlist/struct.h>

#include "../delete.h"

#include "struct.h"

#include "delete.h"

static int delete_call_expression(struct call_expression *this)
{
	int error;
	ENTER;
	
	error = 0
		?: delete_expression(this->func)
		?: linkedlist_foreach(&(this->params),
			(int (*)(void *))delete_expression)
		;
	
	if(!error)
	{
		delete_linkedlist(&(this->params));
		free(this);
	}
	
	EXIT;
	return error;
}

int callbackptr_delete_call_expression(struct expression *this) {
	int error;
	ENTER;

	error = 0
		?: this->kind != ek_call
		?: delete_call_expression((struct call_expression *)this)
		;

	EXIT;
	return error;
}












