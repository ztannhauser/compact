
#include <assert.h>
#include <stdio.h>

#include <debug.h>
#include <linkedlist/struct.h>

#include <call.h>

#include <value/delete.h>
#include <value/function/struct.h>
#include <value/struct.h>

#include "../evaluate.h"
#include "../struct.h"

#include "struct.h"

#include "evaluate.h"

static int call_expression_evaluate(
	struct call_expression *this,
	struct scope *scope,
	struct value **result)
{
	int error;
	struct value *generic_func = NULL;
	struct function_value *func;
	size_t i = 0, n = 0;
	struct value **param_values = NULL;
	ENTER;

	error = 0
		?: expression_evaluate(this->func, scope, &generic_func)
		?: generic_func->kind != vk_function
		;

	if (!error) {
		func = (struct function_value *)generic_func;
	}

	n = this->params.n;
	param_values = malloc(sizeof(struct value *) * n);

	if (!error) {
		for (struct llnode *current = this->params.first; !error && current;
			 current = current->next, i++) {
			error =
				expression_evaluate(current->data, scope, &(param_values[i]));
		}
	}

	error = error ?: call(func, n, param_values, scope, result);

	for (i = 0; !error && i < n; i++) {
		error = delete_value(param_values[i]);
	}

	error = error ?: delete_value(generic_func);

	free(param_values);

	EXIT;
	return error;
}

int callbackptr_call_expression_evaluate(
	struct expression *this,
	struct scope *scope,
	struct value **result)
{
	int error = 0;
	ENTER;

	error = 0
		?: this->kind != ek_call
		?: call_expression_evaluate(
			(struct call_expression *)this, scope, result)
		;

	EXIT;
	return error;
}





