
#include <assert.h>
#include <stdio.h>

#include <debug.h>
#include <linkedlist/struct.h>

#include "../new.h"
#include "../struct.h"

#include "callbacks.h"
#include "struct.h"

#include "new.h"

struct call_expression *new_call_expression(struct expression *func,
											struct linkedlist params) {
	struct call_expression *this = (struct call_expression *)new_expression(
		ek_call, &(call_expression_callbacks), sizeof(*this));
	verpv(this);
	this->func = func;
	this->params = params;
	return this;
}
