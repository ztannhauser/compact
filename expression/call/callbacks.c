
#include <stdlib.h>

#include "../struct.h"

#include "delete.h"
#include "evaluate.h"
#include "print.h"

#include "callbacks.h"

struct expression_callbacks call_expression_callbacks = {
	.evaluate = callbackptr_call_expression_evaluate,
	.print = callbackptr_call_expression_print,
	.delete = callbackptr_delete_call_expression,
};
