
#include <stdio.h>

#include <debug.h>

#include "struct.h"

#include "print.h"

int expression_print(struct expression *this,
					 int (*push)(const wchar_t *, size_t)) {
	int error = 0;
	ENTER;
	HERE;
	verpv(this);
	HERE;
	error = (this->callbacks->print)(this, push);
	EXIT;
	return error;
}
