
#include "linkedlist/struct.h"

#include "../struct.h"

struct index_expression
{
	struct expression super;
	struct expression* list;
	struct expression* index;
};

