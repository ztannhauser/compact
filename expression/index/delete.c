
#include <assert.h>
#include <stdio.h>

#include <debug.h>

#include <linkedlist/struct.h>

#include "../delete.h"

#include "struct.h"

#include "delete.h"

static int delete_index_expression(struct index_expression *this) {
	int error;
	ENTER;
	
	error = 0
		?: delete_expression(this->list)
		?: delete_expression(this->index)
		;
	
	free(this);

	EXIT;
	return error;
}

int callbackptr_delete_index_expression(struct expression *this) {
	int error;
	ENTER;
	
	error = 0
		?: this->kind != ek_index
		?: delete_index_expression((struct index_expression *)this)
		;
	
	EXIT;
	return error;
}
