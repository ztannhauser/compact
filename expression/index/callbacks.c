
#include <stdlib.h>

#include "../struct.h"

#include "delete.h"
#include "evaluate.h"
#include "print.h"

#include "callbacks.h"

struct expression_callbacks index_expression_callbacks = {
	.evaluate = callbackptr_index_expression_evaluate,
	.print = callbackptr_index_expression_print,
	.delete = callbackptr_delete_index_expression,
};
