
#include <assert.h>
#include <stdio.h>

#include "debug.h"

#include <avl/search.h>
#include <avl/struct.h>

#include <linkedlist/struct.h>

#include "call.h"

#include "value/delete.h"
#include "value/list/delete.h"
#include "value/list/struct.h"
#include "value/number/delete.h"
#include "value/number/struct.h"
#include "value/object/element_search.h"
#include "value/object/struct.h"
#include "value/struct.h"

#include "scope/builtins/string/strcmp.h"

#include "../evaluate.h"
#include "../struct.h"

#include "struct.h"

#include "evaluate.h"

static int index_string(
	struct string_value* string,
	struct number_value* index,
	struct value** result)
{
	int error = 0;
	ENTER;
	TODO;
	EXIT;
	return error;
}

static int index_list(
	struct list_value* list,
	struct number_value* index,
	struct value** result)
{
	int error = 0;
	ENTER;
	switch(index->kind)
	{
		case nvk_integer:
		{
			if(0 <= index->integer && index->integer < list->values.n)
			{
				verpv(index->integer);
				*result = array_index(list->values,
					index->integer, struct value*);
				(*result)->refcount++;
			}
			else
			{
				fprintf(stderr, "failed to index list: Integer intex out of "
					"bounds!\n");
				error = 1;
			}
			break;
		}
		default: TODO;
	}
	EXIT;
	return error;
}

static int index_object(
	struct object_value* object,
	struct string_value* fieldname,
	struct value** result)
{
	int error = 0;
	ENTER;
	#if 0
	assert(generic_index->kind == vk_string);
	struct avl_node* possibly_found_node = avl_tree_search(
		&(spef->values),
		(int (*)(const void *, const void *)) element_search,
		generic_index);
	verpv(possibly_found_node);
	assert(possibly_found_node);
	struct object_value_element* possibly_found_element =
		possibly_found_node->data;
	struct value* found_value = possibly_found_element->value;
	verpv(found_value);
	ret = found_value, ret->refcount++;
	delete_value(generic_index);
	HERE;
	#endif
	TODO;
	EXIT;
	return error;
}

static int index_expression_evaluate(
	struct index_expression* this,
	struct scope* scope,
	struct value** result)
{
	int error = 0;
	struct value* generic_target = NULL;
	struct value* generic_index = NULL;
	ENTER;
	
	error = 0
		?: expression_evaluate(this->list, scope, &generic_target)
		?: expression_evaluate(this->index, scope, &generic_index)
		;
	
	if(!error)
	{
		verpv(generic_target->kind);
		switch(generic_target->kind)
		{
			case vk_string:
			{
				if(generic_index->kind == vk_number)
				{
					error = index_string(
						(struct string_value*) generic_target,
						(struct number_value*) generic_index,
						result);
				}
				else
				{
					error = 1;
				}
				break;
			}
			case vk_list:
			{
				if(generic_index->kind == vk_number)
				{
					error = index_list(
						(struct list_value*) generic_target,
						(struct number_value*) generic_index,
						result);
				}
				else
				{
					error = 1;
				}
				break;
			}
			case vk_object:
			{
				if(generic_index->kind == vk_string)
				{
					error = index_object(
						(struct object_value*) generic_target,
						(struct string_value*) generic_index,
						result);
				}
				else
				{
					error = 1;
				}
				break;
			}
			default: TODO;
		}
	}
	
	if(generic_target) error = delete_value(generic_target) || error;
	if(generic_index) error = delete_value(generic_index) || error;
	
	EXIT;
	return error;
}

int callbackptr_index_expression_evaluate(
	struct expression *this,
	struct scope *scope,
	struct value **result)
{
	int error = 0;
	ENTER;
	
	error = 0
		?: this->kind != ek_index
		?: index_expression_evaluate(
			(struct index_expression*) this,
			scope, result);
	
	EXIT;
	return error;
}







