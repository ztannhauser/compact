
#include <assert.h>
#include <stdio.h>

#include <debug.h>

#include <linkedlist/struct.h>

#include "../print.h"

#include "struct.h"

#include "print.h"

static int index_expression_print(
	struct index_expression* this,
	int (*push)(const wchar_t*, size_t))
{
	int error;
	ENTER;
	
	error = 0
		?: expression_print(this->list, push)
		?: push(L"[", 1)
		?: expression_print(this->index, push)
		?: push(L"]", 1)
		;

	EXIT;
	return error;
}

int callbackptr_index_expression_print(struct expression *this,
									   int (*push)(const wchar_t *, size_t)) {
	int error;
	ENTER;
	
	error = 0
		?: this->kind != ek_index
		?: index_expression_print((struct index_expression*) this, push)
		;
	
	EXIT;
	return error;
}
