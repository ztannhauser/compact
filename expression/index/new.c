
#include <stdio.h>

#include "debug.h"

#include "linkedlist/struct.h"

#include "../new.h"
#include "../struct.h"

#include "callbacks.h"
#include "struct.h"

#include "new.h"

struct index_expression *new_index_expression(struct expression *list,
											  struct expression *index) {
	struct index_expression *this = (struct index_expression *)new_expression(
		ek_index, &(index_expression_callbacks), sizeof(*this));
	verpv(this);
	this->list = list;
	this->index = index;
	return this;
}
