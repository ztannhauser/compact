
#include <assert.h>
#include <stdio.h>
#include <string.h>

#include <array/struct.h>
#include <debug.h>

#include "struct.h"

#include "print.h"

int variable_expression_print(struct variable_expression *this,
							  int (*push)(const wchar_t *, size_t)) {
	int error;
	ENTER;
	error = push(this->name, wcslen(this->name));
	EXIT;
	return error;
}

int callbackptr_variable_expression_print(struct expression *this,
										  int (*push)(const wchar_t *,
													  size_t)) {
	int error;
	ENTER;

	error = 0
				?: this->kind != ek_variable
					   ?: variable_expression_print(
							  (struct variable_expression *)this, push);
	;

	EXIT;
	return error;
}
