
#include <stdlib.h>

#include "../struct.h"

#include "delete.h"
#include "evaluate.h"
#include "print.h"

#include "callbacks.h"

struct expression_callbacks variable_expression_callbacks = {
	.evaluate = callbackptr_variable_expression_evaluate,
	.print = callbackptr_variable_expression_print,
	.delete = callbackptr_delete_variable_expression,
};
