
#ifndef VARIABLE_EXPRESSION_H
#define VARIABLE_EXPRESSION_H

#include "../struct.h"

struct variable_expression
{
	struct expression super;
	wchar_t name[256];
};

#endif
