
#include <stdio.h>
#include <string.h>
#include <wchar.h>

#include <debug.h>

#include "../new.h"
#include "../struct.h"

#include "callbacks.h"
#include "struct.h"

#include "new.h"

struct variable_expression *new_variable_expression(wchar_t *name) {
	struct variable_expression *this =
		(struct variable_expression *)new_expression(
			ek_variable, &(variable_expression_callbacks), sizeof(*this));
	wcscpy(this->name, name);
	return this;
}
