
#include <assert.h>
#include <stdio.h>

#include <debug.h>

#include "../delete.h"

#include "struct.h"

#include "delete.h"

static int delete_variable_expression(struct variable_expression *this) {
	int error = 0;
	ENTER;
	
	free(this);
	
	EXIT;
	return error;
}

int callbackptr_delete_variable_expression(struct expression *this) {
	int error;
	ENTER;
	
	error = 0
		?: this->kind != ek_variable
		?: delete_variable_expression(
			(struct variable_expression *)this)
		;
	
	EXIT;
	return error;
}
