
#include <assert.h>
#include <stdio.h>

#include <debug.h>

#include <scope/lookup.h>

#include "../struct.h"

#include "struct.h"

#include "evaluate.h"

static int variable_expression_evaluate(
	struct variable_expression *this,
	struct scope *scope,
	struct value **result)
{
	int error;
	ENTER;

	error = scope_lookup(scope, this->name, result);

	EXIT;
	return error;
}

int callbackptr_variable_expression_evaluate(
	struct expression *this,
	struct scope *scope,
	struct value **result)
{
	int error = 0;
	ENTER;

	error = 0
		?: this->kind != ek_variable
		?: variable_expression_evaluate(
			(struct variable_expression *)this, scope, result)
		;

	EXIT;
	return error;
}
















