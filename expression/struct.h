
#ifndef EXPRESSION_STRUCT_H
#define EXPRESSION_STRUCT_H

#include <stdlib.h>

struct expression;

struct scope;

struct value;

struct expression_callbacks
{
	int (*evaluate)(
		struct expression* this,
		struct scope* scope,
		struct value** result
	) __attribute__ ((warn_unused_result));
	
	int (*print)(
		struct expression* this,
		int (*push)(const wchar_t*, size_t)
			__attribute__ ((warn_unused_result))
	) __attribute__ ((warn_unused_result));
	
	int (*delete)(struct expression* this)
		 __attribute__ ((warn_unused_result));
};

enum expression_kind
{
	ek_feedback,
	ek_variable,
	ek_function,
	ek_literal,
	ek_ternary,
	ek_object,
	ek_binary,
	ek_field,
	ek_comma,
	ek_index,
	ek_unary,
	ek_paren,
	ek_list,
	ek_call,
	ek_let,
	ek_n,
};

struct expression
{
	enum expression_kind kind;
	struct expression_callbacks* callbacks;
};

#endif // EXPRESSION_STRUCT_H
