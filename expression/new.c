
#include <stdio.h>

#include <debug.h>

#include "struct.h"

#include "new.h"

struct expression *new_expression(enum expression_kind kind,
								  struct expression_callbacks *callbacks,
								  int size) {
	struct expression *this;
	this = malloc(size);
	this->kind = kind;
	this->callbacks = callbacks;
	verpv(this);
	return this;
}
