
int callbackptr_paren_expression_print(
	struct expression* this,
	int (*push)(const wchar_t*, size_t))
	__attribute__ ((warn_unused_result));
