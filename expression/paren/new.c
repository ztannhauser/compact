
#include <stdio.h>

#include <debug.h>

#include "../new.h"
#include "../struct.h"

#include "callbacks.h"
#include "struct.h"

#include "new.h"

struct paren_expression *new_paren_expression(struct expression *inner) {
	struct paren_expression *this = (struct paren_expression *)new_expression(
		ek_paren, &(paren_expression_callbacks), sizeof(*this));
	verpv(this);
	this->inner = inner;
	return this;
}
