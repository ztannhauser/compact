
#include <assert.h>
#include <stdio.h>

#include <debug.h>

#include "../delete.h"

#include "struct.h"

#include "delete.h"

static int delete_paren_expression(struct paren_expression *this) {
	int error;
	ENTER;
	
	error = delete_expression(this->inner);

	free(this);
	
	EXIT;
	return error;
}

int callbackptr_delete_paren_expression(struct expression *this) {
	int error;
	ENTER;
	
	error = 0
		?: this->kind != ek_paren
		?: delete_paren_expression((struct paren_expression *)this)
		;
	
	EXIT;
	return error;
}
