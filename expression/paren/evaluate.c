#include <assert.h>
#include <stdio.h>

#include <debug.h>

#include "../evaluate.h"

#include "struct.h"

#include "evaluate.h"

static int paren_expression_evaluate(struct paren_expression *this,
									 struct scope *scope,
									 struct value **result) {
	int error;
	ENTER;

	error = expression_evaluate(this->inner, scope, result);

	EXIT;
	return error;
}

int callbackptr_paren_expression_evaluate(struct expression *this,
										  struct scope *scope,
										  struct value **result) {
	int error;
	ENTER;

	error = 0
		?: this->kind != ek_paren
		?: paren_expression_evaluate(
			(struct paren_expression *)this, scope, result);

	EXIT;
	return error;
}
