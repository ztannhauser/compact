
#include <assert.h>
#include <stdio.h>

#include <debug.h>

#include "../print.h"

#include "struct.h"

#include "print.h"

static int paren_expression_print(struct paren_expression *this,
								  int (*push)(const wchar_t *, size_t)) {
	int error;
	ENTER;

	error = 0
				?: push(L"(", 1)
					   ?: expression_print(this->inner, push) ?: push(L")", 1);

	EXIT;
	return error;
}

int callbackptr_paren_expression_print(struct expression *this,
									   int (*push)(const wchar_t *, size_t)) {
	int error;
	ENTER;

	error = 0
				?: this->kind != ek_paren
					   ?: paren_expression_print(
							  (struct paren_expression *)this, push);

	EXIT;
	return error;
}
