
#ifndef PAREN_EXPRESSION_STRUCT_H
#define PAREN_EXPRESSION_STRUCT_H

#include "../struct.h"

struct paren_expression
{
	struct expression super;
	struct expression* inner;
};

#endif
