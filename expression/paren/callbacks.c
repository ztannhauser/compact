
#include <stdlib.h>

#include "../struct.h"

#include "delete.h"
#include "evaluate.h"
#include "print.h"

#include "callbacks.h"

struct expression_callbacks paren_expression_callbacks = {
	.evaluate = callbackptr_paren_expression_evaluate,
	.print = callbackptr_paren_expression_print,
	.delete = callbackptr_delete_paren_expression,
};
