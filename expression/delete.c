
#include <stdio.h>

#include <debug.h>

#include "struct.h"

#include "delete.h"

int delete_expression(struct expression *this)
{
	int error;
	ENTER;
	error = (this->callbacks->delete)(this);
	EXIT;
	return error;
}
