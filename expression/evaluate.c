
#include <assert.h>
#include <stdio.h>

#include "debug.h"

#include "struct.h"

#include "evaluate.h"

int expression_evaluate(
	struct expression *this,
	struct scope *scope,
	struct value **result)
{
	int error = 0;
	ENTER;
	error = (this->callbacks->evaluate)(this, scope, result);
	verpv(error);
	EXIT;
	return error;
}
