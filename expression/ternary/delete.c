
#include <assert.h>
#include <stdio.h>

#include <debug.h>

#include "../delete.h"

#include "struct.h"

#include "delete.h"

static int delete_ternary_expression(struct ternary_expression *this) {
	int error = 0;
	ENTER;
	error = delete_expression(this->conditional);
	if(!error && this->true_case)
		error = delete_expression(this->true_case);
	if(!error) error = delete_expression(this->false_case);
	if(!error) free(this);
	EXIT;
	return error;
}

int callbackptr_delete_ternary_expression(struct expression *this)
{
	int error;
	ENTER;
	
	error = 0
		?: this->kind != ek_ternary
		?: delete_ternary_expression((struct ternary_expression *)this);

	EXIT;
	return error;
}
