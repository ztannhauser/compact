
#include <assert.h>
#include <stdio.h>

#include <debug.h>

#include <value/boolean/struct.h>
#include <value/delete.h>
#include <value/struct.h>

#include <expression/evaluate.h>

#include <scope/builtins/cast/bool.h>

#include "struct.h"

#include "evaluate.h"

static int ternary_expression_evaluate(
	struct ternary_expression* this,
	struct scope* scope,
	struct value** result)
{
	int error = 0;
	struct value* conditional;
	ENTER;
	
	error = expression_evaluate(this->conditional, scope, &conditional);
	if(!error)
	{
		error = as_bool(conditional) ? (
			this->true_case ? ( 0
				?: delete_value(conditional)
				?: expression_evaluate(this->true_case, scope, result)
			) : (
				*result = conditional, 0
			) 
		) : ( 0
			?: delete_value(conditional)
			?: expression_evaluate(this->false_case, scope, result)
		);
		
	}
	EXIT;
	return error;
}

int callbackptr_ternary_expression_evaluate(
	struct expression *this,
	struct scope *scope,
	struct value **result)
{
	int error;
	ENTER;
	
	error = 0
		?: this->kind != ek_ternary
		?: ternary_expression_evaluate(
			(struct ternary_expression*) this,
			scope,
			result);
	
	EXIT;
	return error;
}













