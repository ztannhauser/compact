
#include <assert.h>
#include <stdio.h>

#include <debug.h>

#include "../print.h"

#include "struct.h"

#include "print.h"

static int ternary_expression_print(struct ternary_expression *this,
									int (*push)(const wchar_t *, size_t)) {
	int error = 0;
	ENTER;

	error = 0
		?: expression_print(this->conditional, push)
		?: (this->true_case)
		? (0
		?: push(L" ? ", 3)
		?: expression_print(this->true_case,
		push)
		?: push(L" : ", 3)
		?: expression_print(
		this->false_case,
		push))
		: (0
		?: push(L" ?: ", 4)
		?: expression_print(this->false_case,
		push));

	EXIT;
	return error;
}

int callbackptr_ternary_expression_print(struct expression *this,
										 int (*push)(const wchar_t *, size_t)) {
	int error;
	ENTER;

	error = 0
				?: this->kind != ek_ternary
					   ?: ternary_expression_print(
							  (struct ternary_expression *)this, push);

	EXIT;
	return error;
}
