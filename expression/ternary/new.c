
#include <debug.h>

#include "../new.h"
#include "../struct.h"

#include "callbacks.h"
#include "struct.h"

#include "new.h"

struct ternary_expression *
new_ternary_expression(struct expression *conditional,
					   struct expression *true_case,
					   struct expression *false_case) {
	struct ternary_expression *this =
		(struct ternary_expression *)new_expression(
			ek_ternary, &(ternary_expression_callbacks), sizeof(*this));
	this->conditional = conditional;
	this->true_case = true_case;
	this->false_case = false_case;
	return this;
}
