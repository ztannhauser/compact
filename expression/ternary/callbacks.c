
#include <stdlib.h>

#include "../struct.h"

#include "delete.h"
#include "evaluate.h"
#include "print.h"

#include "callbacks.h"

struct expression_callbacks ternary_expression_callbacks = {
	.evaluate = callbackptr_ternary_expression_evaluate,
	.print = callbackptr_ternary_expression_print,
	.delete = callbackptr_delete_ternary_expression,
};
