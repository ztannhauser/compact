
#include "struct.h"

struct ternary_expression* new_ternary_expression(
	struct expression* cond,
	struct expression* true_case,
	struct expression* false_case);
