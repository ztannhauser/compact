
#ifndef TERNARY_EXPRESSION_H
#define TERNARY_EXPRESSION_H

#include "../struct.h"

struct ternary_expression
{
	struct expression super;
	struct expression* conditional, *true_case, *false_case;
};

#endif
