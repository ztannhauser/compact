#include <assert.h>
#include <stdio.h>

#include "debug.h"

#include "array/new.h"
#include "array/push_n.h"
#include "array/struct.h"

#include "linkedlist/foreach.h"
#include "linkedlist/struct.h"

#include "value/list/new.h"

#include "../evaluate.h"

#include "struct.h"

#include "evaluate.h"

static int list_expression_evaluate(
	struct list_expression* this,
	struct scope* scope,
	struct value** result)
{
	int error = 0;
	struct array values;
	ENTER;
	values = new_array(struct value*);
	int callback(struct expression* exp)
	{
		int error = 0;
		struct value* ele;
		error = expression_evaluate(exp, scope, &ele);
		array_push_n(&(values), &ele);
		return error;
	}
	error = linkedlist_foreach(&(this->subexpr), (int (*)(void *)) callback);
	verpv(error);
	if(!error)
	{
		*result = (struct value*) new_list_value(values, NULL);
	}
	EXIT;
	return error;
}

int callbackptr_list_expression_evaluate(
	struct expression *this,
	struct scope *scope,
	struct value **result)
{
	int error = 0;
	ENTER;
	
	error = 0
		?: this->kind != ek_list
		?: list_expression_evaluate(
			(struct list_expression*) this, scope, result)
		;
	
	EXIT;
	return error;
}










