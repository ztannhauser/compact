#include <assert.h>
#include <stdio.h>

#include "debug.h"

#include "../print.h"

#include "struct.h"

#include "print.h"

int list_expression_print(struct list_expression *this,
						  int (*push)(const wchar_t *, size_t)) {
	int error = 0;
	ENTER;

	error = 0 ?: push(L"[", 1);

	for (struct llnode *current = this->subexpr.first; !error && current;
		 current = current->next) {
		struct expression *ele = (current->data);
		error = error ?: expression_print(ele, push);
		if (!error && current->next) {
			error = push(L", ", 2);
		}
	}

	error = error ?: push(L"]", 1);

	EXIT;
	return error;
}

int callbackptr_list_expression_print(struct expression *this,
									  int (*push)(const wchar_t *, size_t)) {
	int error;
	ENTER;

	error = 0
				?: this->kind != ek_list
					   ?: list_expression_print((struct list_expression *)this,
												push);

	EXIT;
	return error;
}
