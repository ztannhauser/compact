
#include "linkedlist/struct.h"

#include "../struct.h"

struct list_expression
{
	struct expression super;
	struct linkedlist subexpr;
};

