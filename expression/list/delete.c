#include <assert.h>
#include <stdio.h>

#include "debug.h"

#include "linkedlist/delete.h"
#include "linkedlist/foreach.h"
#include "linkedlist/struct.h"

#include "../delete.h"

#include "struct.h"

#include "delete.h"

static int delete_list_expression(struct list_expression *this) {
	int error = 0;
	ENTER;
	
	error = linkedlist_foreach(&(this->subexpr),
		(int (*)(void *))delete_expression);
	
	if(!error)
	{
		delete_linkedlist(&(this->subexpr));
		free(this);
	}
	
	EXIT;
	return error;
}

int callbackptr_delete_list_expression(struct expression *this) {
	int error;
	ENTER;
	
	error = 0
		?: this->kind != ek_list
		?: delete_list_expression((struct list_expression *)this)
		;
	
	EXIT;
	return error;
}
