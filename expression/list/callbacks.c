
#include <stdlib.h>

#include "../struct.h"

#include "delete.h"
#include "evaluate.h"
#include "print.h"

#include "callbacks.h"

struct expression_callbacks list_expression_callbacks = {
	.evaluate = callbackptr_list_expression_evaluate,
	.print = callbackptr_list_expression_print,
	.delete = callbackptr_delete_list_expression,
};
