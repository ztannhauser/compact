#include <assert.h>
#include <stdio.h>

#include "debug.h"
#include "linkedlist/struct.h"

#include "../new.h"
#include "../struct.h"

#include "callbacks.h"
#include "struct.h"

#include "new.h"

struct list_expression *new_list_expression(struct linkedlist subexpr) {
	struct list_expression *this = (struct list_expression *)new_expression(
		ek_list, &(list_expression_callbacks), sizeof(*this));
	verpv(this);
	this->subexpr = subexpr;
	return this;
}
