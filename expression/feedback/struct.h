

#ifndef FEEDBACK_EXPRESSION_STRUCT_H
#define FEEDBACK_EXPRESSION_STRUCT_H

#include <linkedlist/struct.h>

#include "../struct.h"

struct feedback_expression
{
	struct expression super;
	struct linkedlist names; // wchar_t*
	struct linkedlist inital_expressions; // struct expression*
	struct expression* nth_expression;
	struct linkedlist values; // struct value*
};

#endif
