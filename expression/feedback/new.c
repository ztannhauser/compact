
#include <assert.h>
#include <stdio.h>

#include <debug.h>

#include <linkedlist/new.h>
#include <linkedlist/struct.h>

#include "../new.h"
#include "../struct.h"

#include "callbacks.h"
#include "struct.h"

#include "new.h"

struct feedback_expression *
new_feedback_expression(struct linkedlist names,
						struct linkedlist inital_expressions,
						struct expression *nth_expression) {
	struct feedback_expression *this =
		(struct feedback_expression *)new_expression(
			ek_feedback, &(feedback_expression_callbacks), sizeof(*this));
	verpv(this);
	assert(names.n == inital_expressions.n);
	this->values = new_linkedlist();
	this->names = names;
	this->inital_expressions = inital_expressions;
	this->nth_expression = nth_expression;
	return this;
}
