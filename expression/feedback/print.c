
#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <wchar.h>

#include <debug.h>

#include <value/print.h>

#include "../print.h"

#include "struct.h"

#include "print.h"

static int feedback_expression_print(
	struct feedback_expression* this,
	int (*push)(const wchar_t*, size_t))
{
	int error = 0;
	size_t i, n, vn, en;
	wchar_t* name;
	struct llnode *current_name, *current_value, *current_expression;
	ENTER;
	
	HERE;
	
	error = push(L"feedback ", 9);
	
	verpv(error);
	
	i = 0;
	n = this->names.n, vn = this->values.n, en = this->inital_expressions.n;
	
	// should always be true:
	assert(n == vn + en);
	
	for(current_name = this->names.first,
		current_value = this->values.first;
		!error && i < vn;
		current_name = current_name->next,
		current_value = current_value->next,
		i++)
	{
		verpv(i);
		
		name = current_name->data;
		error = 0
			?: push(name, wcslen(name))
			?: push(L" = ", 3)
			?: value_print(current_value->data, push);

		if(!error && i + 1 < vn)
		{
			error = push(L", ", 2);
		}
	}
	
	// possibly print connection bewteen for loops:
	if(vn > 0 && en > 0)
	{
		error = push(L", ", 2);
	}
	
	if(i < n)
	{
		for(current_expression = this->inital_expressions.first;
			!error && i < n;
			current_name = current_name->next,
			current_expression = current_expression->next,
			i++)
		{
			verpv(i);
			
			name = current_name->data;
			error = 0
				?: push(name, wcslen(name))
				?: push(L" = ", 3)
				?: expression_print(current_expression->data, push);

			if(!error && i + 1 < n)
			{
				error = push(L", ", 2);
			}
		}
	}
	
	error = 0
		?: error
		?: push(L": ", 2)
		?: expression_print(this->nth_expression, push);
	
	EXIT;
	return error;
}

int callbackptr_feedback_expression_print(
	struct expression *this,
	int (*push)(const wchar_t *, size_t))
{
	int error;
	ENTER;
	
	error = 0
		?: this->kind != ek_feedback
		?: feedback_expression_print(
			(struct feedback_expression*) this, push)
		;
	
	EXIT;
	return error;
}








