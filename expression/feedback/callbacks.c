
#include <stdlib.h>

#include "../struct.h"

#include "delete.h"
#include "evaluate.h"
#include "print.h"

#include "callbacks.h"

struct expression_callbacks feedback_expression_callbacks = {
	.evaluate = callbackptr_feedback_expression_evaluate,
	.print = callbackptr_feedback_expression_print,
	.delete = callbackptr_delete_feedback_expression,
};
