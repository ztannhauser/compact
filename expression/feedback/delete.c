
#include <assert.h>
#include <stdio.h>

#include <debug.h>

#include <linkedlist/delete.h>
#include <linkedlist/foreach.h>
#include <linkedlist/free-elements.h>

#include <value/delete.h>

#include "../delete.h"

#include "struct.h"

#include "delete.h"

static int delete_feedback_expression(struct feedback_expression *this)
{
	int error = 0;
	ENTER;
	
	linkedlist_free_elements(&(this->names));
	delete_linkedlist(&(this->names));
	
	error = error ?: linkedlist_foreach(
		&(this->inital_expressions),
		(int (*)(void *)) delete_expression);
	
	delete_linkedlist(&(this->inital_expressions));
	
	error = error
		?: delete_expression(this->nth_expression)
		?: linkedlist_foreach(&(this->values),
			(int (*)(void *))delete_value);
	
	delete_linkedlist(&(this->values));
	
	free(this);
	
	EXIT;
	return error;
}

int callbackptr_delete_feedback_expression(struct expression *this)
{
	int error;
	ENTER;
	
	error = 0
		?: this->kind != ek_feedback
		?: delete_feedback_expression((struct feedback_expression*) this);
	
	EXIT;
	return error;
}



























