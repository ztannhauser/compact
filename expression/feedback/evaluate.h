
#include "value/struct.h"

#include "struct.h"

int callbackptr_feedback_expression_evaluate(
	struct expression* this,
	struct scope* scope,
	struct value** result)
	__attribute__ ((warn_unused_result));
