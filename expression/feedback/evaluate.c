
#include <assert.h>
#include <stdio.h>
#include <string.h>

#include <debug.h>

#include <linkedlist/foreach.h>
#include <linkedlist/pop_0.h>
#include <linkedlist/pop_n.h>
#include <linkedlist/push_n.h>
#include <linkedlist/struct.h>

#include <expression/delete.h>

#include <value/delete.h>

#include <scope/close.h>
#include <scope/open.h>
#include <scope/push.h>

#include "../evaluate.h"

#include "struct.h"

#include "evaluate.h"

static int feedback_expression_evaluate(
	struct feedback_expression* this,
	struct scope* scope,
	struct value** result)
{
	int error = 0;
	size_t i, n;
	struct value* retval;
	struct expression* evalme;
	ENTER;
	
	error = scope_open(scope);
	
	// push current set of variables
	if(!error)
	{
		i = 0, n = this->values.n;
		for(struct llnode
			*current_name = this->names.first,
			*current_value = this->values.first;
			!error && i < n;
			current_name = current_name->next,
			current_value = current_value->next,
			i++)
		{
			error = scope_push(
				scope,
				current_name->data,
				current_value->data,
				true);
		}
	}
	
	if(!error)
	{
		if(this->inital_expressions.n > 0)
		{
			evalme = this->inital_expressions.first->data;
			
			error = expression_evaluate(evalme, scope, &retval);
			
			if(!error)
			{
				linkedlist_pop_0(&(this->inital_expressions));
				error = delete_expression(evalme);
			}
		}
		else
		{
			// evalute nth case:
			{
				error = expression_evaluate(
					this->nth_expression,
					scope,
					&retval);
			}
			
			// shift values down, throw out old last value:
			if(!error)
			{
				error = delete_value(this->values.first->data);
				linkedlist_pop_0(&(this->values));
			}
		}
	}
	
	if(!error)
	{
		retval->refcount++;
		linkedlist_push_n(&(this->values), retval);
		error = scope_close(scope);
	}
	
	if(!error)
	{
		*result = retval;
	}
	
	EXIT;
	return error;
}

int callbackptr_feedback_expression_evaluate(struct expression *this,
											 struct scope *scope,
											 struct value **result) {
	int error;
	ENTER;
	
	error = 0
		?: this->kind != ek_feedback
		?: feedback_expression_evaluate(
			(struct feedback_expression*) this,
			scope,
			result);
	
	return error;
}






