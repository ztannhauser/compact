
#include <expression/struct.h>

struct expression* new_expression(
	enum expression_kind kind,
	struct expression_callbacks* callbacks,
	int size)
	__attribute__ ((warn_unused_result));
