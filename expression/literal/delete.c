#include <assert.h>
#include <stdio.h>

#include "debug.h"

#include "value/delete.h"

#include "struct.h"

#include "delete.h"

static int delete_literal_expression(struct literal_expression *this)
{
	int error;
	ENTER;
	
	error = delete_value(this->value);
	free(this);
	
	EXIT;
	return error;
}

int callbackptr_delete_literal_expression(struct expression *this)
{
	int error;
	ENTER;
	
	error = 0
		?: this->kind != ek_literal
		?: delete_literal_expression(
			(struct literal_expression *)this)
		;
	
	EXIT;
	return error;
}
