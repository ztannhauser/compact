#include <assert.h>
#include <stdio.h>

#include "debug.h"

#include "value/print.h"

#include "struct.h"

#include "print.h"

static int literal_expression_print(struct literal_expression *this,
									int (*push)(const wchar_t *, size_t)) {
	int error;
	ENTER;
	error = 0 ?: value_print(this->value, push);
	EXIT;
	return error;
}

int callbackptr_literal_expression_print(struct expression *this,
										 int (*push)(const wchar_t *, size_t)) {
	int error;
	ENTER;

	error = 0
		?: this->kind != ek_literal
		?: literal_expression_print(
			(struct literal_expression *)this, push);

	EXIT;
	return error;
}
