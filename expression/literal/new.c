#include <assert.h>
#include <stdio.h>

#include "debug.h"

#include "../new.h"
#include "../struct.h"

#include "callbacks.h"
#include "struct.h"

#include "new.h"

struct literal_expression *new_literal_expression(struct value *val) {
	struct literal_expression *this;
	this = (struct literal_expression *)new_expression(
		ek_literal, &(literal_expression_callbacks), sizeof(*this));
	verpv(this);
	this->value = val;
	return this;
}
