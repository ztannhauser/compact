#include <assert.h>
#include <stdio.h>

#include <debug.h>

#include <value/struct.h>

#include "struct.h"

#include "evaluate.h"

static int literal_expression_evaluate(struct literal_expression *this,
									   struct scope *scope,
									   struct value **result) {
	int error = 0;
	ENTER;

	*result = this->value;
	(*result)->refcount++;

	EXIT;
	return error;
}

int callbackptr_literal_expression_evaluate(struct expression *this,
											struct scope *scope,
											struct value **result) {
	int error;
	ENTER;

	error = 0
				?: this->kind != ek_literal
					   ?: literal_expression_evaluate(
							  (struct literal_expression *)this, scope, result);

	EXIT;
	return error;
}
