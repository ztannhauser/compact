
#include <stdlib.h>

#include "delete.h"
#include "evaluate.h"
#include "print.h"

#include "../struct.h"

#include "callbacks.h"

struct expression_callbacks literal_expression_callbacks = {
	.evaluate = callbackptr_literal_expression_evaluate,
	.print = callbackptr_literal_expression_print,
	.delete = callbackptr_delete_literal_expression,
};
