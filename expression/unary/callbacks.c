
#include <stdlib.h>

#include "../struct.h"

#include "delete.h"
#include "evaluate.h"
#include "print.h"

#include "callbacks.h"

struct expression_callbacks unary_expression_callbacks = {
	.evaluate = callbackptr_unary_expression_evaluate,
	.print = callbackptr_unary_expression_print,
	.delete = callbackptr_delete_unary_expression,
};
