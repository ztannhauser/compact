
#include <assert.h>
#include <stdio.h>

#include <debug.h>

#include "../print.h"
#include "../struct.h"

#include "struct.h"

#include "print.h"

static wchar_t lookup[] = 
{
	[uo_bitwise_negate] = L'~',
	[uo_logical_negate] = L'!',
	[uo_numerical_negate] = L'-',
	[uo_numerical_plus] = L'+',
};

static int unary_expression_print(
	struct unary_expression* this,
	int (*push)(const wchar_t*, size_t))
{
	int error = 0;
	ENTER;
	error = 0
		?: push(&(lookup[this->operation]), 1)
		?: expression_print(this->inner, push);

	EXIT;
	return error;
}

int callbackptr_unary_expression_print(
	struct expression *this,
	int (*push)(const wchar_t *, size_t))
{
	int error;
	ENTER;
	
	error = 0
		?: this->kind != ek_unary
		?: unary_expression_print((struct unary_expression*) this, push);

	EXIT;
	return error;
}















