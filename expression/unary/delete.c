
#include <assert.h>
#include <stdio.h>

#include <debug.h>

#include "../delete.h"

#include "struct.h"

#include "delete.h"

static int delete_unary_expression(struct unary_expression *this) {
	int error;
	ENTER;
	
	error = delete_expression(this->inner);
	free(this);
	
	EXIT;
	return error;
}

int callbackptr_delete_unary_expression(struct expression *this)
{
	int error;
	ENTER;
	
	error = 0
		?: this->kind != ek_unary
		?: delete_unary_expression((struct unary_expression *)this);
	
	EXIT;
	return error;
}
