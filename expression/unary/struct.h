
#ifndef UNARY_EXPRESSION_STRUCT_H
#define UNARY_EXPRESSION_STRUCT_H

#include "../struct.h"

enum unary_operation
{
	uo_bitwise_negate,
	uo_logical_negate,
	uo_numerical_negate,
	uo_numerical_plus,
};

struct unary_expression
{
	struct expression super;
	enum unary_operation operation;
	struct expression* inner;
};

#endif
