
#include <stdio.h>

#include "debug.h"

#include "../new.h"
#include "../struct.h"

#include "callbacks.h"
#include "struct.h"

#include "new.h"

struct unary_expression *new_unary_expression(enum unary_operation operation,
											  struct expression *inner) {
	struct unary_expression *this = (struct unary_expression *)new_expression(
		ek_unary, &(unary_expression_callbacks), sizeof(*this));
	verpv(this);
	this->operation = operation;
	this->inner = inner;
	return this;
}
