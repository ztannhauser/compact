
#include "../struct.h"

#include "struct.h"

struct unary_expression* new_unary_expression(
	enum unary_operation operation,
	struct expression* inner);
