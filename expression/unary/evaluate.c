
#include <assert.h>
#include <stdio.h>

#include "debug.h"

#include <value/delete.h>
#include <value/number/new.h>
#include <value/number/struct.h>

#include <scope/builtins/cast/bool.h>
#include <scope/fetch/boolean.h>

#include "../evaluate.h"

#include "struct.h"

#include "evaluate.h"

#if 0
static struct number_value* numerical_negate(struct number_value* val)
{
	struct number_value* ret;
	ENTER;
	switch(val->kind)
	{
		case nvk_integer:
		{
			ret = new_number_value_as_integer(-val->integer);
			break;
		}
		case nvk_long:
		{
			mpz_ptr mp = malloc(sizeof(MP_INT));
			mpz_init(mp);
			mpz_neg(mp, val->mpz_long);
			ret = new_number_value_as_long(mp);
			break;
		}
		case nvk_decimal:
		{
			ret = new_number_value_as_decimal(-val->decimal);
			break;
		}
		default: TODO;
	}
	EXIT;
	return ret;
}

static struct number_value* bitwise_negate(struct number_value* val)
{
	struct number_value* ret;
	ENTER;
	switch(val->kind)
	{
		case nvk_integer:
		{
			ret = new_number_value_as_integer(~val->integer);
			break;
		}
#if 0
		case nvk_long:
		{
			mpz_ptr mp = malloc(sizeof(MP_INT));
			mpz_init(mp);
			mpz_neg(mp, val->mpz_long);
			ret = new_number_value_as_long(mp);
			break;
		}
#endif
		default: TODO;
	}
	EXIT;
	return ret;
}

#endif
static int unary_expression_evaluate(
	struct unary_expression* this,
	struct scope* scope,
	struct value** result)
{
	int error = 0;
	struct value* inner = NULL;
	ENTER;
	
	error = expression_evaluate(this->inner, scope, &inner);
	if(!error)
	{
		switch(this->operation)
		{
			case uo_numerical_negate:
			{
				#if 0
				assert(inner->kind == vk_number);
				struct number_value* spef_inner = (struct number_value*) inner;
				ret = (struct value*) numerical_negate(spef_inner);
				#endif
				TODO;
				break;
			}
			case uo_bitwise_negate:
			{
				#if 0
				assert(inner->kind == vk_number);
				struct number_value* spef_inner = (struct number_value*) inner;
				ret = (struct value*) bitwise_negate(spef_inner);
				#endif
				TODO;
				break;
			}
			case uo_numerical_plus:
			{
				#if 0
				assert(inner->kind == vk_number);
				ret = inner;
				ret->refcount++;
				#endif
				TODO;
				break;
			}
			case uo_logical_negate:
			{
				*result = (struct value*) scope_fetch_boolean(scope, !as_bool(inner));
				break;
			}
			default: TODO;
		}
	}
	
	if(inner) error = delete_value(inner) ?: error;
	
	EXIT;
	return error;
}


int callbackptr_unary_expression_evaluate(
	struct expression *this,
	struct scope *scope,
	struct value **result)
{
	int error = 0;
	ENTER;
	
	error = 0
		?: this->kind != ek_unary
		?: unary_expression_evaluate(
			(struct unary_expression*) this,
			scope,
			result);

	EXIT;
	return error;
}













