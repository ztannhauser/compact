
#include <assert.h>
#include <stdio.h>
#include <string.h>

#include "debug.h"

#include "value/string/print.h"

#include "../print.h"

#include "struct.h"

#include "print.h"

static int object_expression_print(
	struct object_expression* this,
	int (*push)(const wchar_t*, size_t))
{
	int error = 0;
	struct object_expression_bundle* ele;
	ENTER;
	
	error = push(L"{", 1);
	
	for(struct llnode* current = this->bundles.first;
		!error && current;
		current = current->next)
	{
		ele = current->data;
		
		error = 0
			?: string_value_print(ele->label, push)
			?: push(L": ", 2)
			?: expression_print(ele->subexpr, push);
		
		if(!error && current->next)
		{
			error = push(L", ", 2);
		}
	}
	
	if(!error)
	{
		error = push(L"}", 1);
	}
	EXIT;
	return error;
}

int callbackptr_object_expression_print(
	struct expression *this,
	int (*push)(const wchar_t *, size_t))
{
	int error;
	ENTER;
	
	error = 0
		?: this->kind != ek_object
		?: object_expression_print(
			(struct object_expression*) this,
			push);
	
	EXIT;
	return error;
}




























