
#include <assert.h>
#include <stdio.h>
#include <string.h>

#include "debug.h"

#include "avl/init_tree.h"
#include "avl/insert.h"
#include "avl/struct.h"

#include "linkedlist/foreach.h"
#include "linkedlist/struct.h"

#include <value/delete.h>
#include <value/list/new.h>
#include <value/object/element_compare.h>
#include <value/object/element_free.h>
#include <value/object/new.h>
#include <value/object/struct.h>
#include <value/string/struct.h>

#include "../evaluate.h"

#include "struct.h"

#include "evaluate.h"

static int object_expression_evaluate(
	struct object_expression* this,
	struct scope* scope,
	struct value** result)
{
	int error = 0;
	struct avl_tree values;
	ENTER;
	
	avl_init_tree(&values,
		(int (*)(const void *, const void *)) element_compare,
		(void (*)(void *)) element_free);
	
	int callback(struct object_expression_bundle* exp)
	{
		int error = 0;
		ENTER;
		struct object_value_element* ele = malloc(sizeof(*ele));
		ele->label = exp->label;
		ele->label->super.refcount++;
		error = expression_evaluate(exp->subexpr, scope, &(ele->value));
		avl_insert(&(values), ele);
		EXIT;
		return error;
	}
	
	error = linkedlist_foreach(&(this->bundles), (int (*) (void*)) callback);
	
	if(!error)
	{
		*result = (struct value*) new_object_value(ok_generic, values);
	}
	
	EXIT;
	return error;
}


int callbackptr_object_expression_evaluate(
	struct expression *this,
	struct scope *scope,
	struct value **result)
{
	int error;
	ENTER;
	
	error = 0
		?: this->kind != ek_object
		?: object_expression_evaluate(
			(struct object_expression*) this,
			scope,
			result);
	
	EXIT;
	return error;
}



















