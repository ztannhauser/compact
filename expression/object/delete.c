
#include <assert.h>
#include <stdio.h>

#include "debug.h"

#include "linkedlist/delete.h"
#include "linkedlist/foreach.h"
#include "linkedlist/struct.h"

#include "value/string/delete.h"

#include "../delete.h"

#include "struct.h"

#include "delete.h"

static int delete_bundle(struct object_expression_bundle *this)
{
	int error;
	ENTER;
	
	error = 0
		?: delete_string_value(this->label)
		?: delete_expression(this->subexpr);
	
	if(!error)
	{
		free(this);
	}
	
	EXIT;
	return error;
}

static int delete_object_expression(struct object_expression *this)
{
	int error = 0;
	ENTER;
	
	error = linkedlist_foreach(&(this->bundles), (int (*)(void *))delete_bundle);
	
	if(!error)
	{
		delete_linkedlist(&(this->bundles));
		free(this);
	}
	
	EXIT;
	return error;
}


int callbackptr_delete_object_expression(struct expression *this)
{
	int error;
	ENTER;
	
	error = 0
		?: this->kind != ek_object
		?: delete_object_expression(
			(struct object_expression*) this);
	
	EXIT;
	return error;
}



















