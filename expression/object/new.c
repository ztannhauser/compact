
#include <stdio.h>

#include "debug.h"

#include "linkedlist/struct.h"

#include "../new.h"
#include "../struct.h"

#include "callbacks.h"
#include "struct.h"

#include "new.h"

struct object_expression *new_object_expression(struct linkedlist bundles) {
	struct object_expression *this = (struct object_expression *)new_expression(
		ek_object, &(object_expression_callbacks), sizeof(*this));
	verpv(this);
	this->bundles = bundles;
	return this;
}
