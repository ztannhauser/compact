
#include <stdlib.h>

#include "../struct.h"

#include "delete.h"
#include "evaluate.h"
#include "print.h"

#include "callbacks.h"

struct expression_callbacks object_expression_callbacks = {
	.evaluate = callbackptr_object_expression_evaluate,
	.print = callbackptr_object_expression_print,
	.delete = callbackptr_delete_object_expression,
};
