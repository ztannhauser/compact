
#include "linkedlist/struct.h"

#include "../struct.h"

struct object_expression_bundle
{
	struct string_value* label;
	struct expression* subexpr;
};

struct object_expression
{
	struct expression super;
	struct linkedlist bundles; // struct object_expression_bundles*
};

