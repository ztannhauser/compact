
#include <assert.h>
#include <stdio.h>

#include <debug.h>

#include "../delete.h"

#include "struct.h"

#include "delete.h"

static int delete_comma_expression(struct comma_expression *this) {
	int error;
	ENTER;
	
	error = 0
		?: delete_expression(this->left)
		?: delete_expression(this->right)
		;
	
	free(this);
	
	EXIT;
	return error;
}

int callbackptr_delete_comma_expression(struct expression *this)
{
	int error;
	ENTER;
	
	error = 0
		?: this->kind != ek_comma
		?: delete_comma_expression((struct comma_expression *)this)
		;
	
	EXIT;
	return error;
}
