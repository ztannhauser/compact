
#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#include <debug.h>

#include <array/struct.h>

#include <scope/builtins/math/arith/add.h>
#include <scope/builtins/math/arith/mult.h>
#include <scope/builtins/math/arith/sub.h>
#include <scope/fetch/boolean.h>

#include <value/boolean/struct.h>
#include <value/delete.h>
#include <value/number/new.h>
#include <value/number/struct.h>
#include <value/string/new.h>
#include <value/string/struct.h>
#include <value/struct.h>

#include "../evaluate.h"

#include "struct.h"

#include "evaluate.h"

static int comma_expression_evaluate(
	struct comma_expression* this,
	struct scope* scope,
	struct value** result)
{
	int error = 0;
	struct value* left_result;
	ENTER;
	
	error = 0
		?: expression_evaluate(this->left, scope, &left_result)
		?: delete_value(left_result)
		?: expression_evaluate(this->right, scope, result);
	
	EXIT;
	return error;
}


int callbackptr_comma_expression_evaluate(
	struct expression *this,
	struct scope *scope,
	struct value **result)
{
	int error = 0;
	ENTER;
	
	error = 0
		?: this->kind != ek_comma
		?: comma_expression_evaluate(
			(struct comma_expression*) this,
			scope,
			result);
	
	D(if(!error) verpv(*result));
	
	EXIT;
	return error;
}















