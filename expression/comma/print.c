
#include <assert.h>
#include <stdio.h>
#include <string.h>

#include <debug.h>

#include <array/struct.h>

#include <expression/print.h>

#include "struct.h"

#include "print.h"

static int comma_expression_print(
	struct comma_expression *this,
	int (*push)(const wchar_t *, size_t))
{
	int error = 0;
	ENTER;

	error = 0
		?: expression_print(this->left, push)
		?: push(L", ", 2)
		?: expression_print(this->right, push);

	EXIT;
	return error;
}

int callbackptr_comma_expression_print(
	struct expression *this,
	int (*push)(const wchar_t *, size_t))
	{
	int error;
	ENTER;

	error = 0
		?: this->kind != ek_comma
		?: comma_expression_print(
			(struct comma_expression *)this, push);

	EXIT;
	return error;
}


















