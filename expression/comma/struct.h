
#ifndef COMMA_EXPRESSION_STRUCT_H
#define COMMA_EXPRESSION_STRUCT_H

#include "../struct.h"

struct comma_expression
{
	struct expression super;
	struct expression* left;
	struct expression* right;
};

#endif
