
#include <stdio.h>

#include <debug.h>

#include "../new.h"
#include "../struct.h"

#include "callbacks.h"
#include "struct.h"

#include "new.h"

struct comma_expression* new_comma_expression(
	struct expression *left,
	struct expression *right)
{
	struct comma_expression *this;
	ENTER;
	
	this = (struct comma_expression*)
		new_expression(
			ek_comma,
			&(comma_expression_callbacks),
			sizeof(*this)
		);
	
	this->left = left;
	this->right = right;
	
	EXIT;
	return this;
}
