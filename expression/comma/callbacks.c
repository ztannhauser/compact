
#include <stdlib.h>

#include "../struct.h"

#include "delete.h"
#include "evaluate.h"
#include "print.h"

#include "callbacks.h"

struct expression_callbacks comma_expression_callbacks = {
	.evaluate = callbackptr_comma_expression_evaluate,
	.print = callbackptr_comma_expression_print,
	.delete = callbackptr_delete_comma_expression,
};
