
#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>

#include <debug.h>

#include <array/new.h>
#include <array/delete.h>
#include <array/push_n.h>
#include <array/struct.h>

#include "write-to-stdout.h"

#include "expression/delete.h"
#include "expression/evaluate.h"
#include "expression/print.h"

#include "value/list/new.h"
#include "value/string/from_utf8_encoded.h"

#include "parser/parse.h"

#include "process_arg.h"

int process_arg(
	const char ***arg_ptr,
	struct scope *scope,
	struct value **result)
{
	int error = 0;
	struct array values;
	char* m;
	unsigned int i, n;
	struct string_value* element;
	const char *arg;
	ENTER;
	
	arg = (*arg_ptr)[0];
	verpvs(arg);
	if (arg)
	{
		if (!strcmp(arg, "--"))
		{
			arg = *++(*arg_ptr);
			if(arg)
			{
				error = string_value_from_utf8_encoded(
					arg, strlen(arg),
					(struct string_value**) result);
			}
			else
			{
				fprintf(stderr,
					"compact: expected argument after '--' flag!\n");
				error = 1;
			}
			
		}
		else if (!strcmp(arg, "..."))
		{
			arg = *++(*arg_ptr);
			if(arg)
			{
				n = strtoul(arg, &m, 0);
				if(!*m)
				{
					verpv(n);
					values = new_array(struct value*);
					for(i = 0;!error && i < n;i++)
					{
						arg = *++(*arg_ptr);
						if(arg)
						{
							error = string_value_from_utf8_encoded(arg,
									strlen(arg), &element);
							
							if(!error)
							{
								array_push_n(&values, &element);
							}
						}
						else
						{
							TODO; // expected more arguments than given!
							error = 1;
						}
					}
					
					if(error)
					{
						delete_array(&values);
					}
					else
					{
						*result = (struct value*) new_list_value(values, NULL);
					}
				}
				else
				{
					TODO; // malformed length number for '...'!
					error = 1;
				}
			}
			else
			{
				TODO; // print error message
				error = 1;
			}
		}
		else
		{
			// looks like something that needs to be parsed and evaluated
			struct expression *root;

			error = parse(arg, &root);

			if (!error) {

				// possibly print expression
				D(error = error ?: expression_print(root, write_to_stdout));
				
				verpv(error);
				
				// evaluate expression:
				error = error ?: expression_evaluate(root, scope, result);
				
				verpv(error);
				
				// free expression:
				error = error ?: delete_expression(root);
				
				verpv(error);
				
			}
		}
	} else {
		error = 1;
	}
	EXIT;
	return error;
}





















