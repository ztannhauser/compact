
#include <scope/struct.h>

int process_arg(
	const char*** arg_ptr,
	struct scope* scope,
	struct value** result)
	__attribute__ ((warn_unused_result));
